# MLT-WEB

## Prerequisites
- nodejs 8.10 or later
- angular-cli 6

### Install Prerequisites
```bash
# Ubuntu
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
(sudo) npm install -g angular-cli

# Centos
curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -
sudo yum install -y nodejs
(sudo) npm install -g angular-cli
```

### mlt-web (mlt-proxy)
#### 패키지 설치
```bash
cd client
npm install
```

#### 실행
```bash
cd client
ng serve
```
기본적으로 --host localhost --port 4200으로 실행됩니다.
localhost에서 접속하는 경우가 아니라면 host 옵션에 공인 아이피를 입력해줍니다.

```bash
cd client
ng serve --host ... --port ...
```

http://hostname:port 에서 접속을 확인 할 수 있습니다.

#### 배포시 빋드 및 실행  
```bash
./build.sh $MAUM_ROOT web
svctl start mlt-proxy
```