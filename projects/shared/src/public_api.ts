export {
  SharedModule,
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatTableModule,
  MatSortModule,
  MatCommonModule,
  MatSidenavModule,
  MatMenuModule,
  MatSnackBarModule,
  MatDialogModule,
  MatLineModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSelectModule,
  MatCardModule,
  MatRippleModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatSliderModule,
  MatStepperModule,
  FlexLayoutModule,
  CommonModule,
  FormsModule,
  HttpClientModule,
  ReactiveFormsModule,
  AppSidebarComponent,
  AppDummyComponent,
  ConfirmComponent,
  AlertComponent,
  TableComponent,
  GitComponent,
  AudioPlayerComponent,
  FileGroupDialogComponent,
  TranscriptAlertDialogComponent,
  TranscriptTextComponent,
  UploaderComponent,
  UploaderButtonComponent,
  PrettyJsonModule,
  TreeViewComponent,
  TreeModule,
  FileUploadModule,
  CommitDialogComponent,
  FileSizePipe,
  SecondsToTimePipe,
  PageParameters,
  FormErrorStateMatcher,
  StorageBrowser,
  CookieBrowser,
  AppEnumsComponent,
  DictionaryDictionaryUpsertDialogComponent,
  DownloadService,
  DictionaryCategoryUpsertDialogComponent,
  DictionaryUpsertDialogComponent,
  TextareaAutoResizeDirective
} from './lib/shared.module';
