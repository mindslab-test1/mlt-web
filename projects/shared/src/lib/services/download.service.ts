import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';

@Injectable()
export class DownloadService {

  constructor(private datePipe: DatePipe) {

  }

  downloadTxtFile(contents, fileName) {
    let a = document.createElement('a');
    a.setAttribute('style', 'display: none;');
    document.body.appendChild(a);
    let blob = new Blob([contents], {type: 'text/txt'});
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = `${fileName}_${this.datePipe.transform(new Date, 'yyyyMMdd')}.txt`;
    a.click();
  }

	downloadJsonFile(contents, fileName) {
		let a = document.createElement('a');
		a.setAttribute('style', 'display: none;');
		document.body.appendChild(a);
		let blob = new Blob([contents], {type: 'text/json'});
		let url = window.URL.createObjectURL(blob);
		a.href = url;
		a.download = `${fileName}_${this.datePipe.transform(new Date, 'yyyyMMdd')}.json`;
		a.click();
	}

  downloadCsvFile(contents, headers, fileName) {
    let csvData = this.convertToCSV(contents, headers);
    let a = document.createElement('a');
    a.setAttribute('style', 'display: none;');
    document.body.appendChild(a);
    let blob = new Blob([csvData], {type: 'text/csv'});
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = `${fileName}_${this.datePipe.transform(new Date, 'yyyyMMdd')}.csv`;
    a.click();
  }

  convertToCSV(objArray, headers) {
    if (!objArray || !headers) {
      return false;
    }
    const separate = ',';
    const line = '\r\n';
    let str = '';


    for (let i = 0; i < headers.length; i++) {
      if (i === 0) {
        str += headers[i].name;
      } else {
        str += separate + headers[i].name;
      }
      if (i === headers.length - 1) {
        str += line;
      }
    }

    for (let i = 0; i < objArray.length; i++) {
      for (let j = 0; j < headers.length; j++) {

        if (typeof(objArray[i][headers[j].attr]) === 'string' &&
          objArray[i][headers[j].attr].indexOf(',') !== -1) {
          objArray[i][headers[j].attr] = `"${this.getAttr(objArray[i], headers[j].attr)}"`;
        }

        if (j === 0) {
          if (headers[j]['format'] === 'date') {
            str += this.datePipe.transform(this.getAttr(objArray[i], headers[j].attr), 'yyyy-MM-dd HH:mm:ss');
          } else {
            str += this.getAttr(objArray[i], headers[j].attr);
          }
        } else {
          if (headers[j]['format'] === 'date') {
            str += separate + this.datePipe.transform(this.getAttr(objArray[i], headers[j].attr), 'yyyy-MM-dd HH:mm:ss');
          } else {
            str += separate + this.getAttr(objArray[i], headers[j].attr);
          }
        }
        if (j === headers.length - 1) {
          str += line;
        }
      }
    }
    return str;
  }

  getAttr(row, attr) {
    let val = '';

    // for relation model attr and model value
    if (attr) {
      let attrs = attr.split('.');
      while (row && attrs.length > 0) {
        row = row[attrs.shift()];
      }
      val = row;
    }

    if (val) {
      return val;
    } else {
      // for null value
      return '';
    }
  }
}
