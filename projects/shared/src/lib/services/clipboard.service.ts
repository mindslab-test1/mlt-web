import {EventEmitter, Injectable} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

@Injectable()
export class ClipboardService {

  private ignoredTagNames = ['INPUT', 'TEXTAREA'];
  emitter = new EventEmitter();

  // observable should be derived from loopback's angular2 sdk
  subscribe(sub: any): Subscription {
    return this.emitter.subscribe(sub);
  }

  emit(e) {
    if (e.target && this.ignoredTagNames.indexOf(e.target.tagName) !== -1) {
      return;
    }

    e.preventDefault();

    let content;
    if (e.clipboardData || e.originalEvent && e.originalEvent.clipboardData) {
      content = (e.originalEvent || e).clipboardData.getData('text/plain');
    } else if (window['clipboardData']) {
      content = window['clipboardData'].getData('Text');
    }

    if (content) {
      let lines = content.trim()
      .split('\n')
      .map(line => line.trim().replace(/(\s){2,}/g, '\t'))
      .filter(line => line !== '');

      if (lines.length > 0) {
        this.emitter.emit(lines);
      }
    }
  }
}
