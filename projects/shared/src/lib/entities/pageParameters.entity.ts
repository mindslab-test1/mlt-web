export class PageParameters {
  pageIndex: number;
  pageSize: number;
  orderProperty: string;
  orderDirection: string;
}
