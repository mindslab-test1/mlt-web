import {FormControl, FormGroup} from '@angular/forms';

const REGEXP = {
  email: /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
  engNumber: /^[a-z0-9][a-z0-9\_]*$/i
};

// @dynamic
export class CustomValidators {
  // validators for Angular2 FormBuilder
  static matchWithControl(targetControlName, customErrorMessage: string = null) {
    return (c: FormControl) => {
      let correctValue = c.parent && c.parent.get(targetControlName).value;
      if (!!correctValue && correctValue !== c.value) {
        let error: any = {};
        if (customErrorMessage) {
          error.custom = customErrorMessage;
        } else {
          error.matchWithControl = {
            correctValue: correctValue,
            enteredValue: c.value,
          };
        }
        return error;
      } else {
        return null;
      }
    };
  }

  static matchWithValue(value, customErrorMessage: string = null) {
    return (c: FormControl) => {
      if (value !== c.value) {
        let error: any = {};
        if (customErrorMessage) {
          error.custom = customErrorMessage;
        } else {
          error.matchWithValue = {
            correctValue: value,
            enteredValue: c.value,
          };
        }
        return error;
      } else {
        return null;
      }
    };
  }

  static email = (c: FormControl) => {
    return REGEXP.email.test(c.value) ? null : {email: true};
  }

  static engNumber = (c: FormControl) => {
    return REGEXP.engNumber.test(c.value) ? null : {engNumber: true};
  }

  // set server-side (loopback) validation errors to Angular2 FormGroup, FormControl
  static setServerErrors(formGroup: FormGroup, e: any, contexts: string[] = []) {
    // multiple errors (validation)
    if (e && e.details && (contexts.length === 0 || contexts.indexOf(e.details.context) !== -1)) {
      for (let k in e.details.codes) {
        if (e.details.codes.hasOwnProperty(k)) {
          let codes = e.details.codes[k],
            messages = e.details.messages[k];

          let errors = codes.reduce((errorList, code) => {
            errorList[code] = messages[code] || true;
            return errorList;
          }, {});

          let control = formGroup.get(k);
          if (control) {
            control.setErrors(errors);
            control.markAsDirty();
          } else {
            formGroup.setErrors(errors);
            formGroup.markAsDirty();
          }
        }
      }
    }
  }

  // set server-side custom error to Angular2 FormGroup, FormControl
  static setCustomError(formGroup: FormGroup, errorMessage = 'Error!', e: any = null, context: {} = {}) {
    // error 객체가 없거나, context 객체와 맞음
    let matched = !e || Object.keys(context)
    .every(contextKey => context[contextKey] === e[contextKey]);

    if (matched) {
      formGroup.setErrors({custom: errorMessage});
      formGroup.markAsDirty();
    }
  }

  // mark all controls as dirty to show errors when got a submission without any touch
  static markAllAsDirty(formGroup: FormGroup) {
    for (let k in formGroup.controls) {
      if (formGroup.controls.hasOwnProperty(k)) {
        let control = formGroup.controls[k];
        control.markAsDirty();
      }
    }
  }
}
