import {Injectable, Output} from '@angular/core';

@Injectable()
export class AppEnumsComponent {
  @Output() langCode = {
    kor: 'kor',
    eng: 'eng'
  };
  @Output() langCodeKeys = Object.keys(this.langCode);

  @Output() useYnCode = {
    Y: 'Use',
    N: 'Not Use'
  };
  @Output() useYnCodeKeys = Object.keys(this.useYnCode);

  @Output() mainYnCode = {
    Y: 'Use',
    N: 'Not Use'
  };
  @Output() mainYnCodeKeys = Object.keys(this.mainYnCode);

  @Output() paraphraseStep = {
    READY: 'R',
    INVERSION: 'I',
    WORD_EMBEDDING: 'W',
    SYNONYM: 'S',
    ENDING_POST_POSITION: 'E',
    COMPLETE: 'C'
  };

  @Output() paraphraseStepHtml = {
    R: 'Ready',
    I: 'Inversion',
    W: 'Word Embedding',
    S: 'Synonym',
    E: 'Ending/PostPosition',
    C: 'Complete'
  }

}
