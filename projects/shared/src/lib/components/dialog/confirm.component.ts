import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;

  constructor(public dialogRef: MatDialogRef<any>) {
  }

  ngOnInit() {
  }

}
