import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from '../../values/custom.validators.value';

@Component({
  selector: 'app-dictionary-upsert-dialog',
  templateUrl: './dictionary-upsert-dialog.component.html',
})
export class DictionaryUpsertDialogComponent implements OnInit {

  model: FormGroup;
  data: any;
  workspaceId: number;
  @Input() dicApi: any;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
    this.model = this.fb.group({
      name: ['', [
        Validators.required,
        Validators.minLength(4),
        CustomValidators.engNumber,
      ]],
    });
  }

  ngOnInit() {
    // apply data when update
    let data = this.data = this.dialogRef.componentInstance.data;
    if (data) {
      this.model.setValue({
        name: data.name
      });
    }
  }

  submit(model) {
    console.log('this.model : ', this.model);
    console.log('매개변수 model : ', model);
    if (model.valid) {
      // update
      if (this.data) {
        // NOT ALLOWED RENAMING

        // create
      } else {
        this.dicApi
        .create(Object.assign({}, model.value, {workspaceId: this.workspaceId}))
        .subscribe(
          (dic: any) => {
            this.dialogRef.close();
            alert(`Dictionary '${dic.name}' created.`);
          },
          err => {
            CustomValidators.setServerErrors(model, err);
            CustomValidators.setCustomError(model, err.message, err, {status: 500});
          }
        );
      }
    } else {
      CustomValidators.markAllAsDirty(model);
    }
  }
}
