import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertComponent} from './alert.component';

@Component({
  selector: 'app-dictionary-dictionary-upsert-dialog',
  templateUrl: './dictionary-dictionary-upsert-dialog.component.html',
  styleUrls: ['./dictionary-dictionary-upsert-dialog.component.scss']
})
export class DictionaryDictionaryUpsertDialogComponent implements OnInit {

  model: FormGroup;
  data: any;
  workspaceId: any;
  meta: any;
  next: any;
  service: any;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder,
              public dialog: MatDialog) {
    this.model = this.fb.group({
      name: ['', [
        Validators.required
      ]],
    });
  }

  ngOnInit() {
    this.next = this.dialogRef.componentInstance.next;
    let data = this.data = this.dialogRef.componentInstance.data;
    // apply data when update
    if (data) {
      this.model.setValue({
        name: data.name,
      });
    }
  }

  submit() {
    let value = Object.assign({}, this.meta, this.model.value);
    this.service.insertDic(value).subscribe(
      node => {
        if (node.message === 'INSERT_DUPLICATED') {
          this.openAlertDialog('Duplicate', 'Dictionary duplicated.', 'error');
        } else if (node.message === 'INSERT_SUCCESS') {
          this.openAlertDialog('Add', `Dictionary '${node.dictionary.name}' created.`, 'success');
          this.dialogRef.close(true);
        }
      },
      err => {
        this.openAlertDialog('Error', `Server Error. Dictionary '${value.name}' not created.`, 'error');
      }
    )
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
