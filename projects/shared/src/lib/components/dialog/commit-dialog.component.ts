import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-commit-dialog',
  templateUrl: './commit-dialog.component.html',
  styleUrls: ['./commit-dialog.component.scss']
})
export class CommitDialogComponent implements OnInit {

  @Input() title: any = '';
  model: FormGroup;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.model = this.fb.group({
      title: [this.title, [Validators.required]],
      message: ['', []],
    });
  }

  submit() {
    if (this.model.valid) {
      console.log(this.model);
      let title = this.model.value.title.trim();
      let message = this.model.value.message.trim();

      let result = Object.assign({}, {title: title}, {message: message});
      this.dialogRef.close(result);
    }
  }

}
