import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertComponent} from './alert.component';

@Component({
  selector: 'app-dictionary-category-upsert-dialog',
  templateUrl: './dictionary-category-upsert-dialog.component.html',
  styleUrls: ['./dictionary-category-upsert-dialog.component.scss']
})
export class DictionaryCategoryUpsertDialogComponent implements OnInit {

  model: FormGroup;
  data: any;
  meta: any;
  next: any;
  service: any;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder,
              public dialog: MatDialog) {
    this.model = this.fb.group({
      name: ['', [
        Validators.required
      ]],
    });
  }

  ngOnInit() {
    this.next = this.dialogRef.componentInstance.next;
    let data = this.data = this.dialogRef.componentInstance.data;

    // apply data when update
    if (data) {
      this.model.setValue({
        name: data.name
      });
    }
  }

  submit() {
    if (this.model.valid) {
      // update
      if (this.data) {
        let value = Object.assign({}, this.meta, this.model.value);
        // prevent unique constranit
        if (value.name === this.data.name) {
          delete value.name;
        }
        this.service.updateCategory(value).subscribe(
          node => {
            if (node.message === 'UPDATE_SUCCESS') {
              this.openAlertDialog('Update', `Category '${node.category.name}' updated.`, 'success');
              this.next(node.category);
              this.dialogRef.close(true);
            } else if (node.message === 'UPDATE_DUPLICATED') {
              this.openAlertDialog('Duplicate', `Category '${value.name}' duplicated.`, 'error');
            }
          },
          err => {
            this.openAlertDialog('Error', `Server Error. Category '${value.name}' not updated.`, 'error');
          }
        );

        // create
      } else {
        let value = Object.assign({}, this.meta, this.model.value);
        this.service.insertCategory(value).subscribe(
          node => {
            if (node.message === 'INSERT_DUPLICATED') {
              this.openAlertDialog('Duplicate', 'Category duplicated.', 'error');
            } else if (node.message === 'INSERT_SUCCESS') {
              this.openAlertDialog('Add', `Category '${node.category.name}' created.`, 'success');
              node.category.children = [];
              this.next(node.category);
              this.dialogRef.close(true);
            }
          },
          err => {
            this.openAlertDialog('Error', `Server Error. Category '${value.name}' not created.`, 'error');
          }
        )
      }
    } else {
      // CustomValidators.markAllAsDirty(this.model);
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
