import {
  AfterViewInit,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  IterableDiffers,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class TableComponent implements OnInit, AfterViewInit, DoCheck {

  @Input() headers: any[] = [];
  @Input() rows: any[] = [];
  differ: any;

  @Input() dataSource: MatTableDataSource<any>;
  @Input() filterPlaceholder: any;
  @Input() isFilter: any;

  @Input() length: number;
  @Input() isShowPaginator = true;

  @Input() pageIndex = 0;

  @Output() outputSelectedRow = new EventEmitter<any>();
  @Output() outputCheckedRow = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Output() outputSelectedHeader = new EventEmitter<MatSort>();
  @Output() outputPaginator = new EventEmitter<MatPaginator>();

  isCheckedAll: boolean;
  @Output() outputRowInput = new EventEmitter<Object>();

  pageSize = 10;

  constructor(private differs: IterableDiffers) {
    this.differ = differs.find([]).create(null);
  }

  ngOnInit() {
    this.onClickHeader();
  }

  ngAfterViewInit() {
    this.onChangePaginator();
  }

  ngDoCheck() {
    let changesRow = this.differ.diff(this.rows);

    if (changesRow) {
      this.dataSource = new MatTableDataSource(changesRow.collection);
      if (this.rows) {
        this.rows.forEach((row, index) => {
          if (this.isShowPaginator) {
            row['no'] = this.paginator.pageIndex * this.paginator.pageSize + index + 1;
          } else {
            row['no'] = index + 1;
          }
        });
      }
    }
  }

  getColumn(headers) {
    let arr: any[] = [];
    headers.filter(col => col.attr).forEach((col) => {
      arr.push(col.attr);
    });
    return arr;
  }

  onClickCheckAll() {
    this.rows.forEach(row => {
      row.isChecked = this.isCheckedAll;
    });

    this.isChecked();
  }

  isChecked() {
    let checkedRows = this.rows.filter(data => data.isChecked);

    if (checkedRows.length !== 0) {
      this.outputCheckedRow.emit(true);
    } else {
      this.outputCheckedRow.emit(false);
    }
  }

  colClick(row) {
    this.outputSelectedRow.emit(row);
  }

  getButtonClass(buttonColor) {
    return buttonColor === 'accent' ? 'accent-button' : 'primary-button';
  }

  onChangePaginator() {
    this.outputPaginator.emit(this.paginator);
  }

  changeInput(event, row, attr) {
    const attrArr = attr.split('.');
    const newAttr = attrArr[attrArr.length - 1];
    let data = Object.assign({}, row);
    data[newAttr] = event.target.value;
    this.outputRowInput.emit(data);
  }

  // 헤더를 클릭했을 때 눌린 헤더 정보를 emit(정렬을 위해 사용)
  onClickHeader() {
    this.outputSelectedHeader.emit(this.sort);
  }

  getAttr(row, attr) {
    let val = '';

    // for relation model attr and model value
    if (attr) {
      let attrs = attr.split('.');
      while (row && attrs.length > 0) {
        row = row[attrs.shift()];
      }
      val = row;
    }

    if (val) {
      return val;
    } else {
      // for null value
      return '';
    }
  }

}
