import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {StorageBrowser} from '../../storage/storage.browser';
import {AlertComponent} from '../dialog/alert.component';
import {MatDialog} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-uploader-button',
  templateUrl: './uploader-button.component.html'
})

export class UploaderButtonComponent implements OnInit {

  @Input('url') url: string;
  @Input('data') data: any;

  @Output('uploadCallback') uploadCallback = new EventEmitter<boolean>();
  @Output('preUploadCallback') preUploadCallback = new EventEmitter<boolean>();

  preUploadCallbackResult;

  constructor(private http: HttpClient, protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {

  }

  addFiles(event) {
    const file = event.target.files || event.srcElement.files;
    if (file || file.length > 0) {
      let formData = new FormData();
      let workspaceId = this.storage.get('workspaceId');
      let headers = new HttpHeaders();
      headers.append('enctype', 'multipart/form-data');
      // let options = new HttpParams({headers: headers});
      let url = this.storage.get('mltApiUrl') + this.url;

      if (file[0].name != null) {
        this.preUploadCallback.emit();
        formData.append('files', file[0]);
        formData.append('workspaceId', workspaceId);
        formData.append('data', this.data);
        if (this.preUploadCallbackResult === null || this.preUploadCallbackResult === undefined) {
          this.postFile(url, formData, headers);
        } else {
          this.preUploadCallbackResult.then(res => {
            if (res) {
              this.postFile(url, formData, headers);
              (<HTMLInputElement>document.getElementById('file')).value = '';
            }
          }).catch(err => {
            (<HTMLInputElement>document.getElementById('file')).value = '';
            console.error('error' + err);
            this.openAlertDialog('Failed', 'Failed Upload File', 'error');
          });
        }
      }
    }
  }

  postFile(url, formData, options) {
    this.http.post(url, formData, options).subscribe(
      resp => {
        this.openAlertDialog('Success', 'Success Upload File', 'success');
        this.uploadCallback.emit(true);
      },
      error => {
        this.openAlertDialog('Failed', 'Failed Upload File', 'error');
        console.error('error', error);
      }
    );
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
