import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TREE_ACTIONS, TreeComponent, TreeModel, TreeNode} from 'angular-tree-component';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class TreeViewComponent implements OnInit {

  // tree component
  @ViewChild(TreeComponent) tree: TreeComponent;
  treeOptions: any = {
    allowDrag: true,
    isExpandedField: 'id',
    actionMapping: {
      mouse: {
        drop: (tree: TreeModel, node: TreeNode, $event: any, to: { from: any, to: any }) => {
          let callback = this.callback['canSetParent'];
          if (callback) {
            callback(to.from, to.to.parent, () => {
              TREE_ACTIONS.MOVE_NODE(tree, node, $event, to);
            });
          } else {
            TREE_ACTIONS.MOVE_NODE(tree, node, $event, to);
          }
        }
      }
    }
  };

  // editable prop
  _editable = false;
  @Input()
  set editable(editable) {
    this._editable = editable;
    this.treeOptions.allowDrag = editable;
  }

  get editable() {
    return this._editable;
  }

  // nodes
  currentNode: any;
  private _nodes: any; // TreeNode type
  @Input()
  set nodes(nodes) {
    // init when updated
    if (this._nodes) {
      this.tree.treeModel.setActiveNode(null, null);
      this.tree.treeModel.setFocusedNode(null);
    }

    // set nodes
    this.currentNode = null;
    this._nodes = nodes || [];
    // this.tree.treeModel.firstUpdate = true;
  }

  get nodes() {
    return this._nodes;
  }

  // node event api
  @Input() callback: {
    focus?: (node: any) => void,
    blur?: () => void,
    add?: (next: (node: any) => void) => void,
    remove?: (nodeId: number, next: () => void) => void,
    edit?: (node: any, next: (node: any) => void) => void,
    setParent?: (node: any, next: () => void) => void,
    canSetParent?: (node: any, target: any, next: () => void) => void,
  } = {};

  constructor() {
  }

  ngOnInit() {
  }

  // callbacks
  addNode() {
    let callback = this.callback['add'];
    if (!callback) {
      return;
    }

    callback(node => {
      this.nodes.push(node);
      this.tree.treeModel.update();
    });
  }

  removeNode() {
    let callback = this.callback['remove'];
    if (!callback) {
      return;
    }

    let currentNode = this.currentNode.data;
    callback(currentNode, () => {
      // update tree nodes
      if (this.currentNode.isRoot) {
        this.nodes = this.nodes.filter(node => node.id !== currentNode.id);
      } else {
        this.currentNode.parent.data.children = this.currentNode.parent.data.children
        .filter(node => node.id !== currentNode.id);
      }

      this.currentNode = null;
      this.tree.treeModel.update();
    });
  }

  editNode() {
    let callback = this.callback['edit'];
    if (!callback) {
      return;
    }

    let currentNode = this.currentNode.data;
    callback({id: currentNode.id, name: currentNode.name}, node => {
      // Object.assign(this.currentNode.data, node);
      currentNode.name = node.name;
      this.tree.treeModel.update();
    });
  }

  onCurrentNodeChange(e) {
    this.currentNode = e.node;
    let callback = this.callback['focus'];
    if (callback) {
      callback(this.currentNode);
    }
  }

  onCurrentNodeBlur(e) {
    if (this.currentNode !== e.node) {
      this.currentNode = null;

      let callback = this.callback['blur'];
      if (callback) {
        callback();
      }
    }
  }

  onTreeStructureChange(e) {
    let callback = this.callback['setParent'];
    if (!callback) {
      return;
    }

    let parentNodeId = e.to.parent.virtual ? null : e.to.parent.id;
    let currentNode = e.node;
    callback({id: currentNode.id, name: currentNode.name, parentId: parentNodeId}, () => {
      this.currentNode = this.tree.treeModel.getFocusedNode();
      this.tree.treeModel.update();
    });
  }
}
