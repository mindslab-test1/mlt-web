import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app.sidebar.component.html',
  styleUrls: ['./app.sidebar.component.scss']
})
export class AppSidebarComponent implements OnInit, OnChanges {

  @Input() sidebarChild: any[] = [];
  @Input() parentPath = '';
  @Input() root = null;
  @Input() dept = 1;

  constructor(private router: Router, private _changeDetectionRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this._changeDetectionRef.detectChanges();
    this.sidebarChild.forEach(item => {
      const navUrl = '/' + this.parentPath + '/' + item.path
      const routerUrl = this.router.url;
      if (navUrl === routerUrl) {
        this.click(item);
      }
    });
  }

  click(nav, $event?) {
    this.clearActive(this.root);

    if (!nav.children) {
      nav['active'] = true;
      this.clearExpend([], this.root);
      this.router.navigateByUrl(this.parentPath + '/' + nav.path);
    } else {
      nav.open = !nav.open;
    }

    if ($event) {
      $event.preventDefault();
    }
  }

  clearActive(navs) {
    if (navs.length > 0) {
      navs.forEach(nav => {
        if (nav) {
          if (nav.children) {
            this.clearActive(nav.children);
          } else {
            nav['active'] = false;
          }
        }
      });
    }
  }

  clearExpend(pParent?, pRoot?) {
    let parent = pParent;
    let root = pRoot;

    root.forEach((item, index) => {
      if (item.children) {
        item['active'] = false;
        item['open'] = false;
        parent.push(item);

        this.clearExpend(parent, item.children);
      } else {
        if (item['active']) {
          parent.forEach(item2 => {
            item2['active'] = true;
            item2['open'] = true;
          });
        }
      }
    });
    parent.splice(0, parent.length);
  }
}
