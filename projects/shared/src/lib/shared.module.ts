import {NgModule} from '@angular/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatCommonModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatLineModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from 'ng2-file-upload';

import {AppSidebarComponent} from './components/app.sidebar.component';
import {AppDummyComponent} from './components/app.dummy.component';
import {FormErrorStateMatcher} from './values/form.error.state.matcher';
import {ConfirmComponent} from './components/dialog/confirm.component';
import {AlertComponent} from './components/dialog/alert.component';
import {GitComponent} from './components/git/git.component';
import {TableComponent} from './components/table/table.component';
import {ClipboardService} from './services/clipboard.service';
import {AudioPlayerComponent} from './components/audio-player/audio-player.component';
import {TranscriptTextComponent} from './components/transcript-text/transcript.text.component';

import {UploaderComponent} from './components/uploader/uploader.component';
import {UploaderButtonComponent} from './components/uploader/uploader-button.component';
import {FileSizePipe} from './pipes/filesize.pipe';
import {SecondsToTimePipe} from './pipes/seconds-to-time.pipe';
import {PrettyJsonModule} from 'angular2-prettyjson';
import {TreeModule} from 'angular-tree-component';
import {TreeViewComponent} from './components/tree/tree.component';
import {CustomValidators} from './values/custom.validators.value';
import {DictionaryCategoryUpsertDialogComponent} from './components/dialog/dictionary-category-upsert-dialog.component';
import {DictionaryUpsertDialogComponent} from './components/dialog/dictionary-upsert-dialog.component';
import {FileGroupDialogComponent} from './components/dialog/file-group-dialog.component';
import {TranscriptAlertDialogComponent} from './components/dialog/transcript-alert-dialog.component';
import {AppEnumsComponent} from './values/app.enums';
import {DownloadService} from './services/download.service';
import {CommitDialogComponent} from './components/dialog/commit-dialog.component';
import {DictionaryDictionaryUpsertDialogComponent} from './components/dialog/dictionary-dictionary-upsert-dialog.component';
import {HttpClientModule} from '@angular/common/http';
import {StorageBrowser} from './storage/storage.browser';
import {CookieBrowser} from './storage/cookie.browser';
import {PageParameters} from './entities/pageParameters.entity';
import {TextareaAutoResizeDirective} from './directives/textarea-auto-resize.directive';

@NgModule({
  imports: [
    MatButtonModule, MatCheckboxModule, MatRadioModule, MatAutocompleteModule, MatIconModule,
    MatInputModule, MatListModule, MatTableModule, MatSortModule, MatCommonModule, MatSidenavModule, MatMenuModule,
    MatSnackBarModule, MatDialogModule, MatLineModule, MatToolbarModule, MatSelectModule, MatCardModule, MatRippleModule,
    MatPaginatorModule, MatSlideToggleModule, MatTabsModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatButtonToggleModule, MatChipsModule, MatSliderModule, MatStepperModule,
    FlexLayoutModule, CommonModule, FormsModule, HttpClientModule, ReactiveFormsModule, PrettyJsonModule,
    TreeModule, FileUploadModule,MatTooltipModule
  ],
  declarations: [
    AppSidebarComponent, AppDummyComponent, ConfirmComponent, AlertComponent, TableComponent, GitComponent, AudioPlayerComponent,
    TranscriptTextComponent, UploaderComponent, UploaderButtonComponent, TreeViewComponent,
    FileSizePipe, SecondsToTimePipe, DictionaryCategoryUpsertDialogComponent, DictionaryDictionaryUpsertDialogComponent,
    DictionaryUpsertDialogComponent, FileGroupDialogComponent, TranscriptAlertDialogComponent, CommitDialogComponent, TextareaAutoResizeDirective
  ],
  providers: [
    FormErrorStateMatcher, ClipboardService, CustomValidators, AppEnumsComponent, DownloadService,
    UploaderButtonComponent, StorageBrowser, CookieBrowser, DatePipe, PageParameters
  ],
  exports: [
    MatButtonModule, MatCheckboxModule, MatRadioModule, MatAutocompleteModule, MatIconModule,
    MatInputModule, MatListModule, MatTableModule, MatSortModule, MatCommonModule, MatSidenavModule, MatMenuModule,
    MatSnackBarModule, MatDialogModule, MatLineModule, MatToolbarModule, MatSelectModule, MatCardModule, MatRippleModule,
    MatPaginatorModule, MatSlideToggleModule, MatTabsModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatButtonToggleModule, MatChipsModule, MatSliderModule, MatStepperModule,
    MatTooltipModule, FlexLayoutModule, CommonModule, FormsModule, HttpClientModule, ReactiveFormsModule,
    AppSidebarComponent, AppDummyComponent, ConfirmComponent, AlertComponent, TableComponent, GitComponent, AudioPlayerComponent,
    FileGroupDialogComponent, TranscriptAlertDialogComponent,
    TranscriptTextComponent, UploaderComponent, UploaderButtonComponent, PrettyJsonModule, TreeViewComponent,
    TreeModule, FileUploadModule, CommitDialogComponent, FileSizePipe, SecondsToTimePipe, TextareaAutoResizeDirective
  ],
  entryComponents: [
    ConfirmComponent, AlertComponent,
    DictionaryCategoryUpsertDialogComponent, DictionaryDictionaryUpsertDialogComponent, DictionaryUpsertDialogComponent,
    FileGroupDialogComponent, TranscriptAlertDialogComponent, CommitDialogComponent
  ]
})
export class SharedModule {
}

export {
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatTableModule,
  MatSortModule,
  MatCommonModule,
  MatSidenavModule,
  MatMenuModule,
  MatSnackBarModule,
  MatDialogModule,
  MatLineModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSelectModule,
  MatCardModule,
  MatRippleModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatSliderModule,
  MatStepperModule,
  FlexLayoutModule,
  CommonModule,
  FormsModule,
  HttpClientModule,
  ReactiveFormsModule,
  AppSidebarComponent,
  AppDummyComponent,
  ConfirmComponent,
  AlertComponent,
  TableComponent,
  GitComponent,
  AudioPlayerComponent,
  FileGroupDialogComponent,
  TranscriptAlertDialogComponent,
  TranscriptTextComponent,
  UploaderComponent,
  UploaderButtonComponent,
  PrettyJsonModule,
  TreeViewComponent,
  TreeModule,
  FileUploadModule,
  CommitDialogComponent,
  FileSizePipe,
  SecondsToTimePipe,
  PageParameters,
  FormErrorStateMatcher,
  StorageBrowser,
  CookieBrowser,
  AppEnumsComponent,
  DictionaryDictionaryUpsertDialogComponent,
  DownloadService,
  DictionaryCategoryUpsertDialogComponent,
  DictionaryUpsertDialogComponent,
  TextareaAutoResizeDirective
}

