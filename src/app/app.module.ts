import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from './core/core.module';
import {AuthGuard} from './core/auth.guard';
import {AppComponent} from './app.component';
import {Route} from './app.route';
import {AuthModule} from './auth/auth.module';
import {SharedModule} from 'mlt-shared';
import 'hammerjs';
import {COMPOSITION_BUFFER_MODE} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';

// import {DashboardModule} from './dashboard/dashboard.module';

@NgModule({
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    AuthModule,
    BrowserAnimationsModule,
    Route,
    AgGridModule.withComponents([])
  ],
  providers: [AuthGuard,
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
