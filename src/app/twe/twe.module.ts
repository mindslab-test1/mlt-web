import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SharedModule} from 'mlt-shared';
import {TripleWikiService} from './service/twe.service';
import {TripleWikiRoute} from './twe.route';
import {HistoryComponent} from './components/history.component';
import {ScheduleComponent} from './components/schedule.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    TripleWikiRoute,
    HttpClientModule
  ],
  providers: [TripleWikiService],
  declarations: [HistoryComponent, ScheduleComponent],
  exports: [HistoryComponent, ScheduleComponent]
})
export class TripleWikiModule {
}


