import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {HistoryComponent} from './components/history.component';
import {ScheduleComponent} from './components/schedule.component';


const tripleWikiRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'schedule',
        component: ScheduleComponent,
        data: {
          nav: {
            name: 'Schedule',
            comment: '',
            icon: 'today',
          }
        }
      },
      {
        path: 'history',
        component: HistoryComponent,
        data: {
          nav: {
            name: 'History',
            comment: '',
            icon: 'low_priority',
          }
        }
      },
    ]
  }
];

export const TripleWikiRoute: ModuleWithProviders = RouterModule.forChild(tripleWikiRoutes);
