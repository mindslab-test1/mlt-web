import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {ScheduleEntity} from '../entity/twe.schedule.entity'
import {TripleWikiScheduleService} from '../service/twe.schedule.service'
import {AlertComponent} from 'mlt-shared';

@Component({
  selector: 'app-twe-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  providers: [TripleWikiScheduleService],
  encapsulation: ViewEncapsulation.None,
})


export class ScheduleComponent implements OnInit {
  isCheckedAll: boolean;
  selectedPeriod = '';
  periods = [
    'Weekly',
    'Daily',
    'Hourly',
  ];

  scheduleEntities: ScheduleEntity[] = [];

  header = [
    {attr: 'type', name: 'Type'},
    {attr: 'showingPeriod', name: 'Period'},
    {attr: 'hour', name: 'Hour'},
    {attr: 'minute', name: 'Minute'}
    /*
    ,
    {attr: 'uploader', name: 'Uploader'},
    {attr: 'updatedAt', name: 'Updated At'},
    */
  ];

  weeklyEntity: ScheduleEntity = new ScheduleEntity();
  dailyEntity: ScheduleEntity = new ScheduleEntity();
  hourlyEntity: ScheduleEntity = new ScheduleEntity();


  selectedDayOfweek: string;
  dayOfWeeks = this.weeklyEntity.dayOfWeeks;

  selectedHour: string;
  hours = this.weeklyEntity.hours;

  selectedMinute: string;
  minutes = this.weeklyEntity.minutes;

  displayedColumns = ['type', 'period', 'hour', 'minute'];
  dataSource = new MatTableDataSource<ScheduleEntity>(this.scheduleEntities);

  constructor(private tripleWikiScheduleService: TripleWikiScheduleService,
              private dialog: MatDialog) {
  }


  /*
  * Date: 171214
  * Description: apply버튼을 눌렀을 때 선택된 period에 따라 해당 엔티티를 서버로 전송
  */
  onClickApplyButton() {
    let dataChecker: boolean;
    let scheduleEntity: ScheduleEntity = this.getSelectedScheduleEntity();

    if (this.selectedPeriod === '') {
      this.openAlertDialog('Error', `Please input period.`, 'error');
      return;
    }

    if (scheduleEntity.type === '') {
      this.openAlertDialog('Error', `Data is not exist.`, 'error');
      return;
    }


    if (this.selectedPeriod === 'Weekly'
      && (scheduleEntity.hourList.length === 0
        || scheduleEntity.periodList.length === 0
      )) {
      this.openAlertDialog('Error', `Please input all data.`, 'error');
      return;
    } else if (this.selectedPeriod === 'Daily' && (scheduleEntity.hourList.length === 0 || scheduleEntity.minuteList.length === 0)) {
      this.openAlertDialog('Error', `Please input all period.`, 'error');
      return;
    }


    this.tripleWikiScheduleService.insertSchedule(scheduleEntity).subscribe(result => {
        if (result) {
          this.openAlertDialog('Apply', `complete apply.`, 'success');
        }
      }, err => {
        console.log(err);
        this.openAlertDialog('Error', `No applied data in server.`, 'error');
      }
    );
  }

  /*
  * Date: 171214
  * Description: 선택된 period에 따라 해당 엔티티를 반환
  */
  getSelectedScheduleEntity(): ScheduleEntity {
    if (this.selectedPeriod === 'Weekly') {
      return this.weeklyEntity;
    } else if (this.selectedPeriod === 'Daily') {
      return this.dailyEntity;
    } else if (this.selectedPeriod === 'Hourly') {
      return this.hourlyEntity;
    }
  }


  resetList() {
    // scheduleEntities.length = 0;
    // this.ngAfterViewInit();
  }

  /*
  * Date: 171214
  * Description: reset버튼에 대한 리스너
  */
  onClickResetButton() {
    let scheduleEntity: ScheduleEntity = this.getSelectedScheduleEntity();

    if (!this.scheduleEntities.length) {
      this.openAlertDialog('Error', `Data is not exist.`, 'error');
      return;
    }


    this.tripleWikiScheduleService.deleteSchedule(scheduleEntity).subscribe(result => {
      if (result) {
        scheduleEntity.initializeData();

        this.scheduleEntities.length = 0;
        this.dataSource = new MatTableDataSource<ScheduleEntity>(this.scheduleEntities);
        this.openAlertDialog('reset', `complete reset.`, 'success');
      }
    });
  }


  /*
  * Date: 171214
  * Description: add버튼에 대한 리스너
  */
  onClickAddButton(button: string) {
    let type: string = this.selectedPeriod;
    // Monthly가 추가될경우 후 period는 type에 따라 days 혹은 day of week가 될 수 있다.
    let period: string = this.selectedDayOfweek;
    let hour: string = this.selectedHour;
    let minute: string = this.selectedMinute;
    let buttonDataType;

    let addChecker: boolean;
    let scheduleEntity: ScheduleEntity = this.getSelectedScheduleEntity();

    // period를 선택하지 않았을경우 종료
    if (this.selectedPeriod === '') {
      this.openAlertDialog('select', `Please select period.`, 'error');
      return;
    }

    if (type === 'Weekly') {
      period = this.selectedDayOfweek;
    } else if (this.selectedPeriod === 'Daily') {
      period = null;
    } else if (this.selectedPeriod === 'Hourly') {
      period = null;
      hour = null;
    }

    scheduleEntity.type = type;

    if (button === 'DayOfWeek' && period !== '') {
      this.selectedDayOfweek = '';
      addChecker = scheduleEntity.addElement(scheduleEntity.periodList, period);
      buttonDataType = 'period';
    } else if (button === 'Hour' && hour !== '') {
      this.selectedHour = '';
      addChecker = scheduleEntity.addElement(scheduleEntity.hourList, hour);
      buttonDataType = 'hour';
    } else if (button === 'Minute' && minute !== '') {
      this.selectedMinute = '';
      addChecker = scheduleEntity.addElement(scheduleEntity.minuteList, minute);
      buttonDataType = 'minute';
    } else {
      this.openAlertDialog('select', `Data is not selected.`, 'error');
      return;
    }

    if (!addChecker) {
      this.openAlertDialog('duplicate', `Duplicated data.`, 'error');
      return;
    }

    scheduleEntity.makeListToString(scheduleEntity, buttonDataType);

    this.scheduleEntities.length = 0;
    this.scheduleEntities.push(scheduleEntity);
    this.dataSource = new MatTableDataSource<ScheduleEntity>(this.scheduleEntities);
  }

  initSelectedList() {

    this.selectedDayOfweek = '';
    this.selectedHour = '';
    this.selectedMinute = '';
  }

  /*
  * Date: 171215
  * Description: 선택된 period에 따른 Schedule Entitiy 반환
  */
  getScheduleByPeriod(): ScheduleEntity {
    if (this.selectedPeriod === 'Weekly') {
      return this.weeklyEntity;
    } else if (this.selectedPeriod === 'Daily') {
      return this.dailyEntity;
    } else if (this.selectedPeriod === 'Hourly') {
      return this.hourlyEntity;
    }
  }


  /*
  * Date: 171214
  * Description: 선택된 period에 따른 처리
  */
  onChangePeriod() {

    this.initSelectedList();
//    this.displaySelectDayList='';

    let paramScheduleEntity: ScheduleEntity = new ScheduleEntity();

    document.getElementById('selectDayOfWeekList').style.display = '';
    document.getElementById('selectHourList').style.display = '';
    document.getElementById('selectMinuteList').style.display = '';
    // document.getElementById('scheduleAddButton').style.display = '';
    document.getElementById('periodAddButton').style.display = '';
    document.getElementById('hourAddButton').style.display = '';
    document.getElementById('minuteAddButton').style.display = '';


    if (this.selectedPeriod === 'Weekly') {
      paramScheduleEntity.seq = 1;
    } else if (this.selectedPeriod === 'Daily') {
      document.getElementById('periodAddButton').style.display = 'none';
      document.getElementById('selectDayOfWeekList').style.display = 'none';
      paramScheduleEntity.seq = 2;
    } else if (this.selectedPeriod === 'Hourly') {
      paramScheduleEntity.seq = 3;
      document.getElementById('periodAddButton').style.display = 'none';
      document.getElementById('hourAddButton').style.display = 'none';
      document.getElementById('selectDayOfWeekList').style.display = 'none';
      document.getElementById('selectHourList').style.display = 'none';

    }
    let resultScheduleEntity = this.getSelectedScheduleEntity();

    this.scheduleEntities.length = 0;

    this.tripleWikiScheduleService.getSchedule(paramScheduleEntity).subscribe(result => {
      if (result) {
        let resultQuery = result.resultSchedule;

        resultScheduleEntity.initializeData();
        if (resultQuery != null) {
          resultScheduleEntity.processReceivedData(resultQuery);
        }

        // 해당 period에 맞는 type이 정의되어있으면 push하고 출력
        if (resultScheduleEntity.type !== '') {
          this.scheduleEntities.push(resultScheduleEntity);
        }
        this.dataSource = new MatTableDataSource<any>(this.scheduleEntities);
      }
    });

  }

  /*
  *
  * 시퀀스가 중간에 비었을 경우 빈 seq를 찾아가서 매꾼다.
  *

  getNewSeqNo(): number {
    let seq: number = 0;
    let nextSeq: number;

    if (scheduleEntities.length === 0 || scheduleEntities[0].seq != 0) {
      return seq;
    }

    for (let i: number = 0; i < scheduleEntities.length - 1; i++) {

      nextSeq = scheduleEntities[i].seq + 1;

      if (nextSeq != scheduleEntities[i + 1].seq) {
        seq = nextSeq;
        return seq;
      }
    }

    seq = scheduleEntities.length;
    return seq;
  }
  */

  manualExecuteSchedule() {

    this.tripleWikiScheduleService.manualExecuteSchedule().subscribe(result => {
      this.openAlertDialog('success', `indexing button.`, 'success');
    });
  }

  manualStopSchedule() {

    this.tripleWikiScheduleService.manualStopSchedule().subscribe(result => {
      this.openAlertDialog('success', `stop button.`, 'success');
    });
  }


  /*
  * Date: 171214
  * Description: 초기화 함수들 집합
  */
  initPage() {

    let paramScheduleEntity: ScheduleEntity = new ScheduleEntity();
    paramScheduleEntity.seq = -1;

    this.tripleWikiScheduleService.getSchedule(paramScheduleEntity).subscribe(result => {
      if (result) {
        let resultScheduleEntity: ScheduleEntity;
        resultScheduleEntity = result['resultSchedule'];

        // 데이터가 없을 경우
        if (resultScheduleEntity.seq === -1) {
          this.selectedPeriod = 'Weekly';
          this.onChangePeriod();
        } else {
          // 기존에 저장된 데이터가 존재할 경우
          this.selectedPeriod = resultScheduleEntity.type;
          this.onChangePeriod();
        }
      }
    });

    this.weeklyEntity.seq = 1;
    this.dailyEntity.seq = 2;
    this.hourlyEntity.seq = 3;

    this.initSelectedList();

  }

  ngOnInit() {
    this.initPage();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}





