import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TripleWikiService} from '../service/twe.history.service';
import {DatePipe} from '@angular/common';
import {TripleWikiHistoryEntity} from '../entity/twe.history.entity';

@Component({
  selector: 'app-twe-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [TripleWikiService, DatePipe]

})
export class HistoryComponent implements OnInit, AfterViewInit {

  actions: any;
  header = [
    {attr: 'createdAt', name: 'updated time', isSort: true},
    {attr: 'leadTime', name: 'batch time', isSort: true},
    {attr: 'insertCnt', name: 'insert', isSort: true},
    {attr: 'updateCnt', name: 'update', isSort: true},
    {attr: 'deleteCnt', name: 'delete', isSort: true},
  ];

  seletedHistory: any[] = [];
  dataSource;
  date = new Date();
  fromDate;
  toDate;
  length;
  matSort: MatSort = new MatSort();
  paginator: MatPaginator;

  constructor(private triple: TripleWikiService, private datePipe: DatePipe) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.seletedHistory);
    this.seletedHistory.length = 0;
    this.fromDate = new Date(this.date.getFullYear(), (this.date.getMonth() - 1 ), this.date.getDate());
    this.toDate = new Date();
    this.getDataList();
  }


  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  getDataList = (pageIndex?) => {

    if (pageIndex !== undefined) {
      this.paginator.pageIndex = pageIndex;
    }

    let param: TripleWikiHistoryEntity = new TripleWikiHistoryEntity();
    param.fromDate = this.fromDate;
    param.toDate = this.toDate;
    param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    if (this.paginator) {
      param.pageSize = this.paginator.pageSize;
      param.pageIndex = this.paginator.pageIndex;
    } else {
      param.pageSize = 10;
      param.pageIndex = 0;

    }

    this.triple.getHistoryPage(param).subscribe(res => {
      if (res) {
        this.seletedHistory.length = 0;
        res.content.map(val => {
          val.createdAt = this.datePipe.transform(val.createdAt, 'yyyy-MM-dd hh: mm');
        });
        this.seletedHistory = res.content;
        this.length = res.totalElements;
        this.dataSource = new MatTableDataSource(this.seletedHistory);
      }
    });
  }

  orderByHeader(matSort: MatSort) {
    this.matSort = matSort;
    if (this.seletedHistory.length > 0) {
      this.getDataList();
    }
  }

  getPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.seletedHistory.length > 0) {
      this.getDataList();
    }
  }


}
