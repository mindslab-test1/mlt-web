export class ScheduleEntity {

  isCheck: boolean;
  seq: number;
  type: string;
  period: string;
  showingPeriod: string;
  hour: string;
  minute: string;

  periodList: string[] = [];

  hourList: string[] = [];

  minuteList: string[] = [];

  dayOfWeeks = [{sValue: 'Sunday', value: '0'}, {sValue: 'Monday', value: '1'}, {
    sValue: 'Tuesday',
    value: '2'
  }, {sValue: 'Wednesday', value: '3'}, {sValue: 'Thursday', value: '4'}, {
    sValue: 'Friday',
    value: '5'
  }, {sValue: 'Saturday', value: '6'}];

  hours = [{value: '00'}, {value: '01'}, {value: '02'}, {value: '03'}, {value: '04'}, {value: '05'}
    , {value: '06'}, {value: '07'}, {value: '08'}, {value: '09'}, {value: '10'}, {value: '11'}, {value: '12'}
    , {value: '13'}, {value: '14'}, {value: '15'}, {value: '16'}, {value: '17'}, {value: '18'}, {value: '19'}
    , {value: '20'}, {value: '21'}, {value: '22'}, {value: '23'}];

  minutes = [{value: '00'}, {value: '01'}, {value: '02'}, {value: '03'}, {value: '04'}, {value: '05'}
    , {value: '06'}, {value: '07'}, {value: '08'}, {value: '09'}, {value: '10'}, {value: '11'}, {value: '12'}
    , {value: '13'}, {value: '14'}, {value: '15'}, {value: '16'}, {value: '17'}, {value: '18'}, {value: '19'}
    , {value: '20'}, {value: '21'}, {value: '22'}, {value: '23'}, {value: '24'}, {value: '25'}, {value: '26'}
    , {value: '27'}, {value: '28'}, {value: '29'}, {value: '30'}, {value: '31'}, {value: '32'}, {value: '33'}
    , {value: '34'}, {value: '35'}, {value: '36'}, {value: '37'}, {value: '38'}, {value: '39'}, {value: '40'}
    , {value: '41'}, {value: '42'}, {value: '43'}, {value: '44'}, {value: '45'}, {value: '46'}, {value: '47'}
    , {value: '48'}, {value: '49'}, {value: '50'}, {value: '51'}, {value: '52'}, {value: '53'}, {value: '54'}
    , {value: '55'}, {value: '56'}, {value: '57'}, {value: '58'}, {value: '59'}];


  constructor() {
    this.initializeData();
  }


  /*
  * Date: 171215
  * Description: 데이터 초기화
  */
  initializeData() {

    this.type = this.period = this.showingPeriod = this.hour = this.minute = '';

    this.periodList.length = this.hourList.length = this.minuteList.length = 0;
  }

  /*
  * Date: 171215
  * Description: 데이터 추가.
  * 중복체크, 정렬 후 string 으로 변경
  * 중복일 경우 false 반환
  * insert 되었을 경우 true 반환
  */
  addElement(array: string[], data: string): boolean {

    for (let i = 0; i < array.length; i++) {
      if (array[i] === data) {
        return false;
      }

    }

    array.push(data);

    array.sort((a, b) => {
      return parseInt(a, 10) - parseInt(b, 10);
    });

    return true;
  }

  /*
  * Date: 171215
  * Description: 엔터티 데이터를 이용해 출력될 string 생성
  */
  makeListToString(scheduleEntity: ScheduleEntity, dataType: string) {

    let array: string[];
    let stringData: string;

    if (dataType === 'period') {
      array = scheduleEntity.periodList;

      for (let i = 0; i < array.length; i++) {
        if (i === 0) {
          scheduleEntity.period = array[i];
          scheduleEntity.showingPeriod = this.dayOfWeeks[array[i]].sValue;
        } else {
          scheduleEntity.period += ',' + array[i];
          scheduleEntity.showingPeriod += ',' + this.dayOfWeeks[array[i]].sValue;
        }
      }

    } else if (dataType === 'hour') {
      array = scheduleEntity.hourList;

      for (let i = 0; i < array.length; i++) {
        if (i === 0) {
          scheduleEntity.hour = array[i];
        } else {
          scheduleEntity.hour += ',' + array[i];
        }
      }

    } else if (dataType === 'minute') {
      array = scheduleEntity.minuteList;

      for (let i = 0; i < array.length; i++) {
        if (i === 0) {
          scheduleEntity.minute = array[i];
        } else {
          scheduleEntity.minute += ',' + array[i];
        }
      }

    }
  }

  /*
* Date: 171218
* Description: 서버로 부터 받은 데이터를 파싱하여 ScheduleEntity에 저장
*/
  processReceivedData(scheduleEntity: ScheduleEntity) {

    // ,로 구분하여 나누어 배열에 삽입

    this.seq = scheduleEntity.seq;
    this.type = scheduleEntity.type;
    this.period = scheduleEntity.period;
    this.hour = scheduleEntity.hour;
    this.minute = scheduleEntity.minute;

    this.periodList = scheduleEntity.period.split(',');
    this.hourList = scheduleEntity.hour.split(',');
    this.minuteList = scheduleEntity.minute.split(',');

    if (scheduleEntity.type === 'Weekly') {
      this.makeListToString(this, 'period');
    }

  }


}
