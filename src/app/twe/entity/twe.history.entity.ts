import {PageParameters} from 'mlt-shared';

export class TripleWikiHistoryEntity extends PageParameters {

  createAt: Date;
  insertCnt: number;
  updateCnt: number;
  deleteCnt: number;
  leadTime: String;
  fromDate: string;
  toDate: string;


  public static isNull(tripleWikiHistory: TripleWikiHistoryEntity): boolean {
    return tripleWikiHistory.createAt === null &&
      tripleWikiHistory.insertCnt === null &&
      tripleWikiHistory.updateCnt === null &&
      tripleWikiHistory.deleteCnt === null &&
      tripleWikiHistory.leadTime === null;
  }

  constructor() {
    super();
    this.insertCnt = 10;
  }
}
