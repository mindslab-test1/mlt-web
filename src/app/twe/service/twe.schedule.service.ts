import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ScheduleEntity} from '../entity/twe.schedule.entity'
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class TripleWikiScheduleService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getScheduleList(): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/getScheduleList', '');
  }

  getSchedule(scheduleEntity: ScheduleEntity): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/getSchedule', scheduleEntity);
  }

  insertSchedule(scheduleEntity: ScheduleEntity): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/insertSchedule', scheduleEntity);
  }

  deleteSchedule(scheduleEntity: ScheduleEntity): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/deleteSchedule', scheduleEntity);
  }

  manualExecuteSchedule(): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/manualExecuteSchedule', '');
  }

  manualStopSchedule(): Observable<any> {
    return this.http.post(this.API_URL + '/schedule/manualStopSchedule', '');
  }
}
