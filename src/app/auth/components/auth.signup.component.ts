import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

import {AuthService} from '../../core/service/auth.service';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth.signup.component.html'
})
export class AuthSignUpComponent implements OnInit {

  user: FormGroup;
  loading = false;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  register() {
  }
}
