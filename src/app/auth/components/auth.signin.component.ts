import {Component, OnInit} from '@angular/core';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

import {AuthService} from '../../core/service/auth.service';
import {Auth} from '../../core/entity/auth';
import {AlertComponent} from 'mlt-shared';
import {FormErrorStateMatcher} from 'mlt-shared';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth.signin.component.html',
  styleUrls: ['./auth.signin.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})
export class AuthSignInComponent implements OnInit {

  auth: Auth = {
    id: '',
    password: ''
  };
  idFormControl = new FormControl('', [
    Validators.required
  ]);
  pwdFormControl = new FormControl('', [
    Validators.required
  ]);
  submitButton: boolean;

  constructor(private authService: AuthService,
              private dialog: MatDialog,
              public matcher: FormErrorStateMatcher) {
  }


  ngOnInit() {
    this.submitButton = true;
    this.authService.goHome();
  }

  goHome(): void {
    this.authService.goHome();
  }

  signin(): void {
    this.authService.signin(this.auth).subscribe(res => {
      if (res) {
        if (res.message === 'ID_NOT_FOUND') {
          this.openDialog('Failed', 'Please Check Id', 'error');
        } else if (res.message === 'LOGIN_FAILED') {
          this.openDialog('Failed', 'Please Check Password', 'error');
        } else if (res.message === 'LOGIN_SUCCESS') {
          this.goHome();
        }
        this.validation();
      }
    });
  }

  openDialog(title, message, header) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
    ref.componentInstance.header = header
  }

  validation() {
    if (this.matcher.isErrorState(this.idFormControl)) {
      this.submitButton = true;
    } else if (this.matcher.isErrorState(this.pwdFormControl)) {
      this.submitButton = true;
    } else if (this.auth.id === '' || this.auth.password === '') {
      this.submitButton = true;
    } else {
      this.submitButton = false;
    }
  }
}

