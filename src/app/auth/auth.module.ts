import {NgModule, Optional, SkipSelf} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SharedModule} from 'mlt-shared';
import {AuthComponent} from './components/auth.component';
import {AuthSignInComponent} from './components/auth.signin.component';
import {AuthSignUpComponent} from './components/auth.signup.component';

import {AuthService} from '../core/service/auth.service';
import {StorageBrowser} from 'mlt-shared';
import {AuthRoute} from './auth.route';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    SharedModule,
    AuthRoute,
    HttpClientModule
  ],
  providers: [AuthService, StorageBrowser],
  declarations: [AuthComponent, AuthSignInComponent, AuthSignUpComponent],
  exports: [AuthComponent, AuthSignInComponent, AuthSignUpComponent],

})
export class AuthModule {
}


