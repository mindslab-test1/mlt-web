import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {AuthComponent} from './components/auth.component';
import {AuthSignInComponent} from './components/auth.signin.component';
import {AuthSignUpComponent} from './components/auth.signup.component';

const authRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'signin',
        pathMatch: 'full',
      },
      {
        path: 'signin',
        component: AuthSignInComponent,
        data: {hasNoLayout : true}
      },
      {
        path: 'signup',
        component: AuthSignUpComponent
      }
    ]
  },
];

export const AuthRoute: ModuleWithProviders = RouterModule.forChild(authRoutes);
