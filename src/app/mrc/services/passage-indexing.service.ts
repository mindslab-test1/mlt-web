import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class PassageIndexingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getSkillListByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-indexing/skill/list/' + workspaceId);
  }

  fullIndexing(selectedSkill: number, workspaceId: string, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-indexing/fullIndexing/' + selectedSkill + '/' + workspaceId + '/' + clean);
  }

  additionalIndexing(selectedSkill: number, workspaceId: string, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-indexing/additionalIndexing/' + selectedSkill + '/' + workspaceId + '/' + clean);
  }

  getIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-indexing/getIndexingStatus');
  }

  abortIndexing(): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-indexing/abortIndexing');
  }
}
