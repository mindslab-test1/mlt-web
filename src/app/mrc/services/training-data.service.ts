import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {ContextEntity} from '../entity/context.entity';
import {WikiEntity} from '../entity/wiki.entity';
import {QuestionEntity} from '../entity/question.entity';

@Injectable()
export class TrainingDataService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getWikiListByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training-data/wiki/list/' + workspaceId);
  }

  getCategoryList(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training-data/category/list/workspaceId/' + workspaceId);
  }

  getContextList(param: ContextEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context/list', param);
  }

  getContext(param: ContextEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context', param);
  }

  getQuestionListByContextId(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training-data/question/list/contextId/' + id);
  }

  addWiki(wikiEntity: WikiEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/wiki/add', wikiEntity);
  }

  addContext(contextEntity: ContextEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context/add', contextEntity);
  }

  addQuestion(questionEntity: QuestionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/question/add', questionEntity);
  }

  getWikiById(wikiEntity: WikiEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/wiki', wikiEntity);
  }

  editWiki(wikiEntity: WikiEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/wiki/edit', wikiEntity);
  }

  editContext(contextEntity: ContextEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context/edit', contextEntity);
  }

  removeWiki(wikiEntity: WikiEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/wiki/remove', wikiEntity);
  }

  removeContext(contextEntities: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context/remove', contextEntities);
  }

  removeContextAll(wikiId: number): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/context/remove/all', wikiId);
  }

  removeQuestion(deleteData: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/question/remove', deleteData);
  }

  processApply(contextId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training-data/preprocess/' + contextId);
  }

  analyzeNlp(contextEntities: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training-data/nlp-analysis', contextEntities);
  }

  analyzeNlpAll(wikiId: number): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training-data/nlp-analysis/all/' + wikiId);
  }

}
