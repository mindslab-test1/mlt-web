import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MrcModelEntity} from '../entity/mrcModel.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class MrcTrainingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }


  // getCorpusList(): Observable<any> {
  //   return this.http.post(this.API_URL + '/mrc/getCorpusList');
  // }

  traning(mrcModelEntity: MrcModelEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training/mrcOpen', mrcModelEntity);
  }

  getAllProgress(workspaceId: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training/getAllProgress', workspaceId);
  }

  getTrainedMrcModels(workspaceId: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/training/getTrainedMrcModels', workspaceId);
  }

  getGroups(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/data-group/list/workspaceId/' + workspaceId);
  }

  getProgress(gId: String): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training/getProgress/' + gId);
  }

  stop(key: String): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training/stop/' + key);
  }

  close(key: String): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training/close/' + key);
  }

  delete(key: String): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/training/delete/' + key);
  }
}
