import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class PassageSearchTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getSkillListByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage-search/test/skill/list/' + workspaceId);
  }


  findPassage(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage-search/test', param);
  }
}
