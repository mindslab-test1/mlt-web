import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {PassageEntity} from '../entity/passage.entity';
import {PassageSkillEntity} from '../entity/passage-skill.entity';

@Injectable()
export class PassageService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getSkillListByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage/skill/list/' + workspaceId);
  }

  getPassageList(param: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/list', param);
  }

  getPassage(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage/id/' + id);
  }

  // getQuestionListByContextId(id: string): Observable<any> {
  //   return this.http.get(this.API_URL + '/mrc/training-data/question/list/contextId/' + id);
  // }

  addSkill(skillEntity: PassageSkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/skill/add', skillEntity);
  }

  addPassage(passageEntity: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/add', passageEntity);
  }

  // addQuestion(questionEntity: QuestionEntity): Observable<any> {
  //   return this.http.post(this.API_URL + '/mrc/training-data/question/add', questionEntity);
  // }

  getSkillById(skillEntity: PassageSkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/skill', skillEntity);
  }

  editSkill(skillEntity: PassageSkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/skill/edit', skillEntity);
  }

  editPassage(passageEntity: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/edit', passageEntity);
  }

  removeSkill(skillEntity: PassageSkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/skill/remove', skillEntity);
  }

  removePassageList(passageEntities: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/remove/list', passageEntities);
  }

  removePassageListBySkillId(skillId: number): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/remove/all', skillId);
  }

  analyzeNlp(passageEntities: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/nlp-analysis', passageEntities);
  }

  analyzeNlpAll(skillId: number): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage/nlp-analysis/all/' + skillId);
  }
  // removeQuestion(deleteData: any): Observable<any> {
  //   return this.http.post(this.API_URL + '/mrc/training-data/question/remove', deleteData);
  // }

  // processApply(contextId: string): Observable<any> {
  //   return this.http.get(this.API_URL + '/mrc/training-data/preprocess/' + contextId);
  // }

}
