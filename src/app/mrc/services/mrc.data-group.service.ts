import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DataGroupEntity} from '../entity/data-group.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {ContextEntity} from '../entity/context.entity';

@Injectable()
export class MrcDataGroupService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getGroupList(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/data-group/list/workspaceId/' + workspaceId);
  }

  getWikiListByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/data-group/wiki/list/' + workspaceId);
  }


  addGroup(param: DataGroupEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/data-group/add', param);
  }

  editGroup(param: DataGroupEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/data-group/edit', param);
  }

  removeGroup(param: DataGroupEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/data-group/remove', param);
  }

  insertDataGroupRel(param: DataGroupEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/data-group/insertDataGroupRel', param);
  }

  getCategory(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/data-group/category/list/workspaceId/' + workspaceId);
  }

  getContextList(param: ContextEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/data-group/context/list', param);
  }
}
