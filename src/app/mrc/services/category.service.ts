import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class CategoryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getCategoryList(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/category/list/workspaceId/' + workspaceId);
  }

  addCategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/category/add', category);
  }

  editCategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/category/edit', category);
  }

  removeCategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/category/remove', category);
  }
}
