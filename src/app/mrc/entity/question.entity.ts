import {AnswerEntity} from './answer.entity';

export class QuestionEntity {

  id: string;
  contextId: string;
  cId: string;                  // Context Id
  catId: string;                // Category Id
  question: string;
  workspaceId: string;
  answerEntities: AnswerEntity[];

  constructor() {

  }
}
