export class PassageQuestionEntity {

  id: string;
  passageId: number;
  question: string;
  questionMorph: string;
  createdAt: string;
  creatorId: string;
  updatedAt: string;
  updaterId: string;
  workspaceId: string;
}
