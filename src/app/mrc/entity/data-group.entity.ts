import {PageParameters} from 'mlt-shared';

export class DataGroupEntity extends PageParameters {

  id: string;
  workspaceId: string;

  season: number;
  dataType: string;
  title: string;
  context: string;
  catId: string;

  contextId: string;
  dataGroupId: string;
  dataGroupName: string;

  parCateId: string;
  catName: string;
  catPath: string;
  dataTypePath: string;
  checked: boolean;
}
