export class AnswerEntity {

  // id: string;
  answer: string;
  answerMorph: string;
  answerEnd: number;
  answerEndMorph: number;
  answerStart: number;
  answerStartMorph: number;
  workspaceId: string;

  constructor() {
    this.answer = '';
  }

}
