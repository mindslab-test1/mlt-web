export class PassageSkillEntity {

  id: number;
  name: string;
  creatorId: string;
  createdAt: string;
  updatedAt: string;
  updaterId: string;
  workspaceId: string;

  constructor() {
  }
}
