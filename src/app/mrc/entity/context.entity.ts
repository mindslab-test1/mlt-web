import {PageParameters} from 'mlt-shared';
import {QuestionEntity} from "./question.entity";
import {CategoryEntity} from "./category.entity";

export class ContextEntity extends PageParameters {

  id: string;
  catId: string;
  context: string;
  contextMorph: string;
  contextWithMarker: string;
  createAt: string;
  creatorId: string;
  season: number;
  title: string;
  workspaceId;
  wikiId: number;
  searchQuestion: string;
  searchCatId: string;
  contextCategory: CategoryEntity;
  questionEntities: QuestionEntity[];
  deletedQuestionEntities: QuestionEntity[];
}
