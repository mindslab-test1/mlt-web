import {PageParameters} from 'mlt-shared';

export class TrainingDataEntity extends PageParameters {

  workspaceId: string;
  dataGroupId: string;
}
