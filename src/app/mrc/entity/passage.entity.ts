import {PageParameters} from 'mlt-shared';
import {PassageQuestionEntity} from "./passage-question.entity";

export class PassageEntity extends PageParameters {

  id: string;
  skillId: number;
  passage: string;
  passageMorph: string;
  passageQuestionEntities: PassageQuestionEntity[];
  wordList: string;
  src: string;
  section: string;
  createdAt: string;
  creatorId: string;
  updatedAt: string;
  updaterId: string;
  workspaceId: string;
  searchQuestion: string;
  deletedPassageQuestionEntities: PassageQuestionEntity[];
}
