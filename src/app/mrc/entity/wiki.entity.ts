export class WikiEntity {

  id: number;
  name: string;                  // Context Id
  creatorId: string;                // Category Id
  createdAt: string;
  updatedAt: string;
  updaterId: string;
  workspaceId: string;

  constructor() {
  }
}
