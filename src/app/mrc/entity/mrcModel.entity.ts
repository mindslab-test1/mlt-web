export class MrcModelEntity {
  workspaceId: string;
  model: string;
  name: string;
  epoches: number;
  batchSize: number;

  constructor() {
  }
}
