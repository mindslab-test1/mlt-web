export class CategoryEntity {

  id: string;
  catName: string;
  catPath: string;
  parCatId: string;
  workspaceId: string;

}
