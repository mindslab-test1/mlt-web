import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {DataGroupComponent} from './components/data-group.component';
import {TrainingComponent} from './components/training.component';
import {CategoryComponent} from './components/category.component';
import {TrainingDataListComponent} from './components/training-data-list.component';
import {TrainingDataUpsertComponent} from './components/training-data-upsert.component';
import {PassageListComponent} from './components/passage-list.component';
import {PassageUpsertComponent} from './components/passage-upsert.component';
import {PassageDetailComponent} from './components/passage-detail.component';
import {PassageIndexingComponent} from './components/passage-indexing.component';
import {PassageSearchTestComponent} from "./components/passage-search-test.component";
import {TrainingDataDetailComponent} from "./components/training-data-detail.component";

const mrcRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'passage-list',
        component: PassageListComponent,
        data: {
          nav: {
            name: 'Passage',
            comment: 'Passage',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'passage-upsert',
        component: PassageUpsertComponent,
      },
      {
        path: 'passage-detail',
        component: PassageDetailComponent,
      },
      {
        path: 'passage-indexing',
        component: PassageIndexingComponent,
        data: {
          nav: {
            name: 'Passage Indexing',
            comment: 'Passage Indexing',
            icon: 'today',
          }
        },
      },
      {
        path: 'passage-search/test',
        component: PassageSearchTestComponent,
        data: {
          nav: {
            name: 'Passage Search Test',
            comment: 'Passage Search Test',
            icon: 'today',
          }
        },
      },
      {
        path: 'training-data-list',
        component: TrainingDataListComponent,
        data: {
          nav: {
            name: 'Traning Data',
            comment: 'Traning Data',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'training-data-detail',
        component: TrainingDataDetailComponent,
      },
      {
        path: 'training-data-upsert',
        component: TrainingDataUpsertComponent,
      },
      {
        path: 'data-group',
        component: DataGroupComponent,
        data: {
          nav: {
            name: 'Data Group',
            comment: 'Data Group',
            icon: 'move_to_inbox',
          }
        },
      },
      {
        path: 'training',
        component: TrainingComponent,
        data: {
          nav: {
            name: 'Traning',
            comment: 'Traning',
            icon: 'school',
          }
        }
      },
      {
        path: 'category',
        component: CategoryComponent,
        data: {
          nav: {
            name: 'Category',
            comment: 'Category',
            icon: 'account_box',
          }
        }
      },
    ]
  }
];

export const MrcRoute: ModuleWithProviders = RouterModule.forChild(mrcRoutes);
