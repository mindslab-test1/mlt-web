import {NgModule} from '@angular/core';

import {SharedModule} from 'mlt-shared';
import {MrcRoute} from './mrc.route';
import {MrcTrainingService} from './services/mrc.training.service';
import {TrainingComponent} from './components/training.component';
import {DataGroupComponent} from './components/data-group.component';
import {CategoryComponent} from './components/category.component';
import {CategoryService} from './services/category.service';
import {TrainingDataService} from './services/training-data.service';
import {MrcDataGroupService} from './services/mrc.data-group.service';
import {HttpClientModule} from '@angular/common/http';
import {WikiUpsertDialogComponent} from './components/wiki-upsert-dialog.component';
import {TrainingDataListComponent} from './components/training-data-list.component';
import {TrainingDataUpsertComponent} from './components/training-data-upsert.component';
import {PassageListComponent} from './components/passage-list.component';
import {PassageSkillUpsertDialogComponent} from './components/passage-skill-upsert-dialog.component';
import {PassageUpsertComponent} from './components/passage-upsert.component';
import {PassageService} from './services/passage.service';
import {PassageDetailComponent} from './components/passage-detail.component';
import {PassageIndexingService} from './services/passage-indexing.service';
import {PassageIndexingComponent} from './components/passage-indexing.component';
import {PassageSearchTestService} from './services/passage-search-test.service';
import {PassageSearchTestComponent} from './components/passage-search-test.component';
import {TrainingDataDetailComponent} from './components/training-data-detail.component';

@NgModule({
  imports: [
    SharedModule,
    MrcRoute,
    HttpClientModule
  ],
  providers: [MrcTrainingService, CategoryService, TrainingDataService, MrcDataGroupService, PassageService, PassageIndexingService, PassageSearchTestService],
  declarations: [DataGroupComponent, TrainingComponent, CategoryComponent, TrainingDataListComponent, PassageListComponent,
    PassageSkillUpsertDialogComponent, PassageUpsertComponent, PassageDetailComponent, WikiUpsertDialogComponent,
    TrainingDataUpsertComponent, TrainingDataDetailComponent, PassageIndexingComponent, PassageSearchTestComponent],
  entryComponents: [WikiUpsertDialogComponent, PassageSkillUpsertDialogComponent],
  exports: [DataGroupComponent, TrainingComponent, CategoryComponent, TrainingDataListComponent, PassageListComponent,
    PassageSkillUpsertDialogComponent, PassageUpsertComponent, PassageDetailComponent, WikiUpsertDialogComponent,
    TrainingDataUpsertComponent, PassageIndexingComponent, PassageSearchTestComponent, TrainingDataDetailComponent]
})
export class MrcModule {
}


