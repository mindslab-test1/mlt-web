import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {AlertComponent, ConfirmComponent, FormErrorStateMatcher, StorageBrowser} from 'mlt-shared';
import {MrcTrainingService} from '../services/mrc.training.service';
import {MrcModelEntity} from '../entity/mrcModel.entity';

@Component({
  selector: 'app-mrc-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class TrainingComponent implements OnDestroy, OnInit {

  workspace = null;
  workspaceId = null;
  // workspaceEntity = null;

  mrcModelEntity: MrcModelEntity = {
    workspaceId: null,
    model: null,
    name: null,
    epoches: null,
    batchSize: null,
  };

  dataGroups: any[] = [];
  actions: any;

  // progress: any;
  models: any[] = [];
  trainings: any[] = [];
  isTrain: boolean;

  nodeFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  repeatFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  timeout: number;
  intervalHandler: any = null;

  constructor(private mrcService: MrcTrainingService,
              private storage: StorageBrowser,
              public matcher: FormErrorStateMatcher,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.isTrain = true;
    this.actions = [
      {type: 'train', text: 'Train', icon: 'school', callback: this.startTrain},
    ];
    this.workspaceId = this.storage.get('workspaceId');
    this.timeout = 5000;
    this.getGroups();
    this.initGetAllProgress();
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
  }

  triggerInterval() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this.intervalHandler = setInterval(() => {
      this.syncProgress();
    }, this.timeout);
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  startTrain = () => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }

      this.mrcModelEntity.workspaceId = this.workspaceId;
      this.mrcService.traning(this.mrcModelEntity).subscribe(
        result => {
          clearInterval(this.intervalHandler);
          this.triggerInterval();
          this.openAlertDialog('Train', 'Train mrc success.', 'success');
        },
        err => {
          clearInterval(this.intervalHandler);
          this.openAlertDialog('Error', 'Server error. train mrc failed.', 'error');
        }
      );
    });
  };

  stopTraining = (key) => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }

      this.mrcService.stop(key).subscribe(result => {
          clearInterval(this.intervalHandler);
          this.triggerInterval();
          this.openAlertDialog('Stop', 'Mrc stop.', 'success');
        },
        err => {
          this.openAlertDialog('Error', 'Server error. mrc stop failed.', 'error');
        });
    });
  };

  removeTraining = (key) => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.mrcService.close(key).subscribe(result => {
          this.openAlertDialog('Remove', `Remove Training mrc ${result['message']}`, 'success');
          clearInterval(this.intervalHandler);
          this.triggerInterval();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. mrc remove training failed.', 'error');
        });
    });
  };

  removeModel(key) {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }

      this.mrcService.delete(key).subscribe(result => {
          clearInterval(this.intervalHandler);
          this.syncProgress();
          this.openAlertDialog('Remove', `Remove model mrc ${result['message']}`, 'success');
          this.triggerInterval();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. mrc remove model failed.', 'error');
        });
    });
  }

  checkReadyToTrain() {
    if (this.mrcModelEntity.model && this.mrcModelEntity.name != null
      && this.mrcModelEntity.epoches != null && this.mrcModelEntity.batchSize != null
      && !this.matcher.isErrorState(this.nodeFormControl)
      && !this.matcher.isErrorState(this.repeatFormControl)) {
      this.isTrain = false;
    } else {
      this.isTrain = true;
    }
  }

  getClassButton() {
    return this.isTrain === true ? 'mlt-disabled' : 'mlt-not-disabled';
  }


  getGroups() {
    this.mrcService.getGroups(this.workspaceId).subscribe(result => {
      if (result) {
        result.forEach(item => {
          this.dataGroups.push({
            viewValue: item.dataGroupName,
            groupId: item.id
          });
        });
      }
    });
  }

  initGetAllProgress() {
    this.mrcService.getAllProgress(this.workspaceId).subscribe(result => {
        clearInterval(this.intervalHandler);
        if (result['mtmList'].length > 0) {
          this.timeout = 450;
          result['mtmList'].forEach(item => {
            this.trainings.push({
              key: item.key,
              name: item.name,
              color: 'primary',
              mode: 'buffer',
              progress: item.mrcProgress,
              status: item.status
            })
          });
        } else {
          this.timeout = 5000;
        }
        this.triggerInterval();
        this.getTrainedMrcModels();
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          clearInterval(this.intervalHandler);
          this.openAlertDialog('Error', 'Server error. mrc connect exception.', 'error');
        }
      }
    );
  };

  syncProgress = () => {
    this.mrcService.getAllProgress(this.workspaceId).subscribe(res => {
        clearInterval(this.intervalHandler);
        if (this.trainings.length === 0) {
          this.timeout = 450;
          res['mtmList'].forEach(item => {
            this.trainings.push({
              key: item.key,
              name: item.name,
              color: 'primary',
              mode: 'buffer',
              progress: item.mrcProgress,
              status: item.status
            })
          });
        } else {
          this.timeout = 450;
          if (res['mtmList'].length === 0) {
            this.trainings = [];
            this.timeout = 5000;
          } else {
            res['mtmList'].forEach(mrcModel => {
              let training: MrcModelEntity = this.trainings.find(item => item.key === mrcModel.key);
              if (training) {
                training['progress'] = mrcModel.mrcProgress;
              } else {
                this.trainings.push(mrcModel);
              }
            });
            this.trainings.forEach((mrcModel, index) => {
              let training: MrcModelEntity = this.trainings.find(item => item.key === mrcModel.key);
              if (!training) {
                this.trainings.splice(index, 1);
              }
            });
          }
        }
        ;
        this.triggerInterval();
        this.syncTrainedMrcModels();
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          clearInterval(this.intervalHandler);
          this.openAlertDialog('Error', 'Server error. mrc connect exception.', 'error');
        }
      });
  }

  getTrainedMrcModels() {
    this.mrcService.getTrainedMrcModels(this.workspaceId).subscribe(result => {
        if (result['tmmList']) {
          result['tmmList'].forEach(item => {
            this.models.push({
              key: item.key,
              name: item.name,
              binary: item.mrcBinary,
              status: item.status,
              trainer: item.creatorId,
              trainedAt: item.createdAt,
              lastApplier: item.updaterId,
              lastAppliedAt: item.updateAt,
              deleteButtonState: 'Delete',
              applyButtonState: 'Applied'
            })
          });
        }
      },
      err => {
        this.openAlertDialog('Error', 'Server error. mrc get train mrc models.', 'error');
      }
    );
  }

  syncTrainedMrcModels = () => {
    this.mrcService.getTrainedMrcModels(this.workspaceId).subscribe(res => {
        if (this.models.length === 0) {
          res['tmmList'].forEach(item => {
            this.models.push({
              key: item.key,
              name: item.name,
              trainer: item.creatorId,
              trainedAt: item.createdAt,
              lastApplier: item.updaterId,
              lastAppliedAt: item.updateAt,
              deleteButtonState: 'Delete',
              applyButtonState: 'Applied'
            })
          });
        } else {
          if (res['tmmList'].length === 0) {
            this.models = [];
          } else {
            res['tmmList'].forEach(mrcTrainedModel => {
              let training = this.models.find(item => item.key === mrcTrainedModel.key);
              if (!training) {
                this.models.push(mrcTrainedModel);
              }
            });
            this.models.forEach((mrcTrainedModel, index) => {
              let training = this.models.find(item => item.key === mrcTrainedModel.key);
              if (!training) {
                this.models.splice(index, 1);
              }
            });
          }
        }
        ;
      },
      err => {
        this.openAlertDialog('Error', 'Server error. mrc get train mrc models.', 'error');
      })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
