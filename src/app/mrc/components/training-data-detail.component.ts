import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ErrorStateMatcher, MatDialog, MatTableDataSource, ShowOnDirtyErrorStateMatcher} from '@angular/material';

import {AlertComponent, FormErrorStateMatcher, StorageBrowser, TableComponent} from 'mlt-shared';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {WikiEntity} from '../entity/wiki.entity';
import {ContextEntity} from '../entity/context.entity';
import {QuestionEntity} from '../entity/question.entity';
import {TrainingDataService} from '../services/training-data.service';

@Component({
  selector: 'app-mrc-training-data-detail',
  templateUrl: './training-data-detail.component.html',
  styleUrls: ['./training-data-detail.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class TrainingDataDetailComponent implements OnInit {
  // default set
  flag: string;
  workspaceId: string;
  wiki: WikiEntity = new WikiEntity();
  invalidFlag = true;

  // qa upsert
  contextEntity: ContextEntity = new ContextEntity();
  contextQuestionEntity: QuestionEntity = new QuestionEntity();
  contextQuestionEntities: QuestionEntity[] = [];

  questionFormControl = new FormControl('', [
    Validators.required,
  ]);

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'question', name: 'Question'},
    {attr: 'questionMorph', name: 'Question Morph'},
    {attr: 'answer', name: 'Question'},
    {attr: 'answerMorph', name: 'Question Morph'},
  ];

  hideContextMorphFlag = true;

  constructor(private trainingDataService: TrainingDataService,
              private _formBuilder: FormBuilder,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {

    this.route.queryParams.subscribe(
      query => {
        this.wiki.id = query['wikiId'];
        this.contextEntity.id = query['contextId'];
      });

    this.workspaceId = this.storage.get('workspaceId');
    this.getSkill();

    if (!this.contextEntity.id) {
      this.flag = 'Add';
    } else {
      this.flag = 'Edit';

      this.cdr.detectChanges();


      this.getContext();
    }
    this.cdr.detectChanges();
  }

  getSkill() {
    this.wiki.workspaceId = this.workspaceId;
    this.trainingDataService.getWikiById(this.wiki).subscribe(res => {
      this.wiki = res;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get wiki list.', 'error');
    })
  }

  getContext() {

      this.contextEntity.workspaceId = this.workspaceId;
      this.trainingDataService.getContext(this.contextEntity).subscribe(res => {
        this.contextEntity = res;
        this.contextQuestionEntities = this.contextEntity.questionEntities;
      }, err => {
        this.openAlertDialog('Error', 'Failed to get context.', 'error');
      })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }


  goBack() {
    let param = `wikiId=${encodeURI(this.wiki.id.toString())}`;
    this.router.navigateByUrl('mlt/mrc/training-data-list?' + param);
  }

  getErrorMessage() {
    return this.questionFormControl.hasError('required') ? 'Please enter question' :
      this.questionFormControl.hasError('duplicate') ? 'Question already exist' :
        '';
  }

  changeHideContextMorphFlag() {
    if (this.hideContextMorphFlag) {
      this.hideContextMorphFlag = false;
    } else if (!this.hideContextMorphFlag) {
      this.hideContextMorphFlag = true;
    }
  }

  goEdit() {
    let param = `wikiId=${encodeURI(this.wiki.id.toString())}&contextId=${encodeURI(this.contextEntity.id.toString())}`;
    this.router.navigateByUrl('mlt/mrc/training-data-upsert?' + param);
  }
}
