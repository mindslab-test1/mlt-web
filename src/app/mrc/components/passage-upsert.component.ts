import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ErrorStateMatcher, MatDialog, MatTableDataSource, ShowOnDirtyErrorStateMatcher} from '@angular/material';

import {AlertComponent, FormErrorStateMatcher, StorageBrowser, TableComponent} from 'mlt-shared';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PassageSkillEntity} from '../entity/passage-skill.entity';
import {PassageEntity} from '../entity/passage.entity';
import {PassageService} from '../services/passage.service';
import {PassageQuestionEntity} from '../entity/passage-question.entity';

@Component({
  selector: 'app-mrc-passage-upsert',
  templateUrl: './passage-upsert.component.html',
  styleUrls: ['./passage-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class PassageUpsertComponent implements OnInit {
  // default set
  flag: string;
  workspaceId: string;
  skill: PassageSkillEntity = new PassageSkillEntity();
  invalidFlag = true;

  // qa upsert
  passageEntity: PassageEntity = new PassageEntity();
  passageQuestionEntity: PassageQuestionEntity = new PassageQuestionEntity();
  passageQuestionEntities: PassageQuestionEntity[] = [];

  questionFormControl = new FormControl('', [
    Validators.required,
  ]);

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'question', name: 'Question', width: '80%'},
    {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Delete'},
  ];

  constructor(private passageService: PassageService,
              private _formBuilder: FormBuilder,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {

    this.route.queryParams.subscribe(
      query => {
        this.skill.id = query['skillId'];
        this.passageEntity.id = query['passageId'];
      });

    this.workspaceId = this.storage.get('workspaceId');
    this.getSkill();

    if (!this.passageEntity.id) {
      this.flag = 'Add';
    } else {
      this.flag = 'Edit';

      this.cdr.detectChanges();


      this.getPassage().then(() => {
        this.checkValid();
      })
    }

    this.cdr.detectChanges();
  }

  getSkill() {
    this.skill.workspaceId = this.workspaceId;
    this.passageService.getSkillById(this.skill).subscribe(res => {
      this.skill = res;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get skill list.', 'error');
    })
  }

  getPassage() {
    return new Promise((resolve) => {
      this.passageEntity.workspaceId = this.workspaceId;
      this.passageService.getPassage(this.passageEntity.id).subscribe(res => {
        this.passageEntity = res;
        this.passageQuestionEntities = this.passageEntity.passageQuestionEntities;
        resolve();
      }, err => {
        this.openAlertDialog('Error', 'Failed to get passage.', 'error');
      })
    })
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addPassage() {
    this.passageEntity.workspaceId = this.workspaceId;
    this.passageEntity.skillId = this.skill.id
    this.passageEntity.passageQuestionEntities = this.passageQuestionEntities;
    this.passageService.addPassage(this.passageEntity).subscribe(res => {
      this.openAlertDialog('Success', `Save Success.`, 'success');
      let param = `skillId=${encodeURI(this.skill.id.toString())}`;
      this.router.navigateByUrl('mlt/mrc/passage-list?' + param);
    })
  }

  addPassageQuestion() {
    if (this.passageQuestionEntity.question === undefined || this.passageQuestionEntity.question.trim() === '') {
      this.questionFormControl.setErrors({'required': true});
    } else if (this.passageQuestionEntities.findIndex(q => q.question === this.passageQuestionEntity.question) !== -1) {
      this.questionFormControl.setErrors({'duplicate': true});
    } else {
      this.passageQuestionEntity.workspaceId = this.workspaceId;
      this.passageQuestionEntities.push(this.passageQuestionEntity);
      this.passageQuestionEntity = new PassageQuestionEntity();
      this.checkValid();
      this.questionFormControl.reset();
    }
  }

  editPassage() {
    this.passageEntity.passageQuestionEntities = this.passageQuestionEntities;
    this.passageService.editPassage(this.passageEntity).subscribe(res => {
      this.openAlertDialog('Success', `Save Success.`, 'success');
      let param = `skillId=${encodeURI(this.skill.id.toString())}`;
      this.router.navigateByUrl('mlt/mrc/passage-list?' + param);
    })
  }

  deletePassageQuestion = (row) => {
    if (this.flag === 'Edit') {
      if (this.passageEntity.deletedPassageQuestionEntities === null) {
        this.passageEntity.deletedPassageQuestionEntities = [];
      }
      this.passageEntity.deletedPassageQuestionEntities.push(row);
    }
    this.passageQuestionEntities.splice(this.passageQuestionEntities.findIndex(q => q.question === row.question), 1);
    this.checkValid();
  }


  goBack() {
    let param = `skillId=${encodeURI(this.skill.id.toString())}`;
    this.router.navigateByUrl('mlt/mrc/passage-list?' + param);
  }

  checkValid() {
    if (this.passageEntity.section === undefined || this.passageEntity.section.trim() === '' ||
      this.passageEntity.src === undefined || this.passageEntity.src.trim() === '' ||
      this.passageEntity.passage === undefined || this.passageEntity.passage.trim() === '' || this.passageQuestionEntities.length === 0) {
      this.invalidFlag = true;
    } else {
      this.invalidFlag = false;
    }
  }

  getErrorMessage() {
    return this.questionFormControl.hasError('required') ? 'Please enter question' :
      this.questionFormControl.hasError('duplicate') ? 'Question already exist' :
        '';
  }

  save() {
    if (this.flag === 'Add') {
      this.addPassage();
    } else if (this.flag === 'Edit') {
      this.editPassage();
    }
  }

}
