import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {PassageSkillEntity} from '../entity/passage-skill.entity';
import {PassageSearchTestService} from '../services/passage-search-test.service';


@Component({
  selector: 'app-mrc-passage-search-test',
  templateUrl: './passage-search-test.component.html',
  styleUrls: ['./passage-search-test.component.scss'],
})

export class PassageSearchTestComponent implements OnInit {

  loading = false;
  workspaceId: string;
  skillList: PassageSkillEntity[] = [];
  selectedSkill: PassageSkillEntity;
  nTop: number;
  nTopFormControl: FormControl;
  searchQuestion: string;
  searchSection: string;
  searchSource: string;
  result: any;

  constructor(private passageSearchTestService: PassageSearchTestService,
              private storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  /**
   *  화면 초기화
   */
  ngOnInit() {
    this.getSkillList();
    this.nTop = 3;
    this.nTopFormControl = new FormControl(3, [
      Validators.required,
      () => (this.nTop <= 0) ? {negativeInteger: true} : null
    ]);
  }

  getSkillList() {
    this.passageSearchTestService.getSkillListByWorkspaceId(this.storage.get('workspaceId')).subscribe(res => {
        this.skillList = res;
      },
      err => {
        this.openAlertDialog('Error', 'Skill list not fetched.', 'error');
      }
    );
  }

  findPassage() {
    this.loading = true;
    let param = {
      'question': this.searchQuestion,
      'ntop': this.nTop,
      'skillId': this.selectedSkill,
      'src': this.searchSource,
      'section': this.searchSection
    }
    this.passageSearchTestService.findPassage(param).subscribe(
      res => {
        this.result = res;
        this.loading = false;
      },
      err => {
            this.openAlertDialog('Error', 'server error. Test failed.', 'error');
            this.loading = false;
      }
    )

  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
