import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

import {AlertComponent, ConfirmComponent, FormErrorStateMatcher, StorageBrowser} from 'mlt-shared';
import {CategoryService} from '../services/category.service';
import {CategoryEntity} from '../entity/category.entity';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class CategoryComponent implements OnInit {
  tipsVisible = false;
  workspaceId: string;

  sourceList: string[] = [];
  selectedSource: string;

  treeData: CategoryEntity[] = [];

  inputCategory: string[] = [];
  categoryList: CategoryEntity[][] = [[], [], [], []];
  selectedCategory: CategoryEntity[] = [];

  beforeClickData: HTMLInputElement[] = [];
  currentClickData: HTMLInputElement[] = [];

  cat1formControl = new FormControl('', [
    Validators.required,
  ]);
  cat2formControl = new FormControl('', [
    Validators.required
  ]);
  cat3formControl = new FormControl('', [
    Validators.required
  ]);
  cat4formControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private categoryService: CategoryService,
              private dialog: MatDialog,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.getAllCategory().then(() => {
      this.categoryList[0] = this.treeData[this.selectedSource];
    });
  }


  getAllCategory() {
    return new Promise(resolve => {
      this.categoryService.getCategoryList(this.workspaceId).subscribe(
        res => {
          if (res) {
            Object.keys(res).forEach(value => {
              this.sourceList.push(value);
            });
            this.sourceList.splice(this.sourceList.indexOf('dataType'), 1);
            this.treeData = res;
          }
          this.selectedSource = this.sourceList[0];
          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Error. Categories not imported.', 'error');
        }
      )
    });
  }

  getCategory(step, category, index) {
    this.initCategoryList(step);
    this.selectedCategory[step - 1] = category;
    if (category.children !== 0) {
      this.categoryList[step] = category.children;
    }
    this.isSelected(step, index);
  }

  isSelected(step, index) {
    this.inputCategory[step - 1] = this.selectedCategory[step - 1].catName;
    this.beforeClickData[step - 1] = this.currentClickData[step - 1];
    if (this.beforeClickData[step - 1] != null) {
      this.beforeClickData[step - 1].style.backgroundColor = '#ffffff';
    }
    this.currentClickData[step - 1] = <HTMLInputElement>document.getElementsByName(`category${step}`)[index];
    this.currentClickData[step - 1].style.backgroundColor = '#c1d9ff';
    for (let i = step; i < 4; i++) {
      this.inputCategory[i] = null;
    }
  }

  isDelete(category) {
    return category.children.length === 0;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  openConfirmDialog(step, type, category) {
    if (type === 'edit' && this.getErrorState(step)) {
      this.openAlertDialog('Failed', 'Please Input Category', 'error');
      return;
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = type === 'edit' ? `Do you want to Edit
    ${this.selectedCategory[step - 1].catName} -> ${this.inputCategory[step - 1]} ?`
        : `Do you want to Delete ${category.catName}?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          if (type === 'delete') {
            this.deleteCategory(step, category);
          } else if (type === 'edit') {
            this.editCategory(step);
          }
        }
      });
    }
  }

  initCategoryList(step) {
    for (let i = step; i < 4; i++) {
      this.categoryList[i] = null;
      this.selectedCategory[i] = null;
      if (this.currentClickData[i] != null) {
        this.currentClickData[i].style.backgroundColor = '#ffffff';
      }
    }
  }

  refresh(step) {
    for (let i = step - 1; i < 4; i++) {
      this.selectedCategory[i] = null;
      this.inputCategory[i] = null;
      if (this.currentClickData[i] != null) {
        this.currentClickData[i].style.backgroundColor = '#ffffff';
      }
    }

    if (step !== 4) {
      this.initCategoryList(step);
    }
  }

  changeSource() {
    this.refresh(1);
    this.initCategoryList(0);
    this.categoryList[0] = this.treeData[this.selectedSource];
  }

  getErrorState(step) {
    if (this.inputCategory[step - 1] === null) {
      return true;
    } else if (step === 1 && this.matcher.isErrorState(this.cat1formControl)) {
      return true;
    } else if (step === 2 && this.matcher.isErrorState(this.cat2formControl)) {
      return true;
    } else if (step === 3 && this.matcher.isErrorState(this.cat3formControl)) {
      return true;
    } else if (step === 4 && this.matcher.isErrorState(this.cat4formControl)) {
      return true;
    } else {
      return false;
    }
  }

  addCategory(step) {
    if (this.getErrorState(step)) {
      this.openAlertDialog('Failed', 'Please Input Category', 'error');
    } else {
      let category: CategoryEntity = new CategoryEntity();
      category.catName = this.inputCategory[step - 1].trim();
      category.workspaceId = this.workspaceId;
      if (step === 1) {
        category.catPath = this.inputCategory[step - 1].trim();
        category.parCatId = this.selectedSource;
      } else {
        category.catPath = `${this.selectedCategory[step - 2].catPath}>${this.inputCategory[step - 1].trim()}`
        category.parCatId = this.selectedCategory[step - 2].id;
      }
      this.categoryService.addCategory(category).subscribe(
        res => {
          if (res.message === 'INSERT_SUCESS') {
            this.categoryList[step - 1].push(res.category);
            this.inputCategory[step - 1] = null;
            this.openAlertDialog('Success', `${category.catName} Insert Success.`, 'success');
          } else if (res.message === 'INSERT_DUPLICATED') {
            this.openAlertDialog('Duplicated', `${category.catName} Duplicated.`, 'error');
          }
        },
        err => {
          this.openAlertDialog('Failed', `Error. ${category.catName} insert Failed.`, 'error');
        }
      );
    }
  }

  editCategory(step) {
    let category: CategoryEntity = new CategoryEntity();
    category = JSON.parse(JSON.stringify(this.selectedCategory[step - 1]));
    category.catName = this.inputCategory[step - 1].trim();
    category['groupCodeName'] = this.selectedSource;
    let categoryTempList: string[] = [];

    if (step === 1) {
      category.catPath = this.inputCategory[step - 1];
    } else {
      for (let i = step - 1; i < 4; i++) {
        if (this.selectedCategory[i]) {
          let oldPaths: string[] = this.selectedCategory[i].catPath.split('>');
          oldPaths.splice(oldPaths.indexOf(this.selectedCategory[step - 1].catName), 1, this.inputCategory[step - 1].trim());
          let newPath: string = oldPaths.join('>');
          categoryTempList[i] = newPath;
        }
      }
      category.catPath = categoryTempList[step - 1];
    }

    this.categoryService.editCategory(category).subscribe(
      res => {
        if (res.message === 'UPDATE_SUCESS') {
          let changeCategory = this.categoryList[step - 1].find(key => key.id === res.category.id);
          changeCategory.catName = res.category.catName;
          changeCategory.catPath = res.category.catPath;
          this.selectedCategory[step - 1] = changeCategory;
          this.selectedCategory.forEach((result, index) => {
            if (categoryTempList[index]) {
              result.catPath = categoryTempList[index];
            }
          });
          this.inputCategory[step - 1] = null;
          this.openAlertDialog('Success', `${category.catName} Edit Success.`, 'success');
        } else if (res.message === 'UPDATE_DUPLICATED') {
          this.openAlertDialog('Duplicated', `${category.catName} Duplicated`, 'error');
        }
      },
      err => {
        this.openAlertDialog('Failed', `Error. ${category.catName} Update Failed.`, 'error');
      }
    );
  }

  deleteCategory(step, category) {
    this.categoryService.removeCategory(category).subscribe(
      res => {
        if (res.message === 'DELETE_SUCESS') {
          this.categoryList[step - 1]
          .splice(this.categoryList[step - 1].indexOf(category), 1);
          this.inputCategory[step - 1] = null;
          this.selectedCategory[step - 1] = null;
          this.openAlertDialog('Success', `${category.catName} Delete Success.`, 'success');
        }
      },
      err => {
        this.openAlertDialog('Failed', `Error. ${category.catName} delete failed.`, 'error');
      }
    );
  }
}
