import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

import {
  AlertComponent,
  ConfirmComponent,
  FormErrorStateMatcher,
  StorageBrowser,
  TableComponent,
  UploaderButtonComponent
} from 'mlt-shared';
import {PassageService} from '../services/passage.service';
import {PassageEntity} from '../entity/passage.entity';
import {PassageSkillEntity} from '../entity/passage-skill.entity';
import {PassageSkillUpsertDialogComponent} from './passage-skill-upsert-dialog.component';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-mrc-passage-list',
  templateUrl: './passage-list.component.html',
  styleUrls: ['./passage-list.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class PassageListComponent implements OnInit, AfterViewInit, OnDestroy {
  skillActionsArray: any[];
  passageActionsArray: any[];

  workspaceId: string;
  skillList: any[];
  selectedSkill = 0;

  pageParam: MatPaginator;
  pageLength: number;
  searchPassage = '';
  searchQuestion = '';
  searchSection = '';
  searchSource = '';

  dataSource: MatTableDataSource<any>;
  passageParam = new PassageEntity();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('passageTemplate') passageTemplate: TemplateRef<any>;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  header = [];
  passageList: any[] = [];

  private stompClient;
  analyzingStatus = false;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredSkillList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private passageService: PassageService,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.skillActionsArray = [
      {
        type: 'add',
        text: 'Add Skill',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popSkillUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Skill',
        icon: 'remove_circle_outline',
        hidden: true,
        disabled: true,
        callback: this.popSkillUpsertDialog,
        params: 'edit'
      },
    ];
    this.passageActionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.addPassage,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removePassage,
        params: 'remove'
      },
      {
        type: 'edit',
        text: 'Remove All',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removePassageAll,
        params: 'remove'
      },
    ];
    this.header = [
      {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      {attr: 'no', name: 'No', no: true},
      {attr: 'passage', name: 'Passage', width: '40%', isSort: true, template: this.passageTemplate},
      {attr: 'questionCnt', name: 'Question Count', width: '10%', isSort: false},
      {attr: 'section', name: 'Section', isSort: true, width: '20%'},
      {attr: 'src', name: 'Source', isSort: true, width: '20%'},
      {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
    ];
    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSkill();
      });
    this.getSkillList();
    this.initializeWebSocketConnection();
  }

  private filterSkill() {
    if (!this.skillList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredSkillList.next(this.skillList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredSkillList.next(
      this.skillList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  initializeWebSocketConnection() {
    let serverUrl = this.storage.get('mltApiUrl') + '/socket';
    let ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;

    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/api/mlt/passage-analyze/status', (message) => {
        if (message.body === 'error') {
          that.openAlertDialog('Error', 'Failed to analyze', 'error');
          that.analyzingStatus = false;
        }
        that.analyzingStatus = message.body;
      })
    })
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.route.queryParams.subscribe(
      query => {
        if (query['skillId'] !== undefined && query['skillId'] !== null) {
          this.selectedSkill = query['skillId'] * 1;
          this.cdr.detectChanges();
          this.getNextPage();
        }
      });
  }

  ngOnDestroy() {
    this.stompClient.unsubscribe();
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  searchKeydown(event) {
    if (event.keyCode === 13) {
      this.onClickSearchButton(0);
    }
  }
  /**
   * Search Button 정의
   * @param  pageIndex
   */
  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      this.passageParam.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.passageParam.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    if (this.selectedSkill === 0) {
      this.skillActionsArray[1].hidden = true;
      this.skillActionsArray[1].disabled = true;
      return false;
    }
    this.skillActionsArray[1].hidden = false;
    this.skillActionsArray[1].disabled = false;
    this.cdr.detectChanges();

    this.passageParam.pageSize = this.pageParam.pageSize;
    this.passageParam.pageIndex = this.pageParam.pageIndex;
    this.passageParam.skillId = this.selectedSkill;

    this.passageParam.passage = this.searchPassage;
    this.passageParam.searchQuestion = this.searchQuestion;
    this.passageParam.section = this.searchSection;
    this.passageParam.src = this.searchSource;

    this.passageService.getPassageList(this.passageParam).subscribe(res => {
      if (res) {
        res.content.forEach(val => {
          val.questionCnt = val.passageQuestionEntities.length;
        })
        this.passageList = res.content;
        this.pageLength = res.totalElements;
        this.dataSource = new MatTableDataSource(this.passageList);
        this.lineTableComponent.isCheckedAll = false;
      }
    });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.passageList.length > 0) {
      this.getNextPage();
    }
  }

  getSkillList() {
    this.passageService.getSkillListByWorkspaceId(this.workspaceId).subscribe(res => {
      this.skillList = res;
      this.filteredSkillList.next(this.skillList.slice());
    }, err => {
      this.openAlertDialog('Error', 'Failed to get skill list.', 'error');
    })
  }

  popSkillUpsertDialog = (type, row?) => {
    let paramData = {};
    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.getSelectedSkillEntity();
    } else {
      return false;
    }

    paramData['service'] = this.passageService;

    let dialogRef = this.dialog.open(PassageSkillUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSkillList();
      if (result === 'delete') {
        this.selectedSkill = 0;
        this.skillActionsArray[1].hidden = true;
        this.skillActionsArray[1].disabled = true;
      } else {
        // this.getNextPage();
      }
    });
  }


  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addPassage = ($evnet) => {
    const skill = this.getSelectedSkillEntity();
    let param = `skillId=${encodeURI(skill.id.toString())}`;
    if (skill) {
      this.router.navigateByUrl('mlt/mrc/passage-upsert?' + param);
    }
  }

  editPassage = (row) => {
    const skill = this.getSelectedSkillEntity();
    if (skill) {
      let param = `skillId=${encodeURI(skill.id.toString())}&passageId=${encodeURI(row.id.toString())}`;
      this.router.navigateByUrl('mlt/mrc/passage-upsert?' + param);
    }
  }

  detailPassage = (row) => {
    const skill = this.getSelectedSkillEntity();
    if (skill) {
      let param = `skillId=${encodeURI(skill.id.toString())}&passageId=${encodeURI(row.id.toString())}`;
      this.router.navigateByUrl('mlt/mrc/passage-detail?' + param);
    }
  }

  removePassage = () => {
    let toBeRemoved = [];
    this.passageList.forEach(passage => {
      if (passage.isChecked) {
        toBeRemoved.push(passage);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please Select Data to Removed', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.lineTableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.lineTableComponent.isCheckedAll = false;
        this.passageService.removePassageList(toBeRemoved).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  }

  removePassageAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove All Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;

        this.pageParam.pageIndex = 0;

        this.lineTableComponent.isCheckedAll = false;

        this.passageService.removePassageListBySkillId(this.selectedSkill).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  }

  getSelectedSkillEntity() {
    let result = new PassageSkillEntity();
    this.skillList.forEach(item => {
      if (item.id === this.selectedSkill) {
        result = item;
      }
    });
    return result;
  }

  // 다운로드 함수
  downloadFile = () => {
    let url = this.storage.get('mltApiUrl') + '/mrc/passage/' + this.selectedSkill + '/' + this.workspaceId + '/download-file';
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  nlpAnalyze(flag?) {
    if (flag === 'all') {
      this.passageService.analyzeNlpAll(this.selectedSkill).subscribe(res => {
          this.openAlertDialog('Success', `NLP Analyze Request Success`, 'success');
          this.getNextPage();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Analyze failed.', 'error');
        });
    } else {
      let checkedList = [];
      this.passageList.forEach(passage => {
        if (passage.isChecked) {
          checkedList.push(passage);
        }
      });
      if (checkedList.length === 0) {
        this.openAlertDialog('Error', 'Please Select Data to Analyze', 'error');
        return false;
      }
      this.passageService.analyzeNlp(checkedList).subscribe(res => {
          this.openAlertDialog('Success', `NLP Analyze Request Success`, 'success');
          this.getNextPage();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Analyze failed.', 'error');
        });
    }
  }


  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  // 업로드 이후 호출되는 함수
  onUploadFile = () => {
    this.getNextPage();
  };

  selectedSkillChange() {
    // init search param
    this.searchPassage = '';
    this.searchSource = '';
    this.searchSection = '';
    this.getNextPage();
  }

}
