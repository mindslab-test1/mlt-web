import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {DatePipe} from '@angular/common';

import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {MatDialog} from '@angular/material';
import {PassageIndexingService} from '../services/passage-indexing.service';
import {FormControl} from "@angular/forms";
import {ReplaySubject, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-mrc-passage-indexing',
  templateUrl: './passage-indexing.component.html',
  styleUrls: ['./passage-indexing.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PassageIndexingComponent implements OnInit, OnDestroy {
  workspaceId: string;
  indexing;
  history;
  progressStatus = false;
  intervalHandler: any;
  timeout;
  disabled = false;

  selected = false;

  skillList: any[];
  selectedSkill = 0;
  clean = false;
  valid = true;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredSkillList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private passageIndexingService: PassageIndexingService,
              private datePipe: DatePipe,
              private dialog: MatDialog,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.getSkillList();
    this.timeout = 5000;
    this.getIndexingStatus();
    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSkill();
      });
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterSkill() {
    if (!this.skillList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredSkillList.next(this.skillList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredSkillList.next(
      this.skillList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.getIndexingStatus()
    }, this.timeout);
  }

  fullIndexing() {
    this.passageIndexingService.fullIndexing(this.selectedSkill, this.workspaceId, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        };
      } else {
        this.disabled = false;
        clearInterval(this.intervalHandler);
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', err.error.message, 'error');
    })
  }

  additionalIndexing() {
    this.passageIndexingService.additionalIndexing(this.selectedSkill, this.workspaceId, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', err.error.message, 'error');
    })
  }

  abortIndexing() {
    this.passageIndexingService.abortIndexing().subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', err.error.message, 'error');
    })
  }

  getIndexingStatus() {
    this.passageIndexingService.getIndexingStatus().subscribe(res => {
      this.progressStatus = res.status;
      clearInterval(this.intervalHandler);
      if (res.status) {
        this.disabled = true;
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.history = res.latestIndexingHistory;
        if (this.history != null) {
          this.history.progress = res.progress;
          let date = new Date(this.history.createdAt);
          this.history.createdAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          if (this.history.stopYn) {
            date = new Date(this.history.updatedAt);
            this.history.updatedAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          }
        }
        this.disabled = false;
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', err.error.message, 'error');
    })
  }

  getSkillList() {
    this.passageIndexingService.getSkillListByWorkspaceId(this.workspaceId).subscribe(res => {
      this.skillList = res;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get skill list.', 'error');
    })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  checkValid() {
    if (this.selectedSkill !== undefined && this.clean !== undefined) {
      this.valid = true;
    }  else {
      this.valid = false;
    }
  }
}
