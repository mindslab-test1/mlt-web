import {ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatStep,
  MatStepper,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';

import {AlertComponent, ConfirmComponent, FormErrorStateMatcher, StorageBrowser} from 'mlt-shared';
import {ContextEntity} from '../entity/context.entity';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WikiEntity} from '../entity/wiki.entity';
import {AnswerEntity} from '../entity/answer.entity';
import {QuestionEntity} from '../entity/question.entity';
import {CategoryEntity} from '../entity/category.entity';
import {TrainingDataService} from '../services/training-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-mrc-training-data-upsert',
  templateUrl: './training-data-upsert.component.html',
  styleUrls: ['./training-data-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class TrainingDataUpsertComponent implements OnInit {
  // default set
  flag: string;
  initCate = {children: []};
  workspaceId: string;
  wiki: WikiEntity = new WikiEntity();

  invalidFlag = true;


  contextFormGroup: FormGroup;
  qaFormGroup: FormGroup

  // category
  contextCate = [];
  inputContextTitle = '';
  inputContextContent = '';
  inputContextSeason: number;
  selectedContextCate1 = {children: []};
  selectedContextCate2 = {children: []};
  selectedContextCate3 = {children: []};
  selectedContextCate4 = {children: []};


  // qa upsert
  questionCate = [];
  contextEntity: ContextEntity = new ContextEntity();
  questionEntities: QuestionEntity[] = [];

  @ViewChild('questionTemplate') questionTemplate: TemplateRef<any>;
  @ViewChild('answerTemplate') answerTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  qaDataSource: MatTableDataSource<any>;
  qaHeader = [];
  inputQuestion: string;
  inputAnswer: string;


  constructor(private trainingDataService: TrainingDataService,
              private _formBuilder: FormBuilder,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.qaHeader = [
      {attr: 'question', name: 'Question', template: this.questionTemplate, width: '40%'},
      {attr: 'answerEntities[0].answer', name: 'Answer', template: this.answerTemplate, width: '40%'},
      {attr: 'action', name: 'Action', template: this.actionTemplate, width: '20%'},
    ];
    this.route.queryParams.subscribe(
      query => {
        this.wiki.id = query['wikiId'];
        this.contextEntity.id = query['contextId'];
      });

    this.workspaceId = this.storage.get('workspaceId');
    this.getWiki();
    this.getCategoryList();

    if (!this.contextEntity.id) {
      this.flag = 'Add';
    } else {
      this.flag = 'Edit';
      this.getContext();
    }
  }

  getCategoryList() {
      this.trainingDataService.getCategoryList(this.wiki.workspaceId).subscribe(res => {
          if (res) {
            this.contextCate = res.context;
            this.questionCate = res.question;
          }
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
          console.log('error', err);
        }
      );
  }

  getWiki() {
    this.wiki.workspaceId = this.workspaceId;
    this.trainingDataService.getWikiById(this.wiki).subscribe(res => {
      this.wiki = res;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get wiki list.', 'error');
    })
  }

  getContext() {
      this.contextEntity.workspaceId = this.workspaceId;
      this.trainingDataService.getContext(this.contextEntity).subscribe(res => {
        this.contextEntity = res;
        this.questionEntities = this.contextEntity.questionEntities;
        let splitedCategory = this.contextEntity.contextCategory.catPath.split('>');
        this.selectedContextCate1 = this.contextCate.filter(item => item.catName === splitedCategory[0])[0];
        if (splitedCategory.length > 1) {
          this.selectedContextCate2 = this.selectedContextCate1.children.filter(item => item.catName === splitedCategory[1])[0];
        }
        if (splitedCategory.length > 2) {
          this.selectedContextCate3 = this.selectedContextCate2.children.filter(item => item.catName === splitedCategory[2])[0];
        }
        if (splitedCategory.length > 3) {
          this.selectedContextCate4 = this.selectedContextCate3.children.filter(item => item.catName === splitedCategory[3])[0];
        }
        this.checkValid();
        }, err => {
        this.openAlertDialog('Error', 'Failed to get context.', 'error');
      })
  }

  clearSelectOption(valStr, dept) {
    // 최대 길이 설정
    let maxLength = 0;
    if (valStr === 'selectedContextCate') {
      maxLength = 4;
    } else if (valStr === 'selectedQuestionCate') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      eval('this.' + valStr + i + ' = this.initCate');
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addTrainingData() {
    if (this.contextEntity.context.length >= 3000) {
      this.openAlertDialog('Error', 'Error. Context is too long.', 'error');
      return;
    }

    if (this.contextEntity.season === undefined) {
      this.openAlertDialog('Error', 'Error. Please fill out season', 'error');
      return;
    }

    if (this.contextEntity.title.trim() === '') {
      this.openAlertDialog('Error', 'Error. Please fill out title', 'error');
      return;
    }

    if (this.contextEntity.context.trim() === '') {
      this.openAlertDialog('Error', 'Error. Please fill out content', 'error');
      return;
    }

    if (this.selectedContextCate1['id'] === undefined && this.selectedContextCate2['id'] === undefined
      && this.selectedContextCate3['id'] === undefined && this.selectedContextCate4['id'] === undefined) {
      this.openAlertDialog('Error', 'Error. Please select context category', 'error');
      return;
    }


    if (this.selectedContextCate2['id'] === undefined) {
      this.contextEntity.catId = this.selectedContextCate1['id'];
    } else {
      if (this.selectedContextCate3['id'] === undefined) {
        this.contextEntity.catId = this.selectedContextCate2['id'];
      } else {
        if (this.selectedContextCate4['id'] === undefined) {
          this.contextEntity.catId = this.selectedContextCate3['id'];
        } else {
          this.contextEntity.catId = this.selectedContextCate4['id'];
        }
      }
    }
    this.contextEntity.workspaceId = this.storage.get('workspaceId');
    this.contextEntity.wikiId = this.wiki.id;
    this.contextEntity.questionEntities = this.questionEntities;
    this.trainingDataService.addContext(this.contextEntity).subscribe(res => {
        let param = `wikiId=${encodeURI(this.wiki.id.toString())}`;
        this.router.navigateByUrl('mlt/mrc/training-data-list?' + param);

      },
      err => {
        this.openAlertDialog('Error', 'Failed Add Item', 'error');
        console.log('error', err);
      }
    );
  }

  editTrainingData() {
    if (this.contextEntity.context.length >= 3000) {
      this.openAlertDialog('Error', 'Error. Context is too long.', 'error');
      return;
    }

    if (this.contextEntity.season === undefined) {
      this.openAlertDialog('Error', 'Error. Please fill out season', 'error');
      return;
    }

    if (this.contextEntity.title.trim() === '') {
      this.openAlertDialog('Error', 'Error. Please fill out title', 'error');
      return;
    }

    if (this.contextEntity.context.trim() === '') {
      this.openAlertDialog('Error', 'Error. Please fill out content', 'error');
      return;
    }

    if (this.selectedContextCate1['id'] === undefined && this.selectedContextCate2['id'] === undefined
      && this.selectedContextCate3['id'] === undefined && this.selectedContextCate4['id'] === undefined) {
      this.openAlertDialog('Error', 'Error. Please select context category', 'error');
      return;
    }


    if (this.selectedContextCate2['id'] === undefined) {
      this.contextEntity.catId = this.selectedContextCate1['id'];
    } else {
      if (this.selectedContextCate3['id'] === undefined) {
        this.contextEntity.catId = this.selectedContextCate2['id'];
      } else {
        if (this.selectedContextCate4['id'] === undefined) {
          this.contextEntity.catId = this.selectedContextCate3['id'];
        } else {
          this.contextEntity.catId = this.selectedContextCate4['id'];
        }
      }
    }
    this.contextEntity.workspaceId = this.storage.get('workspaceId');
    this.contextEntity.wikiId = this.wiki.id;
    this.contextEntity.questionEntities = this.questionEntities;
    this.trainingDataService.editContext(this.contextEntity).subscribe(res => {
        let param = `wikiId=${encodeURI(this.wiki.id.toString())}`;
        this.router.navigateByUrl('mlt/mrc/training-data-list?' + param);

      },
      err => {
        this.openAlertDialog('Error', 'Failed Add Item', 'error');
        console.log('error', err);
      }
    );
  }

  addQuestionAndAnswer() {
    this.inputQuestion = this.inputQuestion.trim();
    this.inputAnswer = this.inputAnswer.trim();
    if (this.inputQuestion === '') {
      this.openAlertDialog('Error', 'Error. Please input question', 'error');
      return;
    }

    if (this.inputAnswer === '') {
      this.openAlertDialog('Error', 'Error. Please input question', 'error');
      return;
    }
    let questionParam = new QuestionEntity();
    let answerParam = new AnswerEntity();
    questionParam.workspaceId = this.workspaceId;
    questionParam.question = this.inputQuestion;
    answerParam.workspaceId = this.workspaceId;
    answerParam.answer = this.inputAnswer;
    questionParam.answerEntities = [answerParam];
    this.questionEntities.push(questionParam);
    // // Answer리스트 리로드
    console.log(this.questionEntities);
    this.qaDataSource = new MatTableDataSource(this.questionEntities);
    this.inputQuestion = '';
    this.inputAnswer = '';
    this.checkValid();
  }


  goBack() {
    let param = `wikiId=${encodeURI(this.wiki.id.toString())}`;
    this.router.navigateByUrl('mlt/mrc/training-data-list?' + param);  }

  checkValid() {
    if (this.contextEntity.season === undefined || (this.selectedContextCate1['id'] === undefined && this.selectedContextCate2['id'] === undefined
      && this.selectedContextCate3['id'] === undefined && this.selectedContextCate4['id'] === undefined) ||
      this.contextEntity.title === undefined || this.contextEntity.title.trim() === '' ||
      this.contextEntity.context === undefined || this.contextEntity.context.trim() === '' || this.questionEntities.length === 0) {
      this.invalidFlag = true;
    } else {
      this.invalidFlag = false;
    }
  }

  save() {
    if (this.flag === 'Add') {
      this.addTrainingData();
    } else if (this.flag === 'Edit') {
      this.editTrainingData();
    }
  }

  editRow(row) {
    row['isEditMode'] = true;
    row.questionTemp = row.question;
    row.answerEntities[0].answerTemp = row.answerEntities[0].answer;
  }

  deleteRow(row) {
    if (this.contextEntity.deletedQuestionEntities == undefined) {
      this.contextEntity.deletedQuestionEntities = [];
    }
    let idx = this.questionEntities.indexOf(row);
    this.contextEntity.deletedQuestionEntities.push(this.questionEntities[idx]);
    this.questionEntities.splice(idx, 1);
  }

  saveRow(row) {
    row.question = row.questionTemp;
    row.answerEntities[0].answer = row.answerEntities[0].answerTemp;
    delete row.questionTemp;
    delete row.answerEntities[0].answerTemp;
    row['isEditMode'] = false;
  }

  cancelEditMode(row) {
    delete row.isEditMode;
    delete row.questionTemp;
    delete row.answerEntities[0].answerTemp;
  }
}
