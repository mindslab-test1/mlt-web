import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit, TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

import {
  AlertComponent,
  ConfirmComponent,
  FormErrorStateMatcher,
  StorageBrowser,
  TableComponent,
  UploaderButtonComponent
} from 'mlt-shared';
import {WikiEntity} from '../entity/wiki.entity';
import {WikiUpsertDialogComponent} from './wiki-upsert-dialog.component';
import {ContextEntity} from '../entity/context.entity';
import {TrainingDataService} from '../services/training-data.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-mrc-training-data-list',
  templateUrl: './training-data-list.component.html',
  styleUrls: ['./training-data-list.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class TrainingDataListComponent implements OnInit, OnDestroy, AfterViewInit {
  wikiActionsArray: any[];
  contextActionsArray: any[];

  workspaceId: string;
  wikiList: any[];
  selectedWiki = 0;

  contextCate = [];
  selectedContextCate1 = {children: []};
  selectedContextCate2 = {children: []};
  selectedContextCate3 = {children: []};
  selectedContextCate4 = {children: []};
  searchSeason: number;
  searchTitle: string = '';
  searchContext: string = '';
  searchQuestion: string = '';
  pageParam: MatPaginator;
  pageLength: number;
  keyword = '';
  dataSource: MatTableDataSource<any>;
  contextParam = new ContextEntity();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('trainingDataTemplate') trainingDataTemplate: TemplateRef<any>;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  contextList: any[] = [];
  header = [];

  analyzingStatus = false;
  private stompClient;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredWikiList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);


  constructor(private trainingDataService: TrainingDataService,
              private router: Router,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              private route: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.wikiActionsArray = [
      {
        type: 'add',
        text: 'Add Wiki',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popWikiUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Wiki',
        icon: 'remove_circle_outline',
        hidden: true,
        disabled: true,
        callback: this.popWikiUpsertDialog,
        params: 'edit'
      },
    ];
    this.contextActionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.addTrainingData,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeTrainingData,
        params: 'remove'
      },
      {
        type: 'edit',
        text: 'Remove ALL',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeTrainingDataAll,
        params: 'remove'
      },
    ];
    this.header = [
      {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      {attr: 'no', name: 'No', no: true},
      {attr: 'title', name: 'Title', width: '40%', template: this.trainingDataTemplate},
      {attr: 'season', name: 'Season', isSort: true, width: '10%'},
      {attr: 'contextCategory.catPath', name: 'Source', isSort: true, width: '20%'},
      {attr: 'createdAt', name: 'CreatedAt', format: 'date', width: '12%', isSort: true},
      {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
    ];
    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterWiki();
      });
    this.getWikiList();
    this.getCategoryList();
    this.initializeWebSocketConnection();
    // let previousWikiId = this.storage.get('mlt-mrc-training-list-selected-wiki-id');
    // if (previousWikiId !== null && previousWikiId !== undefined) {
    //   this.selectedWiki = previousWikiId;
    //   this.storage.remove('mlt-mrc-training-list-selected-wiki-id');
    //   // this.getNextPage()
    // }
  }

  private filterWiki() {
    if (!this.wikiList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredWikiList.next(this.wikiList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredWikiList.next(
      this.wikiList.filter(wiki => wiki.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }


  clearSelectOption(valStr, dept) {
    // 최대 길이 설정
    let maxLength = 0;
    if (valStr === 'selectedContextCate') {
      maxLength = 4;
    } else if (valStr === 'selectedQuestionCate') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      eval('this.' + valStr + i + ' = this.initCate');
    }
  }

  initializeWebSocketConnection() {
    let serverUrl = this.storage.get('mltApiUrl') + '/socket';
    let ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;

    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/api/mlt/training-data/nlp-analyze/status', (message) => {
        if (message.body === 'error') {
          that.openAlertDialog('Error', 'Failed to analyze', 'error');
          that.analyzingStatus = false;
        }
        that.analyzingStatus = message.body;
      })
    })
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.route.queryParams.subscribe(
      query => {
        if (query['wikiId'] !== undefined && query['wikiId'] !== null) {
          this.selectedWiki = query['wikiId'] * 1;
          this.cdr.detectChanges();
          this.getNextPage();
        }
      });
  }

  ngOnDestroy() {
    this.stompClient.unsubscribe();
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * Search Button 정의
   * @param  pageIndex
   */
  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      this.contextParam.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.contextParam.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    if (this.selectedWiki === 0) {
      this.wikiActionsArray[1].hidden = true;
      this.wikiActionsArray[1].disabled = true;
      return false;
    }
    this.wikiActionsArray[1].hidden = false;
    this.wikiActionsArray[1].disabled = false;

    if (this.selectedContextCate1['id'] !== undefined) {
      if (this.selectedContextCate2['id'] === undefined) {
        this.contextParam.searchCatId = this.selectedContextCate1['id'];
      } else {
        if (this.selectedContextCate3['id'] === undefined) {
          this.contextParam.searchCatId = this.selectedContextCate2['id'];
        } else {
          if (this.selectedContextCate4['id'] === undefined) {
            this.contextParam.searchCatId = this.selectedContextCate3['id'];
          } else {
            this.contextParam.searchCatId = this.selectedContextCate4['id'];
          }
        }
      }
    }
    this.contextParam.season = this.searchSeason;
    this.contextParam.title = this.searchTitle;
    this.contextParam.context = this.searchContext;
    this.contextParam.searchQuestion = this.searchQuestion;
    this.contextParam.pageSize = this.pageParam.pageSize;
    this.contextParam.pageIndex = this.pageParam.pageIndex;
    this.contextParam.wikiId = this.selectedWiki;

    this.trainingDataService.getContextList(this.contextParam).subscribe(res => {
      if (res) {
        this.contextList = res.content;
        this.pageLength = res.totalElements;
        this.dataSource = new MatTableDataSource(this.contextList);
        this.lineTableComponent.isCheckedAll = false;
      }
    });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.contextList.length > 0) {
      this.getNextPage();
    }
  }

  getWikiList() {
    this.trainingDataService.getWikiListByWorkspaceId(this.workspaceId).subscribe(res => {
      this.wikiList = res;
      this.filteredWikiList.next(this.wikiList.slice());
    }, err => {
      this.openAlertDialog('Error', 'Failed to get wiki list.', 'error');
    })
  }

  popWikiUpsertDialog = (type, row?) => {
    let paramData = {};
    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.getSelectedWikiEntity();
    } else {
      return false;
    }

    paramData['service'] = this.trainingDataService;

    let dialogRef = this.dialog.open(WikiUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getWikiList();
      if (result === 'delete') {
        this.selectedWiki = 0;
        this.wikiActionsArray[1].hidden = true;
        this.wikiActionsArray[1].disabled = true;
      } else {
        // this.getNextPage();
      }
    });
  }

  getCategoryList() {
    return new Promise(resolve => {
      this.trainingDataService.getCategoryList(this.workspaceId).subscribe(res => {
          if (res) {
            this.contextCate = res.context
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
        }
      );
    });
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addTrainingData = ($evnet) => {
    const wiki = this.getSelectedWikiEntity();
    let param = `wikiId=${encodeURI(wiki.id.toString())}`;
    if (wiki) {
      this.router.navigateByUrl('mlt/mrc/training-data-upsert?' + param);
    }
  }

  editTrainingData = (row) => {
    const wiki = this.getSelectedWikiEntity();
    if (wiki) {
      let param = `wikiId=${encodeURI(wiki.id.toString())}&contextId=${encodeURI(row.id.toString())}`;
      this.router.navigateByUrl('mlt/mrc/training-data-upsert?' + param);
    }
  }

  removeTrainingData = () => {
    let toBeRemoved = [];
    this.contextList.forEach(context => {
      if (context.isChecked) {
        toBeRemoved.push(context);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please Select Data to Removed', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.lineTableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.lineTableComponent.isCheckedAll = false;
        this.trainingDataService.removeContext(toBeRemoved).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  }

  removeTrainingDataAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove All Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;

        this.pageParam.pageIndex = 0;

        this.lineTableComponent.isCheckedAll = false;

        this.trainingDataService.removeContextAll(this.selectedWiki).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  }

  getSelectedWikiEntity() {
    let result = new WikiEntity();
    this.wikiList.forEach(item => {
      if (item.id === this.selectedWiki) {
        result = item;
      }
    });
    return result;
  }


  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  // 업로드 이후 호출되는 함수
  onUploadFile = () => {
    this.getNextPage();
  };

  // 다운로드 함수
  downloadFile = () => {
    let url = this.storage.get('mltApiUrl') + '/mrc/training-data/' + this.selectedWiki + '/' + this.workspaceId + '/download-file';
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  nlpAnalyze(flag?) {
    if (flag === 'all') {
      this.trainingDataService.analyzeNlpAll(this.selectedWiki).subscribe(res => {
          this.openAlertDialog('Success', `NLP Analyze Request Success`, 'success');
          this.getNextPage();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Analyze failed.', 'error');
        });
    } else {
      let checkedList = [];
      this.contextList.forEach(context => {
        if (context.isChecked) {
          checkedList.push(context);
        }
      });
      if (checkedList.length === 0) {
        this.openAlertDialog('Error', 'Please Select Data to Analyze', 'error');
        return false;
      }
      this.trainingDataService.analyzeNlp(checkedList).subscribe(res => {
          this.openAlertDialog('Success', `NLP Analyze Request Success`, 'success');
          this.getNextPage();
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Analyze failed.', 'error');
        });
    }
  }

  detail = (row) => {
    let param = `wikiId=${encodeURI(this.selectedWiki.toString())}&contextId=${encodeURI(row.id.toString())}`;
    this.router.navigateByUrl('mlt/mrc/training-data-detail?' + param);
  }
}
