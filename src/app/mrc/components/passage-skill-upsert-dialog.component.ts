import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {AlertComponent, ConfirmComponent, StorageBrowser} from 'mlt-shared';
import {WikiEntity} from '../entity/wiki.entity';


@Component({
  selector: 'app-mrc-passage-skill-upsert-dialog',
  templateUrl: 'passage-skill-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class PassageSkillUpsertDialogComponent implements OnInit {

  param: any;
  service: any;
  role: string;

  name: string;
  item: WikiEntity = new WikiEntity();
  nameValidator = new FormControl('', [Validators.required, this.noWhitespaceValidator]);
  disabled = true;
  data: any


  constructor(public dialogRef: MatDialogRef<PassageSkillUpsertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;

    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.service.getSkillById(this.data.row).subscribe(res => {
        this.item = res;
      });
    }
  }

  addItem = () => {
    this.item.workspaceId = this.storage.get('workspaceId');
    this.item.name = this.item.name.trim();
    this.service.addSkill(this.item).subscribe(res => {
        if (res) {
          this.dialogRef.close(true);
          this.openAlertDialog('Success', 'Success Add Skill', 'success');
        }
      },
      err => {
        let msg = 'Failed Add Skill';
        if (err.status === 409) {
          msg = err.error;
        }
        this.openAlertDialog('Error', msg, 'error');
        console.log('error', err);
      }
    );
  }

  updateItem = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Skill?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.item.workspaceId = this.storage.get('workspaceId');
        this.item.name = this.item.name.trim();
        this.service.editSkill(this.item).subscribe(res => {
            if (res) {
              this.dialogRef.close(true);
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Edit Skill', 'error');
            console.log('error', err);
          }
        );
      }
    });
  }

  removeItem = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Skill? All related Passage will be removed';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.item.workspaceId = this.storage.get('workspaceId');
        this.service.removeSkill(this.item).subscribe(res => {
            this.dialogRef.close('delete');
          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Skill', 'error');
            console.log('error', err);
          }
        );
      }
    })
  }

  checkInvalid($event) {
    if ('INVALID' === this.nameValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.nameValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  noWhitespaceValidator(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
}
