import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';

import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MrcDataGroupService} from '../services/mrc.data-group.service';
import {AlertComponent, ConfirmComponent, StorageBrowser, TableComponent} from 'mlt-shared';
import {FormControl, Validators} from '@angular/forms';
import {DataGroupEntity} from '../entity/data-group.entity';
import {ContextEntity} from '../entity/context.entity';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-data-group',
  templateUrl: './data-group.component.html',
  styleUrls: ['./data-group.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DataGroupComponent implements OnInit, AfterViewInit {
  // default
  workspaceId;

  // group
  groups: any[] = [];
  groupName = '';
  selectedGroup: any = null;
  focusedChecked = false;
  addRoleInput = new FormControl('', [Validators.required]);
  matSort: MatSort = new MatSort();

  // category
  dataTypeCate = [];
  selectedDataType1 = {children: []};
  selectedDataType2 = {children: []};
  contextCate = [];
  selectedContextCate1 = {children: []};
  selectedContextCate2 = {children: []};
  selectedContextCate3 = {children: []};
  selectedContextCate4 = {children: []};
  search = '';
  tempCate = {children: []};

  // wiki
  selectedWiki;
  wikiList: any[] = [];

  // corpus
  corpusList: any[] = [];
  contextSource: MatTableDataSource<any>;
  contextTotalCount: number;
  contextPage: MatPaginator;
  @ViewChild('corpusTableComponent') corpusTableComponent: TableComponent;

  header = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No', no: true},
    {attr: 'title', name: 'Title', width: '40%', isSort: true},
    {attr: 'contextCategory.catPath', name: 'Source', isSort: true, width: '20%'},
    {attr: 'contextCategory.catName', name: 'Category', isSort: true, width: '12%'},
    {attr: 'createdAt', name: 'CreatedAt', format: 'date', width: '12%', isSort: true},
  ];

  param = new DataGroupEntity();
  searchSeason: number;
  searchTitle: string = '';
  searchContext: string = '';
  searchQuestion: string = '';

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredWikiList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);


  constructor(private mrcDataGroupService: MrcDataGroupService,
              private dialog: MatDialog,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.getDataGroup().then(res => {
      if (this.groups.length !== 0) {
        this.selectedGroup = this.groups[0];
        this.groups[0]['selected'] = true;
        this.getCorpusList();
      }
    });
    this.getWikiList()
    this.getCtegory();
    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterWiki();
      });
  }

  ngAfterViewInit() {

  }

  private filterWiki() {
    if (!this.wikiList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredWikiList.next(this.wikiList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredWikiList.next(
      this.wikiList.filter(wiki => wiki.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  getWikiList() {
    this.mrcDataGroupService.getWikiListByWorkspaceId(this.workspaceId).subscribe(res => {
      this.wikiList = res;
      this.filteredWikiList.next(this.wikiList.slice());

    }, err => {
      this.openAlertDialog('Error', 'Failed to get wiki list.', 'error');
    })
  }

  getDataGroup() {
    return new Promise(resolve => {
      this.mrcDataGroupService.getGroupList(this.workspaceId).subscribe(res => {
          if (res) {
            this.groups = res;
            if (this.selectedGroup != null) {
              this.groups.forEach(item => {
                if (item.id === this.selectedGroup.id) {
                  item.selected = true;
                }
              });
            }
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get DataGroups', 'error');
          console.log('error', err);
        }
      );
    });
  }

  getCtegory() {
    return new Promise(resolve => {
      this.mrcDataGroupService.getCategory(this.workspaceId).subscribe(res => {
          if (res) {
            this.dataTypeCate = res.dataType;
            this.contextCate = res.context
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
          console.log('error', err);
        }
      );
    });
  }

  getCorpusList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.contextPage.pageIndex = pageIndex;
    }

    let contextEntityParam = new ContextEntity();
    contextEntityParam.workspaceId = this.workspaceId;
    contextEntityParam.season = this.searchSeason;
    contextEntityParam.title = this.searchTitle;
    contextEntityParam.context = this.searchContext;
    contextEntityParam.searchQuestion = this.searchQuestion;
    contextEntityParam.pageIndex = this.contextPage.pageIndex;
    contextEntityParam.pageSize = this.contextPage.pageSize;

    if (this.selectedContextCate1['id'] !== undefined) {
      if (this.selectedContextCate2['id'] === undefined) {
        contextEntityParam.searchCatId = this.selectedContextCate1['id'];
      } else {
        if (this.selectedContextCate3['id'] === undefined) {
          contextEntityParam.searchCatId = this.selectedContextCate2['id'];
        } else {
          if (this.selectedContextCate4['id'] === undefined) {
            contextEntityParam.searchCatId = this.selectedContextCate3['id'];
          } else {
            contextEntityParam.searchCatId = this.selectedContextCate4['id'];
          }
        }
      }
    }
    if (this.selectedWiki !== undefined) {
      contextEntityParam.wikiId = this.selectedWiki;
    }
    contextEntityParam.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    contextEntityParam.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.mrcDataGroupService.getContextList(contextEntityParam).subscribe(res => {
        if (res) {
          this.corpusList = res['content'];
          this.contextTotalCount = res['totalElements'];

          this.corpusList.forEach(item => {

            if (typeof(item.contextCategory) !== 'undefined') {
              item['contextCategoryPath'] = item.contextCategory.catPath;
            }
          });

          this.setIsChecked();
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get ContextList', 'error');
        console.log('error', err);
      }
    );
  }

  addGroup() {
    if (this.groupName === '') {
      this.addRoleInput.setErrors({'required': true});
    } else {
      this.param = new DataGroupEntity();
      this.param.workspaceId = this.storage.get('workspaceId');
      this.param.dataGroupName = this.groupName;

      this.mrcDataGroupService.addGroup(this.param).subscribe(res => {
        this.groupName = '';
        this.getDataGroup();
        this.openAlertDialog('Success', 'Success Add Group', 'success');
      }, err => {
        if (err.status === '409') {
          this.addRoleInput.setErrors({
            'duplicate': true
          });
        } else {
          this.openAlertDialog('Failed', 'Failed Insert Group', 'error');
          console.log('error', err);
        }
      });
    }
  }

  editGroup() {
    this.param = new DataGroupEntity();
    this.param.id = this.selectedGroup.id;
    this.param.dataGroupName = this.groupName;

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Group?';
    ref.afterClosed().subscribe(result => {
      if (result) {;
        this.mrcDataGroupService.editGroup(this.param).subscribe(res => {
          this.openAlertDialog('Success', 'Success Edit Group', 'success');
          this.getDataGroup();
        }, err => {
          if (err.status === '409') {
            this.addRoleInput.setErrors({
              'duplicate': true
            });
          } else {
            this.openAlertDialog('Failed', 'Failed Edit Group', 'error');
            console.log('error', err);
          }
        });
      }
    });
  }

  deleteGroup(group) {
    this.param = new DataGroupEntity();
    this.param.dataGroupId = group.id;

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to delete Group?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.mrcDataGroupService.removeGroup(this.param).subscribe(res => {
          this.getDataGroup().then(res => {
            if (this.groups.length !== 0) {
              this.selectedGroup = this.groups[0].id;
              this.groups[0]['selected'] = true;
              this.getCorpusList();
            }
          });
        }, err => {
          this.openAlertDialog('Failed', 'Failed Delete Group', 'error');
          console.log('error', err);
        });
      }
    });
  }

  addListToGroup() {
    let paramArr = [];
    const rows = this.corpusTableComponent.rows;

    if (this.selectedGroup === null) {
      this.openAlertDialog('Warning', 'DataGroup has to be checked', 'notice');
    } else if (rows.length < 1) {
      this.openAlertDialog('Warning', 'Corpus has to be checked', 'notice');
    } else {
      rows.forEach(item => {
        let tempEntity = new DataGroupEntity();
        tempEntity.contextId = item.id;
        tempEntity.dataGroupId = this.selectedGroup.id;
        tempEntity.checked = typeof(item.isChecked) === 'undefined' || item.isChecked === false ? false : true;
        paramArr.push(tempEntity);
      });

      this.mrcDataGroupService.insertDataGroupRel(paramArr).subscribe(res => {
          this.openAlertDialog('Success', 'Success Add List', 'success');
          this.corpusTableComponent.isCheckedAll = false;
          this.getCorpusList();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Apply CorpusList', 'error');
        });
    }
  }

  getErrorMessage() {
    return this.addRoleInput.hasError('required') ? 'You must enter a value' :
      this.addRoleInput.hasError('duplicate') ? 'Role already exist' :
        '';
  }

  clickGroup(group) {
    if (group !== null) {
      if (group.selected) {
        this.corpusTableComponent.isCheckedAll = false;
        this.groups.forEach((item) => {
          if (item.id === group.id) {
            item['selected'] = false;
            this.selectedGroup = null;
            this.focusedChecked = false;
            this.groupName = '';
          }
        });
      } else {
        this.groups.forEach((item) => {
          if (item.id === group.id) {
            item['selected'] = true;
            this.selectedGroup = item;
            this.focusedChecked = true;
            this.groupName = item.dataGroupName;
          } else {
            item['selected'] = false;
          }
        });
      }

      this.getCorpusList();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  setDataPage = (page: MatPaginator) => {
    this.contextPage = page;
    this.getCorpusList();
  }

  getSelectedDataTypeId() {
    if (typeof(this.selectedDataType2['catPath']) !== 'undefined') {
      return this.selectedDataType2['catPath'];
    } else if (this.selectedDataType1.children.length > 0) {
      return this.selectedDataType1['catPath'];
    } else {
      return '';
    }
  }

  getSelectedCategoryId() {
    if (typeof(this.selectedContextCate4['catPath']) !== 'undefined') {
      return this.selectedContextCate4['catPath'];
    } else if (this.selectedContextCate3.children.length > 0) {
      return this.selectedContextCate3['catPath'];
    } else if (this.selectedContextCate2.children.length > 0) {
      return this.selectedContextCate2['catPath'];
    } else if (this.selectedContextCate1.children.length > 0) {
      return this.selectedContextCate1['catPath'];
    } else {
      return '';
    }
  }

  clearSelectOption(valStr, dept) {
    let maxLength = 0;
    if (valStr === 'selectedDataType') {
      maxLength = 2;
    } else if (valStr === 'selectedContextCate') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      eval('this.' + valStr + i + ' = this.tempCate');
    }
  }

  setIsChecked() {
    if (this.selectedGroup === null || this.corpusList.length < 1) {
      return false;
    } else {

      this.corpusList.forEach(corpus => {
        let exist = false;
        corpus.dataGroupRelEntities.forEach(entity => {
          if (this.selectedGroup.id === entity.dataGroupId) {
            exist = true;
            return false;
          }
        });

        if (exist) {
          corpus.isChecked = true;
        }
      });
    }
  }

  orderByHeader(matSort: MatSort) {
    this.matSort = matSort;
    if (this.corpusList.length > 0) {
      this.getCorpusList();
    }
  }
}
