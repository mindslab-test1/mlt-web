import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UserEntity} from '../entity/user.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class ProfileService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  updatePassword(userEntity: UserEntity): Observable<any> {
    return this.http.post(this.API_URL + '/user/updatePassword', userEntity);
  }

}
