import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HistoryEntity} from '../entity/history.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class HistoryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getAllHistories(historyEntity: HistoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/management/histories/getAllHistories', historyEntity);
  }

  getTotalHistories(historyEntity: HistoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/management/histories/getTotalHistories', historyEntity);
  }

}
