import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UserEntity} from '../entity/user.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class UsersUpsertService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  checkIdExist(id: String): Observable<any> {
    return this.http.get(this.API_URL + '/user/exist/' + id);
  }

  getRoleListWithWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/role/workspaceId/' + workspaceId + '/list');
  }

  getUserById(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/user/id/' + id);
  }

  insertUser(userEntity: UserEntity): Observable<any> {
    return this.http.post(this.API_URL + '/user/addUser', userEntity);
  }

  updateUser(userEntity: UserEntity): Observable<any> {
    return this.http.post(this.API_URL + '/user/updateUser', userEntity);
  }

}
