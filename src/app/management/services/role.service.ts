import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MenuRoleEntity} from '../entity/menuRole.entity';
import {RoleEntity} from '../entity/role.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class RoleService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getRoleListWithWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/role/workspaceId/' + workspaceId + '/list');
  }

  getMenuList(): Observable<any> {
    return this.http.get(this.API_URL + '/menu/list');
  }

  getMenuIdByRoleId(roleId: string): Observable<any> {
    return this.http.get(this.API_URL + '/menu/menuId/' + roleId);
  }

  getRoleRelList(): Observable<any> {
    return this.http.get(this.API_URL + '/user/roleRel/list');
  }

  insertRole(role: RoleEntity): Observable<any> {
    return this.http.post(this.API_URL + '/role/addRole', role);
  }

  insertMenuRoles(menuRoleEntity: MenuRoleEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/menu/addMenuRoles', menuRoleEntity);
  }

  deleteRole(role: RoleEntity): Observable<any> {
    return this.http.post(this.API_URL + '/role/deleteRole', role);
  }

  deleteMenuRoles(menuRoleEntity: MenuRoleEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/menu/deleteMenuRoles', menuRoleEntity);
  }
}
