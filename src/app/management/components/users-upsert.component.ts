import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';
import * as CryptoJS from 'crypto-js';

import {StorageBrowser} from 'mlt-shared';
import {UserEntity} from '../entity/user.entity';
import {UsersUpsertService} from '../services/users-upsert.service';
import {ConfirmComponent} from 'mlt-shared';
import {MatDialog} from '@angular/material';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-users-upsert',
  templateUrl: './users-upsert.component.html',
  styleUrls: ['./users-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class UsersUpsertComponent implements OnInit, AfterViewInit {
  role: string;
  user: UserEntity = new UserEntity();
  roleList = [];
  addIdInput = new FormControl('');

  workspace = null;
  selectedId = null;

  constructor(private usersUpsertService: UsersUpsertService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private datePipe: DatePipe,
              private storageBrowser: StorageBrowser,
              private router: Router) {
  }

  ngOnInit() {
    this.workspace = this.storageBrowser.get('workspace');
    this.selectedId = this.workspace[0].id;

    const id = this.storageBrowser.get('management-user-upsert-id');
    if (id !== null) {
      // edit
      this.role = 'edit';
      this.storageBrowser.remove('management-user-upsert-id');
      this.usersUpsertService.getUserById(id).subscribe(result => {
        result.roleId = undefined;

        result.userRoleRelEntities.forEach(userRoleRelEntity => {
          if (userRoleRelEntity.roleEntity.workspaceId === this.selectedId) {
            result.roleId = userRoleRelEntity.roleId;
            result.pastRoleId = userRoleRelEntity.roleId;
          }
        });
        this.user = result;
        this.user.password = null;
      });
    } else {
      this.role = 'add';
    }
    this.usersUpsertService.getRoleListWithWorkspaceId(this.selectedId).subscribe(result => {
      this.roleList = result;
    });
  }

  ngAfterViewInit() {
  }

  onSubmit() {
    let ref = this.dialog.open(ConfirmComponent);
    if (this.role === 'add') {
      ref.componentInstance.message = `Do you want to Add?`;
    } else {
      ref.componentInstance.message = `Do you want to Edit?`;
    }
    ref.afterClosed().subscribe(result => {
      if (result) {
        let data = `!mlt500#00!00429`;
        const key = CryptoJS.enc.Utf8.parse(data);
        const iv = CryptoJS.enc.Utf8.parse(data);
        const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(this.user.password), key,
          {
            keySize: 256 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
          }
        );
        this.user.password = encrypted.toString();
        // Password Encrypt End

        if (this.role === 'add') {
          this.usersUpsertService.insertUser(this.user).subscribe(result2 => {
              if (result2) {
                this.router.navigate(['../users'], {relativeTo: this.route});
              }
            },
            err => {
              if (err.status === '409') {
                this.addIdInput.setErrors({
                  'duplicate': true
                });
              }
            }
          );
        } else if (this.role === 'edit') {
          this.usersUpsertService.updateUser(this.user).subscribe(result3 => {
              if (result3) {
                this.router.navigate(['../users'], {relativeTo: this.route});
              }
            },
            err => {
              console.log('error', err);
            }
          );
        }
      }
    });

  }

  goBack() {
    this.router.navigate(['../users'], {relativeTo: this.route});
  }

  getErrorMessage() {
    return this.addIdInput.hasError('duplicate') ? 'Id Already Exist' : '';
  }

  checkIdExist() {
    this.usersUpsertService.checkIdExist(this.user.id).subscribe(result => {
        if (result) {
          this.addIdInput.setErrors({
            'duplicate': true
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}
