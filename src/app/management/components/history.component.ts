import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StorageBrowser} from 'mlt-shared';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AlertComponent} from  'mlt-shared';
import {HistoryEntity} from '../entity/history.entity';
import {HistoryService} from '../services/history.service';
import {DownloadService} from  'mlt-shared';
import {TableComponent} from 'mlt-shared';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})

export class HistoryComponent implements OnInit, AfterViewInit {

  header: any[] = [];
  actions: any;
  histories: HistoryEntity[] = [];
  dataSource: MatTableDataSource<any>;
  pageLength: number;

  inputSearch: string;

  pageParam: MatPaginator;
  sortParam: MatSort;
  @ViewChild('tableComponent') tableComponent: TableComponent;

  constructor(private storage: StorageBrowser,
              public dialog: MatDialog,
              private historyService: HistoryService,
              private downloadService: DownloadService) {

  }

  ngOnInit() {
    this.header = [
      {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      {attr: 'no', name: 'No', no: true},
      {attr: 'workspaceEntity.name', name: 'Workspace', isSort: true},
      {attr: 'code', name: 'Process', isSort: true},
      {attr: 'message', name: 'Message'},
      {attr: 'startedAt', name: 'Started at', format: 'date', isSort: true},
      {attr: 'endedAt', name: 'Ended at', format: 'date', isSort: true},
      {attr: 'creatorId', name: 'User', isSort: true},
    ];

    this.actions = [
      {
        text: 'Download',
        icon: 'file_download',
        callback: this.download,
        disabled: false,
      },
      {
        text: 'Download Checked',
        icon: 'check_box',
        callback: this.downloadChecked,
        disabled: true,
      }
    ];
  }

  ngAfterViewInit() {
    this.getAllHistories();
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  isChecked(check?: any) {
    if (check) {
      this.actions[1].disabled = false;
    } else {
      this.actions[1].disabled = true;
    }
  }

  getClassName(action) {
    return action.disabled ? 'mlt-disabled' : 'mlt-not-disabled';
  }

  getAllHistories(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    let param = new HistoryEntity();
    param.pageIndex = this.pageParam.pageIndex;
    param.pageSize = this.pageParam.pageSize;
    param.workspaceId = this.storage.get('workspaceId');
    param.code = this.inputSearch;
    param.orderDirection = this.sortParam.direction === '' ||
    this.sortParam.direction === undefined ? 'desc' : this.sortParam.direction;
    param.orderProperty = this.sortParam.active === '' ||
    this.sortParam.active === undefined ? 'updatedAt' : this.sortParam.active;

    this.historyService.getAllHistories(param).subscribe(
      res => {
        if (res) {
          this.pageLength = res['totalElements'];
          this.histories = res['content'];
          this.tableComponent.isCheckedAll = false;
        }
      }, err => {
        this.openAlertDialog('Failed', 'Server error. failed fetch to histories.', 'error');
      }
    );
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.histories.length > 0) {
      this.getAllHistories();
    }
  }

  setSort(sort?: MatSort) {
    this.sortParam = sort;

    if (this.histories.length > 0) {
      this.getAllHistories();
    }
  }

  download = () => {
    let contents: HistoryEntity[] = [];
    let headers = this.header.filter(item => !item.checkbox && !item.no);
    let param = new HistoryEntity();
    param.workspaceId = this.storage.get('workspaceId');

    this.historyService.getTotalHistories(param).subscribe(
      res => {
        if (res) {
          contents = res;
          this.downloadService.downloadCsvFile(contents, headers, 'histories');
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Server error. failed fetch to histories.', 'error');
      }
    );
  };

  downloadChecked = () => {
    let contents = this.tableComponent.rows.filter(row => row.isChecked);
    let headers = this.header.filter(item => !item.checkbox && !item.no);

    if (contents.length !== 0) {
      this.downloadService.downloadCsvFile(contents, headers, 'histories');
    } else {
      this.openAlertDialog('Failed', 'It was not checked.', 'error');
    }
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
