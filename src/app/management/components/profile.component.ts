import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import * as CryptoJS from 'crypto-js';

import {StorageBrowser} from 'mlt-shared';
import {UserEntity} from '../entity/user.entity';
import {UsersUpsertService} from '../services/users-upsert.service';
import {DatePipe} from '@angular/common';
import {ProfileService} from '../services/profile.service';
import {MatDialog} from '@angular/material';
import {ConfirmComponent} from 'mlt-shared';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None,
})

export class ProfileComponent implements OnInit {
  user: UserEntity = new UserEntity();
  createdAt: string;
  roleName: string;
  currentPassword: string;
  changedPassword: string;
  changedPasswordConfirm: string;

  currentPasswordForm = new FormControl('', Validators.required);
  changedPasswordForm = new FormControl('', Validators.required);
  passwordConfirm = new FormControl('');

  workspace = null;
  selectedId = null;

  constructor(private usersUpsertService: UsersUpsertService,
              private profileService: ProfileService,
              private datePipe: DatePipe,
              private dialog: MatDialog,
              private storageBrowser: StorageBrowser,
              private _location: Location) {
  }

  ngOnInit() {
    this.workspace = this.storageBrowser.get('workspace');
    this.selectedId = this.workspace[0].id;
    const id = this.storageBrowser.get('user').id;
    this.usersUpsertService.getUserById(id).subscribe(result => {
      const date = new Date(result.createdAt);
      this.createdAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
      result.userRoleRelEntities.forEach(userRoleRelEntity => {
        if (userRoleRelEntity.roleEntity.workspaceId === this.selectedId) {
          this.roleName = userRoleRelEntity.roleEntity.roleName;
        }
      });
      this.user = result;
    });
  }

  onSubmit() {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Do you want to Update?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        let data = `!mlt500#00!00429`;

        const key = CryptoJS.enc.Utf8.parse(data);
        const iv = CryptoJS.enc.Utf8.parse(data);
        const currentPasswordEncrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(this.currentPassword), key,
          {
            keySize: 256 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
          }
        );
        const changedPasswordEncrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(this.changedPassword), key,
          {
            keySize: 256 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
          }
        );
        this.user.currentPassword = currentPasswordEncrypted.toString();
        this.user.password = changedPasswordEncrypted.toString();
        // Password Encrypt End

        this.profileService.updatePassword(this.user).subscribe(result2 => {
          this._location.back();
        }, err => {
          if (err.status === 401) {
            this.currentPasswordForm.setErrors({
              'wrongPassword': true
            });
          }
        });
      }
    });
  }

  checkPassword() {
    if (this.changedPassword !== this.changedPasswordConfirm) {
      this.passwordConfirm.setErrors({
        'passwordConfirm': true
      });
    }
  }

  getCurrentPasswordErrorMessage() {
    return this.currentPasswordForm.hasError('wrongPassword') ? 'Wrong Password.' : '';
  }

  getPasswordConfirmErrorMessage() {
    return this.passwordConfirm.hasError('passwordConfirm') ? 'Confirmation does not match new password.' : '';
  }

  goBack() {
    this._location.back();
  }
}
