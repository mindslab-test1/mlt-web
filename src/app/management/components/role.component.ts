import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';

import {RoleService} from '../services/role.service';
import {MenuRoleEntity} from '../entity/menuRole.entity';
import {AlertComponent, ConfirmComponent, StorageBrowser} from 'mlt-shared';
import {RoleEntity} from '../entity/role.entity';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
  providers: [DatePipe]
})

export class RoleComponent implements OnInit, AfterViewInit {
  roles: any[] = [];
  role: string;
  isCheckedAll: boolean;
  selectedRole: string;
  // 테이블
  header = [
    {attr: 'checkbox', name: ''},
    {attr: 'path', name: 'path'},
  ];
  menus: any[] = [];
  menus_init: any[] = [];
  dataSource: MatTableDataSource<any>;
  addRoleInput = new FormControl('', [Validators.required]);

  workspace = null;
  selectedId = null;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private roleService: RoleService,
              private dialog: MatDialog,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspace = this.storage.get('workspace');
    this.selectedId = this.workspace[0].id;

    this.getRoleList().then(() => {
      this.getMenuIdByRoleId(this.roles[0].id);
      this.selectedRole = this.roles[0].id;
    });
  }

  ngAfterViewInit() {
  }

  getRoleList() {
    return new Promise(resolve => {
      this.roleService.getRoleListWithWorkspaceId(this.selectedId).subscribe(roles => {
        this.roles = roles;
        this.roleService.getRoleRelList().subscribe(roleRels => {
          this.roles.forEach(role => {
            role['userCount'] = 0;
            roleRels.forEach(roleRel => {
              if (roleRel.roleId === role.id) {
                role.userCount++;
              }
            });
          });
          resolve();
        });
      });
    });
  }

  getMenuIdByRoleId(roleId: string) {
    this.selectedRole = roleId;
    this.roleService.getMenuList().subscribe(menus => {
      this.menus = menus;
      this.roleService.getMenuIdByRoleId(roleId).subscribe(menuRoles => {
        const arr = [];
        menuRoles.forEach(val => {
          arr.push(val.menuId);
        });
        this.menus.forEach(val => {
          val['isChecked'] = arr.includes(val.id);
        });

        this.menus_init = JSON.parse(JSON.stringify(this.menus));
        this.dataSource = new MatTableDataSource(this.menus);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
  }

  addRole() {
    if (this.role === undefined || this.role.trim() === '') {
      this.addRoleInput.setErrors({
        'required': true
      });
    } else {
      const role: RoleEntity = {
        id: null,
        roleName: this.role,
        workspaceId: this.selectedId
      };
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = `Do you want to Add Role?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          this.roleService.insertRole(role).subscribe(result2 => {
              this.getRoleList().then(() => {
                this.getMenuIdByRoleId(result.id);
                this.selectedRole = result.id;
              });
            },
            err => {
              if (err.status === '409') {
                this.addRoleInput.setErrors({
                  'duplicate': true
                });
              }
            }
          );
        }
      });
    }
  }

  updateMenuRole() {
    const toBeDeleted = [];
    const toBeAdded = [];
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Do you want to Save Menu?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        for (let i = 0; i < this.menus.length; i++) {
          if (this.menus[i].isChecked !== this.menus_init[i].isChecked) { // isChecked 값이 변경되었을 경우
            const menuRole: MenuRoleEntity = {
              menuId: this.menus[i].id,
              roleId: this.selectedRole
            };
            if (this.menus[i].isChecked) { // 추가
              toBeAdded.push(menuRole);
            } else if (!this.menus[i].isChecked) { // 삭제
              toBeDeleted.push(menuRole);
            }
          }
        }

        if (toBeAdded.length > 0) {
          this.roleService.insertMenuRoles(toBeAdded).subscribe(
            res => {
              this.openAlertDialog('Success', 'Menu role update success.', 'success');
            }, err => {
              this.openAlertDialog('Failed', 'Server error. Menu role update failed.', 'error');
            });
        }
        if (toBeDeleted.length > 0) {
          this.roleService.deleteMenuRoles(toBeDeleted).subscribe(
            res => {
              this.openAlertDialog('Success', 'Menu role update success.', 'success');
            },
            err => {
              this.openAlertDialog('Failed', 'Server error. Menu role update failed.', 'error');
            }
          );
        }
      }
    });

  }

  deleteRole(val) {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Do you want to Delete Role?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        const role: RoleEntity = {
          id: val.id,
          roleName: val.roleName,
          workspaceId: this.selectedId
        };
        this.roleService.deleteRole(role).subscribe(result2 => {
          this.getRoleList().then(() => {
            this.getMenuIdByRoleId(this.roles[0].id);
            this.selectedRole = this.roles[0].id;
          });
        });
      }
    });

  }

  onClickCheckAll() {
    /* 페이징 적용했을 때 사용하는 부분 */

    const pageIndex: number = this.paginator.pageIndex;
    const pageSize: number = this.paginator.pageSize;

    const start: number = pageIndex * pageSize;
    let end: number = (pageIndex + 1) * pageSize - 1;

    if (this.paginator.length <= end) {
      end = this.paginator.length - 1;
    }

    /*공통 적용부분*/
    for (let i = start; i < end + 1; i++) {
      if (this.menus[i].isChecked !== this.isCheckedAll) {
        // this.updateMenuRole(this.menus[i]);
        this.menus[i].isChecked = this.isCheckedAll;
      }
    }
  }

  getColumn(header) {
    const arr: any[] = [];
    header.filter(col => col.attr).forEach((col, idx) => {
      arr.push(col.attr);
    });
    return arr;
  }

  getErrorMessage() {
    return this.addRoleInput.hasError('required') ? 'You must enter a value' :
      this.addRoleInput.hasError('duplicate') ? 'Role already exist' :
        '';
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}

