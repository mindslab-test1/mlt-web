import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';

import {UsersService} from '../services/users.service';
import {StorageBrowser} from  'mlt-shared';
import {TableComponent} from 'mlt-shared';
import {AlertComponent} from  'mlt-shared';
import {ConfirmComponent} from 'mlt-shared';
import {PageParameters} from 'mlt-shared';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [DatePipe]
})

export class UsersComponent implements OnInit {
  actionsArray: any[] = [];
  users: any[] = [];
  isCheckedAll: boolean;
  // 테이블
  header = [
    {attr: 'checkbox', name: 'CheckBox', type: 'text', checkbox: true, input: false},
    {attr: 'id', name: 'Id'},
    {attr: 'username', name: 'Username'},
    {attr: 'email', name: 'Email'},
    {attr: 'activatedStatus', name: 'activated', width: '15%'},
    {
      attr: 'createdAt',
      name: 'createdAt',
      type: 'text',
      format: 'date',
      checkbox: false,
      input: false,
      width: '15%'
    },
    {attr: 'edit', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '15%'},
  ];
  pageParam: PageParameters = new PageParameters();
  dataSource: MatTableDataSource<any>;
  keyword: string;

  pageLength;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  //TEST
  @ViewChild('userListTableComponent') userListTableComponent: TableComponent;

  constructor(private router: Router,
              private datePipe: DatePipe,
              private dialog: MatDialog,
              private storageBrowser: StorageBrowser,
              private userService: UsersService) {
  }

  ngOnInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.getList();

    this.actionsArray = [
      {text: 'Add', icon: 'add_circle_outline', hidden: false, disabled: false, type: 'add'},
      {
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        type: 'delete'
      }
    ];
  }

  doAction(type) {
    if (type === 'add') {
      this.addUser();
    } else if (type === 'delete') {
      this.deleteUser();
    }
  }

  getList(event?) {
    this.userService.getUserList(this.pageParam.pageSize, this.pageParam.pageIndex, this.keyword).subscribe(result => {
      result.content.map(val => {
        if (val.activated == 0) {
          val['activatedStatus'] = 'deactivated';
        } else {
          val['activatedStatus'] = 'activated';
        }
        val['roleName'] = null;
        val['isChecked'] = false;
      });
      this.users = result.content;
      this.pageLength = result.totalElements;
      this.dataSource = new MatTableDataSource(this.users);
    });
  }

  setPage(page?: MatPaginator) {
    this.pageParam.pageSize = page.pageSize;
    this.pageParam.pageIndex = page.pageIndex;
    this.getList();
  }

  addUser() {
    this.router.navigateByUrl('mlt/management/users-upsert');
  }

  editUser(row) {
    this.storageBrowser.set('management-user-upsert-id', row.id);
    this.router.navigateByUrl('mlt/management/users-upsert');
  }

  deleteUser() {
    const deletedUser = [];
    const temp = JSON.parse(JSON.stringify(this.users));
    temp.forEach(val => {
      if (val.isChecked) {
        delete val.isChecked;
        delete val.activatedStatus;
        delete val.createdAt;

        deletedUser.push(val);
      }
    });
    if (deletedUser.length == 0) {
      this.openErrorDialog('Please Select User')
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = `Do you want to Delete?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          this.userService.deleteUser(deletedUser).subscribe(result => {
              this.getList();
            },
            err => {
              console.log('error', err);
            }
          );
        }
      });
    }
  }

  openErrorDialog(message) {
    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.title = 'Failed';
    ref.componentInstance.message = message;
    ref.componentInstance.header = 'error';
  }

}
