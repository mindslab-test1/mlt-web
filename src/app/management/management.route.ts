import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './components/users.component';
import {ModuleWithProviders} from '@angular/core';
import {UsersUpsertComponent} from './components/users-upsert.component';
import {RoleComponent} from './components/role.component';
import {ProfileComponent} from './components/profile.component';
import {HistoryComponent} from './components/history.component';

const managementRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'users',
        component: UsersComponent,
        data: {
          nav: {
            name: 'Users',
            comment: 'Traning Data',
            icon: 'account_box',
          }
        },
      },
      {
        path: 'users-upsert',
        component: UsersUpsertComponent
      },
      {
        path: 'roles',
        component: RoleComponent,
        data: {
          nav: {
            name: 'Roles',
            comment: 'Traning Data',
            icon: 'account_box',
          }
        },
      },
      {
        path: 'histories',
        component: HistoryComponent,
        data: {
          nav: {
            name: 'Histories',
            comment: 'Histories',
            icon: 'history',
          }
        },
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
    ]
  }
];

export const ManagementRoute: ModuleWithProviders = RouterModule.forChild(managementRoutes);
