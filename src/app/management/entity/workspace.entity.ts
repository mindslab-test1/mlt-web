export class WorkspaceEntity {

  id: string;
  name: string;
  langCode: string;
  creatorId: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;
  roleCode: string;

}
