import {PageParameters} from 'mlt-shared';
import {WorkspaceEntity} from './workspace.entity';

export class HistoryEntity extends PageParameters {

  id: string;
  code: string;
  startedAt: Date;
  endedAt: Date;
  message: string;
  data: string;
  workspaceId: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

  workspaceEntity: WorkspaceEntity;

}
