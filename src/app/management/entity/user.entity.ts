
export class UserEntity {

  id: string;
  password: string;
  currentPassword?: string;
  username: string;
  email: string;
  roleId: string;
  pastRoleId: string;
  activated: string;
  userRoleRelEntities: any[];
  createdAt?: any;
  constructor() {
  }
}
