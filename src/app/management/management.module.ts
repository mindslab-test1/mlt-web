import {NgModule} from '@angular/core';
import {DatePipe} from '@angular/common';
import {SharedModule} from 'mlt-shared';
import {ManagementRoute} from './management.route';
import {UsersService} from './services/users.service';
import {UsersUpsertService} from './services/users-upsert.service';
import {UsersComponent} from './components/users.component';
import {UsersUpsertComponent} from './components/users-upsert.component';
import {RoleService} from './services/role.service';
import {RoleComponent} from './components/role.component';
import {ProfileComponent} from './components/profile.component';
import {ProfileService} from './services/profile.service';
import {HistoryService} from './services/history.service';
import {HistoryComponent} from './components/history.component';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  imports: [
    SharedModule,
    ManagementRoute,
    HttpClientModule
  ],
  providers: [
    UsersService, UsersUpsertService, RoleService, ProfileService, DatePipe,
    HistoryService
  ],
  declarations: [
    UsersComponent, UsersUpsertComponent, RoleComponent, ProfileComponent,
    HistoryComponent
  ],
  exports: [
    UsersComponent, UsersUpsertComponent, RoleComponent, ProfileComponent,
    HistoryComponent
  ]
})

export class ManagementModule {
}


