import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AgGridNg2} from 'ag-grid-angular';
import {ActivatedRoute, Router} from '@angular/router';
import {QaSetService} from '../service/qaSet.service';
import {AnswerEntity} from '../entity/answer.entity';

@Component({
  selector: 'app-basic-qa-v2-qa-sets-detail',
  templateUrl: './qa-sets-detail.component.html',
  styleUrls: ['./qa-sets-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetsDetailComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridNg2;
  colResizeDefault: string;

  itemId: number;

  answer: AnswerEntity;
  columnDefs = [
    {
      headerName: 'Id',
      field: 'id',
      width: 90,
    },
    {
      headerName: 'Question',
      field: 'question',
      width: 180,
    },
    {
      headerName: 'Soruce',
      field: 'source',
      width: 180,
    },
    {
      headerName: 'Attr1',
      field: 'attr1',
      width: 130,
    },
    {
      headerName: 'Attr2',
      field: 'attr2',
      width: 130,
    },
    {
      headerName: 'Attr3',
      field: 'attr3',
      width: 130,
    },
    {
      headerName: 'Attr4',
      field: 'attr4',
      width: 130,
    },

  ];

  rowData = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private qaSetService: QaSetService) {
    this.answer = new AnswerEntity();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.itemId = Number(params.get('id'));
      this.qaSetService.getQaSetByAnswerId(this.itemId).subscribe(res => {
        this.answer = res;
        this.rowData = res.questions;
      })
    });
  }

  /**
   * 수정 페이지로 이동
   */
  goEdit() {
    this.router.navigateByUrl('mlt/basicqa-v2/qa-sets/' + this.itemId + '/update');
  }

  /**
   * 뒤로가기 페이지로 이동
   */
  goBack() {
    let param = `categoryId=${encodeURI(this.answer.category.id.toString())}`;
    this.router.navigateByUrl('mlt/basicqa-v2/qa-sets?' + param);
  }
}
