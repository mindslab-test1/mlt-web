import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {MatChipInputEvent, MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerEntity} from '../entity/answer.entity';
import {FormControl, Validators} from '@angular/forms';
import {QuestionEntity} from '../entity/question.entity';
import {LayerEntity} from '../entity/layer.entity';
import {GridOptions} from 'ag-grid-community/dist/lib/entities/gridOptions';
import {AlertComponent} from 'mlt-shared';
import {QaSetService} from '../service/qaSet.service';
import {CategoryEntity} from '../entity/category.entity';
import {CategoryService} from '../service/category.service';
import {TagEntity} from '../entity/tag.entity';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-basic-qa-v2-qa-sets-upsert',
  templateUrl: './qa-sets-upsert.component.html',
  styleUrls: ['./qa-sets-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetsUpsertComponent implements OnInit {

  upsertFlag: string; // add, edit

  answerId: number;
  categoryId: number;
  categoryName: string;
  answerEntity: AnswerEntity;
  questionEntity: QuestionEntity;
  questionEntities: QuestionEntity[] = [];
  addedQuestionEntities: QuestionEntity[] = [];
  editedQuestionEntities: QuestionEntity[] = [];
  removedQuestionEntities: QuestionEntity[] = [];
  layerEntities: LayerEntity[];
  layer1Entities: LayerEntity[];
  layer2Entities: LayerEntity[];
  layer3Entities: LayerEntity[];
  layer4Entities: LayerEntity[];
  selectedLayer1Id: number;
  selectedLayer2Id: number;
  selectedLayer3Id: number;
  selectedLayer4Id: number;
  invalidFlag: boolean;


  questionFormControl = new FormControl('', [
    Validators.required,
  ]);

  public gridApi;
  public gridColumnApi;
  public gridOptions: GridOptions;
  public defaultColDef;
  rowData = [];

  tagList: TagEntity[]; // db에서 가져온 tag list
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;
  separatorKeysCodes = [ENTER, COMMA];
  inputTags: TagEntity[] = []; // answerEntity에 입력된 Tag list
  tagControl: FormControl = new FormControl();
  filteredTagOptions: Observable<TagEntity[]>;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private qaSetService: QaSetService,
              private categoryService: CategoryService,) {
    this.invalidFlag = true;
  }

  ngOnInit() {
    this.answerEntity = new AnswerEntity();
    this.questionEntity = new QuestionEntity();
    this.getLayerList().then(getLayerListFinish => {
      if (getLayerListFinish) {
        this.route.paramMap.subscribe(params => {
          if (params.get('id') === null || params.get('id') === undefined) {
            // add
            this.categoryId = Number(params.get('categoryId'));
            this.upsertFlag = 'Add';
            this.answerEntity.removedTags = [];
            this.answerEntity.addedTags = [];

            this.categoryService.getCategoryById(this.categoryId).subscribe(res => {
              this.categoryName = res.name;
            })
          } else {
            // edit
            this.upsertFlag = 'Edit';
            this.answerId = Number(params.get('id'));
            this.qaSetService.getQaSetByAnswerId(this.answerId).subscribe(res => {
              this.answerEntity = res;
              this.categoryId = this.answerEntity.category.id;
              this.categoryName = this.answerEntity.category.name;
              this.selectedLayer1Id = this.answerEntity.layer1.id;
              this.selectedLayer2Id = this.answerEntity.layer2.id;
              this.selectedLayer3Id = this.answerEntity.layer3.id;
              this.selectedLayer4Id = this.answerEntity.layer4.id;
              this.inputTags = this.answerEntity.tags;
              this.answerEntity.removedTags = [];
              this.answerEntity.addedTags = [];
              this.gridOptions.api.updateRowData({add: res.questions});
              this.invalidFlag = false;
            })
          }

          this.qaSetService.getTagList().subscribe(res => {
            this.tagList = res;
            this.filteredTagOptions = this.tagControl.valueChanges
                .pipe(
                    startWith(''),
                    map(val => this.tagFilter(val))
                );
          })
        });
      }
    });

    /**
     * aggrid setting
     */
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };
  }

  /**
   * tag 추가하는 함수
   * @param event
   */
  addTag(event: MatChipInputEvent): void {
    let input = event.input;
    let value = event.value;

    // answerEntity tag에 이미 추가된 tag인지 확인
    let duplicateTag = this.inputTags.filter(tag =>
        tag.name.toLowerCase().trim() === value.toLowerCase().trim()
    );

    if (duplicateTag.length === 0) {
      // answerEntity tag에 추가
      if ((value || '').trim()) {
        let searchTag = this.tagList.filter(tag =>
            tag.name.toLowerCase().trim() === value.toLowerCase().trim()
        );
        if (searchTag.length === 0) {
          // 등록되지 않은 신규 tag
          let newTag = new TagEntity();
          newTag.name = value.trim();
          this.inputTags.push(newTag);
          this.answerEntity.addedTags.push(newTag);
        } else {
          // 등록된 신규 tag
          this.inputTags.push(searchTag[0]);
          this.answerEntity.addedTags.push(searchTag[0]);
        }
      }
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  /**
   * 입력된 tag 삭제하는 함수
   * @param tag
   */
  removeTag(tag: any): void {
    if (tag.id !== undefined) {
      this.answerEntity.removedTags.push(tag);
    }
    let index = this.inputTags.indexOf(tag);

    if (index >= 0) {
      this.inputTags.splice(index, 1);
    }
  }

  /**
   * tag input에 입력할때 db에서 가져온 태그들 목록 필터 해서 보여줌
   * @param val
   */
  tagFilter(val): TagEntity[] {
    return this.tagList.filter(tag =>
        tag.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  /**
   * ag grid column setting
   */
  createColumnDefs() {
    return [
      {
        headerName: 'Id',
        field: 'id',
        width: 90,
      },
      {
        headerName: 'Question',
        field: 'question',
        editable: true
      },
      {
        headerName: 'Soruce',
        field: 'source',
        width: 180,
        editable: true
      },
      {
        headerName: 'Attr1',
        field: 'attr1',
        width: 130,
        editable: true
      },
      {
        headerName: 'Attr2',
        field: 'attr2',
        width: 130,
        editable: true
      },
      {
        headerName: 'Attr3',
        field: 'attr3',
        width: 130,
        editable: true
      },
      {
        headerName: 'Attr4',
        field: 'attr4',
        width: 130,
        editable: true
      },
    ];
  }

  /**
   * question add 버튼 클릭시 aggrid table에 빈 row 생성
   */
  addQuestionRow() {
    this.gridOptions.api.updateRowData({add: [{}]});
  }

  /**
   * 선택한 questionrow를 삭제
   */
  deleteQuestionRow() {
    let selectedData = this.gridOptions.api.getSelectedRows();
    selectedData.forEach(row => {
      if (row.id != undefined && row.id != null && row.id != 0) {
        // 만약 DB에 저장된 값이라면
        this.removedQuestionEntities.push(row);
      }
    })
    this.gridOptions.api.updateRowData({remove: selectedData});
  }

  /**
   * layer 목록 조회
   */
  getLayerList(): Promise<boolean> {
    return new Promise(resolve => {
      this.qaSetService.getLayerList().subscribe(res => {
        this.layerEntities = res;
        this.layer1Entities = [];
        this.layer2Entities = [];
        this.layer3Entities = [];
        this.layer4Entities = [];
        this.layerEntities.forEach(layer => {
          if (layer.layerSection === 1) {
            this.layer1Entities.push(layer);
          }
          if (layer.layerSection === 2) {
            this.layer2Entities.push(layer);
          }
          if (layer.layerSection === 3) {
            this.layer3Entities.push(layer);
          }
          if (layer.layerSection === 4) {
            this.layer4Entities.push(layer);
          }
        });
        resolve(true);
      }, err => {
        this.openAlertDialog('Failed', 'Failed Get LayerData', 'error');
      })
    });
  }

  /**
   * 필수값 입력했는지 확인하는 함수
   */
  checkValid() {
    if (this.answerEntity.answer === undefined || this.answerEntity.answer.trim() === '') {
      this.invalidFlag = true;
    } else {
      this.invalidFlag = false;
    }
  }

  /**
   * input 밑에 error message 표시해주는 함수
   */
  getErrorMessage() {
    return this.questionFormControl.hasError('required') ? 'Please enter question' :
        this.questionFormControl.hasError('duplicate') ? 'Question already exist' :
            '';
  }

  /**
   * Save버튼 클릭시 호출
   */
  qaSetSave() {
    let canSave: boolean = true;
    this.addedQuestionEntities = [];
    this.editedQuestionEntities = [];
    this.gridOptions.api.forEachNode(node => {
      if (node.data.question === undefined || node.data.question === '' || node.data.question === null) {
        // question ag grid 테이블에 필수값(question)내용이 안들어가있는경우 호출 및 등록 취소처리
        this.openAlertDialog('Warning', 'Enter required value', 'notice');
        canSave = false;
      }
      if (node.data.id === undefined || node.data.id === null || node.data.id === 0) {
        this.addedQuestionEntities.push(node.data);
      } else {
        this.editedQuestionEntities.push(node.data);
      }
    });
    if (canSave) {
      if (this.upsertFlag === 'Add') {
        this.answerEntity.category = new CategoryEntity();
        this.answerEntity.category.id = this.categoryId;
      }
      if (this.answerEntity.layer1 === undefined || this.answerEntity.layer1 === null) {
        this.answerEntity.layer1 = new LayerEntity();
      }
      if (this.answerEntity.layer2 === undefined || this.answerEntity.layer2 === null) {
        this.answerEntity.layer2 = new LayerEntity();
      }
      if (this.answerEntity.layer3 === undefined || this.answerEntity.layer3 === null) {
        this.answerEntity.layer3 = new LayerEntity();
      }
      if (this.answerEntity.layer4 === undefined || this.answerEntity.layer4 === null) {
        this.answerEntity.layer4 = new LayerEntity();
      }
      this.answerEntity.layer1.id = this.selectedLayer1Id;
      this.answerEntity.layer2.id = this.selectedLayer2Id;
      this.answerEntity.layer3.id = this.selectedLayer3Id;
      this.answerEntity.layer4.id = this.selectedLayer4Id;
      this.answerEntity.tags = this.inputTags;
      this.answerEntity.addedQuestions = this.addedQuestionEntities;
      this.answerEntity.editedQuestions = this.editedQuestionEntities;
      this.answerEntity.removedQuestions = this.removedQuestionEntities;
      if (this.upsertFlag === 'Add') {
        this.qaSetService.addQaSet(this.answerEntity).subscribe(res => {
          this.openAlertDialog('Success', 'Success Add', 'success');
          let param = `categoryId=${encodeURI(this.categoryId.toString())}`;
          this.router.navigateByUrl('mlt/basicqa-v2/qa-sets?' + param);
        }, err => {
          this.openAlertDialog('Failed', 'Failed Add', 'error');
        })
      } else {
        this.qaSetService.editQaSet(this.answerEntity).subscribe(res => {
          this.openAlertDialog('Success', 'Success Edit', 'success');
          let param = `categoryId=${encodeURI(this.categoryId.toString())}`;
          this.router.navigateByUrl('mlt/basicqa-v2/qa-sets?' + param);
        }, err => {
          this.openAlertDialog('Failed', 'Failed Edit', 'error');
        })
      }
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  goBack() {
    let param = `categoryId=${encodeURI(this.categoryId.toString())}`;
    this.router.navigateByUrl('mlt/basicqa-v2/qa-sets?' + param);
  }
}
