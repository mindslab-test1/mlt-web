import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {DatePipe} from '@angular/common';
import {AlertComponent} from 'mlt-shared';
import {MatDialog} from '@angular/material';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {IndexingService} from '../service/indexing.service';

@Component({
  selector: 'app-basicqa-v2-indexing',
  templateUrl: './indexing.component.html',
  styleUrls: ['./indexing.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]

})
export class IndexingComponent implements OnInit, OnDestroy {
  indexing;
  history;
  progressStatus = false;
  intervalHandler: any;
  timeout;
  disabled = false;

  categoryList;
  selectedCategoryId = 0;
  clean = false;
  valid = true;
  selectedCollectionType = 1;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredCategoryList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private indexingService: IndexingService,
              private datePipe: DatePipe,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.selectSearchFormControl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterCategory();
        });
    this.getCategoryList();
    this.timeout = 5000;
    this.getIndexingStatus();
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * category selectbox 검색 filter
   */
  private filterCategory() {
    if (!this.categoryList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredCategoryList.next(this.categoryList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredCategoryList.next(
        this.categoryList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * category selectbox 검색 초기화
   */
  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  /**
   * getIndexingStatus Trigger
   */
  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.getIndexingStatus()
    }, this.timeout);
  }

  /***
   * full indexing 요청
   */
  fullIndexing() {
    this.indexingService.fullIndexing(this.selectedCategoryId, this.selectedCollectionType, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: ((res.fetched / res.total) * 100).toFixed(3),
          stopped: '',
          error: '',
          key: '',
        };
      } else {
        this.disabled = false;
        clearInterval(this.intervalHandler);
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA V2 Indexing failed.', 'error');
    })
  }

  /**
   * additionalIndexing 요청
   */
  additionalIndexing() {
    this.indexingService.additionalIndexing(this.selectedCategoryId, this.selectedCollectionType, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: ((res.fetched / res.total) * 100).toFixed(3),
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA V2 Indexing failed.', 'error');
    })
  }

  /**
   * abortIndexing 요청
   */
  abortIndexing() {
    this.indexingService.abortIndexing(this.selectedCollectionType).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA V2 Indexing failed.', 'error');
    })
  }

  /**
   * getIndexingStatus요청
   */
  getIndexingStatus() {
    this.indexingService.getIndexingStatus().subscribe(res => {
      this.progressStatus = res.status;
      clearInterval(this.intervalHandler);
      if (res.status) {
        this.disabled = true;
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: ((res.fetched / res.total) * 100).toFixed(3),
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.history = res.latestIndexingHistory;
        if (this.history != null) {
          if (this.history.stopYn) {
            this.history.progress = ((this.history.fetched / this.history.total) * 100).toFixed(3);
          } else {
            this.history.progress = 100;
          }
          let date = new Date(this.history.createdAt);
          this.history.createAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          if (this.history.stopYn) {
            date = new Date(this.history.updatedAt);
            this.history.updateAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          }
        }
        this.disabled = false;
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA V2 Indexing failed.', 'error');
    })
  }

  /**
   * Category List 조회 요청
   */
  getCategoryList() {
    this.indexingService.getCategoryList().subscribe(res => {
      this.categoryList = res;
      this.filteredCategoryList.next(this.categoryList.slice());
    })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  checkValid() {
    if (this.selectedCategoryId !== undefined && this.selectedCollectionType !== undefined && this.clean !== undefined) {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }
}
