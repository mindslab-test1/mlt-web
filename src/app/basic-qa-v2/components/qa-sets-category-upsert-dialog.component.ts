import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {AlertComponent, ConfirmComponent, StorageBrowser} from 'mlt-shared';
import {CategoryEntity} from '../entity/category.entity';


@Component({
  selector: 'app-basic-qa-v2-qa-sets-category-upsert-dialog',
  templateUrl: 'qa-sets-category-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class QaSetsCategoryUpsertDialogComponent implements OnInit {

  param: any;
  service: any;
  role: string;

  name: string;
  category: CategoryEntity;
  categoryNameValidator = new FormControl('', [Validators.required, this.noWhitespaceValidator]);
  disabled = true;
  data: any

  constructor(public dialogRef: MatDialogRef<QaSetsCategoryUpsertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
    this.category = new CategoryEntity();
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;

    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.service.getCategoryById(this.data.row).subscribe(res => {
        this.category = res;
      });
    }
  }

  /**
   * ADD 버튼 클릭시 작동하는 함수, Category등록 기능
   */
  addCategory = () => {
    this.category.name = this.category.name.trim();
    this.service.addCategory(this.category).subscribe(res => {
          if (res) {
            this.dialogRef.close('add');
            this.openAlertDialog('Success', 'Success Add Category', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', 'Failed Add Category', 'error');
          console.log('error', err);
        }
    );
  }

  /**
   * Edit 버튼 클릭시 작동하는 함수, Category 수정 기능
   */
  editCategory = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Category?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.category.name = this.category.name.trim();
        this.service.editCategory(this.category).subscribe(res => {
              if (res) {
                this.dialogRef.close('edit');
                this.openAlertDialog('Success', `Success Edit Category.`, 'success');
              }
            },
            err => {
              this.openAlertDialog('Error', 'Failed Edit Category', 'error');
              console.log('error', err);
            }
        );
      }
    });
  }

  /**
   * Remove 버튼 클릭시 작동하는 함수, Category Remove 기능
   */
  removeCategory = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Category? All related Category will be removed';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let list = [];
        list.push(this.category.id);
        this.service.removeCategory(list).subscribe(res => {
              this.dialogRef.close('delete');
              this.openAlertDialog('Success', `Success Remove Category.`, 'success');

            },
            err => {
              this.openAlertDialog('Error', 'Failed Remove Category', 'error');
              console.log('error', err);
            }
        );
      }
    })
  }

  /**
   * 필수값 입력했는지 체크하는 함수, 필수값 입력완료시 등록버튼 활성화
   * @param $event
   */
  checkInvalid($event) {
    if ('INVALID' === this.categoryNameValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.categoryNameValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  /**
   * 빈 object인지 확인하는 함수
   * @param obj
   */
  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * 입력된 값이 빈값인지 확인하는 함수
   * @param control
   */
  noWhitespaceValidator(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : {'whitespace': true}
  }
}
