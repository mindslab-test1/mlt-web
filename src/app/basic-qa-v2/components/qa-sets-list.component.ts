import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AgGridNg2} from 'ag-grid-angular';
import {ActivatedRoute, Router} from '@angular/router';
import {CategoryEntity} from '../entity/category.entity';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {ReplaySubject} from 'rxjs';
import {QaSetsCategoryUpsertDialogComponent} from './qa-sets-category-upsert-dialog.component';
import {QaSetService} from '../service/qaSet.service';
import {CategoryService} from '../service/category.service';
import {takeUntil} from 'rxjs/operators';
import {GridOptions} from 'ag-grid-community/dist/lib/entities/gridOptions';
import {AlertComponent} from 'mlt-shared';


@Component({
  selector: 'app-basic-qa-v2-qa-sets-list',
  templateUrl: './qa-sets-list.component.html',
  styleUrls: ['./qa-sets-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetsListComponent implements OnInit, AfterViewInit {

  @ViewChild('agGrid') agGrid: AgGridNg2;
  colResizeDefault: string;

  categoryActionsArray;
  categoryList: CategoryEntity[];
  selectedCategory: number;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredCategoryList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  isCategorySelected: boolean;

  qaSetActionsArray;

  public gridOptions: GridOptions;
  public defaultColDef;

  rowData = [];

  objectKeys = Object.keys;


  indexedQuestionKeywords = [];
  indexedQuestionKeywordsCnt: number = 0;
  indexedAnswerKeywords = [];
  indexedAnswerKeywordsCnt: number = 0;
  indexedTagKeywords = [];
  indexedAttr1Keywords = [];
  indexedAttr2Keywords = [];
  indexedAttr3Keywords = [];
  indexedAttr4Keywords = [];
  indexedLayer1Keywords = [];
  indexedLayer2Keywords = [];
  indexedLayer3Keywords = [];
  indexedLayer4Keywords = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private categoryService: CategoryService,
              private qaSetService: QaSetService) {
    this.colResizeDefault = 'shift';
    this.isCategorySelected = false;
  }

  ngOnInit() {
    this.qaSetActionsArray = [
      {
        type: 'add',
        text: 'Add QA Set',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.goAdd,
        params: 'add'
      },
      {
        text: 'Remove QA Set',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeAnswer,
        params: 'remove'
      },
    ];

    this.categoryActionsArray = [
      {
        type: 'add',
        text: 'Add Category',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popItemCategoryUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Category',
        icon: 'remove_circle_outline',
        hidden: true,
        disabled: true,
        callback: this.popItemCategoryUpsertDialog,
        params: 'edit'
      },
    ];


    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      floatingFilter: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onCellClicked: (event) => {
        if (event.colDef.field == 'answer') {
          this.router.navigateByUrl('mlt/basicqa-v2/qa-sets/' + event.data.id);
        }
      },
      onCellValueChanged: (event) => {
        // this.updateDictionaryData(event.data);
      },
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };

    this.selectSearchFormControl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterCategory();
        });

  }

  ngAfterViewInit() {
    this.getCategoryList().then(getCategoryListResult => {
      this.route.queryParams.subscribe(
          query => {
            if (query['categoryId'] !== undefined && query['categoryId'] !== null) {
              this.selectedCategory = query['categoryId'] * 1;
              this.onCategorySelected();
              this.cdr.detectChanges();
            }
          });
    });
  }

  createColumnDefs() {
    return [
      {
        checkboxSelection: true,
        width: 35,
        suppressFilter: true,
        headerCheckboxSelection: true
      },
      {
        headerName: 'Id',
        field: 'id',
        width: 50,
        suppressFilter: true
      },
      {
        headerName: 'Answer',
        field: 'answer',
      },
      {
        headerName: 'Question',
        field: 'questionStringfy'
      },
      {
        headerName: 'Tags',
        field: 'tagStringfy',
        width: 80,
      },
      {
        headerName: 'Source',
        field: 'source',
        width: 80,
      },
      {
        headerName: 'Summary',
        field: 'summary',
        width: 100,
      },
      {
        headerName: 'Attr1',
        field: 'attr1',
        width: 80,
      },
      {
        headerName: 'Attr2',
        field: 'attr2',
        width: 80,
      },
      {
        headerName: 'Attr3',
        field: 'attr3',
        width: 80,
      },
      {
        headerName: 'Attr4',
        field: 'attr4',
        width: 80,
      },
      {
        headerName: 'layer1',
        field: 'layer1.name',
        width: 80,
      },
      {
        headerName: 'layer2',
        field: 'layer2.name',
        width: 80,
      },
      {
        headerName: 'layer3',
        field: 'layer3.name',
        width: 80,
      },
      {
        headerName: 'layer4',
        field: 'layer4.name',
        width: 80,
      },
    ];
  }

  /**
   * Category SelectBox 검색 내용 초기화 해주는 함수
   */
  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  }

  /**
   * Category SelectBox에서 검색할수있도록 해주는 함수
   */
  private filterCategory() {
    if (!this.categoryList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredCategoryList.next(this.categoryList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredCategoryList.next(
        this.categoryList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * 서버에서 CategoryList를 조회해서 가져오는 함수
   * @returns {Promise<boolean>}
   */
  getCategoryList(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.categoryService.getCategoryList().subscribe(res => {
        this.categoryList = res;
        this.filteredCategoryList.next(this.categoryList.slice());
        resolve(true);
      }, err => {
        this.openAlertDialog('Error', 'Failed to get category list', 'error');
        reject(true);
      })
    });
  }

  /**
   * 서버에서 AnswerList 조회해서 가져오는 함수
   */
  getAnswerList(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.qaSetService.getQaSets(this.selectedCategory).subscribe(res => {
        // tag, question을  한번에 보여주기 위함....
        res.forEach(answer => {
          let temp = [];
          let questionTemp = [];
          answer.tags.forEach(tag => {
            temp.push(tag.name);
          });
          answer.questions.forEach(question => {
            questionTemp.push(question.question);
          })
          answer['questionStringfy'] = questionTemp;
          answer['tagStringfy'] = temp;
        });
        this.gridOptions.api.setRowData([]); // 초기화
        this.gridOptions.api.updateRowData({add: res});
        resolve(true);
      }, err => {
        this.openAlertDialog('Error', 'Failed to get answer list', 'error');
        reject(true);
      })
    });
  }

  /**
   * indexed keyword 리스트 요청 (answer, question, tag, attr, layer)
   */
  getIndexedKeyword() {
    this.qaSetService.getIndexedKeywords(this.selectedCategory).subscribe(res => {
      // console.log("#@ 1 :", res);

      // #@ Question를 value(index 갯수)기준으로 정렬합니다
      var sortableQuestion = [];
      for (var key in res.question) {
        sortableQuestion.push([key, res.question[key]]);
      }

      sortableQuestion.sort(function(a, b) {
        return b[1] - a[1];
      });
      //console.log('#@ sortable :', sortable);
      this.indexedQuestionKeywords = sortableQuestion;
      this.indexedQuestionKeywordsCnt = Object.keys(this.indexedQuestionKeywords).length;

      // #@ Answer를 value(index 갯수)기준으로 정렬합니다
      var sortableAnswer = [];
      for (var key in res.answer) {
        sortableAnswer.push([key, res.answer[key]]);
      }

      sortableAnswer.sort(function(a, b) {
        return b[1] - a[1];
      });
      //console.log('#@ sortable :', sortable);
      this.indexedAnswerKeywords = sortableAnswer;
      this.indexedAnswerKeywordsCnt = Object.keys(this.indexedAnswerKeywords).length;
      this.indexedLayer1Keywords = res.layer1;
      this.indexedLayer2Keywords = res.layer2;
      this.indexedLayer3Keywords = res.layer3;
      this.indexedLayer4Keywords = res.layer4;
      this.indexedAttr1Keywords = res.attr1;
      this.indexedAttr2Keywords = res.attr2;
      this.indexedAttr3Keywords = res.attr3;
      this.indexedAttr4Keywords = res.attr4;
      this.indexedTagKeywords = res.tag;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get indexed Keyword', 'error');
    })
  }
  /**
   * 서버에 answer 삭제 요청
   */
  removeAnswer = () => {
    let selectedRows = this.gridOptions.api.getSelectedRows();
    if (selectedRows.length === 0) {
      this.openAlertDialog('Warning', 'Please Select Rows', 'notice');
    } else {
      let arr = [];
      selectedRows.forEach(row => {
        arr.push(row.id);
      });
      this.qaSetService.removeQaSets(arr).subscribe(res => {
        this.openAlertDialog('Success', 'Success Remove', 'success');
        this.getAnswerList();
      })
    }
  }

  /**
   * 등록페이지로 이동합니다.
   */
  goAdd = () => {
    this.router.navigateByUrl('mlt/basicqa-v2/qa-sets/categories/' + this.selectedCategory + '/new');
  }

  /**
   * Category Upsert Dialog Component 를 팝업으로 띄어주는 함수
   * @param type
   * @param row
   * @returns {boolean}
   */
  popItemCategoryUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.selectedCategory;
    } else {
      return false;
    }

    paramData['service'] = this.categoryService;
    paramData['entity'] = new CategoryEntity();

    let dialogRef = this.dialog.open(QaSetsCategoryUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit' || result === 'add') {
        this.getCategoryList().then(getCategoryListResult => {
        });
      }
      if (result === 'delete') {
        this.getCategoryList().then(getCategoryListResult => {
          this.selectedCategory = 0;
          this.isCategorySelected = false;
          this.categoryActionsArray[1].hidden = true;
          this.categoryActionsArray[1].disabled = true;
        });
      }
    });
  }

  /**
   * Category 선택되었을경우 isCategorySelected true값설정 및 Answer목록을 조회하는 함수 호출
   */
  onCategorySelected() {
    this.isCategorySelected = true;
    this.categoryActionsArray[1].hidden = false;
    this.categoryActionsArray[1].disabled = false;
    this.getAnswerList();
    this.getIndexedKeyword();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * Index된 keyword로 목록에서 필터링 합니다.
   * @param key
   */
  filterByKeyword(keyword: string, column: string) {
    let filterComponent = this.gridOptions.api.getFilterInstance(column);
    filterComponent.setModel({
      type: 'contains',
      filter: keyword
    });
    this.gridOptions.api.onFilterChanged();
  }
}
