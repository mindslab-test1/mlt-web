import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {SharedModule} from 'mlt-shared';
import {AgGridModule} from 'ag-grid-angular';

import {BasicQaV2Route} from './basic-qa-v2.route';
import {QaSetsListComponent} from './components/qa-sets-list.component';
import {QaSetsDetailComponent} from './components/qa-sets-detail.component';
import {QaSetsUpsertComponent} from './components/qa-sets-upsert.component';
import {QaSetsCategoryUpsertDialogComponent} from './components/qa-sets-category-upsert-dialog.component';
import {QaSetService} from './service/qaSet.service';
import {CategoryService} from './service/category.service';
import {IndexingService} from './service/indexing.service';
import {IndexingComponent} from './components/indexing.component';

@NgModule({
  imports: [
    AgGridModule.withComponents([]),
    CommonModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    BasicQaV2Route,
    HttpClientModule,
  ],
  providers: [QaSetService, CategoryService, IndexingService],
  declarations: [QaSetsListComponent, QaSetsDetailComponent, QaSetsUpsertComponent, QaSetsCategoryUpsertDialogComponent, IndexingComponent],
  exports: [QaSetsListComponent, QaSetsDetailComponent, QaSetsUpsertComponent, QaSetsCategoryUpsertDialogComponent, IndexingComponent],
  entryComponents: [QaSetsCategoryUpsertDialogComponent]
})
export class BasicQaV2Module {
}


