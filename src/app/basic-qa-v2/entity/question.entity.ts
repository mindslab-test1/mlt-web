export class QuestionEntity {

  id: number;
  answerId: number;
  question: string;
  attr1: string;
  attr2: string;
  attr3: string;
  attr4: string;
  source: string;

}
