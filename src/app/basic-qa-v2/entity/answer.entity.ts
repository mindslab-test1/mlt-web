import {CategoryEntity} from './category.entity';
import {LayerEntity} from './layer.entity';
import {QuestionEntity} from './question.entity';
import {TagEntity} from './tag.entity';

export class AnswerEntity {

  id: number;
  answer: string;
  category: CategoryEntity;
  attr1: string;
  attr2: string;
  attr3: string;
  attr4: string;
  layer1: LayerEntity;
  layer2: LayerEntity;
  layer3: LayerEntity;
  layer4: LayerEntity;
  source: string;
  summary: string;

  tags: TagEntity[];
  addedTags: TagEntity[];
  removedTags: TagEntity[];

  addedQuestions: QuestionEntity[];
  editedQuestions: QuestionEntity[];
  removedQuestions: QuestionEntity[];
}
