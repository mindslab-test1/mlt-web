import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {QaSetsListComponent} from './components/qa-sets-list.component';
import {QaSetsDetailComponent} from './components/qa-sets-detail.component';
import {QaSetsUpsertComponent} from './components/qa-sets-upsert.component';
import {IndexingComponent} from './components/indexing.component';

const basicQaV2Route: Routes = [
  {
    path: '',
    children: [
      {
        path: 'qa-sets',
        component: QaSetsListComponent,
        data: {
          nav: {
            name: 'QA Sets',
            comment: '',
            icon: 'library_books',
          }
        }
      },
      {
        path: 'qa-sets/categories/:categoryId/new',
        component: QaSetsUpsertComponent
      },
      {
        path: 'qa-sets/:id',
        component: QaSetsDetailComponent
      },
      {
        path: 'qa-sets/:id/update',
        component: QaSetsUpsertComponent
      },
      {
        path: 'indexing',
        component: IndexingComponent,
        data: {
          nav: {
            name: 'Indexing',
            comment: '',
            icon: 'library_books',
          }
        }
      },
    ]
  }
];

export const BasicQaV2Route: ModuleWithProviders = RouterModule.forChild(basicQaV2Route);
