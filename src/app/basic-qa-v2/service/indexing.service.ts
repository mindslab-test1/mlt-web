import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class IndexingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getCategoryList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/indexing/categories');
  }

  fullIndexing(selectedCategoryId: number, selectedCollectionType: number, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/indexing/fullIndexing/' + selectedCategoryId + '/' + selectedCollectionType + '/' + clean);
  }

  additionalIndexing(selectedCategoryId: number, selectedCollectionType: number, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/indexing/additionalIndexing/' + selectedCategoryId +  '/' + selectedCollectionType + '/' + clean);
  }

  getIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/indexing/getIndexingStatus');
  }

  abortIndexing(selectedCollectionType: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/indexing/abortIndexing/' + selectedCollectionType);
  }
}

