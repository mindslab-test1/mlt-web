import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {StorageBrowser} from 'mlt-shared';
import {CategoryEntity} from '../entity/category.entity';

@Injectable()
export class CategoryService {

  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getCategoryList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/categories');
  }

  getCategoryById(categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/categories/' + categoryId);
  }

  addCategory(categoryEntity: CategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/categories/add', categoryEntity);
  }

  editCategory(categoryEntity: CategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/categories/edit', categoryEntity);
  }

  removeCategory(categoryIds: any): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/categories/remove', categoryIds);
  }
}

