import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {StorageBrowser} from 'mlt-shared';
import {AnswerEntity} from '../entity/answer.entity';

@Injectable()
export class QaSetService {
  API_URL;
  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getIndexedKeywords(categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/qa-sets/indexed-keywords/' + categoryId);
  }

  getQaSets(categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/qa-sets/answers/category-id/' + categoryId);
  }

  getQaSetByAnswerId(answerId: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/qa-sets/answers/' + answerId);
  }

  getLayerList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/qa-sets/layers');
  }

  getTagList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa-v2/qa-sets/tags');
  }

  addQaSet(answerEntity: AnswerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/qa-sets/answers/add', answerEntity);
  }

  editQaSet(answerEntity: AnswerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/qa-sets/answers/edit', answerEntity);
  }

  removeQaSets(ids: any): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa-v2/qa-sets/answers/remove', ids);
  }
}

