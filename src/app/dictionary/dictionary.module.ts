import {NgModule} from '@angular/core';

import {SharedModule} from 'mlt-shared';
import {DictionaryRoute} from './dictionary.route';
import {DictionaryCompoundNounService} from './services/dictionary.compound-noun.service';
import {DictionaryMorphemeService} from './services/dictionary.morpheme.service';
import {DictionaryPostProcessService} from './services/dictionary.post-process.service';
import {DictionaryPreProcessService} from './services/dictionary.pre-process.service';
import {DictionaryWiseNluNerService} from './services/dictionary.wise-nlu-ner.service';

import {DictionaryCompoundNounComponent} from './components/dictionary.compound-noun.component';
import {DictionaryMorphemeComponent} from './components/dictionary.morpheme.component';
import {DictionaryPostProcessComponent} from './components/dictionary.post-process.component';
import {DictionaryPreProcessComponent} from './components/dictionary.pre-process.component';
import {DictionaryWiseNluNerComponent} from './components/dictionary.wise-nlu-ner.component';
import {HttpClientModule} from '@angular/common/http';
import {DictionaryWiseNluNerCategoryComponent} from './components/dictionary.wise-nlu-ner-category.component';
import {DictionaryWiseNluNerCategoryService} from './services/dictionary.wise-nlu-ner-category.service';
import {AgGridModule} from 'ag-grid-angular';
import {NerCorpusComponent} from './components/dictionary.ner-corpus.component';
import {FormsModule} from '@angular/forms';
import {NerCorpusService} from './services/dictionary.ner-corpus.service';
import {NerCorpusTagComponent} from './components/dictionary.ner-corpus-tag.component';
import {NerCorpusTagService} from './services/dictionary.ner-corpus-tag.service';
import {NerDictionaryService} from './services/dictionary.ner-dictionary.service';
import {NerDictionaryComponent} from './components/dictionary.ner-dictionary.component';
import {NerTestComponent} from './components/dictionary.ner-test.component';
import {DictionaryVersionComponent} from './components/dictionary.version-manager';
import {NerDictionariesManagementComponent} from './components/dictionary.ner-dictionaries-management.component';
import {PosDictionaryComponent} from './components/dictionary.pos-dictionary.component';
import {PosDictionaryService} from './services/dictionary.pos-dictionary.service';
import {PosDictionariesManagementComponent} from './components/dictionary.pos-dictionaries-management.component';
import {PosTestComponent} from './components/dictionary.pos-test.component';
import {FileManagementComponent} from './components/file-management.component';
import {FileManagementService} from './services/file-management.service';
import {TestFileComponent} from './components/dictionary.test-file.component';
import {TestResultComponent} from './components/dictionary.test-result.component';
import {TestFileService} from './services/dictionary.test-file.service';
import {TestResultService} from './services/dictionary.test-result.service';
import {TestResultDetailComponent} from './components/dictionary.test-result-detail.component';

@NgModule({
	imports: [
		SharedModule,
		FormsModule,
		HttpClientModule,
		DictionaryRoute,
		AgGridModule.withComponents([])
	],
	providers: [
		DictionaryCompoundNounService, DictionaryMorphemeService,
		DictionaryPostProcessService, DictionaryPreProcessService,
		DictionaryWiseNluNerService, DictionaryWiseNluNerCategoryService,
		NerCorpusService, NerCorpusTagService, NerDictionaryService,
		PosDictionaryService, FileManagementService, TestFileService, TestResultService
	],
	declarations: [
		DictionaryCompoundNounComponent, DictionaryMorphemeComponent,
		DictionaryPostProcessComponent, DictionaryPreProcessComponent,
		DictionaryWiseNluNerComponent, DictionaryWiseNluNerCategoryComponent,
		NerCorpusComponent, NerCorpusTagComponent, NerDictionaryComponent,
		NerDictionariesManagementComponent, DictionaryVersionComponent, NerTestComponent,
		PosDictionaryComponent, PosDictionariesManagementComponent, PosTestComponent,
    FileManagementComponent, TestFileComponent, TestResultComponent, TestResultDetailComponent
	],
	exports: [
		DictionaryCompoundNounComponent, DictionaryMorphemeComponent,
		DictionaryPostProcessComponent, DictionaryPreProcessComponent,
		DictionaryWiseNluNerComponent, DictionaryWiseNluNerCategoryComponent,
		NerCorpusComponent, NerCorpusTagComponent, NerDictionaryComponent,
		NerDictionariesManagementComponent, DictionaryVersionComponent, NerTestComponent,
		PosDictionaryComponent, PosDictionariesManagementComponent, PosTestComponent,
    FileManagementComponent, TestFileComponent, TestResultComponent, TestResultDetailComponent
	]
})
export class DictionaryModule {
}


