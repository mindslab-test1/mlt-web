import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {DictionaryMorphemeComponent} from './components/dictionary.morpheme.component';
import {DictionaryCompoundNounComponent} from './components/dictionary.compound-noun.component';
import {DictionaryPreProcessComponent} from './components/dictionary.pre-process.component';
import {DictionaryPostProcessComponent} from './components/dictionary.post-process.component';
import {DictionaryWiseNluNerComponent} from './components/dictionary.wise-nlu-ner.component';
import {DictionaryWiseNluNerCategoryComponent} from './components/dictionary.wise-nlu-ner-category.component';
import {NerCorpusComponent} from './components/dictionary.ner-corpus.component';
import {NerCorpusTagComponent} from './components/dictionary.ner-corpus-tag.component';
import {DictionaryVersionComponent} from './components/dictionary.version-manager';
import {NerDictionaryComponent} from './components/dictionary.ner-dictionary.component';
import {NerTestComponent} from './components/dictionary.ner-test.component';
import {NerDictionariesManagementComponent} from './components/dictionary.ner-dictionaries-management.component';
import {PosDictionaryComponent} from './components/dictionary.pos-dictionary.component';
import {PosDictionariesManagementComponent} from './components/dictionary.pos-dictionaries-management.component';
import {PosTestComponent} from './components/dictionary.pos-test.component';
import {TestResultDetailComponent} from './components/dictionary.test-result-detail.component';

const dictionaryRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'morpheme',
        component: DictionaryMorphemeComponent,
        data: {
          nav: {
            name: 'Morpheme',
            comment: 'Data Group',
            icon: 'text_format',
          }
        },
      },
      {
        path: 'compound_noun',
        component: DictionaryCompoundNounComponent,
        data: {
          nav: {
            name: 'Compound Noun',
            comment: 'Compound Noun',
            icon: 'translate',
          }
        },
      },
      {
        path: 'preprocess',
        component: DictionaryPreProcessComponent,
        data: {
          nav: {
            name: 'Preprocess',
            comment: 'Preprocess NE Dictionary',
            icon: 'skip_previous',
          }
        },
      },
      {
        path: 'postprocess',
        component: DictionaryPostProcessComponent,
        data: {
          nav: {
            name: 'Postprocess NE',
            comment: 'Postprocess NE Dictionary',
            icon: 'skip_next',
          }
        },
      },
      {
        path: 'wise-nlu-ner-category',
        component: DictionaryWiseNluNerCategoryComponent,
        data: {
          nav: {
            name: 'Wise NLU NER Category',
            comment: 'Wise NLU NER Category',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'wise-nlu-ner',
        component: DictionaryWiseNluNerComponent,
        data: {
          nav: {
            name: 'Wise NLU NER',
            comment: 'Wise NLU NER Category',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'morph',
        component: PosDictionariesManagementComponent,
        data: {
          nav: {
            name: 'Morpheme Dictionary',
            comment: 'Morpheme Dictionary',
          }
        },
      },
      {
        path: 'ner',
        component: NerDictionariesManagementComponent,
        data: {
          nav: {
            name: 'NER Dictionary',
            comment: 'NER Dictionary',
          }
        },
        pathMatch: 'full',
      },
      {
        path: 'ner',
        data: {
          nav: {
            name: 'Corpus & Tag for NER test',
            comment: 'Corpus & Tag for NER test',
          }
        },
        children: [{
            path: 'corpus',
            component: NerCorpusComponent,
            data: {
              nav: {
                name: 'NER Corpus',
                comment: 'NER Corpus',
              }
            },
          }, {
            path: 'tag',
            component: NerCorpusTagComponent,
            data: {
              nav: {
                name: 'NER tags',
                comment: 'NER Tags',
              }
            },
          },
          {
            path: ':name',
            component: NerDictionaryComponent,
          },
        ]
      },
      {
        path: 'morph/:name',
        component: PosDictionaryComponent,
      },
      {
        path: 'morph/:name/test',
        component: PosTestComponent,
      },
      {
        path: 'morph/:name/test/:testId/detail',
        component: TestResultDetailComponent,
      },
      {
        path: 'ner/:name/test',
        component: NerTestComponent,
      },
      {
        path: 'ner/:name/test/:testId/detail',
        component: TestResultDetailComponent,
      },
    ],
  },
  {
    path: 'version/:type/:name',
    component: DictionaryVersionComponent,
  }
];

export const DictionaryRoute: ModuleWithProviders = RouterModule.forChild(dictionaryRoutes);
