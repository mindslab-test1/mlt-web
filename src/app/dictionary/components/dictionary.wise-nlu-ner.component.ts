import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryDictionaryUpsertDialogComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {DictionaryWiseNluNerService} from '../services/dictionary.wise-nlu-ner.service';
import {DictionaryWiseNluNerEntity} from '../entity/dictionary.wise-nlu-ner.entity';

@Component({
  selector: 'app-wise-nlu-ner',
  templateUrl: './dictionary.wise-nlu-ner.component.html',
  styleUrls: ['./dictionary.wise-nlu-ner.component.scss'],
})
export class DictionaryWiseNluNerComponent implements OnInit {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  toolsVisible = false;
  searchCategory = '';
  searchRule = '';
  wiseNluNerDicLineList: any[] = [];
  wiseNluNerDicList = [];
  pageLength;
  editable = false;
  actions: any[] = [];
  initActions: any[] = [];
  pageParam: MatPaginator;
  sortParam: MatSort;
  tipsVisible = false;
  param: DictionaryWiseNluNerEntity = new DictionaryWiseNluNerEntity();
  selectedType: any;
  isTableHidden = true;

  convertWord: string;
  convertResult: string;

  processTypes = [
    {attr: 'PRE_PROCESS', name: 'Pre Process'},
    {attr: 'POST_PROCESS', name: 'Post Process'},
  ];

  defaultHeader = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'category', name: 'Category', width: '20%', isSort: true},
    {attr: 'rule', name: 'Rule', width: '80%'},
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'category', name: 'Category', input: true, width: '20%'},
    {attr: 'rule', name: 'Rule', input: true, width: '80%'},
  ];

  header = [];

  dataSource: MatTableDataSource<any>;
  selectedDictionary: any = '';

  progressState = {
    info: 'info',
    progressing: 'progressing',
    result: 'result'
  };

  progress: string;

  constructor(private dictionaryWiseNluNerService: DictionaryWiseNluNerService
    , @Inject(StorageBrowser) protected storage: StorageBrowser
    , private dialog: MatDialog) {
  }

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.wiseNluNerDicLineList.length > 0) {
      this.getDicLineList();
    }
  }

  setSort(sort?: MatSort) {
    if (sort) {
      this.sortParam = sort;
    }

    if (this.wiseNluNerDicLineList.length > 0) {
      this.getDicLineList();
    }
  }

  changeRecord(data) {
    this.param = new DictionaryWiseNluNerEntity();
    this.param.id = data.id;
    this.param.category = data.category || '';
    this.param.rule = data.rule || '';
    this.dictionaryWiseNluNerService.updateDicLine(this.param).subscribe(result => {
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    )
  }

  onUploadFile() {
    // this.header = this.defaultHeader;
    // this.updateToolbar();
    this.getDicLineList();
  }

  getDicLineList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.isTableHidden = false;
    this.param = new DictionaryWiseNluNerEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.orderDirection = this.sortParam.direction === '' ||
    this.sortParam.direction === undefined ? 'desc' : this.sortParam.direction;
    this.param.orderProperty = this.sortParam.active === '' ||
    this.sortParam.active === undefined ? 'createdAt' : this.sortParam.active;
    this.param.versionId = this.selectedDictionary.id;
    this.param.category = this.searchCategory;
    this.param.rule = this.searchRule;

    this.dictionaryWiseNluNerService.getDicLineList(this.param).subscribe(result => {
        if (result) {
          let resultData = result['content'];

          this.lineTableComponent.isCheckedAll = false;
          this.pageLength = resultData['totalElements'];
          this.wiseNluNerDicLineList = resultData['content'];

        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }

  getDicList() {
    this.param = new DictionaryWiseNluNerEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.processType = this.selectedType.attr;
    this.dictionaryWiseNluNerService.getDicList(this.param).subscribe(result => {
        if (result) {
          this.wiseNluNerDicList = [];
          this.wiseNluNerDicList = result['dicList'];

          if (this.selectedDictionary !== '') {
            let index =
              this.wiseNluNerDicList.findIndex(category => category.id === this.selectedDictionary.id);
            this.selectedDictionary = this.wiseNluNerDicList[index];
          }

          if (this.wiseNluNerDicList.length === 0) {
            this.selectedDictionary = '';
            this.isTableHidden = true;
          }

        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }

  ngOnInit() {
    this.progress = this.progressState.info;
    this.header = this.defaultHeader;
    this.initActions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      }
    ];
    this.actions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      },
      {
        type: 'Remove',
        text: 'Remove',
        icon: 'remove_circle_outline',
        callback: this.remove,
        hidden: false
      },
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'Apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'Add a rule',
        text: 'Add a Rule',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'Delete checked rules',
        text: 'Delete checked rules',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Commit',
        icon: 'save',
        callback: this.commit,
        hidden: true
      },
    ];
  }

  changeProcessType() {
    this.selectedDictionary = '';
    this.wiseNluNerDicList = [];
    this.wiseNluNerDicLineList = [];
    this.isTableHidden = true;
    this.getDicList();
  }

  addLine = () => {
    this.param = new DictionaryWiseNluNerEntity();
    this.param.rule = '';
    this.param.category = '';
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.versionId = this.selectedDictionary.id;

    this.dictionaryWiseNluNerService.insertDicLine(this.param).subscribe(result => {
        if (result) {

          this.wiseNluNerDicLineList.unshift({
            rule: '',
            category: '',
            id: result['content'].id
          });
          this.getDicLineList();
        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed add a rule.', 'error');
      }
    );
  };

  add = () => {
    if (this.selectedType === undefined) {
      this.openAlertDialog('Failed', 'Please select process type.', 'error');
    } else {
      let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
      ref.componentInstance.service = this.dictionaryWiseNluNerService;
      ref.componentInstance.meta = {
        workspaceId: this.storage.get('workspaceId'),
        processType: this.selectedType.attr
      };
      ref.afterClosed().subscribe(result => {
        if (result) {
          this.getDicList();
        }
      });
    }
  };

  remove = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed()
    .subscribe(confirmed => {
      if (confirmed) {
        this.dictionaryWiseNluNerService.deleteDic(this.param).subscribe(result => {
            if (result) {
              this.getDicList();
              this.openAlertDialog('Success', 'Success Add FileGroup', 'success');
            } else {
              return false;
            }
          },
          err => {
            console.error(err);
            this.openAlertDialog('Failed', 'Error. Failed Remove Dictionary', 'error');
          }
        );
      }
    });

    this.param = new DictionaryWiseNluNerEntity();
    this.param.id = this.selectedDictionary.id;
  };

  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };

  apply = () => {
    this.param = new DictionaryWiseNluNerEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.id = this.selectedDictionary.id;
    this.param.processType = this.selectedType.attr;

    this.dictionaryWiseNluNerService.apply(this.param).subscribe(
      res => {
        this.openAlertDialog('Apply', `Apply completed`, 'success');
        this.getDicLineList();
      },
      err => {
        this.openAlertDialog('Error', `Server Error. Can not apply.`, 'error');
      }
    );
  };

  updateToolbar() {
    this.actions.forEach(action => {
      action.hidden = !action.hidden;
    });
    this.editable = !this.editable;
  }

  deleteLine = () => {
    let wiseNluNerEntities: DictionaryWiseNluNerEntity[] =
      this.lineTableComponent.rows.filter(item => item.isChecked);

    if (wiseNluNerEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (wiseNluNerEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dictionaryWiseNluNerService.deleteLines(wiseNluNerEntities).subscribe(
            res => {
              if (res) {
                this.getDicLineList();
                this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
              }
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  commit = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new DictionaryWiseNluNerEntity();
          this.param.dicId = this.selectedDictionary.id;
          this.param.id = this.selectedDictionary.id;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.storage.get('workspaceId');
          this.param.title = result.title;
          this.param.message = result.message;
          this.param.versionId = this.selectedDictionary.id;

          this.dictionaryWiseNluNerService.commit(this.param).subscribe(
            res => {
              this.getDicLineList();
              this.header = this.defaultHeader;
              this.updateToolbar();
            },
            err => {
              console.error(err);
            });
        }
      });
  };

  convert() {
    let param = {
      context: this.convertWord,
      workspaceId: this.storage.get('workspaceId')
    };

    this.progress = this.progressState.progressing;
    this.dictionaryWiseNluNerService.convert(param).subscribe(
      res => {
        this.convertResult = res.result;
        this.progress = this.progressState.result;
      },
      err => {
        this.convertResult = 'Error. Converting failed.';
        this.progress = this.progressState.result;
        this.openAlertDialog('Error', `Server Error. Word converting failed.`, 'error');
      }
    )
  }


  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
