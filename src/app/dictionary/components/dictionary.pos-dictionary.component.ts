import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, CommitDialogComponent, DownloadService, StorageBrowser} from 'mlt-shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryLineEntity} from '../entity/dictionary.pos-dictionary-line.entity';
import {PosDictionaryService} from '../services/dictionary.pos-dictionary.service';
import {PosDictionaryEntity} from '../entity/dictionary.pos-dictionary.entity';

type dic_info = {
	id: string;
	dicType: number;
	name: string;
};

type line = {
	id: string;
	dicType: number;
	pattern: string;
	word: string;
	srcPos: string;
	destPos: string;
	compoundType: string;
	versionId: string;
	workspaceId: string;
  description: string;
}

type dic_contents = line[];

enum pos_dic_type {
	MORP_CUSTOM = 0, // 사용자 사전
	MORP_EOJEOL = 1, // 어절 사전
	MORP_COMPOUND_WORD = 2, // 복합 명사 사전
	MORP_POST_PATTERN_STR = 3, // 후처리 패턴 String 사전
	MORP_POST_PATTERN_MORP = 4, // 후처리 패턴 형태소 사전
}

type dictionary_content = {
	name: string;
	type: pos_dic_type;
}

@Component({
	selector: 'app-pos-dictionary-contents',
	templateUrl: './dictionary.pos-dictionary.component.html',
	styleUrls: ['./dictionary.pos-dictionary.component.scss']
})
export class PosDictionaryComponent {
	public gridOptions: GridOptions;
	public defaultColDef;

	row_data: dic_contents = [];
	pre_row_data: dic_contents = [];
	actions: any[] = [];
	editable: boolean = false;
	dictionaries: dictionary_content[] = [
		{name: 'pre custom dictionary', type: pos_dic_type.MORP_CUSTOM},
		{name: 'post pattern  string dictionary', type: pos_dic_type.MORP_POST_PATTERN_STR},
		{name: 'post pattern  morop dictionary', type: pos_dic_type.MORP_POST_PATTERN_MORP},
		{name: 'compound dictionary', type: pos_dic_type.MORP_COMPOUND_WORD}
	];

	dictionary: dic_info = {id: '', name: '', dicType: pos_dic_type.MORP_CUSTOM};
	dictionary_rows: dic_contents = [];
	selected_click_data: HTMLElement = undefined;
	pos_entities: PosDictionaryLineEntity[] = [];
	workspaceId: string;
	versionId: string = undefined;

	constructor(private posDictionaryService: PosDictionaryService,
		@Inject(StorageBrowser) protected storage: StorageBrowser,
		private downloadService: DownloadService,
		private router: Router,
		private route: ActivatedRoute,
		private dialog: MatDialog) {
	}

	ngOnInit() {
		this.route.params.subscribe(par => {
			this.dictionary.name = par['name'];
			if (!this.dictionary.name || this.dictionary.name == '') {
				this.openAlertDialog('Failed', 'Error. Check your dictionary name in a url', 'error');
			}
		});
		this.gridOptions = <GridOptions>{
			columnDefs: this.createColumnDefs(),
			rowData: this.row_data,
			enableColResize: true,
			enableFilter: true,
			enableSorting: true,
			animateRows: true,
			rowSelection: 'multiple',
			defaultColDef: this.defaultColDef,
			onRowSelected: this.onRowSelected,
			onCellValueChanged: (event) => {
				this.updateDictionaryData(event.data);
			},
			onGridReady(params) {
				params.api.sizeColumnsToFit();
			}
		};

		this.actions = [{
			type: 'edit',
			text: 'Edit',
			icon: 'edit_circle_outline',
			callback: this.edit,
			disabled: false,
			hidden: false
		}, {
			type: 'addLine',
			text: 'Add',
			icon: 'playlist_add',
			callback: this.addLine,
			disabled: false,
			hidden: true
		}, {
			type: 'deleteLine',
			text: 'Delete',
			icon: 'delete_sweep',
			disabled: true,
			callback: this.deleteLine,
			hidden: true
		}, {
			type: 'commit', text: 'Commit', icon: 'save', disabled: false, callback: this.commit, hidden: true
		}, {
			type: 'cancel', text: 'Cancel', icon: 'cancel', disabled: false, callback: this.cancel, hidden: true
		}, {
			type: 'download',
			text: 'Download',
			icon: 'file_download',
			disabled: false,
			callback: this.download,
			hidden: false
		}, {
			type: 'upload',
			text: 'Upload',
			icon: 'file_upload',
			disabled: false,
			callback: this.upload,
			hidden: true,
		},];

		this.load();
	}

	back(msg: string) {
		if (msg !== '') {
			this.openAlertDialog('Failed', msg, 'error');
		}
		this.router.navigateByUrl('mlt/dictionary/morph');
	}

	load() {
		this.workspaceId = this.storage.get('workspaceId');
		let pro = new Promise((resolve, reject) => {
			this.posDictionaryService.getDictionary(this.dictionary.name).subscribe(res => {
        if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
					this.dictionary.id = res.dictionary_id;
					this.gridOptions.columnApi.setColumnVisible('no', false);
					resolve();
				} else {
					reject('Error. Check your dictionary name in a url');
				}
			});
		});
		pro.then(() => {
			this.getDictionaryContents();
		}).catch((msg: string) => {
			this.back(msg);
		});
	}

	onRowSelected = (param) => {
		let deleteBtn = this.actions.filter(y => y.type === 'deleteLine')[0];
		if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
			deleteBtn.disabled = false;
		} else {
			deleteBtn.disabled = true;
		}
	}

	getDictionaryContents = () => {
    let param = {
      "workspaceId": this.workspaceId,
      "id": this.dictionary.id
    };
    try {
      this.posDictionaryService.getDictionaryContents(param).subscribe(res => {
        if (res.hasOwnProperty('dictionary_contents')) {
          this.entitiesToData(res['dictionary_contents']);
          this.pos_entities = res.dictionary_contents;
          this.dictionary_rows = res.dictionary_contents;
        }
        this.selectFilter(this.dictionaries[0]);
      });
    } catch (e) {
      this.back("Something wrong!");
    }
  }

	entityToData = (entity: PosDictionaryLineEntity): line => {
		let one_line: line = {
			id: undefined,
			dicType: undefined,
			pattern: undefined,
			word: undefined,
			srcPos: undefined,
			destPos: undefined,
			compoundType: undefined,
			versionId: undefined,
			workspaceId: undefined,
      description: undefined,
		}
		one_line.id = entity.id;
		one_line.dicType = entity.dicType;
		one_line.srcPos = entity.srcPos;
		one_line.pattern = entity.pattern;
		one_line.destPos = entity.destPos;
		one_line.compoundType = entity.compoundType;
		one_line.word = entity.word;
		one_line.versionId = entity.versionId;
		one_line.workspaceId = entity.workspaceId;
		one_line.description = entity.description;
		return one_line;
	}
	
	entitiesToData = (entities: PosDictionaryLineEntity[]) => {
		this.dictionary_rows = [];
		entities.map(x => {
			let one_line: line = this.entityToData(x);
			this.dictionary_rows.push(one_line);
		});
	}

	dataToEntity = (data: line, b_id: boolean=false): PosDictionaryLineEntity => {
		let entity: PosDictionaryLineEntity = new PosDictionaryLineEntity();
		entity.dicType = data.dicType;
		entity.pattern = data.pattern;
		entity.word = data.word;
		entity.srcPos = data.srcPos;
		entity.destPos = data.destPos;
		entity.compoundType = data.compoundType;
		entity.description = data.description;
		if (!b_id) {
			entity.id = data.id;
			entity.versionId = this.dictionary.id;
			entity.workspaceId = this.workspaceId;
		}
		return entity;
	}

	dataToEntities = (data: dic_contents): PosDictionaryLineEntity[] => {
		let entities: PosDictionaryLineEntity[] = [];
		data.map(x => {
			let entity: PosDictionaryLineEntity = this.dataToEntity(x);
			entities.push(entity);
		});
		return entities;
	}

	updateDictionaryData(data) {
		try {
			let param: PosDictionaryLineEntity = this.dataToEntity(data);
			this.posDictionaryService.updateDictionaryLine(param).subscribe();
		} catch (e) {
			this.back('Something wrong!');
		}
	}

	viewTableOption() {
		switch (this.dictionary.dicType) {
			case pos_dic_type.MORP_COMPOUND_WORD:
				this.gridOptions.columnApi.setColumnVisible('word', false);
				this.gridOptions.columnApi.setColumnVisible('pattern', false);
        this.gridOptions.columnApi.setColumnVisible('description', false);
        this.gridOptions.columnApi.setColumnVisible('srcPos', true);
        this.gridOptions.columnApi.setColumnVisible('destPos', true);
				break;
			case pos_dic_type.MORP_CUSTOM:
				this.gridOptions.columnApi.setColumnVisible('srcPos', false);
				this.gridOptions.columnApi.setColumnVisible('destPos', false);
				this.gridOptions.columnApi.setColumnVisible('word', true);
				this.gridOptions.columnApi.setColumnVisible('pattern', true);
        this.gridOptions.columnApi.setColumnVisible('description', true);
				break;
			case pos_dic_type.MORP_POST_PATTERN_STR:
			case pos_dic_type.MORP_POST_PATTERN_MORP:
				this.gridOptions.columnApi.setColumnVisible('word', false);
        this.gridOptions.columnApi.setColumnVisible('description', false);
				this.gridOptions.columnApi.setColumnVisible('srcPos', true);
				this.gridOptions.columnApi.setColumnVisible('destPos', true);
				this.gridOptions.columnApi.setColumnVisible('pattern', true);
				break;
			default:
				break;
		}
	}

	doAction(action) {
		if (action) {
			action.callback();
		}
	}

	updateToolbar = () => {
		this.actions.forEach(action => {
			if (action.type === 'edit' || action.type === 'download') {
				action.hidden = !this.editable;
			} else {
				action.hidden = this.editable;
			}
		});
		this.editable = !this.editable;
		if (this.editable) {
			this.gridOptions.columnApi.setColumnVisible('no', true);
		} else {
			this.gridOptions.api.stopEditing();
			this.gridOptions.columnApi.setColumnVisible('no', false);
			this.getDictionaryContents();
		}
		this.gridOptions.api.getColumnDef('pattern').editable = !this.gridOptions.api.getColumnDef('pattern').editable;
		this.gridOptions.api.getColumnDef('word').editable = !this.gridOptions.api.getColumnDef('word').editable;
    this.gridOptions.api.getColumnDef('description').editable = !this.gridOptions.api.getColumnDef('description').editable;
		this.gridOptions.api.getColumnDef('srcPos').editable = !this.gridOptions.api.getColumnDef('srcPos').editable;
		this.gridOptions.api.getColumnDef('destPos').editable = !this.gridOptions.api.getColumnDef('destPos').editable;
	}

	edit = () => {
		new Promise((resolve, reject) => {
			if (this.pos_entities.length > 0) {
				try {
					this.pos_entities.forEach(entity => {
						entity.versionId = this.dictionary.id;
						entity.workspaceId = this.workspaceId;
					});
					this.posDictionaryService.insertDictionaryLines(this.pos_entities).subscribe(
						res1 => {
							this.pos_entities = [];
              resolve();
						});
				} catch (e) {
					reject();
				}
			} else {
				resolve();
			}
		}).then(() => {
      this.getDictionaryContents();
      this.updateToolbar();
		}).catch(() => {
			this.back('Something wrong!');
		});
	}

	cancel = () => {
		this.updateToolbar();
	}

	openAlertDialog(title, message, status) {
		let ref = this.dialog.open(AlertComponent);
		ref.componentInstance.header = status;
		ref.componentInstance.title = title;
		ref.componentInstance.message = message;
	}

	download = () => {
		let params: PosDictionaryLineEntity[] = this.dataToEntities(this.dictionary_rows);
		this.posDictionaryService.downloadContents(params).subscribe((res) => {
			if (res.hasOwnProperty('dictionary_contents')) {
				this.downloadService.downloadJsonFile(res.dictionary_contents, this.dictionary.name.toString());
			}
		});
		this.openAlertDialog('Download', `Corpus file downloaded successfully.`, 'success');
	}

	upload = () => {
		let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
		elem.click();
	}

	getdata = (param) => {
		let file = param.target.files[0];
		if (file.type.match(/text\/plain/) || file.type.match(/\/json/)) {
			const reader: FileReader = new FileReader();
			reader.onload = (e: any) => {
				try {
					this.posDictionaryService.deleteDictionaryContentsAll(this.dictionary.id).subscribe(
						res => {
							this.posDictionaryService.uploadContents([reader.result, this.dictionary.id,
								this.workspaceId]).subscribe(res2 => {
								if (res2.hasOwnProperty('dictionary_contents')) {
									this.dictionary_rows = [];
									this.dictionary_rows = res2.dictionary_contents;
									this.selectFilter(this.dictionaries[0]);
								}
							});
						}
					);
				} catch (ex) {
					this.openAlertDialog('Failed', `Check your corpus file data.`, 'error');
					return;
				}
			};
			reader.readAsText(file);
		} else {
			this.openAlertDialog('Failed', `Check your corpus file format.`, 'error');
		}
	}

	addLine = () => {
		let param: PosDictionaryLineEntity = new PosDictionaryLineEntity();
		param.versionId = this.dictionary.id;
		param.dicType = this.dictionary.dicType;
		param.workspaceId = this.workspaceId;
		this.posDictionaryService.insertDictionaryLine(param).subscribe(
			res => {
				if (res && res.dictionary_line) {
					this.gridOptions.api.updateRowData({add: [res.dictionary_line]});
					this.dictionary_rows.push(res.dictionary_line);
				}
			}, err => {
				this.openAlertDialog('Add Line', 'Something wrong.', 'error');
			}
		);
	}

	deleteLine = () => {
		let selectedData = this.gridOptions.api.getSelectedRows();
		return new Promise((resolve, reject) => {
			this.posDictionaryService.deleteDictionaryContents(selectedData).subscribe(
				res => {
					resolve();
				}, err => {
					reject();
				}
			);
		}).then(() => {
			this.gridOptions.api.updateRowData({remove: selectedData});
			this.dictionary_rows = this.dictionary_rows.filter(function(e) {
				return this.indexOf(e.id) < 0;
			}, selectedData.map(data => data.id));
		});
	}

	commit = () => {
		this.gridOptions.api.stopEditing();
		let ref = this.dialog.open(CommitDialogComponent);
		ref.componentInstance.title = `${this.dictionary.name} updated`;
		new Promise((resolve, reject) => {
			ref.afterClosed().subscribe(
				result => {
					if (result) {
						let param: PosDictionaryEntity = new PosDictionaryEntity();
						param.name = this.dictionary.name;
						param.id = this.dictionary.id;
						param.version = this.dictionary.id;
						param.creatorId = this.storage.get('user').id;
						param.workspaceId = this.workspaceId;
						this.posDictionaryService.commitDictionary(param).subscribe(
							res => {
								if (res.hasOwnProperty('entities')) {
									this.dictionary_rows = res.entities;
									resolve();
								} else {
									reject('No data to commit.');
								}
							},
							err => {
								reject('Something wrong.');
							});
					}
				});
		}).then(() => {
			this.selectFilter(this.dictionaries[0]);
			this.updateToolbar();
		}).catch((msg: string) => {
			this.openAlertDialog('Delete Lines', msg, 'error');
		});
	}

	createColumnDefs() {
		return [{
			headerName: '#',
			field: 'no',
			width: 35,
			checkboxSelection: true,
			headerCheckboxSelection: true,
			suppressSorting: true,
			suppressMenu: true,
			pinned: true,
		}, {
			headerName: 'Id',
			field: 'id',
			width: 1,
			filter: 'agTextColumnFilter',
			editable: false,
			hide: true
		}, {
			headerName: 'Dictionary Type',
			field: 'dicType',
			width: 1,
			filter: 'agTextColumnFilter',
			editable: false,
			hide: true
		}, {
			headerName: 'Word',
			field: 'word',
			width: 200,
			filter: 'agTextColumnFilter',
			editable: false,
		}, {
			headerName: 'Origin POS',
			field: 'srcPos',
			width: 200,
			filter: 'agTextColumnFilter',
			editable: false,
		}, {
			headerName: 'Change POS',
			field: 'destPos',
			width: 200,
			filter: 'agTextColumnFilter',
			editable: false,
		}, {
			headerName: 'Pattern',
			field: 'pattern',
			width: 300,
			filter: 'agTextColumnFilter',
			editable: false,
		}, {
			headerName: 'WorkspaceId',
			field: 'workspaceId',
			width: 1,
			filter: 'agTextColumnFilter',
			editable: false,
			hide: true
		}, {
      headerName: 'Description',
      field: 'description',
      width: 450,
      filter: 'agTextColumnFilter',
      editable: false,
    }
		];
	}

	selectFilter(tag: dictionary_content) {
		this.gridOptions.api.setRowData([]);
		if (this.selected_click_data !== undefined) {
			this.selected_click_data.style.background = '#E0E0E0';
		}
		this.selected_click_data = <HTMLInputElement> document.getElementById(tag.name);
		this.dictionary.dicType = tag.type;
		this.selected_click_data.style.background = '#47A8E5';
		this.viewTableOption();
		this.row_data = this.dictionary_rows.filter(x => x.dicType === this.dictionary.dicType);
		this.gridOptions.api.setRowData(this.row_data);
	}

	nlpTest = () => {
		this.router.navigateByUrl('mlt/dictionary/morph/' + this.dictionary.name + '/test');
	}
}
