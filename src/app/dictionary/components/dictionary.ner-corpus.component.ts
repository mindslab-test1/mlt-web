import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, DownloadService, StorageBrowser} from 'mlt-shared';
import {NerCorpusService} from '../services/dictionary.ner-corpus.service';
import {NerCorpusEntity} from '../entity/dictionary.ner-corpus.entity';

type item = {
  sentence: ''
};
type corpus = item[];

@Component({
  selector: 'app-ner-corpus',
  templateUrl: './dictionary.ner-corpus.component.html',
  styleUrls: ['./dictionary.ner-corpus.component.scss']
})
export class NerCorpusComponent {
  public gridApi;
  public gridColumnApi;
  public gridOptions: GridOptions;
  public defaultColDef;
  entityMap = {
    '&': '&amp;', '<': '&lt;', '>': '&gt;'
  };
  row_data: corpus = [];
  pre_row_data: corpus = [];
  sentences: string[] = [];
  actions: any[] = [];
  editable: boolean = false;
  title = 'app';
  selected: string = 'ALL';
  selected_click_data: HTMLInputElement = undefined;
  tags: string[] = [];
  pre_tags: string[] = [];

  constructor(private nerCorpusService: NerCorpusService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private downloadService: DownloadService,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
			onRowSelected: this.onRowSelected,
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };
    this.actions = [{
      type: 'edit',
      text: 'Edit Sentence',
      icon: 'edit_circle_outline',
      disabled: false,
      callback: this.edit,
      hidden: false
    }, {
      type: 'addLine', text: 'Add', icon: 'playlist_add', disabled: false, callback: this.addLine, hidden: true
    }, {
      type: 'cancel', text: 'Cancel', icon: 'cancel', callback: this.cancel, hidden: true
    }, {
      type: 'deleteLine',
	    text: 'Delete',
	    icon: 'delete_sweep',
      disabled: true,
	    callback: this.deleteLine,
	    hidden: true
    }, {
	    type: 'save', text: 'Save', icon: 'save', disabled: false, callback: this.save, hidden: true
    }, {
	    type: 'download',
	    text: 'Download',
	    icon: 'file_download',
			disabled: false,
	    callback: this.download,
	    hidden: false
    }, {
	    type: 'upload',
	    text: 'Upload',
	    icon: 'file_upload',
			disabled: false,
	    callback: this.upload,
	    hidden: true,
    },];
    this.load();
  }

  load = () => {
    this.nerCorpusService.getCorpus().subscribe(
      res => {
        this.tags = [];
        this.sentences = [];
        if (res.hasOwnProperty('corpus_list')) {
          res.corpus_list.forEach(x => {
            this.sentences.push(x.sentence);
            this.getTags(x.sentence);
          });
        }
        this.row_data = this.createrowData();
        this.gridOptions.api.setRowData(this.row_data);
        this.gridOptions.columnApi.setColumnVisible('no', false);
        this.selectFilter('ALL');
      }, err => {
        this.openAlertDialog('Load', 'Something wrong!', 'error');
      }
    );
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'download') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    if (this.editable) {
      this.gridOptions.columnApi.setColumnVisible('no', true);
    } else {
      this.gridOptions.api.stopEditing();
      this.gridOptions.columnApi.setColumnVisible('no', false);
    }
  }

  edit = () => {
    this.pre_row_data = this.row_data;
    this.pre_tags = this.tags;
    this.gridOptions.api.getColumnDef('sentence').editable = true;
    this.tags = [];
    this.updateToolbar();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  download = () => {
    let res: any = this.row_data;
    this.downloadService.downloadJsonFile(JSON.stringify(res), 'corpus_for_ner_test');
    this.openAlertDialog('Download', `Corpus file downloaded successfully.`, 'success');
  }

  upload = () => {
    let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
    elem.click();
  }

  getdata = (param) => {
    let file = param.target.files[0];
    if (file.type.match(/\/json/)) {
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        try {
          let load_data: corpus = JSON.parse(reader.result);
          this.gridOptions.api.setRowData(load_data);
          this.openAlertDialog('Upload', `Your corpus file uploaded successfully.`, 'success');
        } catch (ex) {
          this.openAlertDialog('Upload', `Check your corpus file data.`, 'error');
          return;
        }
      };
      reader.readAsText(file);
    } else {
      this.openAlertDialog('Upload', `Check your corpus file format.`, 'error');
    }
  }

  addLine = () => {
    let newItem = {
      sentence: '',
    };
    let res = this.gridOptions.api.updateRowData({add: [newItem]});
  }

  deleteLine = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    this.gridOptions.api.updateRowData({remove: selectedData});
  }

  cancel = () => {
    this.row_data = this.pre_row_data;
    this.tags = this.pre_tags;
    this.gridOptions.api.setRowData(this.row_data);
    this.updateToolbar();
  }

  save = () => {
    this.gridOptions.api.stopEditing();
    let params: NerCorpusEntity[] = [];
    this.tags = [];
    this.gridOptions.api.forEachNode(function(node) {
      let param: NerCorpusEntity = new NerCorpusEntity();
      param = node.data;
      params.push(param);
    });

    return new Promise((res2, rej2) => {
      this.nerCorpusService.deleteLines().subscribe(
        res => {
          res2(params);
        }, err => {
          rej2();
        }
      );
    }).then((par: NerCorpusEntity[]) => {
      this.gridOptions.api.getColumnDef('sentence').editable = false;
      this.nerCorpusService.insertLines(par).subscribe(
        res => {
          if (res.hasOwnProperty('list')) {
            res.list.forEach(x => {
              this.getTags(x.sentence);
            })
          }
          this.updateToolbar();
          this.openAlertDialog('Save', 'Saved successfully', 'success');
        }, err => {
          throw err;
        }
      );
    }).catch(() => {
      this.gridOptions.api.getColumnDef('sentence').editable = true;
      this.openAlertDialog('Save', 'Something wrong!', 'error');
    });
  }

  getTags = (sentence: string) => {
    let str = sentence.replace(/[&<>]/g, s => ({
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;'
    })[s]);
    let tmp: string[] = str.match(/&lt;ne&gt;(.*?)&lt;\/ne&gt;/g);
    if (tmp !== null) {
      tmp.forEach(x => {
        let tag = x.match('&lt;ne&gt;(.*?)&lt;/ne&gt;');
        if (tag.hasOwnProperty('1') && this.tags.indexOf(tag[1]) < 0) {
          this.tags.push(tag[1]);
        }
      });
    }
  }

  sentenceCellRenderer = (params) => {
    let renderer_sentence: string = '';
    renderer_sentence = params.data.sentence.replace(/[&<>]/g, s => this.entityMap[s]);
    let res: any = renderer_sentence.match(/&lt;ne&gt;(.*?)&lt;\/ne&gt;/g);
    if (res) {
      res.forEach((elem) => {
        renderer_sentence = renderer_sentence.replace(elem, '<span style="color: #E57373;">' + elem + '</span>');
      });
    }
    return renderer_sentence;
  }

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 35,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Sentence',
      field: 'sentence',
      width: 500,
      cellRenderer: this.sentenceCellRenderer,
      filter: 'agTextColumnFilter',
      editable: false,
      cellEditorSelector: function(params) {
        if (params.data.type === 'sentence') {
          return {
            component: 'agLargeTextCellEditor'
          };
        }
      }
    }]
  }

  createrowData(): corpus {
    let result = [];
    for (let i in this.sentences) {
      result.push({sentence: this.sentences[i]});
    }
    return result;
  }

	onRowSelected = (param) => {
		let deleteBtn = this.actions.filter(y => y.type === 'deleteLine')[0];
		if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
			deleteBtn.disabled = false;
		} else {
			deleteBtn.disabled = true;
		}
	}

  selectFilter = (tag) => {
    if (this.selected_click_data) {
      this.selected_click_data.style.background = '#ffffff';
    }
    if (this.selected === tag) {
      this.selected = 'ALL';
      this.selected_click_data = <HTMLInputElement> document.getElementById('ALL');
    } else {
      this.selected = tag;
      this.selected_click_data = <HTMLInputElement> document.getElementById(tag);
    }
    this.selected_click_data.style.background = '#c1d9ff';
    this.gridOptions.api.getFilterInstance('sentence').setModel({
      type: 'contains',
      filter: this.selected === 'ALL' ? '' : '<ne>' + this.selected + '</ne>',
      filterTo: null
    });
    this.gridOptions.api.onFilterChanged();
  }
}
