import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryService} from '../services/dictionary.pos-dictionary.service';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {TestResultService} from '../services/dictionary.test-result.service';

type result_detail = {
  sentence: string;
  nes: string;
  morphs: string;
}

enum dic_type {
  NER = 0, // 개체명 사전
  MORPH = 1, // 형태소 사전
}

@Component({
  selector: 'app-test-result-detail',
  templateUrl: './dictionary.test-result-detail.component.html',
  styleUrls: ['./dictionary.test-result-detail.component.scss']
})
export class TestResultDetailComponent {

  public gridOptions: GridOptions;
  public defaultColDef;

  dictionaryName: string = '';
  dictionaryType: string = '';
  testId: number = 0;
  dictionaryId: string = '';
  row_data: result_detail[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private posDictionaryService: PosDictionaryService,
    private nerDictionaryService: NerDictionaryService,
    private testResultService: TestResultService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      defaultColDef: this.defaultColDef,
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };
    this.load();
  }

  load = () => {
    this.route.url.subscribe(par => {
      this.dictionaryName = par[1].path;
      this.dictionaryType = par[0].path;
      this.testId = parseInt(par[3].path, 10);
      if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.NER) {
        this.nerDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
          if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
            this.dictionaryId = res.dictionary_id;
            this.getData();
          }
        });
      } else if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.MORPH) {
        this.posDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
          if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
            this.dictionaryId = res.dictionary_id;
          }
          this.getData();
        });
      }
    }, err => {
      this.openAlertDialog('Load', 'Something wrong.', 'error');
      window.history.back();
    });
  };

  getData = () => {
    this.row_data = [];
    if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.NER) {
      this.gridOptions.columnApi.setColumnVisible('morphs', false);
      this.gridOptions.columnApi.setColumnVisible('nes', true);
    } else if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.MORPH) {
      this.gridOptions.columnApi.setColumnVisible('nes', false);
      this.gridOptions.columnApi.setColumnVisible('morphs', true);
    }
    this.testResultService.getResultDetail(this.dictionaryType, this.testId).subscribe(
      res => {
        if (res.hasOwnProperty('detail')) {
          let contents: result_detail[] = JSON.parse(res.detail.testResult);
          contents.forEach(one => {
            let elem: result_detail = {
              sentence: '',
              nes: '',
              morphs: ''
            }
            elem.sentence = one.sentence;
            elem.nes = one.nes;
            elem.morphs = one.morphs;
            this.row_data.push(elem);
          });
          this.gridOptions.api.setRowData(this.row_data);
        }
      }, err => {
        this.openAlertDialog('Load Result', 'Something wrong!', 'error');
      }
    );
  };

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 1,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
      hide: true
    }, {
      headerName: 'Sentence',
      field: 'sentence',
      width: 100,
      editable: false
    }, {
      headerName: 'Morphs',
      field: 'morphs',
      width: 50,
      editable: false
    }, {
      headerName: 'NES',
      field: 'nes',
      width: 50,
      editable: false
    },]
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  back = () => {
    if (this.dictionaryType != '') {
      this.router.navigateByUrl('mlt/dictionary/' + this.dictionaryType + '/' + this.dictionaryName + '/test');
    } else {
    }
  }
}
