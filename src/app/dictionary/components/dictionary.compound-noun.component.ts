import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryDictionaryUpsertDialogComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {CompoundNounEntity} from '../entity/dictionary.compound-noun.entity';
import {DictionaryCompoundNounService} from '../services/dictionary.compound-noun.service';

@Component({
  selector: 'app-compound-noun',
  templateUrl: './dictionary.compound-noun.component.html',
  styleUrls: ['./dictionary.compound-noun.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DictionaryCompoundNounComponent implements OnInit {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;

  searchWord = '';
  compoundNounDicLineList: any[] = [];
  compoundNounDicList = [];
  pageLength;
  editable = false;
  actions: any[] = [];
  initActions: any[] = [];
  pageParam: MatPaginator;
  param: CompoundNounEntity = new CompoundNounEntity();

  defaultHeader = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'word', name: 'Word'},
    {attr: 'pos', name: 'POS'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'word', name: 'Word', input: true},
    {attr: 'pos', name: 'POS', input: true}
  ];

  header = [];

  dataSource: MatTableDataSource<any>;
  selectedDictionary: any = '';
  isTableHidden = true;

  constructor(private dictionaryCompoundNounService: DictionaryCompoundNounService
    , @Inject(StorageBrowser) protected storage: StorageBrowser
    , private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getDicList();
    this.header = this.defaultHeader;
    this.initActions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      }
    ];
    this.actions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      },
      {
        type: 'Remove',
        text: 'Remove',
        icon: 'remove_circle_outline',
        callback: this.remove,
        hidden: false
      },
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'Apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'Add a word',
        text: 'Add a word',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'Delete checked rules',
        text: 'Delete checked rules',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Commit',
        icon: 'save',
        callback: this.commit,
        hidden: true
      },
    ];

  }

  getPaginator(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.compoundNounDicLineList.length > 0) {
      this.getDicLineList();
    }
  }

  changeRecord(data) {
    this.param = new CompoundNounEntity();
    this.param.id = data.id;
    this.param.pos = data.pos || '';
    this.param.word = data.word || '';
    this.dictionaryCompoundNounService.updateDicLine(this.param).subscribe(result => {
      },
      err => {
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    )
  }

  onUploadFile() {
    // this.header = this.defaultHeader;
    // this.updateToolbar();
    this.getDicLineList();
  }

  getDicLineList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.isTableHidden = false;
    this.param = new CompoundNounEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.versionId = this.selectedDictionary.id;
    this.param.word = this.searchWord;

    this.dictionaryCompoundNounService.getDicLineList(this.param).subscribe(result => {
        if (result) {
          let resultData = result['content'];
          this.lineTableComponent.isCheckedAll = false;
          this.pageLength = resultData['totalElements'];
          this.compoundNounDicLineList = resultData['content'];

        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }


  getDicList() {
    this.param = new CompoundNounEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.compoundNounDicList.length = 0;
    this.dictionaryCompoundNounService.getDicList(this.param).subscribe(result => {
        if (result) {
          console.log('result', result);
          result['dicList'].forEach((value) => {
            this.compoundNounDicList.push({id: value.id, name: value.name});
          });
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }


  addLine = () => {
    this.param = new CompoundNounEntity();
    this.param.word = '';
    this.param.pos = '';
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.versionId = this.selectedDictionary.id;


    this.dictionaryCompoundNounService.insertDicLine(this.param).subscribe(result => {
        if (result) {

          this.compoundNounDicLineList.unshift({
            word: '',
            pos: '',
            id: result['content'].id
          });
          this.getDicLineList();
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed add a word', 'error');
      }
    );

  };
  add = () => {
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
    ref.componentInstance.service = this.dictionaryCompoundNounService;
    ref.componentInstance.meta = {
      workspaceId: this.storage.get('workspaceId'),
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.getDicList();
      }
    })
  };
  remove = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed()
    .subscribe(confirmed => {
      if (confirmed) {
        this.dictionaryCompoundNounService.deleteDic(this.param).subscribe(result => {
            if (result) {
              this.getDicList();

              if (this.compoundNounDicList.length === 0) {
                this.selectedDictionary = '';
                this.isTableHidden = true;
              }

              this.openAlertDialog('Success', 'Success Add FileGroup', 'success');
            } else {
              return false;
            }
          },
          err => {
            console.log(err);
            this.openAlertDialog('Failed', 'Error. Failed Remove Dictionary', 'error');
          }
        );
      }
    });

    this.param = new CompoundNounEntity();
    this.param.id = this.selectedDictionary.id;

  };
  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };
  apply = () => {
    this.param = new CompoundNounEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.id = this.selectedDictionary.id;

    this.dictionaryCompoundNounService.apply(this.param).subscribe(
      res => {
        if (res) {
          this.getDicLineList();
          this.openAlertDialog('Apply', `Apply completed`, 'success');
        }
      },
      err => {
        this.openAlertDialog('Error', `Server Error. Can not apply.`, 'error');
      }
    );

  };

  updateToolbar() {
    this.actions.forEach(action => {
      action.hidden = !action.hidden;
    });
    this.editable = !this.editable;
  }


  deleteLine = () => {
    let compoundNounEntities: CompoundNounEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (compoundNounEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (compoundNounEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dictionaryCompoundNounService.deleteLines(compoundNounEntities).subscribe(
            res => {
              if (res) {
                this.getDicLineList();
                this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
              }
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  commit = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new CompoundNounEntity();
          this.param.dicId = this.selectedDictionary.id;
          this.param.id = this.selectedDictionary.id;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.storage.get('workspaceId');
          this.param.title = result.title;
          this.param.message = result.message;
          this.param.versionId = this.selectedDictionary.id;

          this.dictionaryCompoundNounService.commit(this.param).subscribe(
            res => {
              this.getDicLineList();
              this.header = this.defaultHeader;
              this.updateToolbar();
            },
            err => {
              console.log(err);
            });
        }
      });


  };

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}



