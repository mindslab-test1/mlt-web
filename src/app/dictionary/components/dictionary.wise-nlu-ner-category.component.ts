import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryDictionaryUpsertDialogComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {DictionaryWiseNluNerCategoryEntity} from '../entity/dictionary.wise-nlu-ner-category.entity';
import {DictionaryWiseNluNerCategoryService} from '../services/dictionary.wise-nlu-ner-category.service';

@Component({
  selector: 'app-wise-nlu-ner-category',
  templateUrl: './dictionary.wise-nlu-ner-category.component.html',
  styleUrls: ['./dictionary.wise-nlu-ner-category.component.scss'],
})
export class DictionaryWiseNluNerCategoryComponent implements OnInit {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;

  searchNewCategory = '';
  searchOldCategory = '';

  searchWord = '';
  wiseNluNerCategoryLineList: any[] = [];
  wiseNluNerCategoryList = [];
  pageLength;
  editable = false;
  actions: any[] = [];
  initActions: any[] = [];
  pageParam: MatPaginator;
  sortParam: MatSort;
  tipsVisible = false;
  param: DictionaryWiseNluNerCategoryEntity = new DictionaryWiseNluNerCategoryEntity();
  isTableHidden = true;

  defaultHeader = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'oldCategory', name: 'Old Category', width: '10%', isSort: true},
    {attr: 'newCategory', name: 'New Category', width: '10%', isSort: true},
    {attr: 'word', name: 'Word', width: '70%'},
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'oldCategory', name: 'Old Category', input: true, width: '10%'},
    {attr: 'newCategory', name: 'New Category', input: true, width: '10%'},
    {attr: 'word', name: 'Word', input: true, width: '70%'},
  ];

  header = [];

  dataSource: MatTableDataSource<any>;
  selectedDictionary: any = '';

  constructor(private dictionaryWiseNluNerCategoryService: DictionaryWiseNluNerCategoryService
    , @Inject(StorageBrowser) protected storage: StorageBrowser
    , private dialog: MatDialog) {
  }

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.wiseNluNerCategoryLineList.length > 0) {
      this.getDicLineList();
    }
  }

  setSort(sort?: MatSort) {
    if (sort) {
      this.sortParam = sort;
    }

    if (this.wiseNluNerCategoryLineList.length > 0) {
      this.getDicLineList();
    }
  }

  changeRecord(data) {
    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.id = data.id;
    this.param.oldCategory = data.oldCategory || '';
    this.param.newCategory = data.newCategory || '';
    this.param.word = data.word || '';
    this.dictionaryWiseNluNerCategoryService.updateDicLine(this.param).subscribe(result => {
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    )
  }

  onUploadFile() {
    // this.header = this.defaultHeader;
    // this.updateToolbar();
    this.getDicLineList();
  }

  getDicLineList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.isTableHidden = false;
    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.orderDirection = this.sortParam.direction === '' ||
    this.sortParam.direction === undefined ? 'desc' : this.sortParam.direction;
    this.param.orderProperty = this.sortParam.active === '' ||
    this.sortParam.active === undefined ? 'createdAt' : this.sortParam.active;
    this.param.versionId = this.selectedDictionary.id;
    this.param.oldCategory = this.searchOldCategory;
    this.param.newCategory = this.searchNewCategory;
    this.param.word = this.searchWord;

    this.dictionaryWiseNluNerCategoryService.getDicLineList(this.param).subscribe(result => {
        if (result) {
          let resultData = result['content'];

          this.lineTableComponent.isCheckedAll = false;
          this.pageLength = resultData['totalElements'];
          this.wiseNluNerCategoryLineList = resultData['content'];

        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }

  getDicList() {
    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.dictionaryWiseNluNerCategoryService.getDicList(this.param).subscribe(result => {
        if (result) {
          this.wiseNluNerCategoryList = [];
          this.wiseNluNerCategoryList = result['dicList'];
          if (this.selectedDictionary !== '') {
            let index =
              this.wiseNluNerCategoryList.findIndex(category => category.id === this.selectedDictionary.id);
            this.selectedDictionary = this.wiseNluNerCategoryList[index];
          }

          if (this.wiseNluNerCategoryList.length === 0) {
            this.selectedDictionary = '';
            this.isTableHidden = true;
          }

        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }

  ngOnInit() {
    this.getDicList();
    this.header = this.defaultHeader;
    this.initActions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      }
    ];
    this.actions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      },
      {
        type: 'Remove',
        text: 'Remove',
        icon: 'remove_circle_outline',
        callback: this.remove,
        hidden: false
      },
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'Apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'Add a word',
        text: 'Add a Word',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'Delete checked words',
        text: 'Delete checked words',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Commit',
        icon: 'save',
        callback: this.commit,
        hidden: true
      },
    ];

  }

  addLine = () => {
    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.word = '';
    this.param.oldCategory = '';
    this.param.newCategory = '';
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.versionId = this.selectedDictionary.id;

    this.dictionaryWiseNluNerCategoryService.insertDicLine(this.param).subscribe(result => {
        if (result) {
          this.wiseNluNerCategoryLineList.unshift({
            word: '',
            oldCategory: '',
            newCategory: '',
            id: result['content'].id
          });
          this.getDicLineList();
        } else {
          return false;
        }
      },
      err => {
        console.error(err);
        this.openAlertDialog('Failed', 'Error. Failed add a word', 'error');
      }
    );
  };

  add = () => {
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
    ref.componentInstance.service = this.dictionaryWiseNluNerCategoryService;
    ref.componentInstance.meta = {
      workspaceId: this.storage.get('workspaceId'),
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.getDicList();
      }
    })
  };

  remove = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed()
    .subscribe(confirmed => {
      if (confirmed) {
        this.dictionaryWiseNluNerCategoryService.deleteDic(this.param).subscribe(result => {
            if (result) {
              this.getDicList();

              if (this.wiseNluNerCategoryList.length === 0) {
                this.selectedDictionary = '';
                this.isTableHidden = true;
              }

              this.openAlertDialog('Success', 'Success Add FileGroup', 'success');
            } else {
              return false;
            }
          },
          err => {
            console.error(err);
            this.openAlertDialog('Failed', 'Error. Failed Remove Dictionary', 'error');
          }
        );
      }
    });

    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.id = this.selectedDictionary.id;
  };

  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };

  apply = () => {
    this.param = new DictionaryWiseNluNerCategoryEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.versionId = this.selectedDictionary.id;

    this.dictionaryWiseNluNerCategoryService.apply(this.param).subscribe(
      res => {
        this.openAlertDialog('Apply', `Apply completed`, 'success');
        this.getDicLineList();
      },
      err => {
        this.openAlertDialog('Error', `Server Error. Can not apply.`, 'error');
      }
    );
  };

  updateToolbar() {
    this.actions.forEach(action => {
      action.hidden = !action.hidden;
    });
    this.editable = !this.editable;
  }

  deleteLine = () => {
    let wiseNluNerEntities: DictionaryWiseNluNerCategoryEntity[] =
      this.lineTableComponent.rows.filter(item => item.isChecked);

    if (wiseNluNerEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (wiseNluNerEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dictionaryWiseNluNerCategoryService.deleteLines(wiseNluNerEntities).subscribe(
            res => {
              if (res) {
                this.getDicLineList();
                this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
              }
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  commit = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new DictionaryWiseNluNerCategoryEntity();
          this.param.dicId = this.selectedDictionary.id;
          this.param.id = this.selectedDictionary.id;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.storage.get('workspaceId');
          this.param.title = result.title;
          this.param.message = result.message;
          this.param.versionId = this.selectedDictionary.id;

          this.dictionaryWiseNluNerCategoryService.commit(this.param).subscribe(
            res => {
              this.getDicLineList();
              this.header = this.defaultHeader;
              this.updateToolbar();
            },
            err => {
              console.error(err);
            });
        }
      });
  };

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
