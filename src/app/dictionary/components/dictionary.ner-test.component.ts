import {Component, Inject} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TestResultService} from '../services/dictionary.test-result.service';

enum ner_dic_type {
	NE_PRE_PATTERN = 0, // 전처리 패턴 사전
	NE_POST_PATTERN = 1, // 후처리 패전 사전
	NE_POST_CHANGE = 2, // 후처리 태그 변환 사전
	NE_FILTER = 3, // 예외 처리 사전
	NE_NEW_TAG = 4, // 태그 추가 사전
	NE_ADD_PRE_DIC = 5, // 전처리 추가 사전
	NE_ADD_POST_DIC = 6, // 추처리 추가 사전
	NE_ETC_DIC = 7 // 기타 사전
}

@Component({
	selector: 'app-ner-test',
	templateUrl: './dictionary.ner-test.component.html',
	styleUrls: ['./dictionary.ner-test.component.scss']
})
export class NerTestComponent {

	test_dictionaries = [
		{name: 'post pattern dictionary', value: ner_dic_type.NE_POST_PATTERN, checked: false, disabled: true},
		{name: 'pre pattern dictionary', value: ner_dic_type.NE_PRE_PATTERN, checked: false, disabled: true},
		{name: 'change dictionary', value: ner_dic_type.NE_POST_CHANGE, checked: false, disabled: true},
		{name: 'post add dictionary', value: ner_dic_type.NE_ADD_POST_DIC, checked: false, disabled: true},
		{name: 'pre add dictionary', value: ner_dic_type.NE_ADD_PRE_DIC, checked: false, disabled: true}
	];

  checkedList: number[] = [];

	dictionaryName: string = '';
	dictionaryId: string = '';
	dictionaryType: string = '';
  workspaceId: string = '';

  isReset: boolean;
	isTest: boolean;

	test_btn: HTMLElement;
	reset_btn: HTMLElement;
  testStat: number = 0;

	constructor(private nerDictionaryService: NerDictionaryService,
    private testResultService: TestResultService,
		private route: ActivatedRoute,
		private router: Router,
		@Inject(StorageBrowser) protected storage: StorageBrowser,
		private dialog: MatDialog) {
	}

	ngOnInit() {
    this.isReset = false;
    this.isTest = false;
    this.changeBtnColor();

    this.load();
	}

	load = () => {
		new Promise((resolve, reject) => {
      // 파일 사전 기본 정보 가져오기
      this.route.url.subscribe(par => {
        this.dictionaryType = par[0].path;
        this.dictionaryName = par[1].path;
        this.nerDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
          if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
            this.dictionaryId = res.dictionary_id;
            this.workspaceId = this.storage.get('workspace')[0].id;
            resolve();
          } else {
            reject('Error. Something wrong!');
          }
        }, err => {
          reject('Error. Something wrong!');
        });
      }, err2 => reject('Error. Something wrong!'));
    }).then(() => {
      this.nerDictionaryService.getDictionaryContentsTypes(this.dictionaryId).subscribe(res => {
        if (res.hasOwnProperty('list')) {
          let typeList: number[] = res.list;
          this.test_dictionaries.forEach(x => {
            if (typeList.includes(x.value)) {
              x.disabled = false;
            }
          });
        }
      });
      this.checkButton();
    }).catch((msg: string) => {
      this.back(msg);
    });
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  changeBtnColor = () => {
    this.test_btn = <HTMLElement> document.getElementById('test-btn');
    this.test_btn.style.backgroundColor = this.isTest ? '#3b78e7' : '#777777';
    this.reset_btn = <HTMLElement> document.getElementById('reset-btn');
    this.reset_btn.style.backgroundColor = this.isReset ? '#3b78e7' : '#777777';
  }

  test = () => {
    // 사전 적용 및 테스트 결과  보여주기
    new Promise((resolve, reject) => {
      let param = {
        "checkedList": this.checkedList,
        "isReset": false
      }
      this.isReset = false;
      this.isTest = false;
      this.changeBtnColor();
      this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
        resolve();
      }, err => {
        reject();
      });
    }).then(() => {
      let tmp = this.dictionaryId;
      this.dictionaryId = '';
      this.dictionaryId = tmp;
      this.testStat += 1;
      this.checkButton();
      this.openAlertDialog('Test', 'Test done.', 'success');
    }).catch(() => {
      this.checkButton();
      this.openAlertDialog('Test', 'Something wrong', 'error');
    });
  }

  reset = () => {
    let param = {
      "checkedList": this.checkedList,
	    "isReset": true
    }
    this.isReset = false;
    this.isTest = false;
    this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
    }, error1 => {
      this.openAlertDialog('Reset', 'Something wrong!', 'error');
    });
    this.checkButton();
  }

  back = (msg: string) => {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    } else {
      this.router.navigateByUrl('mlt/dictionary/ner/' + this.dictionaryName);
    }
  }

  clickCheckbox = (val) => {
    let target = this.test_dictionaries.filter(x => x.value === val)[0];
    target.checked = !target.checked;
    this.checkButton();
  }

  checkButton = () => {
    this.checkedList = this.test_dictionaries.filter(x => x.checked == true  && x.disabled == false).map(e => e.value);
    if (this.checkedList.length > 0) {
      this.isTest = true;
      this.isReset = true;
    } else {
      this.isTest = false;
      this.isReset = false;
    }
    this.changeBtnColor();
  }
}
