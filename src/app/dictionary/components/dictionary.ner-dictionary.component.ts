import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, CommitDialogComponent, DownloadService, StorageBrowser} from 'mlt-shared';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {NerDictionaryLineEntity} from '../entity/dictionary.ner-dictionary-line.entity';
import {ActivatedRoute, Router} from '@angular/router';
import {NerDictionaryEntity} from '../entity/dictionary.ner-dictionary.entity';

type dic_info = {
  id: string;
  dicType: number;
  name: string;
};

type line = {
  id: string;
  dicType: number;
  tag: string;
  pattern: string;
  word: string;
  originTag: string;
  changeTag: string;
  versionId: string;
  workspaceId: string;
}

type dic_contents = line[];

enum ner_dic_type {
  NE_PRE_PATTERN = 0, // 전처리 패턴 사전
  NE_POST_PATTERN = 1, // 후처리 패전 사전
  NE_POST_CHANGE = 2, // 후처리 태그 변환 사전
  NE_FILTER = 3, // 예외 처리 사전
  NE_NEW_TAG = 4, // 태그 추가 사전
  NE_ADD_PRE_DIC = 5, // 전처리 추가 사전
  NE_ADD_POST_DIC = 6, // 추처리 추가 사전
  NE_ETC_DIC = 7 // 기타 사전
}

type dictionary_content = {
  name: string;
  type: ner_dic_type;
}

@Component({
  selector: 'app-ner-dictionary-contents',
  templateUrl: './dictionary.ner-dictionary.component.html',
  styleUrls: ['./dictionary.ner-dictionary.component.scss']
})
export class NerDictionaryComponent {
  public gridOptions: GridOptions;
  public defaultColDef;

  row_data: dic_contents = [];
  pre_row_data: dic_contents = [];
  actions: any[] = [];
  editable: boolean = false;
  dictionaries = [
    {name: 'post pattern dictionary', type: ner_dic_type.NE_POST_PATTERN},
    {name: 'pre pattern dictionary', type: ner_dic_type.NE_PRE_PATTERN},
    {name: 'change dictionary', type: ner_dic_type.NE_POST_CHANGE},
    {name: 'post add dictionary', type: ner_dic_type.NE_ADD_POST_DIC},
    {name: 'pre add dictionary', type: ner_dic_type.NE_ADD_PRE_DIC}
  ];

  dictionary: dic_info = {id: '', name: '', dicType: ner_dic_type.NE_POST_PATTERN};
  dictionary_rows: dic_contents = [];
  selected_click_data: HTMLElement = undefined;
  ner_entities: NerDictionaryLineEntity[] = [];
  workspaceId: string;
  versionId: string = undefined;

  constructor(private nerDictionaryService: NerDictionaryService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private downloadService: DownloadService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.dictionary.name = par['name'];
      if (!this.dictionary.name || this.dictionary.name == '') {
        this.openAlertDialog('Failed', 'Error. Check your dictionary name in a url', 'error');
      }
    });
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
			onRowSelected: this.onRowSelected,
      onCellValueChanged: (event) => {
        this.updateDictionaryData(event.data);
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };

    this.actions = [{
      type: 'edit',
      text: 'Edit',
      icon: 'edit_circle_outline',
			disabled: false,
			callback: this.edit,
      hidden: false
    }, {
      type: 'addLine',
      text: 'Add',
      icon: 'playlist_add',
			disabled: false,
			callback: this.addLine,
      hidden: true
    }, {
      type: 'deleteLine',
      text: 'Delete',
      icon: 'delete_sweep',
      callback: this.deleteLine,
      hidden: true,
      disabled: true,
    }, {
      type: 'commit', text: 'Commit', icon: 'save', disabled: false, callback: this.commit, hidden: true
    }, {
      type: 'cancel', text: 'Cancel', icon: 'cancel', disabled: false, callback: this.cancel, hidden: true
    }, {
      type: 'download',
      text: 'Download',
      icon: 'file_download',
			disabled: false,
			callback: this.download,
      hidden: false
    }, {
      type: 'upload',
      text: 'Upload',
      icon: 'file_upload',
			disabled: false,
			callback: this.upload,
      hidden: true,
    },];

    this.load();
  }

  back(msg: string) {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    this.router.navigateByUrl('mlt/dictionary/ner');
  }

  load() {
    this.workspaceId = this.storage.get('workspaceId');
    this.nerDictionaryService.getDictionary(this.dictionary.name).subscribe(res => {
      if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
        this.dictionary.id = res.dictionary_id;
        this.gridOptions.columnApi.setColumnVisible('no', false);
        this.getDictionaryContents();
      } else {
        this.back('Error. Check your dictionary name in a url');
      }
    }, err => {
      this.back('Something wrong');
    });
  }

	onRowSelected = (param) => {
		let deleteBtn = this.actions.filter(y => y.type === 'deleteLine')[0];
		if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
			deleteBtn.disabled = false;
		} else {
			deleteBtn.disabled = true;
		}
	}

	getDictionaryContents = () => {
    let param = {
      "workspaceId": this.workspaceId,
      "id": this.dictionary.id
    }
    try {
      this.nerDictionaryService.getDictionaryContents(param).subscribe(res => {
        if (res.hasOwnProperty('dictionary_contents')) {
          this.entityToData(res['dictionary_contents']);
          this.ner_entities = res.dictionary_contents;
          this.dictionary_rows = res.dictionary_contents;
        }
        this.selectFilter(this.dictionaries[0]);
      });
    } catch (e) {
      this.back("Something wrong!");
    }
  }

  entityToData = (entity: NerDictionaryLineEntity[]) => {
    this.dictionary_rows = [];
    entity.map(x => {
      let one_line: line = {
        id: undefined,
        dicType: undefined,
        tag: undefined,
        pattern: undefined,
        word: undefined,
        originTag: undefined,
        changeTag: undefined,
        versionId: undefined,
        workspaceId: undefined,
      };
      one_line.id = x.id;
      one_line.dicType = x.dicType;
      one_line.tag = x.tag;
      one_line.pattern = x.pattern;
      one_line.changeTag = x.changeTag;
      one_line.originTag = x.originTag;
      one_line.word = x.word;
      one_line.versionId = x.versionId;
      one_line.workspaceId = x.workspaceId;
      this.dictionary_rows.push(one_line);
    });
  }

  dataToEntity = (data: dic_contents): NerDictionaryLineEntity[] => {
    let entities: NerDictionaryLineEntity[] = [];
    data.map(x => {
      let entity: NerDictionaryLineEntity = new NerDictionaryLineEntity();
      entity.id = x.id;
      entity.dicType = x.dicType;
      entity.tag = x.tag;
      entity.pattern = x.pattern;
      entity.changeTag = x.changeTag;
      entity.originTag = x.originTag;
      entity.word = x.word;
      entity.versionId = x.versionId;
      entity.workspaceId = x.workspaceId;
      entities.push(entity);
    });
    return entities;
  }

  updateDictionaryData(data) {
    try {
      let param: NerDictionaryLineEntity = new NerDictionaryLineEntity();
      param.id = data.id;
      param.dicType = data.dicType;
      param.versionId = data.versionId;
      param.tag = data.tag;
      param.pattern = data.pattern;
      param.word = data.word;
      param.originTag = data.originTag;
      this.nerDictionaryService.updateDictionaryLine(data).subscribe();
    } catch (e) {
      this.back('Something wrong!');
    }
  }

  viewTableOption() {
    switch (this.dictionary.dicType) {
      case ner_dic_type.NE_POST_PATTERN:
      case ner_dic_type.NE_PRE_PATTERN:
        this.gridOptions.columnApi.setColumnVisible('word', false);
        this.gridOptions.columnApi.setColumnVisible('originTag', false);
        this.gridOptions.columnApi.setColumnVisible('changeTag', false);
        this.gridOptions.columnApi.setColumnVisible('pattern', true);
        this.gridOptions.columnApi.setColumnVisible('tag', true);
        break;
      case ner_dic_type.NE_POST_CHANGE:
        this.gridOptions.columnApi.setColumnVisible('word', true);
        this.gridOptions.columnApi.setColumnVisible('originTag', true);
        this.gridOptions.columnApi.setColumnVisible('changeTag', true);
        this.gridOptions.columnApi.setColumnVisible('pattern', false);
        this.gridOptions.columnApi.setColumnVisible('tag', false);
        break;
      case ner_dic_type.NE_ADD_POST_DIC:
      case ner_dic_type.NE_ADD_PRE_DIC:
        this.gridOptions.columnApi.setColumnVisible('word', true);
        this.gridOptions.columnApi.setColumnVisible('originTag', false);
        this.gridOptions.columnApi.setColumnVisible('changeTag', false);
        this.gridOptions.columnApi.setColumnVisible('pattern', false);
        this.gridOptions.columnApi.setColumnVisible('tag', true);
        break;
      case ner_dic_type.NE_FILTER:
        break;
      default:
        break;
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'download') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    if (this.editable) {
      this.gridOptions.columnApi.setColumnVisible('no', true);
    } else {
      this.gridOptions.api.stopEditing();
      this.gridOptions.columnApi.setColumnVisible('no', false);
      this.getDictionaryContents();
    }
    this.gridOptions.api.getColumnDef('tag').editable = !this.gridOptions.api.getColumnDef('tag').editable;
    this.gridOptions.api.getColumnDef('pattern').editable = !this.gridOptions.api.getColumnDef('pattern').editable;
    this.gridOptions.api.getColumnDef('word').editable = !this.gridOptions.api.getColumnDef('word').editable;
    this.gridOptions.api.getColumnDef('originTag').editable = !this.gridOptions.api.getColumnDef('originTag').editable;
    this.gridOptions.api.getColumnDef('changeTag').editable = !this.gridOptions.api.getColumnDef('changeTag').editable;
  }

  edit = () => {
    new Promise((resolve, reject) => {
      if (this.ner_entities.length > 0) {
        try {
          this.ner_entities.forEach(entity => {
            entity.versionId = this.dictionary.id;
            entity.workspaceId = this.workspaceId;
          });
          this.nerDictionaryService.insertDictionaryLines(this.ner_entities).subscribe(
            res1 => {
              this.ner_entities = [];
              resolve();
            });
        } catch (e) {
          reject();
        }
      } else {
        resolve();
      }
    }).then(() => {
      this.getDictionaryContents();
      this.updateToolbar();
    }).catch(() => {
      this.back('Something wrong!');
    });
  }

  cancel = () => {
    this.updateToolbar();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  download = () => {
		this.gridOptions.api.stopEditing();
    let param = this.dataToEntity(this.dictionary_rows);
    this.nerDictionaryService.downloadContents(param).subscribe((res) => {
      if (res.hasOwnProperty('dictionary_contents')) {
        this.downloadService.downloadJsonFile(res.dictionary_contents, this.dictionary.name.toString());
      }
    });
    this.openAlertDialog('Download', `Corpus file downloaded successfully.`, 'success');
  }

  upload = () => {
		this.gridOptions.api.stopEditing();
    let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
    elem.click();
  }

  getdata = (param) => {
    let file = param.target.files[0];
    if (file.type.match(/text\/plain/) || file.type.match(/\/json/)) {
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        try {
          this.nerDictionaryService.deleteDictionaryContentsAll(this.dictionary.id).subscribe(
            res => {
              this.nerDictionaryService.uploadContents([reader.result, this.dictionary.id,
                this.workspaceId]).subscribe(res2 => {
                if (res2.hasOwnProperty('dictionary_contents')) {
                  this.dictionary_rows = [];
                  this.dictionary_rows = res2.dictionary_contents;
                  this.selectFilter(this.dictionaries[0]);
                }
              });
            }
          );
        } catch (ex) {
          this.openAlertDialog('Failed', `Check your corpus file data.`, 'error');
          return;
        }
      };
      reader.readAsText(file);
    } else {
      this.openAlertDialog('Failed', `Check your corpus file format.`, 'error');
    }
  }

  addLine = () => {
		this.gridOptions.api.stopEditing();
    let newItem = {
      dicType: this.dictionary.dicType,
      tag: '',
      pattern: '',
      word: '',
      originTag: '',
      changeTag: '',
      versionId: this.dictionary.id,
      workspaceId: this.workspaceId
    };
    let param: NerDictionaryLineEntity = new NerDictionaryLineEntity();
    param.versionId = this.dictionary.id;
    param.dicType = this.dictionary.dicType;
    param.workspaceId = this.workspaceId;
    this.nerDictionaryService.insertDictionaryLine(param).subscribe(
      res => {
        if (res && res.dictionary_line) {
          this.gridOptions.api.updateRowData({add: [res.dictionary_line]});
          this.dictionary_rows.push(res.dictionary_line);
        }
      }, err => {
        this.openAlertDialog('Add Line', 'Something wrong.', 'error');
      }
    );
  }

  deleteLine = () => {
		this.gridOptions.api.stopEditing();
    let selectedData = this.gridOptions.api.getSelectedRows();
    return new Promise((resolve, reject) => {
      this.nerDictionaryService.deleteDictionaryContents(selectedData).subscribe(
        res => {
          resolve();
        }, err => {
          reject();
        }
      );
    }).then(() => {
      this.gridOptions.api.updateRowData({remove: selectedData});
      this.dictionary_rows = this.dictionary_rows.filter(function(e) {
        return this.indexOf(e.id) < 0;
      }, selectedData.map(data => data.id));
    });
  }

  commit = () => {
    this.gridOptions.api.stopEditing();
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.dictionary.name} updated`;
    new Promise((resolve, reject) => {
      ref.afterClosed().subscribe(
        result => {
          if (result) {
            let param: NerDictionaryEntity = new NerDictionaryEntity();
            param.name = this.dictionary.name;
            param.id = this.dictionary.id;
            param.version = this.dictionary.id;
            param.creatorId = this.storage.get('user').id;
            param.workspaceId = this.workspaceId;
            this.nerDictionaryService.commitDictionary(param).subscribe(
              res => {
                if (res.hasOwnProperty('entities')) {
                  this.dictionary_rows = res.entities;
                  resolve();
                } else {
                  reject('No data to commit.');
                }
              },
              err => {
                reject('Something wrong.');
              });
          }
        });
    }).then(() => {
      this.selectFilter(this.dictionaries[0]);
      this.updateToolbar();
    }).catch((msg: string) => {
      this.openAlertDialog('Delete Lines', msg, 'error');
    });
  }

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 35,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }, {
      headerName: 'DicType',
      field: 'dicType',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    },
      {
        headerName: 'Pattern',
        field: 'pattern',
        width: 200,
        filter: 'agTextColumnFilter',
        editable: false,
      },
      {
        headerName: 'Word',
        field: 'word',
        width: 200,
        filter: 'agTextColumnFilter',
        editable: false,
      },
      {
        headerName: 'Origin tag',
        field: 'originTag',
        width: 200,
        filter: 'agTextColumnFilter',
        editable: false,
      },
      {
        headerName: 'Change tag',
        field: 'changeTag',
        width: 200,
        filter: 'agTextColumnFilter',
        editable: false,
      },
      {
        headerName: 'NE tag',
        field: 'tag',
        width: 200,
        filter: 'agTextColumnFilter',
        editable: false,
      },
      {
        headerName: 'WorkspaceId',
        field: 'workspaceId',
        width: 1,
        filter: 'agTextColumnFilter',
        editable: false,
        hide: true
      }
    ];
  }

  selectFilter(tag: dictionary_content) {
    if (this.selected_click_data !== undefined) {
      this.selected_click_data.style.background = '#E0E0E0';
    }
    this.selected_click_data = <HTMLInputElement> document.getElementById(tag.name);
    this.dictionary.dicType = tag.type;
    this.selected_click_data.style.background = '#47A8E5';
    this.viewTableOption();
    this.row_data = this.dictionary_rows.filter(x => x.dicType === this.dictionary.dicType);
    this.gridOptions.api.setRowData(this.row_data);
  }

  nlpTest = () => {
		this.gridOptions.api.stopEditing();
    this.router.navigateByUrl('mlt/dictionary/ner/' + this.dictionary.name + '/test');
  }
}
