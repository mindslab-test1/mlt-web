import {
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryService} from '../services/dictionary.pos-dictionary.service';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {TestResultService} from '../services/dictionary.test-result.service';

type result_info = {
  id: string;
  dictionaryName: string;
  createdAt: Date;
  tester: string;
  workspaceId: string;
}

type dic_info = {
  id: string;
  name: string;
  workspace: string;
  type: string;
}

enum dic_type {
  NER = 0, // 개체명 사전
  MORPH = 1, // 형태소 사전
}

@Component({
  selector: 'app-test-result',
  templateUrl: './dictionary.test-result.component.html',
  styleUrls: ['./dictionary.test-result.component.scss']
})
export class TestResultComponent implements OnChanges {

  @Input() dictionaryId: string;
  @Input() dictionaryName: string;
  @Input() dictionaryType: string;
  @Input() testState: number;

  public gridOptions: GridOptions;
  public defaultColDef;

  actions: any;
  type: dic_type;
  dictionary: dic_info = {
    id: undefined,
    name: '',
    workspace: undefined,
    type: undefined
  };
  row_data: result_info[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private posDictionaryService: PosDictionaryService,
    private nerDictionaryService: NerDictionaryService,
    private testResultService: TestResultService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
			onRowSelected: this.onRowSelected,
      onCellClicked: (event) => {
        if (event.colDef.field == 'detail') {
          let str: string = 'mlt/dictionary/' + this.dictionaryType + '/' + this.dictionaryName + '/test/' + event.data.id + '/detail';
          this.router.navigateByUrl(str);
        }
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };

    this.actions = [{
      type: 'delete',
      text: 'Delete',
			icon: 'delete_sweep',
      callback: this.delete,
      disabled: true,
      hidden: false
    }];
  }

  ngOnChanges(changes: SimpleChanges) {
    try {
      if (this.dictionaryId && this.dictionaryType.toUpperCase() in dic_type) {
        this.row_data = [];
        if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.NER) {
          this.nerDictionaryService.getDictionaryName(this.dictionaryId).subscribe(res => {
            if (res.hasOwnProperty('dictionary_name')) {
              this.dictionaryName = res.dictionary_name;
            }
          });
          this.getData();
        } else if (dic_type[this.dictionaryType.toUpperCase()] == dic_type.MORPH) {
          this.posDictionaryService.getDictionaryName(this.dictionaryId).subscribe(res => {
            if (res.hasOwnProperty('dictionary_name')) {
              this.dictionaryName = res.dictionary_name;
            }
          });
          this.getData();
        }
      }
    } catch (e) {
      this.openAlertDialog('Load Result', 'Something wrong!', 'error');
    }
  }

  getData = () => {
    this.row_data = [];
    this.testResultService.getResult(this.dictionaryType, this.dictionaryId).subscribe(
      res => {
        if (res.hasOwnProperty('list')) {
          res.list.forEach(one => {
            let elem: result_info = {
              id: undefined,
              dictionaryName: this.dictionaryName,
              createdAt: undefined,
              tester: undefined,
              workspaceId: undefined,
            }
            elem.id = one.id;
            elem.createdAt = one.createdAt;
            elem.tester = one.tester;
            elem.workspaceId = one.workspaceId;
            this.row_data.push(elem);
          });
          this.gridOptions.api.setRowData(this.row_data);
        }
      }, err => {
        this.openAlertDialog('Load Result', 'Something wrong!', 'error');
      }
    );
  }

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    if (selectedData.length > 0) {
      let ids: number[] = selectedData.map(i => i.id);
      this.testResultService.deleteResult(this.dictionaryType, ids).subscribe(res => {
        this.openAlertDialog('Delete', `Corpus file downloaded successfully.`, 'success');
        this.getData();
      }, err => {
        this.openAlertDialog('Delete', 'Something wrong.', 'error');
      });
    }
  }

	onRowSelected = (param) => {
		let deleteBtn = this.actions.filter(y => y.type === 'delete')[0];
		if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
			deleteBtn.disabled = false;
		} else {
			deleteBtn.disabled = true;
		}
	}

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 2,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 1,
      hide: true,
      editable: false,
    }, {
      headerName: 'Dictionary name',
      field: 'dictionaryName',
      width: 40,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Created date',
      field: 'createdAt',
      width: 40,
      cellRenderer: this.dateCellRenderer,
      editable: false
    }, {
      headerName: 'Detail',
      field: 'detail',
      width: 40,
      filter: 'agTextColumnFilter',
      cellRenderer: this.nameRenderer,
      editable: false
    },]
  }

  dateCellRenderer = (params) => {
    let date: Date = new Date(params.value);
    let result = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    return result;
  }

  nameRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">view</sapn>';
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }
}
