import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser,	DictionaryDictionaryUpsertDialogComponent} from 'mlt-shared';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {Router} from '@angular/router';
import {NerDictionaryEntity} from '../entity/dictionary.ner-dictionary.entity';

type dic_info = {
  id: string;
  name: string;
  changeCnt: number;
  creatorId: string;
  workspaceId: string;
  description: string;
};

type dic_list = dic_info[];

@Component({
  selector: 'app-ner-dictionary',
  templateUrl: './dictionary.ner-dictionaries-management.component.html',
  styleUrls: ['./dictionary.ner-dictionaries-management.component.scss']
})
export class NerDictionariesManagementComponent {
  public gridOptions: GridOptions;
  public defaultColDef;
  rowData: dic_list = [];
  workspaceId: string;

  dictionary: string[] = [];
  actions: any[] = [];

  constructor(private nerDictionaryService: NerDictionaryService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private router: Router,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.dictionaryTableOption(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onCellClicked: (event) => {
        if (event.colDef.field == 'name') {
          this.router.navigateByUrl('mlt/dictionary/ner/' + event.value);
        } else if (event.colDef.field == 'changeCnt') {
          this.router.navigateByUrl('mlt/dictionary/version/ner/' + event.data.name);
        }
      },
      onCellValueChanged: (change_event) => {
        this.updateDesc(change_event.data);
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };
    this.actions = [{
      type: 'Add',
      text: 'Add',
      icon: 'add_circle_outline',
			disabled: false,
			callback: this.add,
      hidden: false
    }, {
      type: 'Delete',
      text: 'Delete',
      icon: 'remove_circle_outline',
			disabled: false,
			callback: this.delete,
      hidden: false
    }
    ];
    this.changeDictionary();
  }

  load = () => {
    this.gridOptions.api.setRowData(this.rowData);
    this.workspaceId = this.storage.get('workspaceId');
  }

  changeDictionary() {
    new Promise((resolve, reject) => {
      this.nerDictionaryService.getDictionaryList().subscribe(
        res => {
          if (res && res.hasOwnProperty('dict_list')) {
            this.rowData = [];
            res.dict_list.forEach(x => {
              let dict: dic_info = {
                id: undefined,
                name: undefined,
                changeCnt: undefined,
                description: undefined,
                creatorId: undefined,
                workspaceId: undefined,
              };
              dict.id = x.id;
              dict.creatorId = x.creatorId;
              dict.changeCnt = x.changeCnt;
              dict.description = x.description;
              dict.workspaceId = x.workspaceId;
              dict.name = x.name;
              this.rowData.push(dict);
            });
            this.load();
          }
        });
    });
  }

  dictionaryTableOption = () => {
    let column_list = [{
      headerName: '#',
      field: 'no',
      width: 20,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Dictionary id',
      field: 'id',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }, {
      headerName: 'Dictionary Name',
      field: 'name',
      width: 150,
      filter: 'agTextColumnFilter',
      editable: false,
      cellRenderer: this.nameRenderer,
      cellEditorSelector: function(params) {
        if (params.data.type === 'name') {
          return {

            component: 'numericCellEditor'
          };
        }
      }
    }, {
      headerName: 'Creator',
      field: 'creatorId',
      width: 100,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Change count',
      field: 'changeCnt',
      width: 50,
      filter: 'agTextColumnFilter',
      cellRenderer: this.cntRenderer,
      editable: false,
    }, {
      headerName: 'Description',
      field: 'description',
      width: 250,
      filter: 'agTextColumnFilter',
      editable: true,
    }, {
      headerName: 'WorkspaceId',
      field: 'workspaceId',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }];
    return column_list;
  }

  updateDesc = (data) => {
    let param: NerDictionaryEntity = new NerDictionaryEntity();
    param.id = data.id;
    param.description = data.description;
    this.nerDictionaryService.updateDictionary(param).subscribe();
  }

  nameRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">' + params.value + '</sapn>';
  }

  cntRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">' + params.value + '</sapn>';
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }


  add = () => {
    let param = {};
    param['type'] = 'ner';
    param['workspaceId'] = this.workspaceId;
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent, {data: param});
    ref.componentInstance.service = this.nerDictionaryService;
    ref.componentInstance.title = 'Add Dictionary';
    ref.componentInstance.meta = {
      workspaceId: this.storage.get('workspaceId'),
      type: 'ner'
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.changeDictionary();
      }
    });
  }

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    let idList: string[] = selectedData.map(v => v.id);
    this.nerDictionaryService.deleteDictionaries(idList).subscribe();
    this.changeDictionary();
  }
}
