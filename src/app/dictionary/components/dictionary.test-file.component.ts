import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {ActivatedRoute, Router} from '@angular/router';
import {TestFileService} from '../services/dictionary.test-file.service';

enum dic_type {
  NER = 0, // 개체명 사전
  MORPH = 1, // 형태소 사전
}

type file = {
  id: number;
  fileName: string;
  fileId: string;
  createdAt: Date;
  lineCnt: number;
  testType: number;
}

type file_list = file[];

@Component({
  selector: 'app-test-file',
  templateUrl: './dictionary.test-file.component.html',
  styleUrls: ['./dictionary.test-file.component.scss']
})
export class TestFileComponent {

  @Output() selectFiles: EventEmitter<string[]> = new EventEmitter<string[]>();

  public gridOptions: GridOptions;
  public defaultColDef;

  row_data: file_list = [];
  type: string;
  workspaceId: string;
  actions: any;
  fileList: string[] = [];

  constructor(private route: ActivatedRoute,
    private fileService: TestFileService,
    private router: Router,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
			onRowSelected: this.onRowSelected,
			onGridReady(params) {
        params.api.sizeColumnsToFit();
      },
    };
    this.workspaceId = this.storage.get('workspaceId');
    this.load();
  }

  load = () => {
    this.route.url.subscribe(par => {
      if (par[0].path.toUpperCase() in dic_type) {
        this.type = par[0].path.toLowerCase();
      }
    }, err => {
      this.openAlertDialog('Url', 'Something wrong.', 'error');
      window.history.back();
    });

    this.actions = [{
      type: 'upload',
			icon: 'file_upload',
      text: 'Upload',
			disabled: false,
			callback: this.upload,
      hidden: false
    }, {
      type: 'download',
      text: 'Download',
			icon: 'file_download',
      callback: this.download,
			disabled: true,
      hidden: false
    }, {
      type: 'delete',
      text: 'Delete',
			icon: 'delete_sweep',
			disabled: true,
      callback: this.delete,
      hidden: false
    }];
    this.getFiles();
  }

  upload = () => {
    let elem: HTMLInputElement = document.getElementById('selectFile1') as HTMLInputElement;
    elem.click();
  }

  getFiles = () => {
    this.row_data = [];
    this.fileService.getFileList(this.type).subscribe(res => {
      if (res.hasOwnProperty('list')) {
        res.list.forEach(x => {
          let file_info: file = {
            id: undefined,
            fileName: undefined,
            fileId: undefined,
            createdAt: undefined,
            lineCnt: undefined,
            testType: undefined,
          };
          file_info.id = x.id;
          file_info.fileName = x.fileName;
          file_info.fileId = x.fileId;
          file_info.createdAt = x.createdAt;
          file_info.lineCnt = x.lineCnt;
          file_info.testType = x.testType;
          this.row_data.push(file_info);
        });
        this.gridOptions.api.setRowData(this.row_data);
      } else {
        this.gridOptions.api.setRowData(this.row_data);
      }
    }, err => {
      this.openAlertDialog('Load', 'Something wrong.', 'error');
    });
  }

  download = () => {
		const res = this.downloadWork();
  }

	delay(milliseconds: number) {
		return new Promise(resolve => setTimeout(resolve, milliseconds));
	}

	async downloadWork() {
		let selectedData = this.gridOptions.api.getSelectedRows();
		for (const item of selectedData) {
			await this.flow(item.fileId);
    }
		this.openAlertDialog('Download', `Corpus file downloaded successfully.`, 'success');
	}

	async flow(id: string) {
		return await this.process(id);
  }

	async process(id: string) {

		let url = this.storage.get('mltApiUrl') + '/dictionary/test/download-file/' + this.workspaceId + '/' + id;
		let a = document.createElement('a');
		document.body.appendChild(a);
		a.style.display = 'none';
		a.href = url;
		a.click();
		await this.delay(100);
	}

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    if (selectedData.length > 0) {
      let ids: number[] = selectedData.map(i => i.id);
      this.fileService.deleteFiles(this.type.toString(), ids).subscribe(res => {
        this.openAlertDialog('Delete', `Corpus file downloaded successfully.`, 'success');
        this.getFiles();
      }, err => {
        this.openAlertDialog('Delete', 'Something wrong.', 'error');
      });
    }
  }

  getdata = (param) => {
    try {
      let file = param.target.files[0];
      if (file.type.match(/text\/plain/)) {
        let formData = new FormData();

        formData.append('file', file, file.name);
        this.fileService.insertFile(this.type.toString(), formData).subscribe(res => {
          this.openAlertDialog('Upload', `File downloaded successfully.`, 'success');
          this.getFiles();
        }, err => {
          this.openAlertDialog('Upload', 'Something wrong.', 'error');
        });
      }
    } catch (ex) {
      this.openAlertDialog('Upload', `Check your corpus file data.`, 'error');
      return;
    }
  }

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 5,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 2,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true,
    }, {
      headerName: 'File name',
      field: 'fileName',
      width: 30,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Line count',
      field: 'lineCnt',
      width: 25,
      filter: 'agTextColumnFilter',
      editable: false
    }, {
      headerName: 'Created date',
      field: 'createdAt',
      width: 35,
      cellRenderer: this.dateCellRenderer,
      editable: false
    }, {
      headerName: 'Creator',
      field: 'creatorId',
      width: 25,
      editable: false
    }, {
      headerName: 'File id',
      field: 'fileId',
      width: 5,
      hide: true,
      editable: false
    }]
  }

	onRowSelected = (param) => {
		let deleteBtn = this.actions.filter(y => y.type === 'delete')[0];
		let downBtn = this.actions.filter(y => y.type === 'download')[0];
		if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
			let selectedData = this.gridOptions.api.getSelectedRows().map(x=> x.id);
			this.selectFiles.emit(selectedData);
			deleteBtn.disabled = false;
			downBtn.disabled = false;
		} else {
			this.selectFiles.emit([]);
			deleteBtn.disabled = true;
			downBtn.disabled = true;
		}

	}

  dateCellRenderer = (params) => {
    let date: Date = new Date(params.value);
    let result =  date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    return result;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = (msg: string) => {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
      this.router.navigateByUrl('mlt/dictionary/');
    } else {
      window.history.back();
    }
  }
}
