import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryDictionaryUpsertDialogComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {PostProcessEntity} from '../entity/dictionary.post-process.entity';
import {DictionaryPostProcessService} from '../services/dictionary.post-process.service';

@Component({
  selector: 'app-post-process',
  templateUrl: './dictionary.post-process.component.html',
  styleUrls: ['./dictionary.post-process.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DictionaryPostProcessComponent implements OnInit {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;

  searchWord = '';
  searchCategory = '';
  postProcessDicLineList: any[] = [];
  postProcessDicList = [];
  pageLength;
  editable = false;
  actions: any[] = [];
  initActions: any[] = [];
  pageParam: MatPaginator;
  param: PostProcessEntity = new PostProcessEntity();

  defaultHeader = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'category', name: 'Category'},
    {attr: 'word', name: 'Word'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'category', name: 'Category', input: true},
    {attr: 'word', name: 'Word', input: true}
  ];

  header = [];

  dataSource: MatTableDataSource<any>;
  selectedDictionary: any = '';
  isTableHidden = true;

  constructor(private dictionaryPostProcessService: DictionaryPostProcessService
    , @Inject(StorageBrowser) protected storage: StorageBrowser
    , private dialog: MatDialog) {
  }


  getPaginator(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.postProcessDicLineList.length > 0) {
      this.getDicLineList();
    }
  }

  changeRecord(data) {
    this.param = new PostProcessEntity();
    this.param.id = data.id;
    this.param.category = data.category || '';
    this.param.word = data.word || '';
    this.dictionaryPostProcessService.updateDicLine(this.param).subscribe(result => {
      },
      err => {
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    )
  }

  onUploadFile() {
    // this.header = this.defaultHeader;
    // this.updateToolbar();
    this.getDicLineList();
  }

  getDicLineList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.isTableHidden = false;
    this.param = new PostProcessEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.versionId = this.selectedDictionary.id;
    this.param.word = this.searchWord;
    this.param.category = this.searchCategory;

    this.dictionaryPostProcessService.getDicLineList(this.param).subscribe(result => {
        if (result) {
          let resultData = result['content'];

          this.lineTableComponent.isCheckedAll = false;
          this.pageLength = resultData['totalElements'];
          this.postProcessDicLineList = resultData['content'];

        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }


  getDicList() {
    this.param = new PostProcessEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.postProcessDicList.length = 0;
    this.dictionaryPostProcessService.getDicList(this.param).subscribe(result => {
        if (result) {
          result['dicList'].forEach((value) => {
            this.postProcessDicList.push({id: value.id, name: value.name});
          });
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );
  }

  ngOnInit() {
    this.getDicList();
    this.header = this.defaultHeader;
    this.initActions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      }
    ];
    this.actions = [
      {
        type: 'Add',
        text: 'Add',
        icon: 'add_circle_outline',
        callback: this.add,
        hidden: false
      },
      {
        type: 'Remove',
        text: 'Remove',
        icon: 'remove_circle_outline',
        callback: this.remove,
        hidden: false
      },
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'Apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'Add a word',
        text: 'Add a word',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'Delete checked rules',
        text: 'Delete checked rules',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Commit',
        icon: 'save',
        callback: this.commit,
        hidden: true
      },
    ];

  }

  addLine = () => {
    this.param = new PostProcessEntity();
    this.param.word = '';
    this.param.category = '';
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.versionId = this.selectedDictionary.id;

    this.dictionaryPostProcessService.insertDicLine(this.param).subscribe(result => {
        if (result) {

          this.postProcessDicLineList.unshift({
            word: '',
            category: '',
            id: result['content'].id
          });
          this.getDicLineList();
        } else {
          return false;
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Error. Failed add a word', 'error');
      }
    );

  };
  add = () => {
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
    ref.componentInstance.service = this.dictionaryPostProcessService;
    ref.componentInstance.meta = {
      workspaceId: this.storage.get('workspaceId'),
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.getDicList();
      }
    })
  };
  remove = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed()
    .subscribe(confirmed => {
      if (confirmed) {
        this.dictionaryPostProcessService.deleteDic(this.param).subscribe(result => {
            if (result) {
              this.getDicList();

              if (this.postProcessDicList.length === 0) {
                this.selectedDictionary = '';
                this.isTableHidden = true;
              }

              this.openAlertDialog('Success', 'Success Add FileGroup', 'success');
            } else {
              return false;
            }
          },
          err => {
            console.log(err);
            this.openAlertDialog('Failed', 'Error. Failed Remove Dictionary', 'error');
          }
        );
      }
    });

    this.param = new PostProcessEntity();
    this.param.id = this.selectedDictionary.id;

  };
  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };
  apply = () => {

    this.param = new PostProcessEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.id = this.selectedDictionary.id;

    this.dictionaryPostProcessService.apply(this.param).subscribe(
      res => {
        if (res) {
          this.getDicLineList();
          this.openAlertDialog('Apply', `Apply completed`, 'success');
        }
      },
      err => {
        this.openAlertDialog('Error', `Server Error. Can not apply.`, 'error');
      }
    );
  };

  updateToolbar() {
    this.actions.forEach(action => {
      action.hidden = !action.hidden;
    });
    this.editable = !this.editable;
  }


  deleteLine = () => {
    let postProcessEntities: PostProcessEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (postProcessEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (postProcessEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dictionaryPostProcessService.deleteLines(postProcessEntities).subscribe(
            res => {
              if (res) {
                this.getDicLineList();
                this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
              }
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  commit = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new PostProcessEntity();
          this.param.dicId = this.selectedDictionary.id;
          this.param.id = this.selectedDictionary.id;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.storage.get('workspaceId');
          this.param.title = result.title;
          this.param.message = result.message;
          this.param.versionId = this.selectedDictionary.id;

          this.dictionaryPostProcessService.commit(this.param).subscribe(
            res => {
              this.getDicLineList();
              this.header = this.defaultHeader;
              this.updateToolbar();
            },
            err => {
              console.log(err);
            });
        }
      });
  };

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}



