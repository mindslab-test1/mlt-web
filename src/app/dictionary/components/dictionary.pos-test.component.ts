import {Component, Inject, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryService} from '../services/dictionary.pos-dictionary.service';
import {FileManagementService} from '../services/file-management.service';
import {TestResultService} from '../services/dictionary.test-result.service';

enum pos_dic_type {
	MORP_CUSTOM = 0, // 사용자 사전
	MORP_EOJEOL = 1, // 어절 사전
	MORP_COMPOUND_WORD = 2, // 복합 명사 사전
	MORP_POST_PATTERN_STR = 3, // 후처리 패턴 String 사전
	MORP_POST_PATTERN_MORP = 4, // 후처리 패턴 형태소 사전
}

@Component({
	selector: 'app-pos-test',
	templateUrl: './dictionary.pos-test.component.html',
	styleUrls: ['./dictionary.pos-test.component.scss']
})
export class PosTestComponent {

	test_dictionaries = [
		{
			name: 'pre custom dictionary',
			value: pos_dic_type.MORP_CUSTOM,
			checked: false,
			disabled: true
		},
		{
			name: 'post pattern string dictionary',
			value: pos_dic_type.MORP_POST_PATTERN_STR,
			checked: false, disabled: true
		},
		{
			name: 'post pattern morph dictionary',
			value: pos_dic_type.MORP_POST_PATTERN_MORP,
			checked: false, disabled: true
		},
		{
			name: 'compound dictionary',
			value: pos_dic_type.MORP_COMPOUND_WORD,
			checked: false,
			disabled: true
		},
	];

	fileList: string[] = [];
	testStat: number = 0;
	checkedList: number[] = [];

	isTest: boolean;
	isReset: boolean;
	test_btn: HTMLElement;
	reset_btn: HTMLElement;

	dictionaryId: string = '';
	dictionaryType: string = '';
	dictionaryName: string = '';
	workspaceId: string = '';

	constructor(private posDictionaryService: PosDictionaryService,
		private fileManagementService: FileManagementService,
		private testResultService: TestResultService,
		private route: ActivatedRoute,
		private router: Router,
		@Inject(StorageBrowser) protected storage: StorageBrowser,
		private dialog: MatDialog) {
	}

	ngOnInit() {
		this.isReset = false;
		this.isTest = false;

		this.load();
	}

	load = () => {
		new Promise((resolve, reject) => {
			// 파일 사전 기본 정보 가져오기
			this.route.url.subscribe(par => {
				this.dictionaryType = par[0].path;
				this.dictionaryName = par[1].path;
				this.posDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
					if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
						this.dictionaryId = res.dictionary_id;
						this.dictionaryId = res.dictionary_id;
						this.workspaceId = this.storage.get('workspace')[0].id;
						resolve();
					} else {
						reject('Error. Something wrong!');
					}
				}, err => {
					reject('Error. Something wrong!');
				});
			}, err2 => reject('Error. Something wrong!'));
		}).then(() => {
			this.posDictionaryService.getDictionaryContentsTypes(this.dictionaryId).subscribe(res => {
				if (res.hasOwnProperty('list')) {
					let typeList: number[] = res.list;
					this.test_dictionaries.forEach(x => {
						if (typeList.includes(x.value)) {
							x.disabled = false;
						}
					});
				}
			});
			this.checkButton();
		}).catch((msg: string) => {
			this.back(msg);
		});
	}

	openAlertDialog(title, message, status) {
		let ref = this.dialog.open(AlertComponent);
		ref.componentInstance.header = status;
		ref.componentInstance.title = title;
		ref.componentInstance.message = message;
	}

	test = () => {
		// 사전 적용 및 테스트 결과  보여주기
		new Promise((resolve, reject) => {
			let param = {
				"checkedList": this.checkedList,
				"fileList": this.fileList
			}
			this.isReset = false;
			this.isTest = false;
			this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
				resolve();
			}, err => {
				reject();
			});
		}).then(() => {
			let tmp = this.dictionaryId;
			this.dictionaryId = '';
			this.dictionaryId = tmp;
			this.testStat += 1;
			this.checkButton();
			this.openAlertDialog('Test', 'Test done.', 'success');
		}).catch(() => {
			this.checkButton();
			this.openAlertDialog('Test', 'Something wrong', 'error');
		});
	}

	reset = () => {
		let param = {
			"checkedList": this.checkedList,
			"fileList": []
		}
		this.isReset = false;
		this.isTest = false;
		this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
			this.openAlertDialog('Test', 'Reset done.', 'success');
		}, error1 => {
			this.openAlertDialog('Reset', 'Something wrong!', 'error');
		});
		this.checkButton();
	}

	back = (msg: string) => {
		if (msg !== '') {
			this.openAlertDialog('Failed', msg, 'error');
		}
		this.router.navigateByUrl('mlt/dictionary/morph/' + this.dictionaryName);
	}

	clickCheckbox = (val) => {
		let target = this.test_dictionaries.filter(x => x.value === val)[0];
		target.checked = !target.checked;
		this.checkButton();
	}

	selectFiles = (event) => {
		this.fileList = event;
		this.checkButton();
	}

	checkButton = () => {
		this.checkedList = this.test_dictionaries.filter(x => x.checked == true && x.disabled == false).map(e => e.value);
		if (this.checkedList.length > 0) {
			this.isReset = true;
			console.log(this.checkedList, this.fileList);
			if (this.fileList.length > 0) {
				this.isTest = true;
			} else {
				this.isTest = false;
			}
		} else {
			this.isTest = false;
			this.isReset = false;
		}
	}
}
