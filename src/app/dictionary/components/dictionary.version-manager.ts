import {ChangeDetectorRef, Component, Inject, ViewChild} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, DownloadService, GitComponent, StorageBrowser} from 'mlt-shared';
import {NerCorpusService} from '../services/dictionary.ner-corpus.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NerDictionaryService} from '../services/dictionary.ner-dictionary.service';
import {PosDictionaryService} from '../services/dictionary.pos-dictionary.service';

type dic_info = {
  id: string;
  type: string;
  name: string;
};

enum dic_type {
  ner = 0,
  morph = 1
}

@Component({
  selector: 'app-dictionary-version',
  templateUrl: './dictionary.version-manager.html',
  styleUrls: ['./dictionary.version-manager.html']
})
export class DictionaryVersionComponent {
  public gridApi;
  public gridColumnApi;
  public gridOptions: GridOptions;
  public defaultColDef;
  @ViewChild(GitComponent) gitComp:GitComponent;

  dictionary: dic_info = {
    id: '',
    type: toString(),
    name: ''
  };
  isTableHidden: boolean = true;

  select1: any;
  select2: any;

  constructor(private nerCorpusService: NerCorpusService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private downloadService: DownloadService,
    private nerDictionaryService: NerDictionaryService,
    private posDictionaryService: PosDictionaryService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.dictionary.name = par['name'];
      this.dictionary.type = par['type'];
      if (par['type'] in dic_type === false) {
        this.back('Error. Check a type of dictionary  in a url');
      }
      if (!this.dictionary.name || this.dictionary.name == '') {
        this.back('Error. Check your dictionary name in a url', this.dictionary.type);
      }
    });
    this.load();
  }

  load = () => {
    if (this.dictionary.type === dic_type[0]) {
      this.nerDictionaryService.getDictionary(this.dictionary.name).subscribe(res => {
        if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
          this.dictionary.id = res.dictionary_id;
          this.gitComp.fileId = this.dictionary.id;
          this.gitComp.getCommitList();
          this.gitComp.changeLatestValue();
        } else {
          this.back('Error. Check your dictionary name in a url', this.dictionary.type);
        }
        this.cdr.detectChanges();
      });
    } else if (this.dictionary.type === dic_type[1]) {
      this.posDictionaryService.getDictionary(this.dictionary.name).subscribe(res => {
        if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
          this.dictionary.id = res.dictionary_id;
          this.cdr.detectChanges();
          this.gitComp.fileId = this.dictionary.id;
          this.gitComp.getCommitList();
          this.gitComp.changeLatestValue();
        } else {
          this.back('Error. Check your dictionary name in a url', this.dictionary.type);
        }
      });
    }
  }

  back(msg: string = '', type: string = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (type === '') {
      window.history.back();
    } else {
      // 목록으로 이동
      this.router.navigateByUrl('mlt/dictionary/' + type);
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
