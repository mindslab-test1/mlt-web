import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {DictionaryWiseNluNerCategoryEntity} from '../entity/dictionary.wise-nlu-ner-category.entity';

@Injectable()
export class DictionaryWiseNluNerCategoryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  insertDic(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/insert-dic', param);
  }

  getDicList(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/get-dicList', param);
  }

  deleteDic(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/delete-dic', param);
  }

  insertDicLine(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/insert-line', param);
  }

  updateDicLine(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/update-line', param);
  }

  getDicLineList(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/get-lines', param);
  }

  deleteLines(param: DictionaryWiseNluNerCategoryEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/delete-lines', param);
  }

  commit(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/commit', param);
  }

  apply(param: DictionaryWiseNluNerCategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner-category/apply', param);
  }
}
