import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FileManagementEntity} from '../entity/file-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class FileManagementService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getFileGroups', param);
  }

  insertGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/insertGroup', param);
  }

  updateGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/updateGroup', param);
  }

  deleteGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/deleteGroup', param);
  }

  getFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getFileList', param);
  }

  getGroupFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getGroupFileList', param);
  }

  deleteFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/deleteFiles', param);
  }

  includeFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/include-files', param);
  }

  excludeFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/exclude-files', param);
  }
}
