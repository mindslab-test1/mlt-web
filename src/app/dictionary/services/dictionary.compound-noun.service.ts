import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CompoundNounEntity} from '../entity/dictionary.compound-noun.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DictionaryCompoundNounService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertDic(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/insert-dic', compoundNounEntity);
  }

  getDicList(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/get-dicList', compoundNounEntity);
  }

  deleteDic(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/delete-dic', compoundNounEntity);
  }

  insertDicLine(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/insert-line', compoundNounEntity);
  }

  updateDicLine(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/update-line', compoundNounEntity);
  }

  getDicLineList(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/get-lines', compoundNounEntity);
  }

  deleteLines(compoundNounEntity: CompoundNounEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/delete-lines', compoundNounEntity);
  }

  commit(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/commit', compoundNounEntity);
  }

  apply(compoundNounEntity: CompoundNounEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/compound/apply', compoundNounEntity);
  }
}
