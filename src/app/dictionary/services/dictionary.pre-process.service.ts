import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {PreProcessEntity} from '../entity/dictionary.pre-process.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DictionaryPreProcessService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }


  insertDic(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/insert-dic', preProcessEntity);
  }

  getDicList(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/get-dicList', preProcessEntity);
  }

  deleteDic(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/delete-dic', preProcessEntity);
  }

  insertDicLine(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/insert-line', preProcessEntity);
  }

  updateDicLine(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/update-line', preProcessEntity);
  }

  getDicLineList(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/get-lines', preProcessEntity);
  }

  deleteLines(preProcessEntity: PreProcessEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/delete-lines', preProcessEntity);
  }

  commit(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/commit', preProcessEntity);
  }

  apply(preProcessEntity: PreProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/pre-process/apply', preProcessEntity);
  }
}
