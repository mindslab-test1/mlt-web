import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {PosDictionaryLineEntity} from '../entity/dictionary.pos-dictionary-line.entity';
import {PosDictionaryEntity} from '../entity/dictionary.pos-dictionary.entity';
import {PosTestEntity} from '../entity/dictionary-test.entity';

type dictionary_data = {
  type: number,
  name: string
}

@Injectable()
export class PosDictionaryService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
    @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('workspaceId');
  }

  // pos 사전 관련
  getDictionaryList(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionaryList', null);
  }

  getDictionaryName(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionaryName', id);
  }

  updateDictionary(entity: PosDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/update-dictionary', entity);
  }

  deleteDictionaries(idList: string[]): Observable<any> {
    let entities: PosDictionaryEntity[] = [];
    for (let param of idList) {
      let entity: PosDictionaryEntity = new PosDictionaryEntity();
      entity.id = param;
      entity.name = '';
      entities.push(entity);
    }
    return this.http.post(this.API_URL + '/dictionary/morph/delete-dictionaries', entities);
  }

  insertDic(param: any): Observable<any> {
    let entity: PosDictionaryEntity = new PosDictionaryEntity();
    entity.workspaceId = param.workspaceId;
    entity.creatorId = this.ID;
    entity.name = param.name;
    return this.http.post(this.API_URL + '/dictionary/morph/insert-dictionary', entity);
  }

  getDictionary(name: String): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionary', name);
  }

  // ner 사전 contents로 존재하는 타입 리스트 가져오기
  getDictionaryContentsTypes(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionaryContents-type/' + this.WORKSPACE_ID
      + '/' + id, null);
  }

  // pos 사전 contents 관련
  getDictionaryContents(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionaryContents', param);
  }

  deleteDictionaryContents(posEntities: PosDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/delete-dictionaryContents', posEntities);
  }

  deleteDictionaryContentsAll(versionId: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/delete-dictionaryContentsAll', versionId);
  }

  insertDictionaryLines(posEntities: PosDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/insert-dictionaryContents', posEntities);
  }

  insertDictionaryLine(posEntity: PosDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/insert-dictionaryLine', posEntity);
  }

  updateDictionaryLine(posEntity: PosDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/update-dictionaryLine', posEntity);
  }

  // git
  commitDictionary(posEntity: PosDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/commit-dictionary', posEntity);
  }

  // Download
  downloadContents(data: PosDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/download-dictionary', data);
  }

  // Upload
  uploadContents(data: String[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/upload-dictionary', data);
  }

  // 테스트
  test(id: string, params: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/apply-and-test/' + this.WORKSPACE_ID
      + '/' + id +  '/' + this.ID, params);
  }

  getContentsFromGit(posEntity: PosDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/get-dictionary-from-git', posEntity);
  }

  // 특정 사전 제거
  deleteDictionary(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/delete-dictionary', id);
  }

  // 사전 적용
  applyDictionary(id: string, params: number[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morph/apply-dictionary/' + this.WORKSPACE_ID
      + '/' + id, params);
  }
}
