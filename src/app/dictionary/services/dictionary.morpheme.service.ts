import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MorphemeEntity} from '../entity/dictionary.morpheme.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DictionaryMorphemeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertDic(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/insert-dic', morphemeEntity);
  }

  getDicList(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/get-dicList', morphemeEntity);
  }

  deleteDic(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/delete-dic', morphemeEntity);
  }

  insertDicLine(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/insert-line', morphemeEntity);
  }

  updateDicLine(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/update-line', morphemeEntity);
  }

  getDicLineList(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/get-lines', morphemeEntity);
  }

  deleteLines(morphemeEntity: MorphemeEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/delete-lines', morphemeEntity);
  }

  commit(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/commit', morphemeEntity);
  }

  apply(morphemeEntity: MorphemeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/morpheme/apply', morphemeEntity);
  }
}
