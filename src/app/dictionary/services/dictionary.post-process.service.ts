import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {PostProcessEntity} from '../entity/dictionary.post-process.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DictionaryPostProcessService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertDic(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/insert-dic', postProcessEntity);
  }

  getDicList(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/get-dicList', postProcessEntity);
  }

  deleteDic(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/delete-dic', postProcessEntity);
  }

  insertDicLine(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/insert-line', postProcessEntity);
  }

  updateDicLine(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/update-line', postProcessEntity);
  }

  getDicLineList(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/get-lines', postProcessEntity);
  }

  deleteLines(postProcessEntity: PostProcessEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/delete-lines', postProcessEntity);
  }

  commit(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/commit', postProcessEntity);
  }

  apply(postProcessEntity: PostProcessEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/post-process/apply', postProcessEntity);
  }
}
