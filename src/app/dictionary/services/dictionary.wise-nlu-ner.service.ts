import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {DictionaryWiseNluNerEntity} from '../entity/dictionary.wise-nlu-ner.entity';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DictionaryWiseNluNerService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertDic(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/insert-dic', param);
  }

  getDicList(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/get-dicList', param);
  }

  deleteDic(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/delete-dic', param);
  }

  insertDicLine(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/insert-line', param);
  }

  updateDicLine(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/update-line', param);
  }

  getDicLineList(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/get-lines', param);
  }

  deleteLines(param: DictionaryWiseNluNerEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/delete-lines', param);
  }

  commit(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/commit', param);
  }

  apply(param: DictionaryWiseNluNerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/apply', param);
  }

  convert(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/wise-nlu-ner/convert', param);
  }
}
