import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {NerCorpusTagEntity} from '../entity/dictionary.ner-corpus-tag.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class NerCorpusTagService {
  API_URL;

  constructor(private http: HttpClient,
    @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertTags(tagEntities: NerCorpusTagEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/insert-tagList', tagEntities);
  }

  deleteTags(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/delete-tagList', null);
  }

  getCorpusTags(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/get-tagList', null);
  }
}
