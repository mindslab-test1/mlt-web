import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {NerDictionaryLineEntity} from '../entity/dictionary.ner-dictionary-line.entity';
import {NerDictionaryEntity} from '../entity/dictionary.ner-dictionary.entity';
import {TestFileEntity} from '../entity/test-file.entity';

@Injectable()
export class TestFileService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
    @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('workspaceId');
  }

  // type에 따른 리스트 조회(ner, morph)
  getFileList(param: string): Observable<any> {
    let url: string = this.API_URL + '/dictionary/test/get-fileList/' + param + '/' + this.WORKSPACE_ID;
    console.log('getFileList - ', url);
    return this.http.post(url, null);
  }

  // type에 따른 파일 추가(ner, morph)
  insertFile(param: string, data: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/upload-file/' + param + '/'
      + this.WORKSPACE_ID + '/' + this.ID, data);
  }

  // type에 따른 파일 삭제(ner, morph)
  deleteFiles(param: string, ids: number[]): Observable<any> {
    console.log("delete");
    return this.http.post(this.API_URL + '/dictionary/test/delete-files/' + param + '/' + this.WORKSPACE_ID, ids);
  }
}
