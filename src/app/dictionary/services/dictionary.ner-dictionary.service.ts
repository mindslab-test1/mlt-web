import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {NerDictionaryLineEntity} from '../entity/dictionary.ner-dictionary-line.entity';
import {NerDictionaryEntity} from '../entity/dictionary.ner-dictionary.entity';

@Injectable()
export class NerDictionaryService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
    @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('workspaceId');
  }

  // ner 사전 관련
  getDictionaryList(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionaryList', null);
  }

  getDictionaryName(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionaryName', id);
  }


  updateDictionary(entity: NerDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/update-dictionary', entity);
  }

  deleteDictionaries(idList: string[]): Observable<any> {
    let entities: NerDictionaryEntity[] = [];
    for (let param of idList) {
      let entity: NerDictionaryEntity = new NerDictionaryEntity();
      entity.id = param;
      entity.name = '';
      entities.push(entity);
    }
    return this.http.post(this.API_URL + '/dictionary/ner/delete-dictionaries', entities);
  }

  insertDic(param: any): Observable<any> {
    let entity: NerDictionaryEntity = new NerDictionaryEntity();
    entity.workspaceId = param.workspaceId;
    entity.creatorId = this.ID;
    entity.name = param.name;
    return this.http.post(this.API_URL + '/dictionary/ner/insert-dictionary', entity);
  }

  getDictionary(name: String): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionary', name);
  }

  // ner 사전 contents로 존재하는 타입 리스트 가져오기
  getDictionaryContentsTypes(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionaryContents-type/' + this.WORKSPACE_ID
      + '/' + id, null);
  }

  // ner 사전 contents 관련
  getDictionaryContents(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionaryContents', param);
  }

  deleteDictionaryContents(nerEntities: NerDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/delete-dictionaryContents', nerEntities);
  }

  deleteDictionaryContentsAll(versionId: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/delete-dictionaryContentsAll', versionId);
  }

  insertDictionaryLines(nerEntities: NerDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/insert-dictionaryContents', nerEntities);
  }

  insertDictionaryLine(nerEntity: NerDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/insert-dictionaryLine', nerEntity);
  }

  updateDictionaryLine(nerEntity: NerDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/update-dictionaryLine', nerEntity);
  }

  // git
  commitDictionary(nerEntity: NerDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/commit-dictionary', nerEntity);
  }

  getContentsFromGit(nerEntity: NerDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/get-dictionary-from-git', nerEntity);
  }

  // Download
  downloadContents(data: NerDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/download-dictionary', data);
  }

  // Upload
  uploadContents(data: String[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/upload-dictionary', data);
  }

  // 특정 사전 제거
  deleteDictionary(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/delete-dictionary', id);
  }

  // 사전 적용
  applyDictionary(params: NerDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/apply-dictionary', params);
  }
}
