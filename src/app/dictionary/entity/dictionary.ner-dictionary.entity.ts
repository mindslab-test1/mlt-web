import {PageParameters} from 'mlt-shared';

export class NerDictionaryEntity extends PageParameters {
  id: string;
  name: string;
  creatorId: string;
  workspaceId: string;
  changeCnt: number;
  version: string;
  description: string;
  createdAt: Date;
}
