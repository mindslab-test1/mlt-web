import {PageParameters} from 'mlt-shared';

export class PostProcessEntity extends PageParameters {
  id: string;
  word: string;
  dicId: string;
  versionId: string;
  category: string;
  name: string;
  workspaceId: string;
  title: string;
  message: string;
}
