import {PageParameters} from 'mlt-shared';
export class DictionaryWiseNluNerEntity extends PageParameters {
  id: string;
  rule: string;
  category: string;
  dicId: string;
  versionId: string;
  name: string;
  workspaceId: string;
  title: string;
  message: string;
  processType: string;
}
