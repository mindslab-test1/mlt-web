import {PageParameters} from 'mlt-shared';

export class NerCorpusEntity extends PageParameters {
  id: number;
  sentence: string;
  createdAt: Date;
}
