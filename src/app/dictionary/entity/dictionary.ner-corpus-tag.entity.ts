import {PageParameters} from 'mlt-shared';

export class NerCorpusTagEntity extends PageParameters {
  id: number;
  tag: string;
  word: string;
  createdAt: Date;
}
