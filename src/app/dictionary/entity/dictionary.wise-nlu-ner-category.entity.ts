import {PageParameters} from 'mlt-shared';
export class DictionaryWiseNluNerCategoryEntity extends PageParameters {
  id: string;
  word: string;
  oldCategory: string;
  newCategory: string;
  dicId: string;
  versionId: string;
  name: string;
  workspaceId: string;
  title: string;
  message: string;
}
