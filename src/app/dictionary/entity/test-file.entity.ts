import {PageParameters} from 'mlt-shared';

export class TestFileEntity extends PageParameters {

  id: string;

  name: string;

  creator: string;

  date: Date;

  // Workspace
  workspaceId: string;

  // ner, morph
  type: string;

}
