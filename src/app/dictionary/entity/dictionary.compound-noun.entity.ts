import {PageParameters} from 'mlt-shared';

export class CompoundNounEntity extends PageParameters {

  id;

  word;
  pos;
  dicId;
  versionId;

  name;

  workspaceId;

  title;
  message;

}
