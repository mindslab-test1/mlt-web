import {PageParameters} from 'mlt-shared';

export class NerDictionaryLineEntity extends PageParameters {
  id: string;
  dicType: number;
  tag: string;
  pattern: string;
  word: string;
  originTag: string;
  changeTag: string;
  description: string;
  versionId: string;
  createdAt: Date;
  workspaceId: string;
}
