import {PageParameters} from 'mlt-shared';

export class MorphemeEntity extends PageParameters {

  id;

  // MorphemeDicLine
  word;
  pos;
  dicId;
  versionId;

  name;

  workspaceId;

  title;
  message;
}
