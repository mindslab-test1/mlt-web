import {PageParameters} from 'mlt-shared';

export class PosDictionaryLineEntity extends PageParameters {
  id: string;
  dicType: number;
  pattern: string;
  word: string;
	srcPos: string;
	destPos: string;
	compoundType: string;
  description: string;
  versionId: string;
  createdAt: Date;
  workspaceId: string;
}
