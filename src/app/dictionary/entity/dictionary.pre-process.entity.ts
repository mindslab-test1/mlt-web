import {PageParameters} from 'mlt-shared';

export class PreProcessEntity extends PageParameters {
  id: string;
  word: string;
  category: string;
  dicId: string;
  versionId: string;
  name: string;
  workspaceId: string;
  title: string;
  message: string;
}
