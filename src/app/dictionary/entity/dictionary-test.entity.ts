import {PageParameters} from 'mlt-shared';

export class PosTestEntity extends PageParameters {

  id: string;

  // Workspace
  workspaceId: string;

  // POSGroup
  fileGroupId: string;

  // POSResult
  fileGroup: string;

  // POSDic
  dicId: string;
  dicName: string;
  purpose: string;

  // checked list
  checkedTypeList: number[];
}
