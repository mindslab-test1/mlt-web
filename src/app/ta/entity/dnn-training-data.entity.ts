import {PageParameters} from 'mlt-shared';

export class DnnTrainingDataEntity extends PageParameters {

  // common
  id: string;
  workspaceId: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

  // DnnCategoryEntity
  dnnDicId: string;
  parentId: string;
  count: number;

  // DnnDicEntity
  versions: string;
  applied: string;


  // DnnDicLineEntity
  seq: number;
  sentence: string;
  category: string;
  versionId: string;

  // DnnCategoryEntity not entity, only data transfer use
  oldName: string;
  updateState: string;

  // DnnDicLineEntity not entity, only data transfer use
  duplicateCount: number;

  title: string;
  message: string;

}
