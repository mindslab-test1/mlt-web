export class NlpTestEntity {
  nlpName: string;
  workspaceId: string;
  context: string;
  level: string;
  isSplit: boolean;
}
