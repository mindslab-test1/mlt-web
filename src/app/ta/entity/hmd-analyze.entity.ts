import {PageParameters} from 'mlt-shared';

export class HmdAnalyzeEntity extends PageParameters {

  id: string;

  // Workspace
  workspaceId: string;

  // FileGroup
  fileGroupId: string;

  // HMDResult
  fileGroup: string;

  // HMDDic
  dicId: string;
  dicName: string;
  purpose: string;
}
