import {PageParameters} from 'mlt-shared';

export class ModelManagementEntity extends PageParameters {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;
  dnnUse: boolean;
  hmdUse: boolean;
  manDesc: string;
  name: string;
  workspaceId: string;
}
