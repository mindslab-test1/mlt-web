import {PageParameters} from 'mlt-shared';

export class HmdAnalyzeTestEntity extends PageParameters {

  id: string;

  // Workspace
  workspaceId: string;

  // FileGroup
  // HMDResult
  fileGroup: string;

  // HMDDic
  name: string;

  // File
  // for analyze HMD Test
  textData: string;
}
