import {PageParameters} from 'mlt-shared';

export class DnnResultEntity extends PageParameters {

  id: string;
  workspaceId: string;
  model: string;
  fileGroup: string;
  sentence: string;
  category: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

}
