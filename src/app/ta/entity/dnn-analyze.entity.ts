import {PageParameters} from 'mlt-shared';

export class DnnAnalyzeEntity extends PageParameters {

  // common
  id: string;
  name: string;

  // only DnnModelEntity
  dnnKey: string;
  workspaceId: string;
  dnnBinary: string;
  creatorId: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;

  // only FileGroup
  fileGroup: string;
  fileGroupId: string;
  purpose: string;

  // only File
  type: string;
  size: number;
  uploader: string;
  uploadedAt: Date;

}
