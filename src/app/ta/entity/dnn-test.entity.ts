export class DnnTestEntity {

  // dnnModelEntity
  dnnKey: string;
  workspaceId: string;
  name: string;
  dnnBinary: string;
  creatorId: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;

  // Transient
  textData: string;

  // grpc parameter
  probability: number;
  sentence: string;
  category: string;


}
