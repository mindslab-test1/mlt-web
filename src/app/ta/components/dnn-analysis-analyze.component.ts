import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AlertComponent, ConfirmComponent, StorageBrowser, TableComponent} from 'mlt-shared';
import {DnnAnalyzeEntity} from '../entity/dnn-analyze.entity';
import {DnnAnalyzeService} from '../services/dnn-analyze.service';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-ta-dnn-analysis-analyze',
  templateUrl: './dnn-analysis-analyze.component.html',
  styleUrls: ['./dnn-analysis-analyze.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DnnAnalysisAnalyzeComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;
  modelSelectSearchFormControl: FormControl = new FormControl();
  groupSelectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredModels: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredGroups: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  actions: any;

  pageLength: number;
  header = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'name', name: 'Name', isSort: true},
    {attr: 'type', name: 'Type', isSort: true},
    {attr: 'size', name: 'Size', format: 'file', isSort: true},
    {attr: 'creatorId', name: 'Uploader', isSort: true},
    {attr: 'createdAt', name: 'Uploaded at', format: 'date', isSort: true},
  ];
  fileList: DnnAnalyzeEntity[] = [];
  dataSource: MatTableDataSource<any>;

  fileGroupCodes: DnnAnalyzeEntity[] = [];
  models: DnnAnalyzeEntity[] = [];
  selectedCode: DnnAnalyzeEntity;
  selectedModel: DnnAnalyzeEntity;

  isAnalyze = true;
  matSort: MatSort;
  pageParam: MatPaginator;

  constructor(private storage: StorageBrowser,
              public dialog: MatDialog,
              private dnnAnalyzeService: DnnAnalyzeService) {
  }

  ngOnInit() {
    this.actions = [
      {
        type: 'analyze',
        text: 'Analyze',
        icon: 'insert_chart',
        callback: this.analyze,
        disabled: true,
      },
    ];

    this.modelSelectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterModel();
      });
    this.groupSelectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroup();
      });
  }

  ngAfterViewInit() {
    this.getAllModels();
    this.getFileGroups();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterModel() {
    if (!this.models) {
      return;
    }

    let keyword = this.modelSelectSearchFormControl.value;

    if (!keyword) {
      this.filteredModels.next(this.models.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredModels.next(
      this.models.filter(model => model.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  private filterGroup() {
    if (!this.fileGroupCodes) {
      return;
    }

    let keyword = this.groupSelectSearchFormControl.value;

    if (!keyword) {
      this.filteredGroups.next(this.fileGroupCodes.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredGroups.next(
      this.fileGroupCodes.filter(group => group.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearGroupSelectSearchFormControlValue() {
    this.groupSelectSearchFormControl.setValue('');
  }

  clearModelSelectSearchFormControlValue() {
    this.modelSelectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  getClassButton() {
    return this.isAnalyze === true ? 'mlt-disabled' : 'mlt-not-disabled';
  }

  readyToTrain() {
    if (this.selectedModel !== undefined && this.selectedCode !== undefined) {
      this.isAnalyze = false;
    } else {
      this.isAnalyze = true;
    }
  }

  analyze = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want Analyze?';

    ref.afterClosed().subscribe(result => {
      if (result) {
        let param = new DnnAnalyzeEntity();
        param.fileGroupId = this.selectedCode.id;
        param.fileGroup = this.selectedCode.name;
        param.purpose = this.selectedCode.purpose;
        param.workspaceId = this.storage.get('workspaceId');
        param.name = this.selectedModel.name;
        param.dnnKey = this.selectedModel.dnnKey;

        this.dnnAnalyzeService.startAnalyze(param).subscribe(
          res => {
            this.openAlertDialog('Analyze', `complete analyze.`, 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Error. Failed to Analyze', 'error');
          });
      }
    });
  };

  getFileGroups() {
    let param: DnnAnalyzeEntity = new DnnAnalyzeEntity();
    param.workspaceId = this.storage.get('workspaceId');

    this.dnnAnalyzeService.getFileGroups(param).subscribe(res => {
        this.fileGroupCodes = res;
        if (this.fileGroupCodes.length !== 0 && this.selectedCode === undefined) {
          this.selectedCode = this.fileGroupCodes[0];
          this.getFiles();
        }
        this.filteredGroups.next(this.fileGroupCodes.slice());

      },
      err => {
        this.openAlertDialog('Failed', 'Failed. get file groups.', 'error');
      });
  }

  getAllModels() {
    let param: DnnAnalyzeEntity = new DnnAnalyzeEntity();
    param.workspaceId = this.storage.get('workspaceId');

    this.dnnAnalyzeService.getAllModels(param).subscribe(
      res => {
        this.models = res;
        if (this.models.length !== 0 && this.selectedModel === undefined) {
          this.selectedModel = res[0];
        }
        this.filteredModels.next(this.models.slice());
      },
      err => {
        this.openAlertDialog('Failed', 'Failed. get dnn models.', 'error');
      });
  }

  getFiles() {
    let param = new DnnAnalyzeEntity();
    param.fileGroupId = this.selectedCode.id;
    param.fileGroup = this.selectedCode.name;
    param.purpose = this.selectedCode.purpose;
    param.workspaceId = this.storage.get('workspaceId');
    param.pageSize = this.pageParam.pageSize;
    param.pageIndex = this.pageParam.pageIndex;
    param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.dnnAnalyzeService.getFileList(param).subscribe(res => {
        if (res) {
          this.pageLength = res['totalElements'];
          this.fileList = res['content'];
          this.tableComponent.isCheckedAll = false;
          this.readyToTrain();
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Error. Failed get FileData', 'error');
      });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.fileList.length > 0) {
      this.getFiles();
    }
  }

  setSort(sort?: MatSort) {
    this.matSort = sort;

    if (this.fileList.length > 0) {
      this.getFiles();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
