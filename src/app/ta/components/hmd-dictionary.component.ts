import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryCategoryUpsertDialogComponent,
  GitComponent,
  StorageBrowser,
  TableComponent,
  TreeViewComponent
} from 'mlt-shared';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import {HmdDictionaryEntity} from '../entity/hmd-dictionary.entity';
import {HmdDictionaryService} from '../services/hmd-dictionary.service';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-hmd-dictionary',
  templateUrl: './hmd-dictionary.component.html',
  styleUrls: ['./hmd-dictionary.component.scss'],
  providers: [HmdDictionaryService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],
})

export class HmdDictionaryComponent implements OnInit, OnDestroy {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('appTree') appTree: TreeViewComponent;
  @ViewChild('gitComponent') git: GitComponent;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredHmdDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  workspace: any;
  selectedDictionary: any = '';
  hmdDicList = [];
  actions: any[] = [];
  editable = false;
  tipsVisible = false;
  currentCategory: any = null;
  pageParam: MatPaginator;
  pageLength: number;
  inputData = null;
  searchRule = null;

  header = [];

  defaultHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'category', name: 'Category', width: '15%'},
    {attr: 'rule', name: 'Rule', input: false}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'category', name: 'Category', width: '15%'},
    {attr: 'rule', name: 'Rule', input: true}
  ];

  hmdDictionaryLineList: any[] = [];
  data: any[] = [];
  dataSource: MatTableDataSource<any>;

  param: HmdDictionaryEntity = new HmdDictionaryEntity();
  categories: any = null;

  categoryCallback = {
    add: (next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.hmdDictionaryService;
      ref.componentInstance.next = next;

      ref.componentInstance.meta = {
        workspaceId: this.storage.get('workspaceId'),
        hmdDicId: this.selectedDictionary.id
      };
    },
    edit: (node: any, next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.hmdDictionaryService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        id: this.currentCategory.id,
        parentId: this.currentCategory.data.parentId,
        workspaceId: this.storage.get('workspaceId'),
        oldName: this.currentCategory.data.name,
        hmdDicId: this.selectedDictionary.id
      };
      ref.componentInstance.data = node;
      ref.afterClosed().subscribe(res => {
        if (res) {
          this.getHmdDictionaryLineAllList();
        }
      });
    },
    remove: (node: HmdDictionaryEntity, next: () => void) => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed()
        .subscribe(confirmed => {
          if (confirmed) {
            this.hmdDictionaryService.deleteCategory(node).subscribe(
              res => {
                if (res.message === 'DELETE_SUCCESS') {
                  this.openAlertDialog('Delete', `Category ${node.name} deleted.`, 'success');
                  next();
                } else if (res.message === 'RULE_EXISTENCE') {
                  this.openAlertDialog('Failed', `There is a rule in the category. Please delete the rule.`, 'error');
                }
              },
              err => {
                console.error('error', err);
                this.openAlertDialog('Error', `Server Error. Category ${node.name} not deleted.`, 'error');
              }
            );
          }
        });
    },
    setParent: (node: any, next: () => void) => {
      let hmdCategory: HmdDictionaryEntity = node;
      hmdCategory.workspaceId = this.storage.get('workspaceId');
      hmdCategory.hmdDicId = this.selectedDictionary.id;
      hmdCategory.updateState = 'structure';
      this.hmdDictionaryService.updateCategory(hmdCategory).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Update', `Category ${node.name} updated.`, 'success');
            next();
          }
        },
        err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server Error. Category Updated Failed.`, 'error');
        }
      );
    },
    canSetParent: (node: any, target: any, next: () => void) => {
      let targetCategory = target && target.data.name;
      if (!targetCategory) {
        return next();
      }
      this.param = new HmdDictionaryEntity();
      this.param.versionId = this.selectedDictionary.id;
      this.param.category = targetCategory;

      this.hmdDictionaryService.hasLines(this.param).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Failed',
              `There is a rule in the category you want to move. You can only delete category that do not have rules.`, 'error');
            // has no lines
          } else {
            next();
          }
        });
    },
    focus: (node: any) => {
      this.currentCategory = node;
      this.getHmdDictionaryLineAllList(0);
    },
    blur: () => {
      this.currentCategory = null;
    },
  };

  constructor(private hmdDictionaryService: HmdDictionaryService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.param = new HmdDictionaryEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.header = this.defaultHeader;

    this.getHmdDitionaryList().then(() => {
      this.getHmdCategoryAllList();
      this.filteredHmdDicList.next(this.hmdDicList.slice());

      this.selectSearchFormControl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterDictionary();
        });
    });

    this.actions = [
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'addLine',
        text: 'Add a rule',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'deleteLine',
        text: 'Delete checked rules',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Save',
        icon: 'save',
        callback: this.save,
        hidden: true
      },
    ];
  }

  private filterDictionary() {
    if (!this.hmdDicList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredHmdDicList.next(this.hmdDicList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredHmdDicList.next(
      this.hmdDicList.filter(hmdDic => hmdDic.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  getHmdDitionaryList() {
    return new Promise(resolve => {
      this.param = new HmdDictionaryEntity();
      this.param.workspaceId = this.storage.get('workspaceId');
      this.hmdDictionaryService.getHmdDicList(this.param).subscribe(
        res => {

          if (res['hmdDicList'].length !== 0) {
            this.hmdDicList = res['hmdDicList'];
            this.selectedDictionary = this.hmdDicList[0];
          }
          resolve();
        },
        err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server error. Failed to fetch sentences.`, 'error');
        }
      );
    });
  }

  changeRule(data) {
    this.inputData = data;
    this.param = new HmdDictionaryEntity();
    this.param.seq = this.inputData.seq;
    this.param.versionId = this.selectedDictionary.id;
    this.param.category = this.inputData.category;
    this.param.rule = this.inputData.rule;

    this.hmdDictionaryService.insertLine(this.param).subscribe(
      res => {
        if (res) {
        } else {
          return false;
        }
      }, err => {
        console.error('error', err);
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };

  apply = () => {
    if (this.lineTableComponent.rows.length !== 0) {
      this.param = new HmdDictionaryEntity();
      this.param.workspaceId = this.storage.get('workspaceId');
      this.param.name = this.selectedDictionary.name;
      this.param.id = this.selectedDictionary.id;
      this.param.applied = this.git.commitList[0].hashKey;
      this.param.message = this.git.commitList[0].message;

      this.hmdDictionaryService.hmdApply(this.param).subscribe(res => {
          if (res) {
            this.openAlertDialog('Apply', `complete apply.`, 'success');
          } else {
            return false;
          }
        }, err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server error. Failed to apply.`, 'error');
        }
      )
    } else {
      this.openAlertDialog('Failed', `Please add rule.`, 'error');
    }
  };

  getHmdCategoryAllList() {
    let promise = new Promise((resolve, reject) => {
      this.param = new HmdDictionaryEntity();
      this.param.id = this.selectedDictionary.id;
      this.param.workspaceId = this.storage.get('workspaceId');

      this.hmdDictionaryService.getHmdCategoryAllList(this.param).subscribe(
        res => {
          if (res) {
            this.categories = res;
            resolve();
          } else {
            return false;
          }
        }, err => {
          console.error('error', err);
          reject();
          this.openAlertDialog('Error', `Server error. Failed to fetch categories.`, 'error');
        }
      );
    });

    promise.then(() => {
      this.getHmdDictionaryLineAllList();
    });
  }

  changeCategory() {
    this.hmdDictionaryLineList = null;
    this.currentCategory = null;
    this.pageParam.pageIndex = 0;

    this.getHmdCategoryAllList();
  }

  addLine = (rule = '') => {
    if (!this.currentCategory || !this.currentCategory['isLeaf']) {
      this.openAlertDialog('Failed', `Cannot add a line to non-leaf category.`, 'error');
      return false;
    }

    let temp: HmdDictionaryEntity = new HmdDictionaryEntity();
    temp.category = this.currentCategory.data.name;
    temp.rule = rule;
    temp.versionId = this.selectedDictionary.id;

    this.hmdDictionaryService.insertLine(temp).subscribe(
      res => {
        if (res) {
          this.hmdDictionaryLineList.unshift({
            category: this.currentCategory.data.name,
            rule: null
          });
          this.getHmdDictionaryLineAllList().then(() => {
            this.appTree.currentNode.data.count += 1;
          });
        } else {
          return false;
        }
      }, err => {
        console.error('error', err);
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  };

  setLeafChildrenCount(node, hmdDicLineList) {
    if (node.children.length > 0) {
      node.children.forEach(childChildren => {
        this.setLeafChildrenCount(childChildren, hmdDicLineList);
      });
    } else {
      let findNodeCount: number = hmdDicLineList.filter(hmdDicLine => node.name === hmdDicLine.category).length;
      if (findNodeCount > 0) {
        node.count -= findNodeCount;
      }
    }
  }

  deleteLine = () => {
    let hmdDicLineList: HmdDictionaryEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (hmdDicLineList.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (hmdDicLineList.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.hmdDictionaryService.deleteLines(hmdDicLineList).subscribe(
            res => {
              if (res) {
                this.getHmdDictionaryLineAllList().then(() => {
                  this.appTree.nodes.forEach(node => {
                    this.setLeafChildrenCount(node, hmdDicLineList);
                  });
                  this.openAlertDialog('Delete', `The selected rule has been deleted.`, 'success');
                });
              } else {
                return false;
              }
            },
            err => {
              console.error('error', err);
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected rules.`, 'error');
            }
          );
        }
      });
    }
  };

  save = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new HmdDictionaryEntity();
          this.param.hmdDicId = this.selectedDictionary.id;
          this.param.id = this.selectedDictionary.id;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.storage.get('workspaceId');
          this.param.title = result.title;
          this.param.message = result.message;

          this.hmdDictionaryService.commit(this.param).subscribe(
            res => {
              this.header = this.defaultHeader;
              this.updateToolbar();
              this.getHmdDictionaryLineAllList();
            },
            err => {
              console.error('error', err);
            });
        }
      });
  };


  onUploadFile = () => {
    // if (this.header.find(item => item.input)) {
    // this.header = this.defaultHeader;
    //   this.getSaveAction(false);
    //   this.updateToolbar();
    // }
    this.getHmdCategoryAllList();
  };

  updateToolbar() {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'apply') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
  }

  getSaveAction(state) {
    let action = this.actions.filter(item => {
      return item.type === 'save';
    });
    action.forEach(item => {
      item.hidden = state;
    });
  }

  download() {
    alert('download');
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.hmdDictionaryLineList.length > 0) {
      this.getHmdDictionaryLineAllList();
    }

  }

  getHmdDictionaryLineAllList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.param = new HmdDictionaryEntity();
    this.param.versionId = this.selectedDictionary.id;
    if (this.currentCategory) {
      this.param.category = this.currentCategory.data.name;
    }
    this.param.rule = this.searchRule;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;

    return new Promise(resolve => {
      this.hmdDictionaryService.getAllDicLines(this.param).subscribe(
        res => {
          if (res) {
            this.lineTableComponent.isCheckedAll = false;
            this.hmdDictionaryLineList = res['dicLineList'];
            this.pageLength = res['total'];
          } else {
            return false;
          }
          resolve();
        },
        err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server Error. Failed to fetch rules.`, 'error');
        }
      )
    });
  }
}
