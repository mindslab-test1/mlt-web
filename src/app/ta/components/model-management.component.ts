import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AlertComponent, ConfirmComponent, StorageBrowser, TableComponent} from 'mlt-shared';
import {ModelUpsertDialogComponent} from './model-upsert-dialog.component';
import {ModelInsertService} from '../services/model-insert.service';
import {ModelManagementEntity} from '../entity/modelManagement.entity';

@Component({
  selector: 'app-ta-model-management',
  templateUrl: './model-management.component.html',
  styleUrls: ['./model-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ModelInsertService],
})
export class ModelManagementComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  actions: any;

  data: any[] = [];
  inputName: string;
  inputDescription: string;
  deleteModel: any[] = [];
  length: number;
  pageParam: MatPaginator;
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'checkbox', name: '', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'name', name: 'Name', width: '15%', isSort: true},
    {attr: 'manDesc', name: 'Description', isSort: true},
    {attr: 'hmdUse', name: 'HMD', width: '7%', isSort: true},
    {attr: 'dnnUse', name: 'DNN', width: '7%', isSort: true},
    {attr: 'action', name: 'Action', width: '7%', isButton: true, buttonName: 'Edit'},
  ];
  sort: MatSort;

  constructor(private dialog: MatDialog,
              private modelService: ModelInsertService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.actions = [
      {type: 'add', text: 'Add', icon: 'add_circle_outline', callback: this.createModel},
      {type: 'remove', text: 'Remove', icon: 'remove_circle_outline', callback: this.deleteModels},
    ];
  }

  ngAfterViewInit() {
    this.getList();
  }


  setSort(matSort?: MatSort) {
    this.sort = matSort;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getpaginator(page?: MatPaginator) {
    this.pageParam = page;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getList(pageIndex?) {
    let model: ModelManagementEntity = new ModelManagementEntity();
    this.cdr.detectChanges();
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    model.pageSize = this.pageParam.pageSize;
    model.pageIndex = this.pageParam.pageIndex;
    model.orderDirection = this.sort.direction === '' ||
    this.sort.direction === undefined ? 'desc' : this.sort.direction;
    model.orderProperty = this.sort.active === '' ||
    this.sort.active === undefined ? 'createdAt' : this.sort.active;
    model.name = this.inputName;
    model.manDesc = this.inputDescription;
    model.workspaceId = this.storage.get('workspaceId');

    this.modelService.getHmdOrDnn(model).subscribe(res => {
      this.data = res.content;
      this.tableComponent.isCheckedAll = false;
      this.length = res.totalElements;
    });
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  createModel = () => {
    let ref = this.dialog.open(ModelUpsertDialogComponent);
    ref.afterClosed().subscribe(res => {
      if (res === true) {
        this.openAlertDialog('Success', 'Success Add Model', 'success');
        this.getList();
      }
    })
  };

  editModel(data) {
    let ref: any = this.dialog.open(ModelUpsertDialogComponent);
    ref.componentInstance.data = data;
    ref.afterClosed().subscribe(res => {
      if (res === true) {
        this.openAlertDialog('Success', 'Success Edit Model', 'success');
        this.getList();
      }
    })
  }

  deleteModels = () => {
    this.deleteModel.length = 0;
    this.data.forEach(items => {
      if (items.isChecked) {
        this.deleteModel.push({
          id: items.id,
          hmdDicId: items.hmdDicId,
          dnnDicId: items.dnnDicId
        });
      }
    });

    if (this.deleteModel.length === 0) {
      this.openAlertDialog('notice', 'Model has to be selected', 'notice');
      return null;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove models?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (this.deleteModel.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.modelService.deleteSelectedModels(this.deleteModel).subscribe(res => {
          if (res) {
            this.openAlertDialog('Success', 'Success Remove Model', 'success');
            this.getList();
          }
        }, err => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. remove models failed.', 'error');
        });
      }
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
