import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  DictionaryCategoryUpsertDialogComponent,
  DictionaryUpsertDialogComponent,
  StorageBrowser,
  TableComponent,
  TreeViewComponent
} from 'mlt-shared';
import {DnnTrainingDataService} from '../services/dnn-training-data.service';
import {DnnTrainingDataEntity} from '../entity/dnn-training-data.entity';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-dnn-training-data',
  templateUrl: './dnn-training-data.component.html',
  styleUrls: ['./dnn-training-data.component.scss'],
})

export class DnnTrainingDataComponent implements OnInit, OnDestroy {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('appTree') appTree: TreeViewComponent;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredDnnDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  pageParam: MatPaginator;
  pageLength: number;

  selectedDnnDic = new DnnTrainingDataEntity;
  currentCategory: any = null;
  dnnDics: DnnTrainingDataEntity[] = [];
  actions: any[] = [];
  editable = false;
  inputData = null;
  searchSentence: string;

  dnnDicLineEntity = new DnnTrainingDataEntity;
  categories: any = null;

  header = [];

  defaultHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: false},
    {attr: 'category', name: 'Category', width: '35%'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: true},
    {attr: 'category', name: 'Category', width: '35%'}
  ];

  dnnDicLineList: any[] = [];
  dataSource: MatTableDataSource<any>;

  workspaceId: string;

  categoryCallback = {
    add: (next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.dnnTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        workspaceId: this.workspaceId,
        dnnDicId: this.selectedDnnDic.id
      };
    },
    edit: (node: any, next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.dnnTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        id: this.currentCategory.id,
        parentId: this.currentCategory.data.parentId,
        workspaceId: this.workspaceId,
        oldName: this.currentCategory.data.name,
        dnnDicId: this.selectedDnnDic.id
      };
      ref.componentInstance.data = node;

      ref.afterClosed().subscribe(res => {
        if (res) {
          this.getDnnDicLineAllList();
        }
      });
    },
    remove: (node: DnnTrainingDataEntity, next: () => void) => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed()
      .subscribe(confirmed => {
        if (confirmed) {
          this.dnnTrainingDataService.deleteCategory(node).subscribe(
            res => {
              if (res.message === 'DELETE_SUCCESS') {
                this.openAlertDialog('Delete', `Category ${node.name} deleted.`, 'success');
                next();
              } else if (res.message === 'SENTENCE_EXISTENCE') {
                this.openAlertDialog('Failed', `There is a sentence in the category. Please delete the sentence.`, 'error');
              }
            },
            err => {
              this.openAlertDialog('Error', `Server Error. Category ${node.name} not deleted.`, 'error');
            }
          );
        }
      });
    },
    setParent: (node: any, next: () => void) => {
      let dnnCategory: DnnTrainingDataEntity = node;
      dnnCategory.workspaceId = this.workspaceId;
      dnnCategory.dnnDicId = this.selectedDnnDic.id;
      dnnCategory.updateState = 'structure';
      this.dnnTrainingDataService.updateCategory(dnnCategory).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Update', `Category ${node.name} updated.`, 'success');
            next();
          }
        },
        err => {
          this.openAlertDialog('Error', `Server Error. Category Updated Failed.`, 'error');
        }
      );
    },
    canSetParent: (node: any, target: any, next: () => void) => {
      let targetCategory = target && target.data.name;
      if (!targetCategory) {
        return next();
      }

      this.dnnDicLineEntity = new DnnTrainingDataEntity();
      this.dnnDicLineEntity.versionId = this.selectedDnnDic.id;
      this.dnnDicLineEntity.category = targetCategory;

      this.dnnTrainingDataService.hasLines(this.dnnDicLineEntity).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Failed',
              `There is a sentence in the category you want to move. You can only delete category that do not have sentences.`, 'error');
            // has no lines
          } else {
            next();
          }
        });
    },
    focus: (node: any) => {
      this.currentCategory = node;
      this.getDnnDicLineAllList(0);
    },
    blur: () => {
      this.currentCategory = null;
    },
  };

  constructor(private dnnTrainingDataService: DnnTrainingDataService,
              private storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');
    this.header = this.defaultHeader;

    this.getDnnDicList().then(() => {
      this.getDnnCategoryAllList();
      this.filteredDnnDicList.next(this.dnnDics.slice());

      this.selectSearchFormControl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterDictionary();
        });
    });

    this.actions = [
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'addLine',
        text: 'Add a sentence',
        icon: 'playlist_add',
        callback: this.addLine,
        hidden: true
      },
      {
        type: 'deleteLine',
        text: 'Delete checked sentences',
        icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: true
      },
      {
        type: 'deleteDupLine',
        text: 'Delete duplicates',
        icon: 'delete_sweep',
        callback: this.deleteDupLine,
        hidden: true
      },
      {
        type: 'save',
        text: 'Save',
        icon: 'save',
        callback: this.save,
        hidden: true
      },
    ];
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterDictionary() {
    if (!this.dnnDics) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredDnnDicList.next(this.dnnDics.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredDnnDicList.next(
      this.dnnDics.filter(dnnDic => dnnDic.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  getDnnDicList() {
    return new Promise(resolve => {
      this.dnnTrainingDataService.getDnnDicAllList(this.workspaceId).subscribe(
        res => {
          if (res.length !== 0) {
            this.dnnDics = res;
            this.selectedDnnDic = this.dnnDics[0];
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Error', `Server error. Failed to fetch sentences.`, 'error');
        }
      );
    });
  }

  getDnnCategoryAllList() {
    let promise = new Promise((resolve, reject) => {
      this.dnnTrainingDataService.getDnnCategoryAllList(this.workspaceId, this.selectedDnnDic.id).subscribe(
        res => {
          if (res) {
            this.categories = res;
            resolve();
          }
        }, err => {
          this.openAlertDialog('Error', `Server error. Failed to fetch categories.`, 'error');
          reject();
        }
      );
    });

    promise.then(() => {
      this.getDnnDicLineAllList();
    });
  }

  changeCategory() {
    this.dnnDicLineList = null;
    this.currentCategory = null;
    this.pageParam.pageIndex = 0;

    this.getDnnCategoryAllList();
  }

  edit = () => {
    this.header = this.editHeader;
    this.updateToolbar();
  };

  addLine = (sentence = '') => {
    if (!this.currentCategory || !this.currentCategory['isLeaf']) {
      this.openAlertDialog('Failed', `Cannot add a line to non-leaf category.`, 'error');
      return false;
    }
    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.category = this.currentCategory.data.name;
    this.dnnDicLineEntity.sentence = sentence;
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.id;

    this.dnnTrainingDataService.insertLine(this.dnnDicLineEntity).subscribe(
      res => {
        this.dnnDicLineList.unshift({category: this.currentCategory.data.name, sentence: null});
        this.getDnnDicLineAllList().then(() => {
          this.appTree.currentNode.data.count += 1;
        });
      }, err => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  };

  setLeafChildrenCount(node, dnnDicLineList) {
    if (node.children.length > 0) {
      node.children.forEach(childChildren => {
        this.setLeafChildrenCount(childChildren, dnnDicLineList);
      });
    } else {
      let findNodeCount: number = dnnDicLineList.filter(dnnDicLine => node.name === dnnDicLine.category).length;
      if (findNodeCount > 0) {
        node.count -= findNodeCount;
      }
    }
  }

  deleteLine = () => {
    let dnnTrainingDataEntities: DnnTrainingDataEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (dnnTrainingDataEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (dnnTrainingDataEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dnnTrainingDataService.deleteLines(dnnTrainingDataEntities).subscribe(
            res => {
              if (res) {
                this.getDnnDicLineAllList().then(() => {
                  this.appTree.nodes.forEach(node => {
                    this.setLeafChildrenCount(node, dnnTrainingDataEntities);
                  });
                  this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
                });
              }
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  deleteDupLine = () => {
    if (this.currentCategory === null) {
      this.openAlertDialog('Notice', `select category please.`, 'notice');
    } else {
      this.dnnTrainingDataService.getDuplicatesLines(this.selectedDnnDic.id, this.currentCategory.data.name).subscribe(
        res => {
          if (res['resultCode'] === 'SUCCESS') {
            let resultMessage: string;
            res['dicLine'].forEach(item => {
              resultMessage += `
                <b>Sentence</b> : ${item.sentence}  <b>Count</b>: [${item.duplicateCount}]<br>
            `;
            });
            let ref = this.dialog.open(ConfirmComponent);
            ref.componentInstance.message = `
                There are ${res['dicLine'].length} kinds of duplicate sentences.<br>
                <br>${resultMessage}<br>
                Do you want to clear above duplicates?
          `;
            ref.afterClosed().subscribe(
              confirmed => {
                if (confirmed) {
                  this.dnnDicLineEntity = new DnnTrainingDataEntity();
                  this.dnnDicLineEntity.versionId = this.selectedDnnDic.id;
                  this.dnnDicLineEntity.category = this.currentCategory.data.name;

                  this.dnnTrainingDataService.deleteDuplicatesLines(this.dnnDicLineEntity).subscribe(
                    result => {
                      if (result) {
                        let sum = 0;
                        res['dicLine'].forEach(dicLine => {
                          // 자기 자신을 제외한 dicLine 삭제 하기 때문에 -1을 해줌
                          sum += (dicLine.duplicateCount - 1);
                        });

                        this.getDnnDicLineAllList(0).then(() => {
                          this.appTree.currentNode.data.count -= sum;
                        });
                      }
                    }, err => {
                      this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
                    }
                  )
                }
              });
          } else if (res['resultCode'] === 'NO_DUPLICATION') {
            this.openAlertDialog('No Duplication', `There are no duplicate sentence in this category.`, 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
        }
      )
    }
  };

  save = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDnnDic.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          let param = new DnnTrainingDataEntity();
          param.id = this.selectedDnnDic.id;
          param.name = this.selectedDnnDic.name;
          param.workspaceId = this.selectedDnnDic.workspaceId;
          param.title = result.title;
          param.message = result.message;

          this.dnnTrainingDataService.commit(param).subscribe(
            res => {
              this.header = this.defaultHeader;
              this.updateToolbar();
              this.getDnnDicLineAllList();
            },
            err => {

            });
        }
      });
  };

  onUploadFile = () => {
    // if (this.header.find(item => item.input)) {
    //   this.header = this.defaultHeader;
    //   this.getSaveAction(false);
    //   this.updateToolbar();
    // }
    this.getDnnCategoryAllList();
  };

  updateToolbar() {
    this.actions.forEach(action => {
      if (action.type === 'edit') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
  }

  getSaveAction(state) {
    let action = this.actions.filter(item => {
      return item.type === 'commit';
    });
    action.forEach(item => {
      item.hidden = state;
    });
  }

  add = () => {
    let ref: any = this.dialog.open(DictionaryUpsertDialogComponent);
    ref.componentInstance.dicApi = this.dnnTrainingDataService;
    ref.componentInstance.workspaceId = this.workspaceId;
  };

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }
    if (this.dnnDicLineList.length > 0) {
      this.getDnnDicLineAllList();
    }
  };

  getDnnDicLineAllList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.id;
    if (this.currentCategory) {
      this.dnnDicLineEntity.category = this.currentCategory.data.name;
    }

    this.dnnDicLineEntity.sentence = this.searchSentence;
    this.dnnDicLineEntity.pageSize = this.pageParam.pageSize;
    this.dnnDicLineEntity.pageIndex = this.pageParam.pageIndex;

    return new Promise(resolve => {
      this.dnnTrainingDataService.getAllDicLines(this.dnnDicLineEntity).subscribe(
        res => {
          if (res) {
            this.dnnDicLineList = res['dicLineList'];
            this.lineTableComponent.isCheckedAll = false;
            this.pageLength = res['total'];
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Error', `Server Error. Failed to fetch sentences.`, 'error');
        }
      )
    });
  }

  changeSentence(data) {
    this.inputData = data;
    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.seq = this.inputData.seq;
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.id;
    this.dnnDicLineEntity.category = this.inputData.category;
    this.dnnDicLineEntity.sentence = this.inputData.sentence;

    this.dnnTrainingDataService.insertLine(this.dnnDicLineEntity).subscribe(
      res => {
      }, err => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
