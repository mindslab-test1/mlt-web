import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';

import {AlertComponent, FormErrorStateMatcher, StorageBrowser, TableComponent} from 'mlt-shared';
import {DnnTestService} from '../services/dnn-test.service';
import {DnnTestEntity} from '../entity/dnn-test.entity';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-ta-dnn-analysis-test',
  templateUrl: './dnn-analysis-test.component.html',
  styleUrls: ['./dnn-analysis-test.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class DnnAnalysisTestComponent implements OnInit, OnDestroy {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredDnnModels: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  // select
  dnnModels: DnnTestEntity[] = [];
  selectedModel: DnnTestEntity = null;

  textData: string;
  error: string;
  loading = false;
  textFormControl = new FormControl('', [
    Validators.required,
  ]);
  submitButton = true;


  // table
  header = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence'},
    {attr: 'category', name: 'Category', isSort: true},
    {attr: 'probability', name: 'Probability', isSort: true},
  ];
  rows: DnnTestEntity[] = [];
  dataSource: MatTableDataSource<any>;
  param: DnnTestEntity;

  constructor(private storage: StorageBrowser,
              public matcher: FormErrorStateMatcher,
              private dialog: MatDialog,
              private dnnTestService: DnnTestService) {
  }

  ngOnInit() {
    this.getAllModels();
    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDnnModels();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterDnnModels() {
    if (!this.dnnModels) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredDnnModels.next(this.dnnModels.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredDnnModels.next(
      this.dnnModels.filter(model => model.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  sortHeader(sort?: MatSort) {
    if (this.dnnModels.length > 0) {
      const data = this.tableComponent.dataSource.data.slice();
      if (!sort.active || sort.direction === '') {
        this.rows = data;
        return;
      }

      this.rows = data.sort((a, b) => {
        let isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'category':
            return this.compare(a.category, b.category, isAsc);
          case 'probability':
            return this.compare(+a.probability, +b.probability, isAsc);
          default:
            return 0;
        }
      });
    }
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getAllModels() {
    this.param = new DnnTestEntity();
    this.param.workspaceId = this.storage.get('workspaceId');

    this.dnnTestService.getAllModels(this.param).subscribe(
      res => {
        this.dnnModels = res;
        this.tableComponent.isCheckedAll = false;
        if (this.dnnModels.length !== 0 && this.selectedModel === null) {
          this.selectedModel = res[0];
        }
        this.filteredDnnModels.next(this.dnnModels.slice());

      },
      err => {
        this.openAlertDialog('Failed', 'Error. failed fetch dnn model list.', 'error');
      });

  }

  isSubmit() {
    if (!this.matcher.isErrorState(this.textFormControl) && this.textData !== undefined) {
      this.submitButton = false;
    } else {
      this.submitButton = true;
    }
  }

  submit() {
    this.param = new DnnTestEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.dnnKey = this.selectedModel.dnnKey;
    this.param.name = this.selectedModel.name;
    this.param.textData = this.textData;

    if (!this.submitButton) {
      this.loading = true;
      this.dnnTestService.taDnnAnalyzeText(this.param).subscribe(
        res => {
          if (res) {
            this.rows = res;
            this.loading = false;
          }
        },
        err => {
          this.openAlertDialog('Failed', 'Server error. test failed.', 'error');
          this.loading = false;
        }
      );
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
