import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  ConfirmComponent,
  DownloadService,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {DnnResultEntity} from '../entity/dnn-result.entity';
import {DnnResultService} from '../services/dnn-result.service';

@Component({
  selector: 'app-ta-dnn-analysis-result',
  templateUrl: './dnn-analysis-result.component.html',
  styleUrls: ['./dnn-analysis-result.component.scss'],
})
export class DnnAnalysisResultComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  inputModel: string;
  inputFileGroup: string;
  inputSentence: string;
  inputCategory: string;

  actions: any;
  header = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'model', name: 'Model', width: '10%', isSort: true},
    {attr: 'fileGroup', name: 'File Group', width: '10%', isSort: true},
    {attr: 'sentence', name: 'Sentence'},
    {attr: 'category', name: 'Category', width: '10%', isSort: true},
    {attr: 'probability', name: 'Probability', isSort: true},
    {attr: 'createdAt', name: 'Created at', width: '10%', format: 'date', isSort: true},
  ];
  matSort: MatSort;
  dnnResults: DnnResultEntity[] = [];
  dataSource: MatTableDataSource<any>;
  pageLength: number;
  pageParam: MatPaginator;
  param: DnnResultEntity;

  constructor(private storage: StorageBrowser,
              private dnnResultService: DnnResultService,
              private downloadService: DownloadService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.actions = [
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        callback: this.download,
        disabled: true,
      },
      {
        type: 'clearResults',
        text: 'Clear Results',
        icon: 'delete_sweep',
        callback: this.clear,
        disabled: true,
      },
    ];
  }

  ngAfterViewInit() {
    this.getAllDnnResults();
  }

  search(matSort?: MatSort) {
    this.matSort = matSort;

    if (this.dnnResults.length > 0) {
      this.getAllDnnResults();
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  download = () => {
    this.downloadService.downloadCsvFile(this.tableComponent.rows, this.header, 'result');
  };

  clear = () => {
    this.dialog.open(ConfirmComponent).afterClosed().subscribe(
      result => {
        if (!result) {
          return false;
        } else {
          if (this.dnnResults.length > 0) {
            this.dnnResultService.deleteByDnnResult(this.param).subscribe(
              res => {
                this.openAlertDialog('Delete', `Delete success. ${res['deleteCount']} deleted.`, 'success');
                this.getAllDnnResults();
              },
              err => {
                this.openAlertDialog('Failed', 'Error. delete failed.', 'error');
              });
          }
        }
      });
  };

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.dnnResults.length > 0) {
      this.getAllDnnResults();
    }
  }

  getAllDnnResults(pageIndex?) {

    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.param = new DnnResultEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.model = this.inputModel;
    this.param.fileGroup = this.inputFileGroup;
    if (this.inputSentence === '') {
      this.param.sentence = null;
    } else {
      this.param.sentence = this.inputSentence;
    }
    this.param.category = this.inputCategory;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.dnnResultService.getAllDnnResults(this.param).subscribe(
      res => {
        this.pageLength = res['totalElements'];
        this.dnnResults = res['content'];
        this.tableComponent.isCheckedAll = false;
      },
      err => {
        this.openAlertDialog('Failed', 'Error. failed fetch analysis result list', 'error');
      });
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
