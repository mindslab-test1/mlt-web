import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModelInsertService} from '../services/model-insert.service';
import {StorageBrowser} from 'mlt-shared';
import {ModelManagementEntity} from '../entity/modelManagement.entity';

@Component({
  selector: 'app-ta-model-upsert-dialog',
  templateUrl: './model-upsert-dialog.component.html',
  providers: [ModelInsertService]
})
export class ModelUpsertDialogComponent implements OnInit {

  model: FormGroup;
  data: any;
  userId: string;
  result: any;
  modelManagementEntity: ModelManagementEntity = new ModelManagementEntity();
  addModelInput = new FormControl('', [Validators.required]);

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder,
              private modelService: ModelInsertService,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.userId = this.storage.get('user').id
    console.log('addModelInput', this.addModelInput);
    this.model = this.fb.group({
      name: ['', []],
      manDesc: ['', []],
      hmdUse: [false, []],
      dnnUse: [false, []],
      hmdDicId: ['', []],
      dnnDicId: ['', []],
      id: [null, []]
    });
  }

  ngOnInit() {
    // apply data when update
    let data = this.data = this.dialogRef.componentInstance.data;
    if (data) {
      this.model.setValue({
        name: data.name,
        manDesc: data.manDesc,
        hmdUse: data.hmdUse,
        dnnUse: data.dnnUse,
        hmdDicId: data.hmdDicId,
        dnnDicId: data.dnnDicId,
        id: data.id
      });
      this.model.controls['name'].disable();
    }
  }

  checkDnnAndHmd(): boolean {
    if (!(this.model.value.hmdUse || this.model.value.dnnUse)) {
      this.addModelInput.setErrors({
        'select': true
      });
      return false;
    } else {
      return true
    }
    ;
  }

  upsertModel() {
    if (this.checkDnnAndHmd()) {
      if (!this.data) {
        this.modelManagementEntity = this.setModelManagement();
        if (this.modelManagementEntity === null) {
          return null;
        }
        this.modelService.insertHmdOrDnn(this.modelManagementEntity).subscribe(res => {
          this.dialogRef.close(true);
        }, err => {
          if (err.status === 409) {
            this.addModelInput.setErrors({
              'duplicate': true
            });
          }
        });
      } else if (this.data) {
        this.modelManagementEntity = this.setModelManagement();
        if (this.modelManagementEntity === null) {
          return null;
        }
        this.modelService.updateHmdOrDnn(this.modelManagementEntity).subscribe(res => {
          this.dialogRef.close(true);
        }, err => {
          if (err.status === 409) {
            this.addModelInput.setErrors({
              'duplicate': true
            });
          }
        });
      }
    }
  }

  setModelManagement() {
    if (this.model.value.name === '') {
      this.addModelInput.setErrors({
        'null': true
      });
      return null;
    }
    this.modelManagementEntity = this.model.value;
    this.modelManagementEntity.creatorId = this.userId;
    this.modelManagementEntity.updaterId = this.userId;
    this.modelManagementEntity.workspaceId = this.storage.get('workspaceId');
    return this.modelManagementEntity;
  }

  getErrorMessage() {
    return this.addModelInput.hasError('duplicate') ? 'Model Name already exist' :
      this.addModelInput.hasError('select') ? 'Dnn or Hmd have to be selected' :
        this.addModelInput.hasError('null') ? 'Name required' :
          '';
  }
}
