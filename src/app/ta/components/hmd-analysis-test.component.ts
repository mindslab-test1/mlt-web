import {Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {AlertComponent, StorageBrowser, TableComponent} from 'mlt-shared';
import {HmdAnalyzeTestEntity} from '../entity/hmd-analyze-test.entity';
import {HmdAnalyzeTestService} from '../services/hmd-analyze-test.service';
import {FormControl} from "@angular/forms";
import {Subject, ReplaySubject} from 'rxjs';
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-ta-hmd-analysis-test',
  templateUrl: './hmd-analysis-test.component.html',
  styleUrls: ['./hmd-analysis-test.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [HmdAnalyzeTestService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],

})
export class HmdAnalysisTestComponent implements OnInit, OnDestroy {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredHmdDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);


  // select
  workspace: any;
  workspaceId: any;
  hmdDicList = [];
  selectedDictionary: any = '';
  loading = false;
  textData: any;
  length: number;

  // table
  testResultData: any[] = [];
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'no', name: 'No', no: true,  width: '2%'},
    {attr: 'sentence', name: 'Sentence',  width: '60%'},
    {attr: 'rule', name: 'Rule',  width: '30%'},
    {attr: 'category', name: 'Category', isSort: true,  width: '8%'}
  ];


  constructor(private hmdAnalyzeTestService: HmdAnalyzeTestService,
              private dialog: MatDialog,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');

    this.hmdAnalyzeTestService.getHMDDicList(this.getHMDAnalyzeTestEntity()).subscribe(result => {
        if (result) {
          result['hmdDicList'].forEach((value) => {
            this.hmdDicList.push({value: value.id, viewValue: value.name});
          });
          this.filteredHmdDicList.next(this.hmdDicList.slice());

          this.selectSearchFormControl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
              this.filterDictionary();
            });
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterDictionary() {
    if (!this.hmdDicList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredHmdDicList.next(this.hmdDicList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredHmdDicList.next(
      this.hmdDicList.filter(hmdDic => hmdDic.viewValue.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  orderByHeader(sort?: MatSort) {
    if (this.hmdDicList.length <= 0) {
      return false;
    }
    const data = this.tableComponent.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.testResultData = data;
      return;
    }

    this.testResultData = data.sort((a, b) => {
      let isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'category':
          return this.compare(a.category, b.category, isAsc);
        default:
          return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getHMDAnalyzeTestEntity(): HmdAnalyzeTestEntity {
    let hmdAnalyzeTestEntity: HmdAnalyzeTestEntity = new HmdAnalyzeTestEntity();
    hmdAnalyzeTestEntity.workspaceId = this.workspaceId;
    hmdAnalyzeTestEntity.textData = this.textData;
    if (!(this.selectedDictionary === '')) {
      hmdAnalyzeTestEntity.name = this.selectedDictionary.viewValue;
    }
    return hmdAnalyzeTestEntity;
  }

  submit() {
    this.loading = true;
    this.getHMDAnalyzeTestResultList();
    this.loading = false;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  getHMDAnalyzeTestResultList() {
    this.testResultData.length = 0;
    this.hmdAnalyzeTestService.getHMDAnalyzeTestResultList(this.getHMDAnalyzeTestEntity()).subscribe(result => {
        if (result) {
          this.openAlertDialog('Analyze', `Classified Sentence: ${result.success}<br> Not classified Sentence: ${result.fail}`, 'success');
          console.log('category, ', result);
          this.length = result['totalElement'];
          result.result.forEach((value) => {
            this.testResultData.push({
              id: value['id'],
              sentence: value['sentence'],
              rule: value['rule'],
              category: value['category'],
            })
          });
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Test', 'error');
      }
    );
  }
}
