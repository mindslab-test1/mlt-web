import {
  AfterViewInit,
  Component,
  Inject, OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {AlertComponent, ConfirmComponent, StorageBrowser, TableComponent} from 'mlt-shared';
import {HmdAnalyzeService} from '../services/hmd-analyze-analyze.service';
import {HmdAnalyzeEntity} from '../entity/hmd-analyze.entity';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-ta-hmd-analysis-analyze',
  templateUrl: './hmd-analysis-analyze.component.html',
  styleUrls: ['./hmd-analysis-analyze.component.scss'],
  providers: [HmdAnalyzeService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],
  encapsulation: ViewEncapsulation.None
})
export class HmdAnalysisAnalyzeComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  dictionarySelectSearchFormControl: FormControl = new FormControl();
  groupSelectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredHmdDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredFileGroupDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);


  matSort: MatSort = new MatSort();
  workspace: any;
  workspaceId: any;
  actions: any;
  fileList: any[] = [];
  dataSource: MatTableDataSource<any>;
  length: number;
  param: HmdAnalyzeEntity;
  header = [
    {attr: 'no', name: 'No', no: true},
    {attr: 'name', name: 'Name', isSort: true},
    {attr: 'type', name: 'Type', isSort: true},
    {attr: 'size', name: 'Size', isSort: true}
  ];

  fileGroupList = [];

  hmdDicList = [];

  paginator: MatPaginator;
  selectedFileGroup: any;
  selectedDictionary: any;

  constructor(private hmdAnalyzeService: HmdAnalyzeService,
              private dialog: MatDialog,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('workspaceId');

    let hmdAnalyzeEntity: HmdAnalyzeEntity = new HmdAnalyzeEntity();
    hmdAnalyzeEntity.workspaceId = this.workspaceId;

    this.hmdAnalyzeService.getHMDDicList(hmdAnalyzeEntity).subscribe(result => {
        if (result) {
          result['hmdDicList'].forEach((value) => {
            this.hmdDicList.push({value: value.id, viewValue: value.name});
          });
          this.filteredHmdDicList.next(this.hmdDicList.slice());

          this.dictionarySelectSearchFormControl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
              this.filterDictionary();
            });
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Dic List', 'error');
      }
    );

    this.hmdAnalyzeService.getFileGroupList(hmdAnalyzeEntity).subscribe(result => {
        if (result) {
          result.forEach((value) => {
            this.fileGroupList.push({value: value['id'], viewValue: value['name']});
          });
          this.filteredFileGroupDicList.next(this.fileGroupList.slice());

          this.groupSelectSearchFormControl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
              this.filterFileGroup();
            });
        } else {
          return false;
        }
      }
      ,
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading File Group List', 'error');
      }
    );


    this.actions = [
      {
        type: 'analyze',
        text: 'Analyze',
        icon: 'insert_chart',
        callback: this.onClickAnalizeButton,
        disabled: true,
      },
    ];

  }

  private filterDictionary() {
    if (!this.hmdDicList) {
      return;
    }

    let keyword = this.dictionarySelectSearchFormControl.value;

    if (!keyword) {
      this.filteredHmdDicList.next(this.hmdDicList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredHmdDicList.next(
      this.hmdDicList.filter(hmdDic => hmdDic.viewValue.toLowerCase().indexOf(keyword) > -1)
    );
  }

  private filterFileGroup() {
    if (!this.fileGroupList) {
      return;
    }

    let keyword = this.groupSelectSearchFormControl.value;

    if (!keyword) {
      this.filteredFileGroupDicList.next(this.fileGroupList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredFileGroupDicList.next(
      this.fileGroupList.filter(fileGroup => fileGroup.viewValue.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearDictionarySelectSearchFormControlValue() {
    this.dictionarySelectSearchFormControl.setValue('');
  }

  clearGroupSelectSearchFormControlValue() {
    this.groupSelectSearchFormControl.setValue('');
  }


  ngAfterViewInit() {

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getDataList() {

    this.fileList.length = 0;
    this.param = new HmdAnalyzeEntity();
    this.param.fileGroupId = this.selectedFileGroup.value;
    this.param.fileGroup = this.selectedFileGroup.viewValue;
    this.param.dicId = this.selectedDictionary.value;
    this.param.dicName = this.selectedDictionary.viewValue;
    this.param.purpose = 'TA';
    this.param.workspaceId = this.workspaceId;
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    if (this.paginator) {
      this.param.pageIndex = this.paginator.pageIndex;
      this.param.pageSize = this.paginator.pageSize;
    } else {
      this.param.pageIndex = 0;
      this.param.pageSize = 10;
    }

    this.fileList.length = 0;
    if ((this.selectedFileGroup && this.selectedDictionary)) {
      this.hmdAnalyzeService.getFileList(this.param).subscribe(res => {
          if (res) {
            this.length = res['totalElements'];

            res['content'].forEach((value) => {
              this.fileList.push({
                id: value['id'],
                name: value['name'],
                type: value['type'],
                size: value['size'],
              });
            });
          } else {
            return false;
          }
        },
        err => {
          console.log(err);
          this.openAlertDialog('Failed', 'Error. Failed get FileData', 'error');
        }
      );
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }


  onClickAnalizeButton = () => {
    if ((this.selectedFileGroup && this.selectedDictionary)) {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want Analyze?';

      ref.afterClosed().subscribe(result => {
        if (result) {
          this.hmdAnalyzeService.startAnalyze(this.param).subscribe(res => {
              this.openAlertDialog('Analyze', `Classified Sentence: ${res.success}<br> Not classified Sentence: ${res.fail}`, 'success');
            },
            err => {
              console.log(err);
              this.openAlertDialog('Failed', 'Error. Failed to Analyze', 'error');
            }
          );
        }
      });
    } else {
      this.openAlertDialog('Failed', 'Error. Please select File Group And Dictionary', 'error');
    }
  };

  getPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.fileList.length > 0) {
      this.getDataList();
    }
  }

  orderByHeader(matSort: MatSort) {
    this.matSort = matSort;
    if (this.fileList.length > 0) {
      this.getDataList();
    }
  }

  onChangeSelectBox() {
    if ((this.selectedFileGroup && this.selectedDictionary)) {
      this.getDataList();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
