import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {AlertComponent, DownloadService, StorageBrowser, TableComponent} from 'mlt-shared';
import {HmdAnalyzeResultService} from '../services/hmd-analyze-result.service';
import {HmdAnalyzeResultEntity} from '../entity/hmd-analyze-result.entity';

@Component({
  selector: 'app-ta-hmd-analysis-result',
  templateUrl: './hmd-analysis-result.component.html',
  styleUrls: ['./hmd-analysis-result.component.scss'],
  providers: [HmdAnalyzeResultService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],
  encapsulation: ViewEncapsulation.None,
})
export class HmdAnalysisResultComponent implements OnInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  paginator: MatPaginator;
  length: number;
  actions: any;
  inputDictionary: string;
  inputFileGroup: string;
  inputSentence: string;
  inputCategory: string;
  inputRule: string;
  matSort: MatSort = new MatSort();
  param: HmdAnalyzeResultEntity = new HmdAnalyzeResultEntity();

  analyzedResultData: any[] = [];
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'no', name: 'No', no: true, width: '2%'},
    {attr: 'model', name: 'Dictionary', isSort: true, width: '8%'},
    {attr: 'fileGroup', name: 'FileGroup', isSort: true, width: '8%'},
    {attr: 'sentence', name: 'Sentence', width: '35%'},
    {attr: 'category', name: 'Category', isSort: true, width: '10%'},
    {attr: 'rule', name: 'Rule', width: '30%'},
    {attr: 'createdAt', name: 'Created At', isSort: true, width: '10%', format: 'date'},
  ];

  constructor(private hmdAnalyzeResultService: HmdAnalyzeResultService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog,
              private downloadService: DownloadService) {
  }

  ngOnInit() {
    this.getDataList();
    this.actions = [
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        callback: this.onClickDownLoadButton,
        disabled: true,
      },
      {
        type: 'clearResults',
        text: 'Clear Results',
        icon: 'delete_sweep',
        callback: this.onClickClearResultButton,
        disabled: true,
      },
    ];

  }

  onClickDownLoadButton = () => {
    this.downloadService.downloadCsvFile(this.analyzedResultData, this.header, 'HMDAnalysisResult');
  };

  getDataList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.paginator.pageIndex = pageIndex;
    }

    this.param = new HmdAnalyzeResultEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.model = this.inputDictionary;
    this.param.fileGroup = this.inputFileGroup;
    this.param.sentence = this.inputSentence;
    this.param.category = this.inputCategory;
    this.param.rule = this.inputRule;
    if (this.paginator) {
      this.param.pageSize = this.paginator.pageSize;
      this.param.pageIndex = this.paginator.pageIndex;
    } else {
      this.param.pageSize = 10;
      this.param.pageIndex = 0;
    }
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.hmdAnalyzeResultService.getHmdAnalyzeResult(this.param).subscribe(res => {
        if (res) {
          this.analyzedResultData.length = 0;
          this.length = res['totalElements'];

          res['content'].forEach((value) => {
            this.analyzedResultData.push({
              id: value['id'],
              model: value['model'],
              fileGroup: value['fileGroup'],
              sentence: value['sentence'],
              category: value['category'],
              rule: value['rule'],
              createdAt: value['createdAt']
            });
          });
        } else {
          return false;
        }

      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Analysis Result List', 'error');
      }
    );

  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  onClickClearResultButton = () => {
    this.param = new HmdAnalyzeResultEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.model = this.inputDictionary;
    this.param.fileGroup = this.inputFileGroup;
    this.param.sentence = this.inputSentence;
    this.param.category = this.inputCategory;
    this.param.rule = this.inputRule;
    this.hmdAnalyzeResultService.deleteByHmdAnalyzeResultEntity(this.param).subscribe(res => {
        if (res) {
          this.analyzedResultData.length = 0;
        } else {
          return false;
        }
      },
      err => {
        console.log(err);
        this.openAlertDialog('Failed', 'Error. Failed Loading Analysis Result List', 'error');
      }
    );
  };

  orderByHeader(matSort: MatSort) {
    this.matSort = matSort;
    if (this.analyzedResultData.length > 0) {
      this.getDataList();
    }
  }

  getPaginator(paginator: MatPaginator) {
    this.paginator = paginator;
    if (this.analyzedResultData.length > 0) {
      this.getDataList();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
