import {NgModule} from '@angular/core';
import {SharedModule} from 'mlt-shared';
import {TaRoute} from './ta.route';
import {ModelManagementComponent} from './components/model-management.component';
import {HmdDictionaryComponent} from './components/hmd-dictionary.component';
import {HmdAnalysisAnalyzeComponent} from './components/hmd-analysis-analyze.component';
import {HmdAnalysisResultComponent} from './components/hmd-analysis-result.component';
import {HmdAnalysisTestComponent} from './components/hmd-analysis-test.component';
import {DnnTrainingDataComponent} from './components/dnn-training-data.component';
import {DnnTrainingComponent} from './components/dnn-training.component';
import {DnnAnalysisAnalyzeComponent} from './components/dnn-analysis-analyze.component';
import {DnnAnalysisResultComponent} from './components/dnn-analysis-result.component';
import {DnnAnalysisTestComponent} from './components/dnn-analysis-test.component';
import {ToolsNlpTestComponent} from './components/tools-nlp-test.component';
import {ModelUpsertDialogComponent} from './components/model-upsert-dialog.component';
import {FileManagementComponent} from './components/file-management.component';

import {FileManagementService} from './services/file-management.service';
import {DnnTrainingService} from './services/dnn-training.service';
import {DnnTrainingDataService} from './services/dnn-training-data.service';
import {DnnAnalyzeService} from './services/dnn-analyze.service';
import {DnnResultService} from './services/dnn-result.service';
import {DnnTestService} from './services/dnn-test.service';
import {NlpTestService} from './services/nlpTest.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    SharedModule,
    TaRoute,
    HttpClientModule
  ],
  providers: [
    DnnTrainingDataService, FileManagementService, DnnTrainingService, DnnAnalyzeService,
    DnnResultService, DnnTestService, NlpTestService
  ],
  declarations: [
    FileManagementComponent, ModelManagementComponent, HmdDictionaryComponent,
    HmdAnalysisAnalyzeComponent, HmdAnalysisResultComponent, HmdAnalysisTestComponent,
    DnnTrainingDataComponent, DnnTrainingComponent, DnnAnalysisAnalyzeComponent,
    DnnAnalysisResultComponent, DnnAnalysisTestComponent, ToolsNlpTestComponent,
    ModelUpsertDialogComponent
  ],
  exports: [
    FileManagementComponent, ModelManagementComponent, HmdDictionaryComponent,
    HmdAnalysisAnalyzeComponent, HmdAnalysisResultComponent, HmdAnalysisTestComponent,
    DnnTrainingDataComponent, DnnTrainingComponent, DnnAnalysisAnalyzeComponent,
    DnnAnalysisResultComponent, DnnAnalysisTestComponent, ToolsNlpTestComponent,
    ModelUpsertDialogComponent
  ],
  entryComponents: [
    ModelUpsertDialogComponent
  ]
})
export class TaModule {
}
