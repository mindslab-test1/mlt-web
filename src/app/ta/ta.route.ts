import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {FileManagementComponent} from './components/file-management.component';
import {ModelManagementComponent} from './components/model-management.component';
import {HmdDictionaryComponent} from './components/hmd-dictionary.component';
import {HmdAnalysisAnalyzeComponent} from './components/hmd-analysis-analyze.component';
import {HmdAnalysisResultComponent} from './components/hmd-analysis-result.component';
import {HmdAnalysisTestComponent} from './components/hmd-analysis-test.component';
import {DnnTrainingDataComponent} from './components/dnn-training-data.component';
import {DnnTrainingComponent} from './components/dnn-training.component';
import {DnnAnalysisAnalyzeComponent} from './components/dnn-analysis-analyze.component';
import {DnnAnalysisResultComponent} from './components/dnn-analysis-result.component';
import {DnnAnalysisTestComponent} from './components/dnn-analysis-test.component';
import {ToolsNlpTestComponent} from './components/tools-nlp-test.component';

const taRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'file-management',
        component: FileManagementComponent,
        data: {
          nav: {
            name: 'File Management',
            comment: 'File Management',
            icon: 'insert_drive_file',
          }
        },
      },
      {
        path: 'model-management',
        component: ModelManagementComponent,
        data: {
          nav: {
            name: 'Model Management',
            comment: 'Model Management',
            icon: 'view_module',
          }
        },
      },
      {
        path: 'hmd',
        data: {
          nav: {
            name: 'HMD',
            comment: 'hmd',
          }
        },
        children: [
          {
            path: 'dictionary',
            component: HmdDictionaryComponent,
            data: {
              nav: {
                name: 'Dictionary',
                comment: '사전 작업하기',
                icon: 'library_books',
              }
            }
          },
          {
            path: 'analysis',
            data: {
              nav: {
                name: 'Analysis',
                comment: 'analysis',
              }
            },
            children: [
              {
                path: 'analyze',
                component: HmdAnalysisAnalyzeComponent,
                data: {
                  nav: {
                    name: 'Analyze',
                    icon: 'insert_chart',
                  }
                }
              },
              {
                path: 'result',
                component: HmdAnalysisResultComponent,
                data: {
                  nav: {
                    name: 'Result',
                    icon: 'email',
                  }
                }
              },
              {
                path: 'test',
                component: HmdAnalysisTestComponent,
                data: {
                  nav: {
                    name: 'Test',
                    icon: 'spellcheck',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'dnn',
        data: {
          nav: {
            name: 'DNN',
            comment: 'dnn',
          }
        },
        children: [
          {
            path: 'training-data',
            component: DnnTrainingDataComponent,
            data: {
              nav: {
                name: 'Training Data',
                comment: '훈련 데이터',
                icon: 'library_books',
              }
            }
          },
          {
            path: 'training',
            component: DnnTrainingComponent,
            data: {
              nav: {
                name: 'Training',
                comment: '훈련 시키기',
                icon: 'school',
              }
            }
          },
          {
            path: 'analysis',
            data: {
              nav: {
                name: 'Analysis',
                comment: 'analysis',
              }
            },
            children: [
              {
                path: 'analyze',
                component: DnnAnalysisAnalyzeComponent,
                data: {
                  nav: {
                    name: 'Analyze',
                    icon: 'insert_chart',
                  }
                }
              },
              {
                path: 'result',
                component: DnnAnalysisResultComponent,
                data: {
                  nav: {
                    name: 'Result',
                    icon: 'email',
                  }
                }
              },
              {
                path: 'test',
                component: DnnAnalysisTestComponent,
                data: {
                  nav: {
                    name: 'Test',
                    icon: 'spellcheck',
                  }
                }
              },
            ]
          },
        ]
      },
      {
        path: 'tools',
        data: {
          nav: {
            name: 'Tools',
            comment: 'tools',
          }
        },
        children: [
          {
            path: 'nlp-test',
            component: ToolsNlpTestComponent,
            data: {
              nav: {
                name: 'NLP Test',
                comment: '자연어처리 테스트',
                icon: 'spellcheck',
              }
            }
          },
        ]
      }
    ]
  },
];

export const TaRoute: ModuleWithProviders = RouterModule.forChild(taRoutes);
