import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DnnAnalyzeEntity} from '../entity/dnn-analyze.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DnnAnalyzeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getAllModels(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/analyze/getAllModels/`, param);
  }

  getFileGroups(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/getFileGroups/', param);
  }

  getFileList(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/getFileList/', param);
  }

  startAnalyze(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/startAnalyze', param);
  }
}
