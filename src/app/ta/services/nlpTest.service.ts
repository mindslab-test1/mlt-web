import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class NlpTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getNLPTest(testData: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/analyze', testData);
  }
}
