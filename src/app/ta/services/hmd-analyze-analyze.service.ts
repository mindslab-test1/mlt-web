import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HmdAnalyzeEntity} from '../entity/hmd-analyze.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class HmdAnalyzeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  startAnalyze(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/startAnalyze', hmdAnalyzeEntity);
  }

  getFileGroupList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getFileGroupList', hmdAnalyzeEntity);
  }

  getFileList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getFileList', hmdAnalyzeEntity);
  }

  getHMDDicList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getHMDDicList', hmdAnalyzeEntity);
  }
}
