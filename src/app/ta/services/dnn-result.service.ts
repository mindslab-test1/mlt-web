import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DnnResultEntity} from '../entity/dnn-result.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DnnResultService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getAllDnnResults(dnnResultEntity: DnnResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/result/getAllDnnResults/`, dnnResultEntity);
  }

  deleteByDnnResult(dnnResultEntity: DnnResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/result/deleteByDnnResult/`, dnnResultEntity);
  }
}
