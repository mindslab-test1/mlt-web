import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HmdDictionaryEntity} from '../entity/hmd-dictionary.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class HmdDictionaryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  deleteLines(hmdDicLineList: HmdDictionaryEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/deleteLines`, hmdDicLineList);
  }

  getHmdDicList(hmdDictionaryEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/dic/getHmdDicList', hmdDictionaryEntity);
  }

  getHmdCategoryAllList(hmdDictionaryEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/dic/getHmdCategoryList', hmdDictionaryEntity);
  }

  getAllDicLines(hmdDictionaryEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/dic/getAllDicLines', hmdDictionaryEntity);
  }

  insertLine(hmdDictionaryEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/insertLine`, hmdDictionaryEntity);
  }

  insertCategory(category: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/insertCategory`, category);
  }

  updateCategory(category: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/updateCategory`, category);
  }

  deleteCategory(category: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/deleteCategory`, category);
  }

  hasLines(hmdDicLineEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/hasLines/`, hmdDicLineEntity);
  }

  hmdApply(hmdDicEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/hmdApply/`, hmdDicEntity);
  }

  commit(hmdDicEntity: HmdDictionaryEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/hmd/dic/commit/`, hmdDicEntity);
  }
}
