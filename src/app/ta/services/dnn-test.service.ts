import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DnnTestEntity} from '../entity/dnn-test.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DnnTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }


  getAllModels(param: DnnTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/test/getAllModels/`, param);
  }

  taDnnAnalyzeText(param: DnnTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/test/taDnnAnalyzeText/`, param);
  }
}
