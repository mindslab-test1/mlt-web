import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DnnTrainingEntity} from '../entity/dnn-training.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DnnTrainingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getDnnDicAllList(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/training/getDnnDicAllList/', dnnTrainingEntity);
  }

  getAllModels(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/getAllModels/`, dnnTrainingEntity);
  }

  getAllProgress(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/getAllProgress/`, dnnTrainingEntity);
  }

  deleteModel(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/deleteModel/`, dnnTrainingEntity);
  }

  open(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/open/`, dnnTraining);
  }

  stop(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/stop/`, dnnTraining);
  }

  deleteTraining(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/deleteTraining/`, dnnTraining);
  }
}
