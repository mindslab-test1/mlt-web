import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HmdAnalyzeResultEntity} from '../entity/hmd-analyze-result.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class HmdAnalyzeResultService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getHmdAnalyzeResult(hmdAnalyzeResultEntity: HmdAnalyzeResultEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/result/getHmdAnalyzeResult', hmdAnalyzeResultEntity);
  }

  deleteByHmdAnalyzeResultEntity(hmdAnalyzeResultEntity: HmdAnalyzeResultEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/result/deleteByHmdAnalyzeResultEntity', hmdAnalyzeResultEntity);
  }

  downloadByAnalyzeResultEntity(hmdAnalyzeResultEntity: HmdAnalyzeResultEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/result/downloadByHmdAnalyzeResultEntity', hmdAnalyzeResultEntity);
  }
}
