import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ModelManagementEntity} from '../entity/modelManagement.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class ModelInsertService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  insertHmdOrDnn(insertData: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/insertHmdOrDnn', insertData);
  }

  getHmdOrDnn(model: ModelManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getHmdOrDnn', model);
  }

  deleteSelectedModels(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/deleteSelectedModels', models);
  }

  updateHmdOrDnn(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/updateHmdOrDnn', models);
  }
}
