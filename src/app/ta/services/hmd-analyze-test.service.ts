import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HmdAnalyzeTestEntity} from '../entity/hmd-analyze-test.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class HmdAnalyzeTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getHMDDicList(hmdAnalyzeTestEntity: HmdAnalyzeTestEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/test/getHMDDicList', hmdAnalyzeTestEntity);
  }

  getHMDAnalyzeTestResultList(hmdAnalyzeTestEntity: HmdAnalyzeTestEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/test/getHMDAnalyzeTestResultList', hmdAnalyzeTestEntity);
  }
}
