import {NgModule} from '@angular/core';
import {SharedModule} from 'mlt-shared';
import {ParaphraseRoute} from './paraphrase.route';

import {QueryParaphraseService} from './services/query-paraphrase.service';
import {QueryParaphraseStepService} from './services/query-paraphrase-step.service';

import {QueryParaphraseComponent} from './components/query-paraphrase.component';
import {QueryParaphraseStepComponent} from './components/query-paraphrase-step.component';
import {SynonymDicUpsertDialogComponent} from './components/synonym-dic-upsert-dialog.component';
import {SynonymDicComponent} from './components/synonym-dic.component';
import {SynonymDicService} from './services/synonym-dic.service';
import {EndingPostPositionDicComponent} from './components/ending-post-position-dic.component';
import {EndingPostPositionDicUpsertDialogComponent} from './components/ending-post-position-dic-upsert-dialog.component';
import {EndingPostPositionDicService} from './services/ending-post-position-dic.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    SharedModule,
    ParaphraseRoute,
    HttpClientModule
  ],
  providers: [
    QueryParaphraseService, QueryParaphraseStepService, SynonymDicService, EndingPostPositionDicService
  ],
  declarations: [
    QueryParaphraseComponent, QueryParaphraseStepComponent, SynonymDicComponent, SynonymDicUpsertDialogComponent, EndingPostPositionDicComponent, EndingPostPositionDicUpsertDialogComponent
  ],
  exports: [
    QueryParaphraseComponent, QueryParaphraseStepComponent, SynonymDicComponent, SynonymDicUpsertDialogComponent, EndingPostPositionDicComponent, EndingPostPositionDicUpsertDialogComponent
  ],
  entryComponents: [
    SynonymDicUpsertDialogComponent, EndingPostPositionDicUpsertDialogComponent
  ],
})

export class ParaphraseModule {
}
