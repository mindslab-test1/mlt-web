import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {QueryParaphraseComponent} from './components/query-paraphrase.component';
import {QueryParaphraseStepComponent} from './components/query-paraphrase-step.component';
import {SynonymDicComponent} from './components/synonym-dic.component';
import {EndingPostPositionDicComponent} from './components/ending-post-position-dic.component';

const paraphraseRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: QueryParaphraseComponent,
        data: {
          nav: {
            name: 'Query Paraphrase',
            comment: 'Query Paraphrase',
            icon: 'list',
          }
        },
      },
      {
        path: 'step',
        component: QueryParaphraseStepComponent,
        data: {
          nav: {
            name: 'Query Paraphrase Step',
            comment: 'Query Paraphrase Step',
            icon: 'call_split',
          }
        },
      },
      {
        path: 'synonym',
        component: SynonymDicComponent,
        data: {
          nav: {
            name: 'Synonym',
            comment: 'synonym dic',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'ending-post-position',
        component: EndingPostPositionDicComponent,
        data: {
          nav: {
            name: 'Ending PostPosition',
            comment: 'synonym dic',
            icon: 'library_books',
          }
        },
      }
    ]
  }
];

export const ParaphraseRoute: ModuleWithProviders = RouterModule.forChild(paraphraseRoutes);
