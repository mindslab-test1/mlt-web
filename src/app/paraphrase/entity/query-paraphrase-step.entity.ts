import {PageParameters} from 'mlt-shared';

export class QueryParaphraseStepEntity extends PageParameters {
  id: string;
  queryParaphraseId: string;
  stepCd: string;
  sentence: string;
  orgSentence: string;
  creatorId: string;
  createdAt: Date;
  workspaceId: string;

  // QueryParaphraseEntity
  currentStep: string;
  title: string;
  allStep: string;

  // search
  searchKeyword: string;

  // allStep을 처음 한번만 update 치기 위한 조건
  firstParaphrase: boolean;

  // prevStep paraphrase할 stepCd
  prevStepCd: string;
}
