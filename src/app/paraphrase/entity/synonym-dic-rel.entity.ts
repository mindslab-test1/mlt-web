import {PageParameters} from 'mlt-shared';

export class SynonymDicRelEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representationId: string;
  synonymWord: string;
  createAt: Date;
  createId?: string;
}
