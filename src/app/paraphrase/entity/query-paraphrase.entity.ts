import {PageParameters} from 'mlt-shared';

export class QueryParaphraseEntity extends PageParameters {
  id: string;
  workspaceId: string;
  title: string;
  currentStep: string;
  allStep: string;
  creatorId: string;
  createdAt: Date;
}
