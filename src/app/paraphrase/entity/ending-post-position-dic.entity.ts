import {PageParameters} from 'mlt-shared';
import {EndingPostPositionDicRelEntity} from './ending-post-position-dic-rel.entity';

export class EndingPostPositionDicEntity extends PageParameters {

  id?: string;
  workspaceId?: string;
  representation?: string;
  word?: string;
  type?: string;
  createdAt?: Date;
  creatorId?: string;
  endingPostPositionDicRelEntities?: EndingPostPositionDicRelEntity[];
  toBeDeletedEndingPostPositionDicRelEntities?: EndingPostPositionDicRelEntity[];
  searchRepresentativeWord?: string;
  searchWord?: string;
}
