import {PageParameters} from 'mlt-shared';
import {SynonymDicRelEntity} from './synonym-dic-rel.entity';

export class SynonymDicEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representation: string;
  synonymWord: string;
  createAt: Date;
  createId?: string;
  synonymDicRelEntities: SynonymDicRelEntity[];
  toBeDeletedSynonymDicRelEntities: SynonymDicRelEntity[];
  searchRepresentativeWord?: string;
  searchSynonym?: string;
}
