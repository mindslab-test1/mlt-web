import {PageParameters} from 'mlt-shared';

export class EndingPostPositionDicRelEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representationId: string;
  word: string;
  createdAt: Date;
  creatorId?: string;
}
