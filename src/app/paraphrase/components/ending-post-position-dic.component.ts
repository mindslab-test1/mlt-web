import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {
  AlertComponent,
  ConfirmComponent,
  StorageBrowser,
  TableComponent,
  UploaderButtonComponent
} from 'mlt-shared';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EndingPostPositionDicService} from '../services/ending-post-position-dic.service';
import {EndingPostPositionDicEntity} from '../entity/ending-post-position-dic.entity';
import {EndingPostPositionDicUpsertDialogComponent} from './ending-post-position-dic-upsert-dialog.component';


@Component({
  selector: 'app-paraphrase-ending-dic',
  templateUrl: './ending-post-position-dic.component.html',
  styleUrls: ['./ending-post-position-dic.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [EndingPostPositionDicService]

})
export class EndingPostPositionDicComponent implements OnInit, AfterViewInit {
  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  header = [
    {attr: 'checkbox', name: 'CheckBox', type: 'text', checkbox: true, input: false},
    {attr: 'no', name: 'No.', no: true},
    {
      attr: 'representation',
      name: 'RepresentativeWord',
      type: 'text',
      checkbox: false,
      input: false,
      isSort: true
    },
    {attr: 'wordList', name: 'Word', type: 'text', checkbox: false, input: false},
    {
      attr: 'type',
      name: 'Type',
      type: 'text',
      checkbox: false,
      input: false,
      width: '7%',
      isSort: true
    },
    {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
  ];
  actionsArray: any[];
  dataSource: MatTableDataSource<any>;
  endingPostPositionDicList = [];
  param = new EndingPostPositionDicEntity();
  init = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageParam: MatPaginator;
  pageLength = 10;
  workspaceId;
  representativeWord: string;
  word: string;
  type;

  constructor(private endingPostPositionDicService: EndingPostPositionDicService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.actionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popUpsertDialog,
        params: 'add'
      },
      {
        type: 'delete',
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeDics
      },
      {
        type: 'delete',
        text: 'Remove All',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeDicsAll
      },
    ];
    this.workspaceId = this.storage.get('workspaceId');
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.getNextPage();
  }

  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      this.param.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.param.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
    }
    if (this.init) {
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    if (!this.init) {
      this.init = true;
    }
    this.param.type = this.type;
    this.param.searchRepresentativeWord = this.representativeWord;
    this.param.searchWord = this.word;

    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.workspaceId = this.workspaceId;
    this.endingPostPositionDicService.getEndingPostPositionDicListByWorkspaceId(this.param).subscribe(res => {
      res.content.map(val => {
        val.wordList = '';
        val.endingPostPositionDicRelEntities.map(entity => {
          if (val.wordList === '') {
            val.wordList = entity.word;
          } else {
            val.wordList += ',' + entity.word;
          }
        })
      });

      this.endingPostPositionDicList = res.content;
      this.pageLength = res.totalElements;
      this.dataSource = new MatTableDataSource(this.endingPostPositionDicList);
    })
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.init) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  popUpsertDialog = (type?, row?) => {
    let paramData = {};
    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = row;
    } else {
      return false;
    }

    paramData['service'] = this.endingPostPositionDicService;
    paramData['entity'] = new EndingPostPositionDicEntity();

    let dialogRef = this.dialog.open(EndingPostPositionDicUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getNextPage();
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  removeDics = () => {
    let toBeRemoved = [];
    this.endingPostPositionDicList.forEach(dic => {
      if (dic.isChecked) {
        toBeRemoved.push(dic);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please select to remove', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.tableComponent.isCheckedAll = false;
        this.endingPostPositionDicService.removeEndingPostPositionDics(toBeRemoved).subscribe(res => {
          this.openAlertDialog('Success', 'Remove Success', 'success');
          this.getNextPage();
        }, err => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. failed remove ending post positions.', 'error');
        });
      }
    });
  };

  removeDicsAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove All Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        this.pageParam.pageIndex = 0;
        this.tableComponent.isCheckedAll = false;
        this.endingPostPositionDicService.removeEndingPostPositionDicsByWorkspaceId(this.workspaceId).subscribe(res => {
          this.openAlertDialog('Success', 'Remove Success', 'success');
          this.getNextPage();
        }, err => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. failed remove ending post positions.', 'error');
        });
      }
    });
  };

  onUploadFile = () => {
    this.getNextPage();
  };

  downloadFile = () => {
    let url = this.storage.get('mltApiUrl') + '/paraphrase/endingPostPositionDic/downloadFile/' + this.workspaceId;
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };
}
