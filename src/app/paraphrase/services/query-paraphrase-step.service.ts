import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {QueryParaphraseStepEntity} from '../entity/query-paraphrase-step.entity';
import {QueryParaphraseEntity} from '../entity/query-paraphrase.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class QueryParaphraseStepService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  fileUpload(param: QueryParaphraseStepEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/upload-files/`, param);
  }

  getParaphrase(param: QueryParaphraseEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/get-paraphrase/`, param);
  }

  getSentences(param: QueryParaphraseStepEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/get-sentences/`, param);
  }

  getAllSentences(param: QueryParaphraseStepEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/get-all-sentences/`, param);
  }

  paraphrase(param: QueryParaphraseStepEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/paraphrase/`, param);
  }

  deleteSentences(param: QueryParaphraseStepEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/delete-sentences/`, param);
  }

  complete(param: QueryParaphraseStepEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qps/complete/`, param);
  }

}
