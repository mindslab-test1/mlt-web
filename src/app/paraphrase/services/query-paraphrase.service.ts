import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {QueryParaphraseEntity} from '../entity/query-paraphrase.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class QueryParaphraseService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getParaphrases(param: QueryParaphraseEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qp/get-paraphrases/`, param)
  }

  deleteParaphrases(param: QueryParaphraseEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qp/delete-paraphrases/`, param)
  }

}
