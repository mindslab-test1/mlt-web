import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class BasicQAScheduleService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  fullIndexing(selctedSkill: number, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/schedule/fullIndexing/' + selctedSkill + '/' + clean);
  }

  incrementalIndexing(selctedSkill: number, clean: boolean): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/schedule/incrementalIndexing/' + selctedSkill + '/' + clean);
  }

  getIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/schedule/getIndexingStatus');
  }

  abortIndexing(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/schedule/abortIndexing');
  }

  getSkillList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/schedule/skill/list');
  }
}

