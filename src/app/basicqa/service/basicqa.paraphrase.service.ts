import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {BasicQAParaphraseWordEntity} from '../entity/basicqa.paraphrase-word.entity';
import {BasicQAParaphraseMainWordEntity} from '../entity/basicqa.paraphrase-main-word.entity';
import {BasicQAParaphraseRelEntity} from '../entity/basicqa.paraphrase-rel.entity';

@Injectable()
export class BasicQAParaphraseService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }
  checkWordExist(wordList: String[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/checkMainWordExist', wordList);
  }

  getMainWords(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/getMainWords', basicQAParaphraseMainWordEntity);
  }

  addMainWord(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/addMainWord', basicQAParaphraseMainWordEntity);
  }

  addWords(basicQAParaphraseWordEntities: BasicQAParaphraseWordEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/addWords', basicQAParaphraseWordEntities);
  }

  updateMainWord(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/updateMainWord', basicQAParaphraseMainWordEntity);
  }

  deleteMainWords(basicQAParaphraseMainWordEntities: BasicQAParaphraseMainWordEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/deleteMainWords', basicQAParaphraseMainWordEntities);
  }

  deleteRels(basicQAParaphraseRelEntities: BasicQAParaphraseRelEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/deleteRels', basicQAParaphraseRelEntities);
  }
}

