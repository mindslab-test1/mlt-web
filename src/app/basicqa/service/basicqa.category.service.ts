import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class BasicQACategoryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  /**
   * basicqa Category 전체 조회 쿼리
   *   {Observable<any>}
   */
  getBasicQACategoryAllList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/category/getCategoryAllList');
  }

  /**
   * basicqa Category 데이터 insert 쿼리
   *  category
   *   {Observable<any>}
   */
  insertBasicQACategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/category/insertCategory', category);
  }

  /**
   * basicqa Category 데이터 delete 쿼리
   *  category
   *   {Observable<any>}
   */
  deleteBasicQACategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/category/deleteCategory', category);
  }

  /**
   * basicqa Category 데이터 update 쿼리
   *  category
   *   {Observable<any>}
   */
  updateBasicQACategory(category: any): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/category/updateCategory', category);
  }

}
