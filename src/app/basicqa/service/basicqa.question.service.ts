import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {StorageBrowser} from 'mlt-shared';
import {BasicQAQuestionEntity} from '../entity/basicqa.question.entity';

@Injectable()
export class BasicQAQuestionService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  question(param: BasicQAQuestionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/question/question', param);
  }
}

