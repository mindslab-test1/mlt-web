import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {AlertComponent, AppEnumsComponent, ConfirmComponent, StorageBrowser} from 'mlt-shared';
import {BasicQAParaphraseMainWordEntity} from '../entity/basicqa.paraphrase-main-word.entity';
import {BasicQAParaphraseWordEntity} from '../entity/basicqa.paraphrase-word.entity';
import {BasicQAParaphraseRelEntity} from '../entity/basicqa.paraphrase-rel.entity';


@Component({
  selector: 'app-paraphrase-upsert-dialog',
  templateUrl: 'paraphrase-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class BasicQAParaphraseUpsertDialogComponent implements OnInit {

  param: any;
  service: any;
  role: string;

  name: string;
  item: BasicQAParaphraseMainWordEntity = new BasicQAParaphraseMainWordEntity();
  mainWordValidator = new FormControl('', [Validators.required]);
  wordValidator = new FormControl('');
  useYnValidator = new FormControl('', [Validators.required]);
  disabled = true;
  data: any

  word = '';
  relEntities: BasicQAParaphraseRelEntity[] = [];
  wordEntities: BasicQAParaphraseWordEntity[] = [];

  basicQAParaphraseWordList = '';
  toBeDeletedWordList = [];
  toBeAddedWordList = [];

  constructor(public dialogRef: MatDialogRef<BasicQAParaphraseUpsertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              public appEnum: AppEnumsComponent,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;
    this.item = this.data.row;
    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.relEntities = this.item.basicQAParaphraseRelEntities;
      this.relEntities.forEach(relEntity => {
        let temp = relEntity.basicQAParaphraseWordEntity;
        temp.mainId = relEntity.mainId;
        this.wordEntities.push(temp);
      })
      this.disabled = false;
    }

  }

  addItem = () => {
    this.service.addMainWord(this.item).subscribe(resMainWord => {
        if (resMainWord) {
          if (this.wordEntities.length > 0) {
            this.wordEntities.forEach(wordEntity => {
              wordEntity.mainId = resMainWord.id;
            });
            this.service.addWords(this.wordEntities).subscribe(resAddWords => {
              if (resAddWords) {
                this.dialogRef.close(true);
                this.openAlertDialog('Success', 'Success Add Word', 'success');
              }
            }, err => {
              this.dialogRef.close(true);
              if (err.status === 409) {
                this.openAlertDialog('Error', 'Failed Add Word <br> Word Already Exist In MainWord or Word', 'error');
              } else {
                this.openAlertDialog('Error', 'Failed Add Word', 'error');
                console.log('error', err);
              }
            })
          } else {
            this.dialogRef.close(true);
            this.openAlertDialog('Success', 'Success Add Word', 'success');
          }
        }
      },
      err => {
        if (err.status === 409) {
          this.mainWordValidator.setErrors({
            'duplicate': true
          });
        } else {
          this.openAlertDialog('Error', 'Failed Add MainWord', 'error');
          console.log('error', err);
        }
      }
    );
  }

  updateItem = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Error', 'Failed Edit Word', 'error');
      this.dialogRef.close();
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        delete this.item.basicQAParaphraseWordList;
        delete this.item.convertedCreateAt;
        this.service.updateMainWord(this.item).subscribe(resUpdateMainWord => {
            if (resUpdateMainWord) {
              if (this.toBeDeletedWordList.length !== 0) {
                this.service.deleteRels(this.toBeDeletedWordList).subscribe(resDeleteRels => {
                  if (resDeleteRels) {
                    if (this.toBeAddedWordList.length !== 0) {
                      this.service.addWords(this.toBeAddedWordList).subscribe(resAddWords => {
                        if (resAddWords) {
                          this.dialogRef.close(true);
                        }
                      }, err => {
                        this.dialogRef.close(true);
                        if (err.status === 409) {
                          this.openAlertDialog('Error', 'Failed Add Word <br> Word Already Exist In MainWord or Word', 'error');
                        } else {
                          this.openAlertDialog('Error', 'Failed Add Word', 'error');
                          console.log('error', err);
                        }
                      })
                    } else {
                      this.dialogRef.close(true);
                    }
                  }
                }, err => {
                  this.dialogRef.close(true);
                  this.openAlertDialog('Error', 'Failed Delete Word', 'error');
                  console.log('error', err);
                })
              } else {
                if (this.toBeAddedWordList.length !== 0) {
                  this.service.addWords(this.toBeAddedWordList).subscribe(resAddWords => {
                    if (resAddWords) {
                      this.dialogRef.close(true);
                    }
                  }, err => {
                    this.dialogRef.close(true);
                    if (err.status === 409) {
                      this.openAlertDialog('Error', 'Failed Add Word <br> Word Already Exist In MainWord or Word', 'error');
                    } else {
                      this.openAlertDialog('Error', 'Failed Add Word', 'error');
                      console.log('error', err);
                    }
                  })
                } else {
                  this.dialogRef.close(true);
                }
              }
            }
          }
          ,
          err => {
            if (err.status === 409) {
              this.openAlertDialog('Error', 'Failed Edit MainWord <br> MainWord Already Exist In MainWord or Word', 'error');
            } else {
              this.openAlertDialog('Error', 'Failed Edit MainWord', 'error');
              console.log('error', err);
            }
          }
        );
      }
    });
  }

  removeItem = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Error', 'Failed Remove Word', 'error');
      this.dialogRef.close();
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Word?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let list = [];
        list.push(this.item);
        this.service.deleteMainWords(list).subscribe(res => {
            if (res) {
              this.dialogRef.close(true);
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Word', 'error');
            console.log('error', err);
          }
        );
      }
    })
  }

  checkInvalid($event) {
    if ('INVALID' === this.mainWordValidator.status) {
      this.disabled = true;
      return false;
    } else if (('VALID' === this.mainWordValidator.status) && ('VALID' === this.useYnValidator.status)) {
      this.disabled = false;
      return true;
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }


  addWordToList() {
    let wordListArray = this.basicQAParaphraseWordList.split(',');
    if ((wordListArray.indexOf(this.word) === -1) && (this.word.trim() !== '') && (this.item.word !== this.word)) {
      let tmpArray = [];
      tmpArray.push(this.word);
      this.service.checkWordExist(tmpArray).subscribe(res => {
        let temp: BasicQAParaphraseWordEntity = new BasicQAParaphraseWordEntity();
        temp.word = this.word;
        if (this.role === 'edit') {
          temp.mainId = this.item.id;
          this.toBeAddedWordList.push(temp);
        }
        this.wordEntities.push(temp);

        if (this.basicQAParaphraseWordList === '') {
          this.basicQAParaphraseWordList = this.word;
        } else {
          this.basicQAParaphraseWordList += ',' + this.word;
        }
        this.word = '';

      }, err => {
        if (err.status === 409) {
          this.wordValidator.setErrors({
            'duplicate': true
          });
        } else {
          this.openAlertDialog('Error', 'Failed Add Word', 'error');
          console.log('error', err);
        }
      });
    } else if (this.word.trim() === '') {
      this.wordValidator.setErrors({
        'required': true
      });
    } else if (this.item.word === this.word) {
      this.wordValidator.setErrors({
        'duplicate': true
      });
    } else {
      this.wordValidator.setErrors({
        'duplicate': true
      });

    }
  }

  deleteRel(wordEntity) {
    let tmp = new BasicQAParaphraseRelEntity();
    tmp.mainId = wordEntity.mainId;
    tmp.paraphraseId = wordEntity.id;
    this.toBeDeletedWordList.push(tmp);
    this.wordEntities.splice(this.wordEntities.indexOf(wordEntity), 1);
  }
}
