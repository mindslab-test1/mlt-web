import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {BasicQAItemsComponent} from './components/items.component';
import {BasicQAScheduleComponent} from './components/schedule.component';
import {BasicQAParaphraseComponent} from './components/paraphrase.component';
import {BasicQACategoryComponent} from './components/category.component';
import {BasicQAQuestionComponent} from './components/question.component';

const basicQaRoute: Routes = [
  {
    path: '',
    children: [
      {
        path: 'items',
        component: BasicQAItemsComponent,
        data: {
          nav: {
            name: 'Items',
            comment: '',
            icon: 'library_books',
          }
        }
      },
      {
        path: 'schedule',
        component: BasicQAScheduleComponent,
        data: {
          nav: {
            name: 'Schedule',
            comment: '',
            icon: 'today',
          }
        }
      },
      {
        path: 'question',
        component: BasicQAQuestionComponent,
        data: {
          nav: {
            name: 'Question',
            comment: '',
            icon: 'question_answer',
          }
        }
      },
      {
        path: 'paraphrase',
        component: BasicQAParaphraseComponent,
        data: {
          nav: {
            name: 'Paraphrase',
            comment: '',
            icon: 'library_books',
          }
        }
      },
      {
        path: 'category',
        component: BasicQACategoryComponent,
        data: {
          nav: {
            name: 'Category',
            comment: 'bqa category',
            icon: 'category',
          }
        }
      }
    ]
  }
];

export const BasicQaRoute: ModuleWithProviders = RouterModule.forChild(basicQaRoute);
