import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SharedModule} from 'mlt-shared';
import {BasicQaRoute} from './basic.route';
import {BasicQAItemsComponent} from './components/items.component';
import {BasicQAItemsUpsertDialogComponent} from './components/items-upsert-dialog.component';
import {BasicQAService} from './service/basicqa.items.service';
import {BasicQAScheduleComponent} from './components/schedule.component';
import {BasicQAScheduleService} from './service/basicqa.schedule.service';
import {BasicQAParaphraseService} from './service/basicqa.paraphrase.service';
import {BasicQAParaphraseComponent} from './components/paraphrase.component';
import {BasicQAParaphraseUpsertDialogComponent} from './components/paraphrase-upsert-dialog.component';
import {BasicQASkillUpsertDialogComponent} from './components/skill-upsert-dialog.component';
import {BasicQACategoryComponent} from './components/category.component';
import {BasicQACategoryService} from './service/basicqa.category.service';
import {HttpClientModule} from '@angular/common/http';
import {BasicQAQuestionComponent} from './components/question.component';
import {BasicQAQuestionService} from './service/basicqa.question.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    BasicQaRoute,
    HttpClientModule
  ],
  providers: [BasicQAService, BasicQAScheduleService, BasicQAParaphraseService,
    BasicQACategoryService, BasicQAQuestionService],
  declarations: [BasicQAItemsComponent, BasicQAItemsUpsertDialogComponent,
    BasicQASkillUpsertDialogComponent, BasicQAScheduleComponent, BasicQAParaphraseComponent,
    BasicQAParaphraseUpsertDialogComponent, BasicQACategoryComponent, BasicQAQuestionComponent],
  exports: [BasicQAItemsComponent, BasicQAItemsUpsertDialogComponent,
    BasicQASkillUpsertDialogComponent, BasicQAScheduleComponent, BasicQAParaphraseComponent,
    BasicQAParaphraseUpsertDialogComponent, BasicQACategoryComponent, BasicQAQuestionComponent],
  entryComponents: [BasicQAItemsUpsertDialogComponent, BasicQAParaphraseUpsertDialogComponent,
    BasicQASkillUpsertDialogComponent]
})
export class BasicQaModule {
}


