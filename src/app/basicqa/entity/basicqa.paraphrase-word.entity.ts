export class BasicQAParaphraseWordEntity {

  id: number;
  mainId: number;
  word: string;
  wordType: string;
  createAt: Date;
  createId?: string;
}
