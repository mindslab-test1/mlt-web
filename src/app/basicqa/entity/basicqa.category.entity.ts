export class BasicQACategoryEntity {
  id: string;
  catName: string;
  catPath: string;
  parCatId: string;
}
