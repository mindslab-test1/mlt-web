import {PageParameters} from 'mlt-shared';
import {BasicQAParaphraseRelEntity} from './basicqa.paraphrase-rel.entity';

export class BasicQAParaphraseMainWordEntity extends PageParameters {

  id?: number;
  word?: string;
  wordType?: string;
  useYn?: string;
  createAt?: Date;
  createId?: string;
  basicQAParaphraseRelEntities?: BasicQAParaphraseRelEntity[];
  basicQAParaphraseWordList?: string[];
  convertedUseYn?: string;
  convertedCreateAt?: Date;
  searchMainWord?: string;
  searchWords?: string;
}
