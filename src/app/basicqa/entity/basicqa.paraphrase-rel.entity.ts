import {BasicQAParaphraseWordEntity} from './basicqa.paraphrase-word.entity';

export class BasicQAParaphraseRelEntity {

  mainId: number;
  paraphraseId: number;
  basicQAParaphraseWordEntity: BasicQAParaphraseWordEntity;
}
