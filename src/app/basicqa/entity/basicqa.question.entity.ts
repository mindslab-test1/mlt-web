export class BasicQAQuestionEntity {
  nTop: number;
  skillId: number;
  src: string;
  type: string;
  question: string;
  channel: string;
  category: string;
  subCategory: string;
  subSubCategory: string;
}
