import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AuthGuard} from './core/auth.guard';
import {AppDummyComponent} from 'mlt-shared';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    data: {
      hasNoMenuBar: true
    }
  },
  {
    path: 'dummy',
    pathMatch: 'full',
    component: AppDummyComponent,
    data: {
      hasNoMenuBar: true
    }
  },
  {
    path: 'mlt',
    data: {
      hasNoMenuBar: false
    },
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        data: {
          nav: {
            name: 'DashBoard',
            comment: '대시보드',
            icon: 'dashboard',
          }
        }
      },
      {
        path: 'stt',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './stt/stt.module#SttModule',
        data: {
          nav: {
            name: 'STT',
            comment: 'SpeechToText',
            icon: 'mic',
          }
        }
      },
      {
        path: 'ta',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './ta/ta.module#TaModule',
        data: {
          nav: {
            name: 'TA',
            comment: 'Text Analysis',
            icon: 'translate',
          }
        }
      },
      {
        path: 'basicqa',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './basicqa/basicqa.module#BasicQaModule',
        data: {
          nav: {
            name: 'BasicQA',
            comment: 'BasicQA',
            icon: 'question_answer',
          }
        }
      },
      {
        path: 'basicqa-v2',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './basic-qa-v2/basic-qa-v2.module#BasicQaV2Module',
        data: {
          nav: {
            name: 'BasicQAV2',
            comment: 'BasicQAV2',
            icon: 'question_answer',
          }
        }
      },
      {
        path: 'tripleWiki',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './twe/twe.module#TripleWikiModule',
        data: {
          nav: {
            name: 'TripleWiki',
            comment: 'TripleWiki',
            icon: 'group_work',
          }
        }
      },
      {
        path: 'mrc',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './mrc/mrc.module#MrcModule',
        data: {
          nav: {
            name: 'MRC',
            comment: 'MRC',
            icon: 'location_city',
          }
        }
      },
      {
        path: 'paraphrase',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './paraphrase/paraphrase.module#ParaphraseModule',
        data: {
          nav: {
            name: 'Paraphrase',
            comment: 'Paraphrase',
            icon: 'transform',
          }
        }
      },
      {
        path: 'dictionary',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './dictionary/dictionary.module#DictionaryModule',
        data: {
          nav: {
            name: 'Dictionary',
            comment: 'Dictionary',
            icon: 'import_contacts',
          }
        }
      },
      {
        path: 'management',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: './management/management.module#ManagementModule',
        data: {
          nav: {
            name: 'Management',
            comment: 'Management',
            icon: 'settings',
          }
        }
      },
    ]
  }
];

export const Route: ModuleWithProviders = RouterModule.forRoot(appRoutes);
