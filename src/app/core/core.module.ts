import {NgModule} from '@angular/core';
import {DatePipe} from '@angular/common';

@NgModule({
  providers: [DatePipe]
})

export class CoreModule {

    constructor() {
    }
}


