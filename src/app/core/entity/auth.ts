export class Auth {

  id: string;
  password: string;

  public static isNull(auth: Auth): boolean {
    return auth.id === null &&
      auth.password === null;
  }

  constructor() {
  }
}
