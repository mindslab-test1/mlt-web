import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './service/auth.service';
import {AlertComponent, StorageBrowser} from 'mlt-shared';
import {MatDialog} from '@angular/material';

@Injectable()
export class AuthGuard implements CanActivate {

  modulePath: string;

  constructor(private authService: AuthService,
              private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  getModulePath() {
    return this.modulePath;
  }

  setModulePath(_modulePath: string) {
    this.modulePath = _modulePath;
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const userRole = this.storage.get('userRole');
    let hasMenueRole = false;
    let confUrl = '';
    let statRoot = state.root.firstChild;
    while (statRoot.routeConfig) {
      confUrl += statRoot.routeConfig.path === '' ? '' : '/' + statRoot.routeConfig.path;

      if (statRoot.firstChild !== null) {
        statRoot = statRoot.firstChild;
      } else {
        this.modulePath = confUrl;
        break;
      }
    }

    let urls = state.url.split('/');

    // 빈 공백 제거
    urls.forEach((url, index) => {
      if (url.trim() === '') {
        urls.splice(index, 1);
      }
    });

    // 모듈 까지의 path 가져옴
    let moduleUrl = `/${urls[0]}/${urls[1]}`;
    this.setModulePath(moduleUrl);
    let userPaths = [];

    if (userRole !== null) {
      userRole.forEach((menu) => {
        const path = menu.menuEntity.path;

        let regex = new RegExp(`${moduleUrl}/*`);
        if (regex.test(path)) {
          userPaths.push(path);
        }

        if (confUrl === path) {
          hasMenueRole = true;
        }
      });

      hasMenueRole = this.authService.isAuthenticated() && hasMenueRole;
    } else {
      hasMenueRole = false;
    }

    if (this.authService.isAuthenticated()) {
      if (userPaths.length > 0) {

        // Angular xx.route.ts 에서 명시된 처음 path 랑  db 에 path 값을 비교하여 데이터가 일치하지 않을시에
        // db 값에 0 번째 항목의 Path 값으로 navigate 해준다.
        if (!userPaths.find(userPath => userPath === confUrl)) {
          this.router.navigateByUrl(userPaths[0]);
          return true;
        }

        // TODO dashboard 고정 체크
        if (confUrl === this.modulePath && confUrl !== '/mlt/dashboard') {
          this.router.navigateByUrl(userPaths[0]);
          return true;
        }
        return true;
      } else {
        if (!hasMenueRole) {
          this.openAlertDialog('Failed', 'You don\'t have permission to access', 'error');
          return false;
        }
      }
    }

    return false;
  }

  canActivateChild(next: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const userRole = this.storage.get('userRole');
    let hasMenueRole = false;
    let confUrl = '';

    let statRoot = state.root.firstChild;
    while (statRoot.routeConfig) {
      confUrl += statRoot.routeConfig.path === '' ? '' : '/' + statRoot.routeConfig.path;
      if (statRoot.firstChild !== null) {
        statRoot = statRoot.firstChild;
      } else {
        break;
      }
    }

    let urls = state.url.split('/');

    // 빈 공백 제거
    urls.forEach((url, index) => {
      if (url.trim() === '') {
        urls.splice(index, 1);
      }
    });

    // 모듈 까지의 path 가져옴
    let moduleUrl = `/${urls[0]}/${urls[1]}`;
    this.setModulePath(moduleUrl);
    let userPaths = [];

    if (userRole !== null) {
      userRole.forEach((menu) => {
        const path = menu.menuEntity.path;

        let regex = new RegExp(`${moduleUrl}/*`);
        if (regex.test(path)) {
          userPaths.push(path);
        }

        if (confUrl === path) {
          hasMenueRole = true;
        }
      });

      hasMenueRole = this.authService.isAuthenticated() && hasMenueRole;
    } else {
      hasMenueRole = false;
    }

    if (this.authService.isAuthenticated()) {
      if (userPaths.length > 0) {
        // Angular xx.route.ts 에서 명시된 처음 path 랑  db 에 path 값을 비교하여 데이터가 일치하지 않을시에
        // db 값에 0 번째 항목의 Path 값으로 navigate 해준다.
        if (!userPaths.find(userPath => userPath === confUrl)) {
          this.router.navigateByUrl(userPaths[0]);
          return true;
        }

        // dashboard가 아닐시에 0번째 항목으로 navigate 해준다.
        if (confUrl === this.modulePath && confUrl !== '/mlt/dashboard') {
          this.router.navigateByUrl(userPaths[0]);
          return true;
        }
        return true;
      } else {
        if (!hasMenueRole) {
          this.openAlertDialog('Failed', 'You don\'t have permission to access', 'error');
          return false;
        }
      }
    }
    return false;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
