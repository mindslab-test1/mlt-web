import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from 'mlt-shared';

import {DashboardComponent} from './components/dashboard.component';
import {DashboardService} from './service/dashboard.service';
import {DashboardRoute} from './dashboard.route';

@NgModule({
  imports: [
    SharedModule,
    HttpClientModule,
    DashboardRoute
  ],
  providers: [DashboardService],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],

})
export class DashboardModule {
}


