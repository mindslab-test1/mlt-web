import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {StorageBrowser} from 'mlt-shared';
import {DashboardService} from '../service/dashboard.service';

/* Dashboard alert 창 안 띄움 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {

  isUseSttTa = {stt: true, ta: true};

  // column1
  sttTrainedModels = [];
  sttTrainingsModels = [];

  sttFileCount = 0;
  sttTransCount = 0;
  sttQaCount = 0;
  sttFileGroups = [];
  sttFileList = [];

  // column2
  taDnnDics = [];
  taDnnTotalLength = 0;
  taDnnTrainings = [];

  taHmdDics = [];
  taHmdTotalLength = 0;

  // column 3-1-1
  bsqaSkills: any;
  bsqaSkillCount = 0;
  bsqaIndexing: any;
  bsqaLatestHistory: any;
  bsqaProgressStatus: any;

  mrcDataGroups = [];
  mrcTrainings = [];

  // column3-2
  histories = [];

  param = {};
  intervalHandler: any;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private router: Router,
              private dashboardService: DashboardService) {
  }


  ngOnInit() {
    this.init();
    this.intervalHandler = setInterval(this.init, 2000);
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
  }

  init = () => {
    this.getSttFileGroups();
    this.getSttFileCount();
    this.getTranscriptCompleteCount();
    this.getQualityAssuranceCompleteCount();
    this.getDnnDics();
    this.getHmdDics();
    this.getDnnTrainings();
    this.getHistories();
    this.getSttTrainedModels();
    this.getSttTrainingModels();
    this.getBsqaIndexingStatus();
    this.getBsqaSkills();
    this.getMrcTrainedDataGroups();
    // this.getMrcTrainings();
  };

  getSttFileGroups = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getSttFileGroups(this.param).subscribe(res => {
        if (res) {
          this.sttFileGroups = res;
        }
      },
      err => {
        console.log('error', err);
      }
    );
  };


  getSttFileCount = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getSttFileCount(this.param).subscribe(res => {
        if (res) {
          this.sttFileCount = res;
        }
      },
      err => {
        console.log('error', err);
      }
    );
  };

  getTranscriptCompleteCount = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getTranscriptCompleteCount(this.param).subscribe(res => {
        if (res) {
          this.sttTransCount = res;
        }
      },
      err => {
        console.log('error', err);
      }
    );
  };

  getQualityAssuranceCompleteCount = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getQualityAssuranceCompleteCount(this.param).subscribe(res => {
        if (res) {
          this.sttQaCount = res;
        }
      },
      err => {
        console.log('error', err);
      }
    );
  };

  getDnnDics = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getDnnDics(this.param).subscribe(res => {
        if (res) {
          this.taDnnTotalLength = res.total;
          this.taDnnDics = res.content;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getHmdDics = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getHmdDics(this.param).subscribe(res => {
        if (res) {
          this.taHmdTotalLength = res.total;
          this.taHmdDics = res.content;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };


  getDnnTrainings = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getDnnTrainings(this.param).subscribe(res => {
        if (res) {
          this.taDnnTrainings = res;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getHistories = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getHistories(this.param).subscribe(res => {
        if (res) {
          this.histories = res['content'];
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getSttTrainingModels = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getSttTrainingModels(this.param).subscribe(res => {
        if (res) {
          this.sttTrainingsModels = res;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getSttTrainedModels = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getSttTrainedModels(this.param).subscribe(res => {
        if (res) {
          this.sttTrainedModels = res;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getBsqaSkills = () => {
    this.dashboardService.getBsqaSkills().subscribe(res => {
        if (res) {
          this.bsqaSkillCount = res.length;
          this.bsqaSkills = res.map(bsqaSkill => bsqaSkill.name).join(', ');
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getBsqaIndexingStatus = () => {
    this.dashboardService.getBsqaIndexingStatus().subscribe(res => {
      if (res) {
        this.bsqaProgressStatus = res.status;
        if (res.status) {
          this.bsqaIndexing = {
            progress: res.progress,
            stopped: '',
            error: '',
            key: '',
          };
        } else {
          this.bsqaLatestHistory = res['latestIndexingHistory'];
        }
      }
    }, err => {
      console.log('error', err);
    });
  };

  getMrcTrainedDataGroups = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getMrcGroups(this.param).subscribe(res => {
        if (res) {
          this.mrcDataGroups = res;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  getMrcTrainings = () => {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('workspaceId');

    this.dashboardService.getMrcTrainings(this.param).subscribe(res => {
        if (res) {
          this.mrcTrainings = res;
        }
      }, err => {
        console.log('error', err);
      }
    );
  };

  click = (url) => {
    if (!url) {
      return false;
    }
    this.router.navigateByUrl(url);
  };

}
