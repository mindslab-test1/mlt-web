import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {DashboardComponent} from './components/dashboard.component';

const dashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {hasNoSidebar: true}
  }
];

export const DashboardRoute: ModuleWithProviders = RouterModule.forChild(dashboardRoutes);
