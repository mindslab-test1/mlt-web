import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class DashboardService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getSttFileGroups(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getSttFileGroups', param);
  }

  getSttFileCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getSttFileCount', param);
  }

  getTranscriptCompleteCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getTranscriptCompleteCount', param);
  }

  getQualityAssuranceCompleteCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getQualityAssuranceCompleteCount', param);
  }

  getDnnDics(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getDnnModels', param);
  }

  getHmdDics(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getHmdModels', param);
  }

  getDnnTrainings(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getDnnTrainings', param);
  }

  getHistories(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getHistories', param);
  }

  getSttTrainingModels(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/get-SttTrainingModels', param);
  }

  getSttTrainedModels(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/get-SttTrainedModels', param);
  }

  getSttFileList(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/get-SttFiles', param);
  }

  getBsqaSkills(): Observable<any> {
    return this.http.get(this.API_URL + '/dashboard/getBsqaSkills');
  }

  getBsqaIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/dashboard/getBsqaIndexingStatus');
  }

  getMrcGroups(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getMrcGroups', param);
  }

  getMrcTrainings(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard/getMrcTrainings', param);
  }
}
