import {Component, Inject, OnInit, ViewChild, ViewEncapsulation,} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatMenuTrigger, MatSidenav} from '@angular/material';
import {CookieBrowser, StorageBrowser} from 'mlt-shared';
import {AuthService} from './core/service/auth.service';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthGuard} from './core/auth.guard';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  hasNoLayout = false;
  hasNoSidebar = false;
  navs: any[] = [];
  sidebarChild: any[] = [];
  currentSideBar = {};
  parentPath: '';
  user = null;
  workspace = [];
  selectedId = '';
  childrenSet;

  menus = [];

  @ViewChild('sideNav') sideNav: MatSidenav;
  @ViewChild('authMenu') authMenu: MatMenuTrigger;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private authService: AuthService,
              private authGuard: AuthGuard,
              private http: HttpClient,
              private cookieBrowser: CookieBrowser,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.storage.set('mltApiUrl', environment.apiUrl);
  }

  // sso-login cookie To LocalStorage
  cookieToStorage() {
    let isAuthenticated = this.cookieBrowser.get('isAuthenticated');
    let userId = this.cookieBrowser.get('userId');
    let url = this.cookieBrowser.get('url');

    if (isAuthenticated) {
      let chain = new Promise((resolve, reject) => {
        this.http.get('/api/auth/get-all-workspace/').subscribe(res => {
            this.storage.set('workspace', res);
            this.storage.set('workspaceId', res[0].id);
            resolve();
          },
          err => {
            this.cookieBrowser.delete('userId');
            this.cookieBrowser.delete('isAuthenticated');
            this.cookieBrowser.delete('url');
            reject();
            console.error('error : ', err);
          });
      });

      chain.then(() => {
        if (!userId) {
          console.error('cookie have not userId.');
        } else {
          this.http.get(`/api/user/id/${userId}`).subscribe(res => {
              this.storage.set('user', res);
              this.storage.set('userRole', res['userRoleRelEntities'][0].roleEntity.menuRoleEntities);
              this.storage.set('isAuthenticated', isAuthenticated);
              this.cookieBrowser.delete('userId');
              this.cookieBrowser.delete('isAuthenticated');
              this.cookieBrowser.delete('url');

              if (url) {
                this.router.navigateByUrl(url);
              } else {
                this.router.navigateByUrl('/mlt/dashboard');
              }
            },
            err => {
              this.storage.remove('isAuthenticated');
              this.storage.remove('workspace');
              this.storage.remove('user');
              this.storage.remove('userRole');
              this.storage.remove('workspaceId');

              this.cookieBrowser.delete('isAuthenticated');
              this.cookieBrowser.delete('userId');
              this.cookieBrowser.delete('url');
              console.error('error : ', err);
            });
        }
      });
    }
  }

  ngOnInit() {
    this.cookieToStorage();
    this.router
    .events
    .filter(event => event instanceof NavigationEnd)
    .map(() => {
      this.initWorkspace();
      let result = {
        hasNoLayout: false,
        hasNoSidebar: false,
        parentPath: '',
        navs: [],
        sidebarChild: [],
        currentSideBar: {},
        menus: [],
      };

      result.menus = this.router.config;

      let child = this.activatedRoute.firstChild;
      const routeConfig = child.routeConfig;
      // top nav children
      result.parentPath = routeConfig.path;
      if (result.parentPath !== '') {
        result.navs = routeConfig.children;
      }

      if (child.firstChild === null) {
        return result;
      }

      child.firstChild.routeConfig['active'] = true;
      // sidebar children
      const currentNavPath = child.firstChild.routeConfig.path;
      if (typeof(result.navs) !== 'undefined') {
        result.navs.forEach(children => {
          if (currentNavPath === children.path && children._loadedConfig) {
            result.sidebarChild = children._loadedConfig.routes[0].children;
            result.currentSideBar = children;
          }
        });
      }

      while (child) {
        if (child.firstChild) {
          child = child.firstChild;
        } else if (child.snapshot.data) {
          result.hasNoLayout = child.snapshot.data.hasNoLayout;
          result.hasNoSidebar = child.snapshot.data.hasNoSidebar;
          break;
        } else {
          break;
        }
      }
      return result;
    }).subscribe((configData: any) => {
      this.menus = []; // init tempNavs
      configData.menus.forEach(val => {
        if (val.path !== '') {
          if (val.hasOwnProperty('data')) {
            if (val.data.hasOwnProperty('hasNoMenuBar')) {
              if (!val['data']['hasNoMenuBar']) {
                val.children.forEach(child => {
                  if (child.path !== '') {
                      child['parentPath'] = val.path;
                      child['visible'] = true;
                      this.menus.push(child);
                  }
                })
              }
            }
          }
        }
      })
      this.hasNoLayout = configData.hasNoLayout;
      this.hasNoSidebar = configData.hasNoSidebar;
      this.parentPath = configData.parentPath;
      let roleArr = this.storage.get('userRole');

      this.navs = [];
      if (configData.navs !== undefined) {
        this.navs = configData.navs;
      }
      if (this.menus) {
        this.menus.forEach((nav, index) => {
          if (index !== 0) {
            let findUrl = `${nav.parentPath}/${nav.path}/*`;
            let regex = new RegExp(findUrl);

            let visibleFlag = false;
            if (roleArr === null || roleArr === undefined) {
              nav['visible'] = false;
            } else {
              roleArr.forEach((menu, i) => {
                if (regex.exec(menu.menuEntity.path)) {
                  nav['visible'] = true;
                  visibleFlag = true;
                }

                if (roleArr.length === (i + 1) && !visibleFlag) {
                  nav['visible'] = false;
                }
              });
            }
          }
        });
      }

      if (this.storage.get('userRole') && configData.sidebarChild) {
        this.sidebarChild = [];
        this.childrenSet = new Set();

        let sidebarChildCopy = JSON.parse(JSON.stringify(configData.sidebarChild));

        sidebarChildCopy.some(children => {
          if (children.hasOwnProperty('children')) {
            this.setLeafChildren(children.children, children.path, roleArr, children);

            let rootNoneVisible =
              children.children.filter(grandChild => grandChild['visible'] === 'none').length;

            if (children.children.length !== rootNoneVisible) {
              this.childrenSet.add(children);
            }
          } else {
            let findResult =
              roleArr.find(menu =>
                menu.menuEntity.path === `${this.authGuard.getModulePath()}/${children.path}`);

            if (findResult) {
              this.childrenSet.add(children);
            }
          }
        });

        let childArr = Array.from(this.childrenSet);
        this.sidebarChild = childArr;
        this.currentSideBar = configData.currentSideBar;
      } else {
        this.sidebarChild = configData.sidebarChild;
        this.currentSideBar = configData.currentSideBar;
      }
    });
  }

  setLeafChildren(children, path, menuRoles, root) {
    if (children.length > 0) {
      children.forEach((childChildren) => {
        if (childChildren.hasOwnProperty('children')) {
          path += `/${childChildren.path}`;
          this.setLeafChildren(childChildren.children, path, menuRoles, root);
          let noneVisible
            = childChildren.children.filter(grandChild => grandChild['visible'] === 'none').length;
          if (noneVisible === childChildren.children.length) {
            childChildren['visible'] = 'none';
          }
        } else {
          let result = this.setLeafChildren(childChildren, path, menuRoles, root);
          if (!result) {
            childChildren['visible'] = 'none';
          }
        }
      });
    } else {
      let fullPath = `${this.authGuard.getModulePath()}/${path}/${children.path}`;
      let result = menuRoles.some(menuRole => {
        if (menuRole.menuEntity.path === fullPath) {
          return true;
        }
      });
      return result;
    }
  }

  initWorkspace() {
    this.user = this.storage.get('user');
    this.workspace = this.storage.get('workspace');

    if (this.storage.get('workspaceId') === null && this.storage.get('workspace') !== null) {
      this.selectedId = this.workspace[0].id;
      this.storage.set('workspaceId', this.selectedId);
    } else {
      this.selectedId = '' + this.storage.get('workspaceId');
    }
  }


  goto(navs, nav) {
    console.log(11, navs);
    console.log(22, nav);
    let routeUrl = '';

    if (nav.parentPath === '') {
      routeUrl += '/' + nav.path;
    } else {
      routeUrl += '/' + nav.parentPath + '/' + nav.path;
    }

    navs.forEach(item => {
      item['active'] = false;
    });
    nav['active'] = true;

    if (this.sideNav.opened) {
      this.sideNav.close();
    }

    this.router.navigateByUrl(routeUrl);
  }

  goHome() {
    this.sidebarChild = undefined;
    this.authService.goHome();
  }

  profile() {
  }

  signOut() {
    this.authService.signOut().then(() => {
      this.user = null;
      this.workspace = [];
      this.selectedId = '';
    });
  }

  changeWorkspace() {
    const currentUrl = this.router.url;
    this.storage.set('workspaceId', this.selectedId);
    this.router.navigateByUrl('/dummy', {skipLocationChange: true});
    setTimeout(() => this.router.navigateByUrl(currentUrl), 100);
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  updateProfile() {
    this.router.navigateByUrl('mlt/management/profile');
  }

}
