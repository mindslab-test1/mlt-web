import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {SttEntity} from '../entity/stt.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class SttService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getSttList(): Observable<any> {
    return this.http.get(this.API_URL + '/stt/getSttList');
  }

  getStt(sttId: number): Observable<any> {
    return this.http.get(this.API_URL + '/stt/getStt/' + sttId);
  }

  insertStt(sttEntity: SttEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/insertStt', sttEntity);
  }

  updateStt(sttEntity: SttEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/updateStt', sttEntity);
  }

  deleteStt(sttId: number): Observable<any> {
    return this.http.get(this.API_URL + '/stt/deleteStt' + sttId);
  }

}
