import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FileManagementEntity} from '../entity/file-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class AmTrainingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/getFileGroups', param);
  }

  trainGroupFiles(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/sttOpen', param);
  }

  getAllProgress(workspaceId: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/getAllProgress', workspaceId);
  }

  getTrainedSttModels(workspaceId: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/getTrainedSttModels', workspaceId);
  }

  getProgress(gId: String): Observable<any> {
    return this.http.get(this.API_URL + '/stt/am/training/getProgress/' + gId);
  }

  stop(key: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/stop', key);
  }

  close(key: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/close', key);
  }

  delete(id: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/training/delete', id);
  }
}
