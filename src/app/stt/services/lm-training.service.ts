import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FileManagementEntity} from '../entity/file-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class LmTrainingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/getFileGroups', param);
  }

  trainGroupFiles(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/sttOpen', param);
  }

  getAllProgress(workspaceId: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/getAllProgress', workspaceId);
  }

  getTrainedSttModels(workspaceId: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/getTrainedSttModels', workspaceId);
  }

  getProgress(gId: String): Observable<any> {
    return this.http.get(this.API_URL + '/stt/lm/training/getProgress/' + gId);
  }

  stop(key: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/stop', key);
  }

  close(key: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/close', key);
  }

  delete(id: String): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/training/delete', id);
  }
}
