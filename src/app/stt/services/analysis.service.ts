import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AnalysisEntity} from '../entity/analysis.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class AnalysisService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getFileGroups(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/getFileGroups', param);
  }

  getLmModels(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/getLmModels', param);
  }

  getAmModels(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/getAmModels', param);
  }

  getGroupFileList(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/getGroupFileList', param);
  }

  getAnalysisResultList(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/getAnalysisResultList', param);
  }

  analyze(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/analyze', param);
  }

  deleteResult(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/delete-result', param);
  }

  getResult(param: AnalysisEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/analysis/get-result', param);
  }
}
