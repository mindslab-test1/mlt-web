import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {TranscriptionEntity} from '../entity/transcription.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';
import {FileManagementEntity} from '../entity/file-management.entity';

@Injectable()
export class AmQualityAssuranceService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get('mltApiUrl');
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getFileGroups', param);
  }

  getTranscriptionList(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getTranscriptionList', param);
  }

  getTranscript(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getTranscript', param);
  }

  commit(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/commit', param);
  }

  updateAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/update-alert', param);
  }

  deleteAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/delete-alert', param);
  }
}
