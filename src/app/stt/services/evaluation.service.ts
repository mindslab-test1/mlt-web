import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {EvaluationEntity} from '../entity/evaluation.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'mlt-shared';

@Injectable()
export class EvaluationService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = this.storage.get("mltApiUrl");
  }

  getFileGroups(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getFileGroups', param);
  }

  getLmModels(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getLmModels', param);
  }

  getAmModels(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getAmModels', param);
  }

  getAnalysisResultList(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getAnalysisResultList', param);
  }

  getEvaluationResultList(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/get-evaluation-result', param);
  }

  deleteEvalResultList(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/delete-evaluation-result', param);
  }

  evaluate(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/evaluate', param);
  }
}
