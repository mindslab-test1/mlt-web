import {Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AlertComponent, StorageBrowser, TranscriptTextComponent} from 'mlt-shared';
import {AnalysisEntity} from '../entity/analysis.entity';
import {MatDialog, MatDialogRef} from '@angular/material';
import {EvaluationService} from '../services/evaluation.service';

@Component({
  selector: 'app-stt-evaluation-detail-dialog',
  templateUrl: 'evaluation-detail-dialog.component.html',
})
export class STTEvaluationDetailDialogComponent implements OnInit {

  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;
  @ViewChild('diffContainer') diffContainer: ElementRef;

  title: String;
  audioURL: string;
  langCode = 'kor';

  fileId: String;

  param: AnalysisEntity;
  data: any;

  transcriptResult;
  engineResult;
  row;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              public dialogRef: MatDialogRef<any>,
              private evaluationService: EvaluationService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    // apply data when update
    let data = this.data = this.dialogRef.componentInstance.data;
    if (data) {
      this.row = data;
      this.audioURL = `${this.storage.get('mltApiUrl')}/stt/am/file-manage/download-file/${this.storage.get('workspaceId')}/${this.row.sttAnalysisResultEntity.fileId}`;
      this.transcriptResult = this.row.sttAnalysisResultEntity.sttTranscriptEntity.version;
      this.engineResult = this.row.sttAnalysisResultEntity.text;
    }
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
