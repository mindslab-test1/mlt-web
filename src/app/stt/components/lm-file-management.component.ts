import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  ConfirmComponent,
  FileGroupDialogComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {LmFileManagementService} from '../services/lm-file-management.service';
import {FileManagementEntity} from '../entity/file-management.entity';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SttFileGroupDialogComponent} from './stt-file-group-dialog.component';

@Component({
  selector: 'app-stt-lm-file-management',
  templateUrl: './lm-file-management.component.html',
  styleUrls: ['./lm-file-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LmFileManagementComponent implements OnInit, AfterViewInit {

  fileGroupCodes = [];
  selectedCode: string;
  fileGroupAction: any;
  groupFilesAction: any;
  filesAction: any;

  groupFiles: any[] = [];
  files: any[] = [];

  groupKeyword: string;
  fileKeyword: string;

  groupFileDataSource: MatTableDataSource<any>;
  fileDataSource: MatTableDataSource<any>;

  groupFilePage: MatPaginator;
  filePage: MatPaginator;
  groupFilePageTotalCount: number;
  filePageTotalCount: number;

  groupFilesSort: MatSort;
  filesSort: MatSort;

  fileHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'name', name: 'Name', isSort: true},
    {attr: 'type', name: 'Type', width: '7%', isSort: true},
    {attr: 'size', name: 'Size', width: '7%', format: 'file', isSort: true},
    {attr: 'creatorId', name: 'Uploader', width: '10%', isSort: true},
    {attr: 'createdAt', name: 'UploadedAt', width: '10%', format: 'date', isSort: true},
    {
      attr: 'action',
      name: 'Action',
      width: '7%',
      isButton: true,
      buttonName: 'Download',
    }
  ];

  fileGroupHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'name', name: 'Name', isSort: true},
    {attr: 'type', name: 'Type', width: '7%', isSort: true},
    {attr: 'size', name: 'Size', width: '7%', format: 'file', isSort: true},
    {attr: 'creatorId', name: 'Uploader', width: '10%', isSort: true},
    {attr: 'createdAt', name: 'UploadedAt', width: '10%', format: 'date', isSort: true},
    {
      attr: 'action',
      name: 'Action',
      width: '7%',
      isButton: true,
      buttonName: 'Download',
    }
  ];

  param: FileManagementEntity = new FileManagementEntity();

  @ViewChild('fileTableComponent') fileTableComponent: TableComponent;
  @ViewChild('groupFileTableComponent') groupFileTableComponent: TableComponent;
  @ViewChild('hiidenFileButton') hiidenFileButton: Element;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredFileGroupCodes: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private fileManagementService: LmFileManagementService,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.initActionButton();
    this.getFileGroups().then(() => {
      if (this.fileGroupCodes.length !== 0) {
        this.selectedCode = this.fileGroupCodes[0].id;
        this.fileGroupAction[1].hidden = false;
        this.fileGroupAction[1].disabled = false;
      }
      this.loadTableData();
      this.selectSearchFormControl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterFileGroup();
        });
    });
  }

  ngAfterViewInit() {
  }

  private filterFileGroup() {
    if (!this.fileGroupCodes) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredFileGroupCodes.next(this.fileGroupCodes.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredFileGroupCodes.next(
      this.fileGroupCodes.filter(fileGroup => fileGroup.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }


  initActionButton = () => {
    this.fileGroupAction = [
      {
        type: 'addFileGroup',
        text: 'Add File Group',
        icon: 'add_circle_outline',
        callback: this.popFileGroup,
        params: 'add'
      },
      {
        type: 'editFileGroup',
        text: 'Edit File Group',
        icon: 'info_outline',
        callback: this.popFileGroup,
        params: 'edit',
        hidden: true,
        disabled: true
      }
    ];
    this.groupFilesAction = [
      {
        type: 'excludeFiles',
        text: 'Exclude Files',
        icon: 'arrow_downward',
        callback: this.excludeFiles
      },
      {
        type: 'removeFiles',
        text: 'Remove Files',
        icon: 'delete_sweep',
        callback: this.removeFiles,
        params: this.groupFileTableComponent
      },
    ];

    this.filesAction = [
      {
        type: 'includeFiles',
        text: 'Include Files',
        icon: 'arrow_upward',
        callback: this.includeFiles
      },
      {
        type: 'removeFiles',
        text: 'Remove Files',
        icon: 'delete_sweep',
        callback: this.removeFiles,
        params: this.fileTableComponent
      },
    ];
  };

  loadTableData = () => {
    if (this.selectedCode !== undefined && this.selectedCode !== '') {
      this.fileGroupAction[1].hidden = false;
      this.fileGroupAction[1].disabled = false;
    }
    this.getGroupFiles(this.groupKeyword);
    this.getFiles(this.fileKeyword);
  };

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  getFileGroups = () => {
    this.param = new FileManagementEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.cdr.detectChanges();
    return new Promise(resolve => {
      this.fileManagementService.getFileGroups(this.param).subscribe(res => {
          if (res && res.length > 0) {
            this.fileGroupCodes = res;
            this.fileGroupCodes.forEach(group => {
              group.rate = JSON.parse(group.meta).rate;
            })
            if (!this.selectedCode) {
              this.selectedCode = '';
            }
          } else {
            this.fileGroupCodes = [];
          }
          this.filteredFileGroupCodes.next(this.fileGroupCodes.slice());

          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get File Groups', 'error');
          console.log('error', err);
        }
      );
    });
  };

  setSortGroupFiles(matSort?: MatSort) {
    this.groupFilesSort = matSort;
    if (this.groupFiles.length > 0) {
      this.getGroupFiles(this.groupKeyword);
    }
  }

  setSortFiles(matSort?: MatSort) {
    this.filesSort = matSort;
    if (this.files.length > 0) {
      this.getFiles(this.fileKeyword);
    }
  }

  getGroupFiles = (keyword, pageIndex?) => {
    if (this.selectedCode === '') {
      return false;
    }
    if (pageIndex !== undefined) {
      this.groupFilePage.pageIndex = pageIndex;
    }
    this.param = new FileManagementEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.groupFilePage.pageIndex;
    this.param.pageSize = this.groupFilePage.pageSize;
    this.param.fileGroupId = this.selectedCode;
    this.param.orderDirection = this.groupFilesSort.direction === '' ||
    this.groupFilesSort.direction === undefined ? 'desc' : this.groupFilesSort.direction;
    this.param.orderProperty = this.groupFilesSort.active === '' ||
    this.groupFilesSort.active === undefined ? 'createdAt' : this.groupFilesSort.active;
    this.param.name = keyword === null || typeof(keyword) === 'undefined' ? '' : keyword;

    this.fileManagementService.getGroupFiles(this.param).subscribe(res => {
        if (res) {
          this.groupFileTableComponent.isCheckedAll = false;
          this.groupFiles = res['content'];
          this.groupFilePageTotalCount = res['totalElements'];
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Group FileList', 'error');
        console.log('error', err);
      }
    );
  };

  getFiles = (keyword, pageIndex?) => {
    if (pageIndex !== undefined) {
      this.filePage.pageIndex = pageIndex;
    }
    this.param = new FileManagementEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.pageIndex = this.filePage.pageIndex;
    this.param.pageSize = this.filePage.pageSize;
    this.param.fileGroupId = this.selectedCode;
    this.param.orderDirection = this.filesSort.direction === '' ||
    this.filesSort.direction === undefined ? 'desc' : this.filesSort.direction;
    this.param.orderProperty = this.filesSort.active === '' ||
    this.filesSort.active === undefined ? 'createdAt' : this.filesSort.active;
    this.param.name = keyword === null || typeof(keyword) === 'undefined' ? '' : keyword;

    this.fileManagementService.getFiles(this.param).subscribe(res => {
        if (res) {
          this.fileTableComponent.isCheckedAll = false;
          this.files = res['content'];
          this.filePageTotalCount = res['totalElements'];
          this.getGroupFiles(this.groupKeyword);
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get FileList', 'error');
        console.log('error', err);
      }
    );
  };

  includeFiles = () => {
    const fileArr = this.getCheckedFile(this.fileTableComponent);

    if (fileArr.length < 1 || this.selectedCode === '' || this.selectedCode === '') {
      this.openAlertDialog('Notice', 'File has to be selected', 'notice');
      return false;
    }

    this.param = new FileManagementEntity();
    this.param.fileGroupId = this.selectedCode;
    this.param.workspaceId = this.storage.get('workspaceId');
    fileArr.forEach(item => {
      this.param.ids.push(item.id);
    });

    this.fileManagementService.includeFiles(this.param).subscribe(res => {
        this.fileTableComponent.isCheckedAll = false;
        this.loadTableData();
        this.openAlertDialog('Success', 'Success Include Files', 'success');
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Include Files', 'error');
        console.log('error', err);
      }
    );
  };

  excludeFiles = () => {
    const fileArr = this.getCheckedFile(this.groupFileTableComponent);

    if (fileArr.length < 1 || this.selectedCode === 'undefined') {
      this.openAlertDialog('Notice', 'File has to be selected', 'notice');
      return false;
    }

    this.param = new FileManagementEntity();
    this.param.fileGroupId = this.selectedCode;
    this.param.workspaceId = this.storage.get('workspaceId');
    fileArr.forEach(item => {
      this.param.ids.push(item.id);
    });

    this.fileManagementService.excludeFiles(this.param).subscribe(res => {
        this.groupFileTableComponent.isCheckedAll = false;
        this.loadTableData();
        this.openAlertDialog('Success', 'Success Exclude Files', 'success');
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Exclude Files', 'error');
        console.log('error', err);
      }
    );
  };

  removeFiles = (component) => {
    const fileArr = this.getCheckedFile(component);
    if (fileArr.length === 0) {
      this.openAlertDialog('Notice', 'File has to be selected', 'notice');
      return false;
    }
    this.param = new FileManagementEntity();
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.fileGroupId = this.selectedCode;
    fileArr.forEach(item => {
      this.param.ids.push(item.id);
    });

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove File?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = component.paginator.pageIndex;
        if (fileArr.length === component.rows.length) {
          if (component.paginator.pageIndex === 0) {
            component.paginator.pageIndex = 0;
          } else {
            component.paginator.pageIndex -= 1;
          }
        }

        this.fileManagementService.deleteFiles(this.param).subscribe(res => {
            component.isCheckedAll = false;
            this.loadTableData();
          },
          err => {
            component.paginator = originalPageIndex;
            this.openAlertDialog('Failed', 'Failed Remove Files', 'error');
            console.log('error', err);
          }
        );
      }
    });

  };

  popFileGroup = (type) => {
    let paramData = {};

    if (type === 'add') {
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['row'] = this.getSelectedGroupData()[0];
    } else {
      return false;
    }

    paramData['service'] = this.fileManagementService;
    paramData['entity'] = new FileManagementEntity();

    let dialogRef = this.dialog.open(SttFileGroupDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.getFileGroups();
        this.initActionButton();
        this.getFileGroups().then(() => {
          if (this.fileGroupCodes.length !== 0) {
            this.selectedCode = this.fileGroupCodes[0].id;
            this.fileGroupAction[1].hidden = false;
            this.fileGroupAction[1].disabled = false;
            this.loadTableData();
            this.selectSearchFormControl.valueChanges
              .pipe(takeUntil(this._onDestroy))
              .subscribe(() => {
                this.filterFileGroup();
              });
          } else {
            this.selectedCode = undefined;
          }
        });
      }
    });
  };

  downloadFile = (row) => {
    if (row['id'] === '' || typeof(row['id']) === 'undefined') {
      return false;
    }

    let url = this.storage.get('mltApiUrl') + '/stt/lm/file-manage/download-file/' + this.storage.get('workspaceId') + '/' + row['id'];
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  setFilePage = (page: MatPaginator) => {
    this.filePage = page;
    if (this.files.length > 0) {
      this.getFiles(this.fileKeyword);
    }
  };

  setGroupFilePage = (page: MatPaginator) => {
    this.groupFilePage = page;
    if (this.groupFiles.length > 0) {
      this.getGroupFiles(this.groupKeyword);
    }
  };


  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => item.isChecked);
  };

  getSelectedGroupData = () => {
    return this.fileGroupCodes.filter(item => {
      if (item['id'] === this.selectedCode) {
        return item;
      }
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
