import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {
  AlertComponent,
  ConfirmComponent,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {AnalysisEntity} from '../entity/analysis.entity';
import {AnalysisService} from '../services/analysis.service';

@Component({
  selector: 'app-stt-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnalysisComponent implements OnInit, AfterViewInit {

  @ViewChild('fileTableComponent') fileTableComponent: TableComponent;
  @ViewChild('resultDataTableComponent') resultDataTableComponent: TableComponent;

  fileGroup = [];
  selectedGroup = new AnalysisEntity();

  amModels = [];
  lmModels = [];

  selectedAmModel = new AnalysisEntity();
  selectedLmModel = new AnalysisEntity();

  searchKeyword: string;

  files: any[] = [];

  actions: any;

  fileName: string;

  fileSource: MatTableDataSource<any>;
  filePage: MatPaginator;
  filePageTotalCount: number;

  filesSort: MatSort;

  param = new AnalysisEntity();

  dataHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true, isSort: true},
    {attr: 'name', name: 'Name', isSort: true},
    {attr: 'type', name: 'Type', isSort: true},
    {attr: 'size', name: 'Size', format: 'file', isSort: true},
    {attr: 'duration', name: 'Duration', format: 'time', isSort: true},
    {attr: 'creatorId', name: 'Uploader'},
    {attr: 'createdAt', name: 'UploadedAt', format: 'date', isSort: true},
  ];

  resultDataHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true, isSort: true},
    {attr: 'fileEntity.name', name: 'Name', isSort: true},
    {attr: 'model', name: 'Model', isSort: true},
    {attr: 'createdAt', name: 'Created at', format: 'date', isSort: true},
    {attr: 'result', name: 'Result', isButton: true},
  ];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private analysisService: AnalysisService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getFileGroups().then(() => {
      this.getFileList(this.fileName);
    });

    this.getModels();
  }

  ngAfterViewInit() {
  }

  getFileGroups = () => {
    this.param = new AnalysisEntity();
    this.param.workspaceId = this.storage.get('workspaceId');

    return new Promise(resolve => {
      this.analysisService.getFileGroups(this.param).subscribe(res => {
          if (res && res.length > 0) {
            this.fileGroup = res;
            this.fileGroup.forEach(group => {
              group.rate = JSON.parse(group.meta).rate;
            })
            this.selectedGroup = this.fileGroup[0];
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Error', 'Failed Get File Groups', 'error');
        }
      );
    });
  };

  getModels = () => {
    this.param = new AnalysisEntity();
    this.param.workspaceId = this.storage.get('workspaceId');

    this.analysisService.getLmModels(this.param).subscribe(res => {
        if (res) {
          this.lmModels = res;
          this.selectedLmModel = res[0];
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get STT LmModels', 'error');
      }
    );

    this.analysisService.getAmModels(this.param).subscribe(res => {
        if (res) {
          this.amModels = res;
          this.selectedAmModel = res[0];
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get STT AmModels', 'error');
      }
    );
  };

  getFileList = (keyword?, pageIndex?) => {
    return new Promise(resolve => {

      if (pageIndex !== undefined) {
        this.filePage.pageIndex = pageIndex;
      }

      this.param = new AnalysisEntity();
      this.param.workspaceId = this.storage.get('workspaceId');
      this.param.pageIndex = this.filePage.pageIndex;
      this.param.pageSize = this.filePage.pageSize;
      this.param.fileGroupId = this.selectedGroup.id;
      this.param.orderDirection = this.filesSort.direction === '' ||
      this.filesSort.direction === undefined ? 'desc' : this.filesSort.direction;
      this.param.orderProperty = this.filesSort.active === '' ||
      this.filesSort.active === undefined ? 'createdAt' : this.filesSort.active;
      this.param.name = keyword === null || typeof(keyword) === 'undefined' ? '' : keyword;

      this.analysisService.getGroupFileList(this.param).subscribe(res => {
          if (res) {
            this.files = res['content'];
            this.fileTableComponent.isCheckedAll = false;
            this.filePageTotalCount = res['totalElements'];
          }
          resolve();
        }
        , err => {
          this.openAlertDialog('Error', 'Failed GET FileList', 'error');
        });
    });
  };

  analyze = () => {
    if (this.selectedAmModel['trainType'] !== this.selectedLmModel['trainType']) {
      this.openAlertDialog('notice', 'AM Model and LM Model Train Type has to be same', 'notice');
      return null;
    }

    if (this.selectedAmModel['rate'] !== this.selectedLmModel['rate']) {
      this.openAlertDialog('notice', 'AM Model and LM Model Rate has to be same', 'notice');
      return null;
    }

    if (this.selectedGroup['rate'] === '16000') {
      if (this.selectedAmModel['rate'] !== 16000) {
        this.openAlertDialog('notice', 'Model And FileGroup Rate has to be same', 'notice');
        return null;
      }
    } else if (this.selectedGroup['rate'] === '8000') {
      if (this.selectedAmModel['rate'] !== 8000) {
        this.openAlertDialog('notice', 'Model And FileGroup Rate has to be same', 'notice');
        return null;
      }
    } else {
      this.openAlertDialog('error', 'File Group has no rate', 'error');
      return null;
    }

    const fileArr = this.getCheckedFile(this.fileTableComponent);
    if (fileArr.length === 0) {
      this.openAlertDialog('notice', 'File Has To Be Selected', 'notice');
      return null;
    }

    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.getFileList(null).then(() => {
        this.param = new AnalysisEntity();
        this.param.workspaceId = this.storage.get('workspaceId');
        this.param.fileGroupId = this.selectedGroup.id;
        this.param.amModelId = this.selectedAmModel.id;
        this.param.lmModelId = this.selectedLmModel.id;

        fileArr.forEach(item => {
            this.param.ids.push(item.id);
            this.param.fileNames.push(item.name);
        });
        this.analysisService.analyze(this.param).subscribe(res => {
            this.openAlertDialog('success', 'Success Analyze Selected Files', 'success');
          },
          err => {
            this.openAlertDialog('Error', 'STT Connection Exception', 'error');
          });
      });
    });
  };

  setfilePage = (page: MatPaginator) => {
    this.filePage = page;
    if (this.files.length > 0) {
      this.getFileList(this.fileName);
    }
  };

  setSortFiles(matSort?: MatSort) {
    this.filesSort = matSort;
    if (this.files.length > 0) {
      this.getFileList(this.fileName);
    }
  }

  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => item.isChecked);
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
