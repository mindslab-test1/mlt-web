import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AlertComponent, StorageBrowser, TranscriptTextComponent} from 'mlt-shared';
import {AnalysisEntity} from '../entity/analysis.entity';
import {MatDialog, MatDialogRef} from '@angular/material';
import {AnalysisService} from '../services/analysis.service';

@Component({
  selector: 'app-stt-analysis-result',
  templateUrl: './analysis-result-dialog.component.html',
  styleUrls: ['./analysis-result-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnalysisResultDialogComponent implements OnInit {

  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;

  title: String;
  audioURL: string;
  langCode = 'kor';

  fileId: String;

  param: AnalysisEntity;
  data: any;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              public dialogRef: MatDialogRef<any>,
              private analysisService: AnalysisService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    // apply data when update
    let data = this.data = this.dialogRef.componentInstance.data;
    if (data) {
      this.complete(data);
    }
  }

  complete = (row) => {
    this.getTranscript(row.id);
    this.audioURL = `${this.storage.get('mltApiUrl')}/stt/am/file-manage/download-file/${this.storage.get('workspaceId')}/${row.fileId}`;
  }

  getTranscript = (id) => {
    if (!id || typeof(id) === 'undefined') {
      return false;
    }
    this.param = new AnalysisEntity();
    this.param.id = id;

    this.analysisService.getResult(this.param).subscribe(res => {
        if (res) {
          this.title = res['fileEntity']['name'];
          this.transcriptTextComponent.setContent(res['text']);
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Result', 'error');
      }
    );
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
