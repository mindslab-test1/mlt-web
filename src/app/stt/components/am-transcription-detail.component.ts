import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {
  AlertComponent,
  GitComponent,
  StorageBrowser,
  TranscriptAlertDialogComponent,
  TranscriptTextComponent
} from 'mlt-shared';
import {TranscriptionEntity} from '../entity/transcription.entity';
import {LmTranscriptionService} from '../services/lm-transcription.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-stt-am-transcription-detail',
  templateUrl: './am-transcription-detail.component.html',
  styleUrls: ['./am-transcription-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AmTranscriptionDetailComponent implements OnInit, AfterViewInit {

  // main values

  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;
  @ViewChild('gitComponent') gitComponent: GitComponent;
  actions: any[] = [];

  transcript: any = null;

  langCode = 'kor';
  param: TranscriptionEntity = new TranscriptionEntity();

  selectedCode: string;
  audioURL: string;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private lmTranscriptionService: LmTranscriptionService,
              private router: Router,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.getTranscript(params.get('id'));
    });

    this.actions = [
      {
        text: 'Bigger Text',
        icon: 'zoom_in',
        callback: this.transcriptTextComponent.zoomIn,
      },
      {
        text: 'Smaller Text',
        icon: 'zoom_out',
        callback: this.transcriptTextComponent.zoomOut,
      },
      {
        text: 'Clear Errors',
        icon: 'format_clear',
        callback: this.transcriptTextComponent.clearErrors,
      },
      {
        text: 'Save',
        icon: 'save',
        callback: this.commit,
      },
      {
        text: 'Save with Alert',
        icon: 'error_outline',
        callback: this.updateAlert,
      },
      {
        text: 'Back to List',
        icon: 'keyboard_backspace',
        callback: this.backToList,
      },
    ]
  }

  ngAfterViewInit() {
  }

  doAction = (action) => {
    if (action) {
      action.callback();
    }
  }

  getTranscript = (id) => {
    if (!id || typeof(id) === 'undefined') {
      return false;
    }
    this.param = new TranscriptionEntity();
    this.param.id = id;

    this.lmTranscriptionService.getTranscript(this.param).subscribe(res => {
        if (res) {
          this.transcript = res;
          this.audioURL = `${this.storage.get('mltApiUrl')}/stt/am/file-manage/download-file/${this.storage.get('workspaceId')}/${res.fileId}`;
          if (this.transcript.version) {
            this.transcriptTextComponent.setContent(
              this.transcript.version.toString().replace(/\n/g, '<br>'));
          }
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Transcript', 'error');
        console.log('error', err);
      }
    );
  }

  commit = () => {
    this.param = new TranscriptionEntity();
    this.param.id = this.transcript.id;
    this.param.workspaceId = this.storage.get('workspaceId');
    this.param.version = this.transcriptTextComponent.text;

    this.lmTranscriptionService.commit(this.param).subscribe(res => {
        if (res) {
          this.transcript = res;
          this.openAlertDialog('Success', 'Success Transcript Commit', 'success');
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Version commit', 'error');
        console.log('error', err);
      }
    );
  }

  updateAlert = () => {
    let data = {};
    data['row'] = this.transcript;
    data['service'] = this.lmTranscriptionService;
    data['entity'] = new TranscriptionEntity();

    let dialogRef = this.dialog.open(TranscriptAlertDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.transcript = result;
      }
    });
  }

  backToList = () => {
    let param = `fileGroupId=${encodeURI(this.transcript.fileGroupId.toString())}`;
    this.router.navigateByUrl('mlt/stt/am/transcription?' + param);
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
