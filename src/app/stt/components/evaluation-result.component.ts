import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  AlertComponent,
  ConfirmComponent,
  DownloadService,
  StorageBrowser,
  TableComponent
} from 'mlt-shared';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EvaluationEntity} from '../entity/evaluation.entity';
import {EvaluationService} from '../services/evaluation.service';
import {AnalysisEntity} from '../entity/analysis.entity';
import {AnalysisService} from '../services/analysis.service';
import {AnalysisResultDialogComponent} from './analysis-result-dialog.component';
import {STTEvaluationDetailDialogComponent} from './evaluation-detail-dialog.component';


@Component({
  selector: 'app-stt-evaluation-result',
  templateUrl: './evaluation-result.component.html',
  styleUrls: ['./evaluation-result.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EvaluationResultComponent implements OnInit {
  @ViewChild('fileTableComponent') fileTableComponent: TableComponent;
  @ViewChild('resultDataTableComponent') resultDataTableComponent: TableComponent;

  fileGroup = [];
  selectedGroup = new AnalysisEntity();

  amModels = [];
  lmModels = [];

  selectedAmModel = new AnalysisEntity();
  selectedLmModel = new AnalysisEntity();

  searchKeyword: string;

  files: any[] = [];
  resultData: any[] = [];

  actions: any;

  fileName: string;
  resultModel: string;

  fileSource: MatTableDataSource<any>;
  filePage: MatPaginator;
  filePageTotalCount: number;

  resultDataSource: MatTableDataSource<any>;

  resultDataPage: MatPaginator;
  resultDataPageTotalCount: number;

  filesSort: MatSort;
  resultDataSort: MatSort;

  param = new EvaluationEntity();
  fileGroupSearchKeyword: string;
  lmModelSearchKeyword: string;
  amModelSearchKeyword: string;
  nameSearchKeyword: string;

  evaluationHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true, isSort: true},
    {attr: 'sttAnalysisResultEntity.fileEntity.name', name: 'Name', isSort: true},
    {attr: 'sttAnalysisResultEntity.fileGroupEntity.name', name: 'FileGroup', isSort: true},
    {attr: 'sttAnalysisResultEntity.sttAmModelEntity.name', name: 'AM Model', isSort: true},
    {attr: 'sttAnalysisResultEntity.sttLmModelEntity.name', name: 'LM Model', isSort: true},
    {attr: 'createdAt', name: 'CreatedAt', format: 'date', isSort: true},
    {attr: 'correctness', name: 'Correctness', isSort: true},
    {attr: 'accuracy', name: 'Accuracy', isSort: true},
    {attr: 'detail', name: 'Detail', isButton: true},
  ];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private evaluationService: EvaluationService,
              private downloadService: DownloadService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.actions = [
      {type: 'download', text: 'Download Results', icon: 'file_download', callback: this.download},
      {type: 'clear', text: 'Clear Results', icon: 'delete_sweep', callback: this.deleteResult}
    ];
  }

  ngAfterViewInit() {
    this.getResultDataList();
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }


  getResultDataList = (keyword?, pageIndex?) => {
    if (pageIndex !== undefined) {
      this.resultDataPage.pageIndex = pageIndex;
    }

    this.param = new EvaluationEntity();
    this.param.pageIndex = this.resultDataPage.pageIndex;
    this.param.pageSize = this.resultDataPage.pageSize;
    this.param.fileGroupId = this.selectedGroup.id;
    this.param.orderDirection = this.resultDataSort.direction === '' ||
    this.resultDataSort.direction === undefined ? 'desc' : this.resultDataSort.direction;
    this.param.orderProperty = this.resultDataSort.active === '' ||
    this.resultDataSort.active === undefined ? 'createdAt' : this.resultDataSort.active;
    this.param.amModelName = this.amModelSearchKeyword;
    this.param.lmModelName = this.lmModelSearchKeyword;
    this.param.fileGroupName = this.fileGroupSearchKeyword;
    this.param.name = this.nameSearchKeyword;

    this.evaluationService.getEvaluationResultList(this.param).subscribe(res => {
        if (res) {
          this.resultData = res['content'];
          this.resultData.forEach(item => {
            if (item['result'] !== 'undefined') {
              JSON.parse(item['result'], function (key, value) {
                item[key] = value;
              });
            }
            if (item['text'] !== 'undefined') {
              item['buttonName'] = 'view';
            }
            this.resultDataPageTotalCount = res['totalElements'];
            this.resultDataTableComponent.isCheckedAll = false;
          })
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get Result DataList', 'error');
      }
    );
  };

  download = () => {
    this.dialog.open(ConfirmComponent)
      .afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return false;
        }
        this.downloadService.downloadCsvFile(this.resultDataTableComponent.rows, this.evaluationHeader, 'result');
      });
  };

  deleteResult = () => {
    const paramArr = [];
    const resultArr = this.getCheckedFile(this.resultDataTableComponent);
    if (resultArr.length === 0) {
      this.resultDataTableComponent.isCheckedAll = false;
      this.openAlertDialog('Notice', 'Please select result ', 'notice');
      return null;
    }
    if (resultArr.length !== 0) {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want to remove evaluation result data? All related data will be deleted';
      ref.afterClosed()
        .subscribe(confirmed => {
          if (!confirmed) {
            return false;
          }

          resultArr.forEach(item => {
            this.param = new EvaluationEntity();
            this.param.id = item.id;
            paramArr.push(this.param);
          });

          let originalPageIndex: number = this.resultDataPage.pageIndex;
          if (paramArr.length === this.resultDataTableComponent.rows.length) {
            if (this.resultDataPage.pageIndex === 0) {
              this.resultDataPage.pageIndex = 0;
            } else {
              this.resultDataPage.pageIndex -= 1;
            }
          }
          this.evaluationService.deleteEvalResultList(paramArr).subscribe(res => {
            this.getResultDataList();
            this.openAlertDialog('Success', 'Success delete result file', 'success');
          }, err => {
            this.resultDataPage.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Failed delete result file', 'err');
          });
        });
    }
  };

  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => item.isChecked);
  };

  setResultDataPage = (page: MatPaginator) => {
    this.resultDataPage = page;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  };

  setSortResultData(matSort?: MatSort) {
    this.resultDataSort = matSort;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  }

  openModal(row: any) {
    let ref: any = this.dialog.open(STTEvaluationDetailDialogComponent);
    ref.componentInstance.data = row;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
