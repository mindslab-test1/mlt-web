import {NgModule} from '@angular/core';

import {SharedModule} from 'mlt-shared';
import {SttService} from './services/stt.service';
import {AnalysisService} from './services/analysis.service';
import {SttRoute} from './stt.route';

import {AnalysisComponent} from './components/analysis.component';
import {EvaluationComponent} from './components/evaluation.component';
import {AnalysisResultDialogComponent} from './components/analysis-result-dialog.component';
import {EvaluationService} from './services/evaluation.service';
import {HttpClientModule} from '@angular/common/http';
import {LmFileManagementComponent} from './components/lm-file-management.component';
import {LmFileManagementService} from './services/lm-file-management.service';
import {LmTranscriptionComponent} from './components/lm-transcription.component';
import {LmTrainingComponent} from './components/lm-training.component';
import {LmTranscriptionService} from './services/lm-transcription.service';
import {LmTranscriptionDetailComponent} from './components/lm-transcription-detail.component';
import {AmFileManagementComponent} from './components/am-file-management.component';
import {AmFileManagementService} from './services/am-file-management.service';
import {SttFileGroupDialogComponent} from './components/stt-file-group-dialog.component';
import {LmTrainingService} from './services/lm-training.service';
import {AmTranscriptionService} from './services/am-transcription.service';
import {AmTranscriptionComponent} from './components/am-transcription.component';
import {AmTranscriptionDetailComponent} from './components/am-transcription-detail.component';
import {AmQualityAssuranceComponent} from './components/am-quality-assurance.component';
import {AmQualityAssuranceDetailComponent} from './components/am-quality-assurance-detail.component';
import {AmQualityAssuranceService} from './services/am-quality-assurance.service';
import {AmTrainingComponent} from './components/am-training.component';
import {AmTrainingService} from './services/am-training.service';
import {AnalysisResultComponent} from './components/analysis-result.component';
import {EvaluationResultComponent} from './components/evaluation-result.component';
import {STTEvaluationDetailDialogComponent} from './components/evaluation-detail-dialog.component';
import { DiffMatchPatchModule } from 'ng-diff-match-patch';

@NgModule({
  imports: [
    SharedModule,
    SttRoute,
    HttpClientModule,
    DiffMatchPatchModule
  ],
  providers: [SttService, AnalysisService,
    EvaluationService, LmFileManagementService, LmTranscriptionService, AmFileManagementService, LmTrainingService,
    AmTranscriptionService, AmQualityAssuranceService, AmTrainingService],
  declarations: [AnalysisComponent, EvaluationComponent, AnalysisResultDialogComponent, LmFileManagementComponent,
    LmTranscriptionComponent, LmTrainingComponent, AnalysisResultComponent,
    LmTranscriptionDetailComponent, AmFileManagementComponent, SttFileGroupDialogComponent, AmTranscriptionComponent,
    AmTranscriptionDetailComponent, AmQualityAssuranceComponent, AmQualityAssuranceDetailComponent, AmTrainingComponent,
    EvaluationResultComponent, STTEvaluationDetailDialogComponent],
  exports: [AnalysisComponent, EvaluationComponent, AnalysisResultDialogComponent, LmFileManagementComponent, LmTranscriptionComponent,
    LmTrainingComponent, LmTranscriptionDetailComponent, AmFileManagementComponent, SttFileGroupDialogComponent,
    AmTranscriptionComponent, AmTranscriptionDetailComponent, AmQualityAssuranceComponent, AmQualityAssuranceDetailComponent,
    AmTrainingComponent, AnalysisResultComponent, EvaluationResultComponent, STTEvaluationDetailDialogComponent],
  entryComponents: [AnalysisResultDialogComponent, SttFileGroupDialogComponent, STTEvaluationDetailDialogComponent]

})
export class SttModule {
}


