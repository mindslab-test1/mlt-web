import {PageParameters} from 'mlt-shared';

export class EvaluationEntity extends PageParameters {
  id: string;
  workspaceId: string;
  model: string;
  name: string;
  result: any;
  fileGroupId: string;

  fileId: string;
  amModelId: string;
  lmModelId: string;
  transcriptId: string;

  amModelName: string
  lmModelName: string;
  fileGroupName: string;
}
