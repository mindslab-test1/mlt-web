import {PageParameters} from 'mlt-shared';

export class AnalysisEntity extends PageParameters {
  name: string;
  id: string;
  ids: string[] = [];
  workspaceId: string;
  fileNames: string[] = [];

  fileGroupId: string;

  amModelId: string;
  lmModelId: string;

  amModelName: string;
  lmModelName: string;
  fileGroupName: string;

}
