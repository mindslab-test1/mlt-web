export class SttTrainingEntity {

  // sttTraining entity, sttModel entity use
  sttKey: string;
  name: string;

  // sttModel entity use
  id: string;
  sttBinary: string;

  // only data transfer
  progress: number;


}
