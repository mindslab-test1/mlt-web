export class SttEntity {
  id: number;
  name: string;

  public static isNull(stt: SttEntity): boolean {
    return stt.id === null &&
      stt.name === null;

  }

  constructor() {
  }
}
