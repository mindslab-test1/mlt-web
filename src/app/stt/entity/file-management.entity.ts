import {PageParameters} from 'mlt-shared';

export class FileManagementEntity extends PageParameters {

  id: string;
  ids: string[] = [];
  name: string;
  workspaceId: string;
  meta?: any;
  rate?: string;
  fileGroupId: string;
}
