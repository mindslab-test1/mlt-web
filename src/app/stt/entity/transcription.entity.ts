import {PageParameters} from 'mlt-shared';

export class TranscriptionEntity extends PageParameters {
  id: string;
  ids: string[] = [];
  fileId: string;
  fileGroupId?: string;
  workspaceId: string;
  name: string;
  alert: string;
  version: string;
}
