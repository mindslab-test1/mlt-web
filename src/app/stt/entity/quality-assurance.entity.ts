import {PageParameters} from 'mlt-shared';

export class QualityAssuranceEntity extends PageParameters {
  id: string;
  ids: string[] = [];
  fileId: string;
  workspaceId: string;
  name: string;
  alert: string;
  version: string;
}
